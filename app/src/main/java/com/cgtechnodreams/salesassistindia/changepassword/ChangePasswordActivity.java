package com.cgtechnodreams.salesassistindia.changepassword;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.changepassword.data.ChangePasswordDataService;
import com.cgtechnodreams.salesassistindia.changepassword.model.ChangePasswordModel;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.utils.CustomEditText;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.google.android.gms.tasks.OnSuccessListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class ChangePasswordActivity extends AppCompatActivity{
   
    @NotEmpty(messageId = R.string.error_field_required)
    @BindView(R.id.input_old_password)
    CustomEditText oldPassword;
    @NotEmpty(messageId = R.string.error_field_required)
    @BindView(R.id.input_new_password)
    CustomEditText newPassword;
    ProgressDialog dialog;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
    }
    @OnClick({R.id.btnChangePassword})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnChangePassword:
                //if (ConnectivityReceiver.isConnected()) {
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    changePassword();

                    break;
                }
        }

    }

    private void changePassword() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        final ChangePasswordModel request = new ChangePasswordModel();
        request.setNewPassword(newPassword.getText().toString());
        request.setOldPassword(oldPassword.getText().toString());
        ChangePasswordDataService changePasswordService = new ChangePasswordDataService(ChangePasswordActivity.this);
        changePasswordService.changePassword(request,prefManager.getAuthKey(), new OnSuccessListener() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if(((BaseResponse) response).getStatusCode()== 200){

                    prefManager.clear();
                    Toasty.success(ChangePasswordActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    LocationUtils locationUtils = new LocationUtils(ChangePasswordActivity.this);
                    locationUtils.removeLocationUpdates(null);
                    Intent i = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }else{
                    Toasty.error(ChangePasswordActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }



            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
               Toasty.error(ChangePasswordActivity.this, reason.getErrors(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
