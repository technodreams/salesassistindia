package com.cgtechnodreams.salesassistindia.changepassword.data;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.changepassword.model.ChangePasswordModel;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.google.android.gms.tasks.OnSuccessListener;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 31/05/2018.
 */

public class ChangePasswordDataService {
    private Context mContext;

    public ChangePasswordDataService(Context mContext) {
        this.mContext = mContext;
    }

    public void changePassword(ChangePasswordModel changePasswordModel, String token, final OnSuccessListener onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).changePassword(changePasswordModel,token);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());

                } else {
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                    if(response.code() == APIConstants.ERROR_UNAUTHORIZED ){
                        Intent intent = new Intent(mContext,LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        PrefManager preferenceManager = new PrefManager(mContext);
                        preferenceManager.clear();
                        (mContext).startActivity(intent);
                        ((Activity) mContext).finish();
                    }else{
                        Toasty.error(mContext,response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
            }
        });
    }
}
