package com.cgtechnodreams.salesassistindia.home;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.cgtechnodreams.salesassistindia.BaseApplication;
import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.changepassword.ChangePasswordActivity;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.login.SplashScreenActivity;
import com.cgtechnodreams.salesassistindia.login.data.LoginDataService;
import com.cgtechnodreams.salesassistindia.login.model.LogoutModel;
import com.cgtechnodreams.salesassistindia.sync.Sync;
import com.cgtechnodreams.salesassistindia.sync.SyncFileActivity;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.google.android.material.navigation.NavigationView;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    PrefManager prefManager = null;
    private long mLastClickTime = 0;
    public static final String TAG = "MainActivity";
//    private LocationUpdatesService mService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        prefManager = new PrefManager(this);

        if(prefManager.getAuthKey() == null){
            Intent intent = new Intent(MainActivity.this, SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }

        boolean downloadRequire = getIntent().getBooleanExtra("DownloadRequire",false);
        if(downloadRequire){
            new AlertDialog.Builder(this)
                    .setTitle("Application Message")
                    .setMessage("Please update the app data required for the proper functionality of application.")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            downloadAll();
                        }})
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(MainActivity.this, "You can download the app data from settings tab if required later.", Toast.LENGTH_LONG).show();
                        }
                    }).show();
        }

        if (!Utils.isConnectionAvailable(this)) {
            Toasty.info(this, "Please check your internet connection!!", Toast.LENGTH_SHORT, true).show();
        }

      //  locationUtils.setLastKnownLocation();
        Fragment fragment = new DashboardFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment).commitAllowingStateLoss();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);
        TextView username = hView.findViewById(R.id.username);
        username.setText(prefManager.getName());
        TextView role = hView.findViewById(R.id.userRole);
        role.setText(prefManager.getRole());
        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            AlertDialog alertbox = new AlertDialog.Builder(this)
                    .setMessage("Do you want to exit application?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        // do something when the button is clicked
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                            moveTaskToBack(true);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_upload) {
            int clickCount = 0;
            if (Utils.isConnectionAvailable(MainActivity.this)){

                if (!(SystemClock.elapsedRealtime() - mLastClickTime < 3000)){
                    clickCount++;
                    Log.d("Upload clicked Count",clickCount +"");
                    uploadAll();
                }

                Log.i("Other clicked Count",clickCount +"");
                mLastClickTime = SystemClock.elapsedRealtime();

            }
            else
                Utils.getConnectionToast(MainActivity.this);
            return true;
        } else if (id == R.id.action_download) {
            if (Utils.isConnectionAvailable(MainActivity.this))
                downloadAll();
            else
                Utils.getConnectionToast(MainActivity.this);

        }

        return super.onOptionsItemSelected(item);
    }

    private void uploadAll() {



        //@TODO delete this after implementing in proper way
        Sync sync = new Sync(this, prefManager.getRole(), false);
//        if (prefManager.getRole().equalsIgnoreCase("ISM")) {
//            sync.uploadSurvey();
//        } else if (prefManager.getRole().equalsIgnoreCase("Promoter")) {
//            sync.uploadDailySales();
   //     } else if (prefManager.getRole().equalsIgnoreCase("SalesMan")) {
         //   sync.uploadSalesOrder();
            sync.postDistributorPurchase();
            //
  //      }
    }


    private void downloadAll() {
        Sync sync = new Sync(this, prefManager.getRole(), false);
        sync.getArea();
    }
//@TODO fix me
//    private void syncData(List<SyncModel> data) {
//        Sync sync = new Sync(MainActivity.this);
//        for(SyncModel syncModelData : data){
//            if(syncModelData.getAction().equalsIgnoreCase("Area")){
//                sync.getArea();
//            }
//        }
//    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.content);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            if (!(getCurrentFragment() instanceof DashboardFragment)) {
                getSupportActionBar().setTitle("Dashboard");
                Fragment fragment = new DashboardFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment).commitAllowingStateLoss();
            }

        } else if (id == R.id.nav_sync_data) {
            Intent intent = new Intent(MainActivity.this, SyncFileActivity.class);
            startActivity(intent);

        } else if (id == R.id.change_password) {
            Intent changePassword = new Intent(MainActivity.this, ChangePasswordActivity.class);
            startActivity(changePassword);

        } else if (id == R.id.nav_logout) {
            logout();

        }else if(id == R.id.nav_manual){
            Intent intent = new Intent(MainActivity.this,UserManualActivity.class);
            startActivity(intent);
        }else if(id == R.id.nav_clear){

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            try {
                            File dir = new File(Environment.getExternalStorageDirectory()
                                    + File.separator + BaseApplication.APP_FOLDER_NAME);
                                FileUtils.deleteDirectory(dir);
                                Toast.makeText(MainActivity.this,"Project Folder Delete. Please sync app.",Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Are you sure? Do you want to delete all locally saved files?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Signing out");
        dialog.setCancelable(false);
        dialog.show();
        LogoutModel logoutModel = new LogoutModel();
        logoutModel.setLatitude(0);
        logoutModel.setLongitude(0);
        LoginDataService loginDataService = new LoginDataService(MainActivity.this);
        loginDataService.logout(logoutModel, prefManager.getAuthKey(), new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse locationResponse = (BaseResponse) response;
                if (locationResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(MainActivity.this, locationResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    //   finish();
                    if(locationResponse.getStatusCode() == APIConstants.ERROR_UNAUTHORIZED){
                        prefManager.clear();
                       // Toasty.success(MainActivity.this, locationResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
//                        LocationUtils locationUtils = new LocationUtils(MainActivity.this);
//                        Intent intent = new Intent(MainActivity.this, LocationUpdatesService.class);
//                        stopService(intent);
//                        mService.removeLocationUpdates();
                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    }

                } else {
                    prefManager.clear();
                    Toasty.success(MainActivity.this, locationResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
//                    LocationUtils locationUtils = new LocationUtils(MainActivity.this);
//                    locationUtils.removeLocationUpdates(null);
//                    Intent intent = new Intent(MainActivity.this, LocationUpdatesService.class);
//                    stopService(intent);
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }




}


