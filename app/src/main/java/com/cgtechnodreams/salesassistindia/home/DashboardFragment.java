package com.cgtechnodreams.salesassistindia.home;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.DashboardData;
import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    DashboardAdapter mAdapter;
    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        PrefManager prefManager = new PrefManager(getActivity());
        View rootView =  inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this,rootView);
        String dashboardData = getDashboardData(prefManager.getIsSupervisor());
        ObjectMapper objectMapper = new ObjectMapper();
        List<DashboardData> data = null;
        try {
            data = (List<DashboardData>) objectMapper.readValue(dashboardData, new TypeReference<List<DashboardData>>(){});
            Log.i("test",data.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        // specify an adapter (see also next example)
        mAdapter = new DashboardAdapter(data,getActivity());
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }

    private String getDashboardData(boolean isSupervisor) {
        String dashboardData = null;
        if(isSupervisor){
           // dashboardData = readAssetFile("salesman_supervisor_dashboard.json");
            dashboardData = readAssetFile("salesman_dashboard.json");

        }else if( !isSupervisor){
            dashboardData = readAssetFile("salesman_dashboard.json");
        }
        return dashboardData;
    }
//    private String readSyncFile(String userType) {
//        String syncData = null;
//        if(userType.equalsIgnoreCase("ism")){
//            syncData = readAssetFile("ism_sync_data.json");
//        }else if(userType.equalsIgnoreCase("promoter")){
//            syncData = readAssetFile("promoter_sync_data.json");
//        }else if(userType.equalsIgnoreCase("salesman")){
//            syncData = readAssetFile("salesman_sync_data.json");
//
//        }
//        return syncData;
//    }

    private String readAssetFile(String fileName) {
        String json = null;
        try {
            InputStream is = getResources().getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
