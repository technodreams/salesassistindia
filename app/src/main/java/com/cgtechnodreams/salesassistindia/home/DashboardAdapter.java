package com.cgtechnodreams.salesassistindia.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.DashboardData;
import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.attendance.AttendanceActivity;
import com.cgtechnodreams.salesassistindia.attendance.LeaveActivity;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.MovementGPSActivity;
import com.cgtechnodreams.salesassistindia.dailysales.CompetitorSalesActivity;
import com.cgtechnodreams.salesassistindia.dailysales.DailySalesActivity;
import com.cgtechnodreams.salesassistindia.dailysales.DailySalesListActivity;
import com.cgtechnodreams.salesassistindia.dms.DistributorListActivity;
import com.cgtechnodreams.salesassistindia.footfall.FootfallActivity;
import com.cgtechnodreams.salesassistindia.marketactivities.MarketActivitiesActivity;
import com.cgtechnodreams.salesassistindia.outlet.StockistListActivity;
import com.cgtechnodreams.salesassistindia.photo.PhotoActivity;
import com.cgtechnodreams.salesassistindia.report.ReportActivity;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.PendingSyncActivity;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.SalesOrderActivity;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.SalesOrderTargetActivity;
import com.cgtechnodreams.salesassistindia.salesorder.CollectionActivity;
import com.cgtechnodreams.salesassistindia.salesorder.StockistSalesOrderActivity;
import com.cgtechnodreams.salesassistindia.salesorder.SalesOrderSummaryActivity;
import com.cgtechnodreams.salesassistindia.salesorder.SalesmanSalesOrderReportActivity;
import com.cgtechnodreams.salesassistindia.survey.SurveyActivity;
import com.cgtechnodreams.salesassistindia.target.TargetActivity;

import java.util.List;

/**
 * Created by user on 04/01/2019.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {
    Context mContext;
    List<DashboardData> mData;
    public DashboardAdapter(List<DashboardData> data, Context ctx) {
        this.mData = data;
        this.mContext = ctx;

    }

    @NonNull
    @Override
    public DashboardAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dashboard, parent, false);
        return new DashboardAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardAdapter.MyViewHolder holder, int position) {
        holder.title.setText(mData.get(position).getTitle());
        holder.image.setImageDrawable(getDrawableByName(mData.get(position).getImage()));
        //holder.image.setImageDrawable(R.id.);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;
        public MyViewHolder(View itemView) {

            super(itemView);
             title = itemView.findViewById(R.id.title);
             image = itemView.findViewById(R.id.image);

             itemView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     if(title.getText().equals("Survey")){
                         Intent intent = new Intent(mContext, SurveyActivity.class);
                         mContext.startActivity(intent);
                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("Attendance")){
                         Intent intent = new Intent(mContext, AttendanceActivity.class);
                         mContext.startActivity(intent);
                     }else if(title.getText().equals("Photo")){
                         Intent intent = new Intent(mContext, PhotoActivity.class);
                         mContext.startActivity(intent);
                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("DailySales")){
                         Intent intent = new Intent(mContext, DailySalesActivity.class);
                         mContext.startActivity(intent);
                     }
                     else if (mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("Footfall")){
                         Intent intent = new Intent(mContext, FootfallActivity.class);
                         mContext.startActivity(intent);
                     }
                     else if (mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("Leave")){
                         Intent intent = new Intent(mContext, LeaveActivity.class);
                         mContext.startActivity(intent);
                     }
                     else if (mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("CompetitorSales")){
                         Intent intent = new Intent(mContext, CompetitorSalesActivity.class);
                         mContext.startActivity(intent);
                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("Inspection")){
                         Intent intent = new Intent(mContext, SurveyActivity.class);
                         mContext.startActivity(intent);

                     }
                     else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("Report")){
                         Intent intent = new Intent(mContext, ReportActivity.class);
                         mContext.startActivity(intent);

                     }
                     else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("Target")){
                         Intent intent = new Intent(mContext, TargetActivity.class);
                         mContext.startActivity(intent);

                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("OrderList")){//Promoter
                         Intent intent = new Intent(mContext, DailySalesListActivity.class);
                         mContext.startActivity(intent);
                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("SalesOrder")){//SalesMan
                         Intent intent = new Intent(mContext, SalesOrderActivity.class);
                         mContext.startActivity(intent);
                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("PendingSync")){
                         Intent intent = new Intent(mContext, PendingSyncActivity.class);
                         mContext.startActivity(intent);

                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("MovementGPS")){
                         Intent intent = new Intent(mContext, MovementGPSActivity.class);
                         mContext.startActivity(intent);
                     }
                     else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("SalesTarget")){
                         Intent intent = new Intent(mContext, SalesOrderTargetActivity.class);
                         mContext.startActivity(intent);
                     }
                     else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("NewOutlet")){
                         Intent intent = new Intent(mContext, StockistListActivity.class);
                         mContext.startActivity(intent);


                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("StockistSalesOrderActivity")){
                         Intent intent = new Intent(mContext, StockistSalesOrderActivity.class);
                         mContext.startActivity(intent);
                     }

                     else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("CollectionActivity")){
                         Intent intent = new Intent(mContext, CollectionActivity.class);
                         mContext.startActivity(intent);
                     }
                     else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("PendingSyncOrderActivity")){
                         Intent intent = new Intent(mContext, SalesOrderSummaryActivity.class);
                         mContext.startActivity(intent);
                     }
                     else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("SalesmanSalesReport")){
                         Intent intent = new Intent(mContext, SalesmanSalesOrderReportActivity.class);
                         mContext.startActivity(intent);
                     }
                     else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("MarketActivitiesActivity")){
                         Intent intent = new Intent(mContext, MarketActivitiesActivity.class);
                         mContext.startActivity(intent);
                     }else if(mData.get(getAdapterPosition()).getActivityName().equalsIgnoreCase("NewDistributor")){
                         Intent intent = new Intent(mContext, DistributorListActivity.class);
                         mContext.startActivity(intent);
                     }


                 }
             });
        }
    }
    public Drawable getDrawableByName(String drawableName){

        Resources resources = mContext.getResources();
        final int resourceId = resources.getIdentifier(drawableName, "drawable",
                mContext.getPackageName());

       return resources.getDrawable(resourceId);
}
}

