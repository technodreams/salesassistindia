package com.cgtechnodreams.salesassistindia.bank;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.bank.model.Bank;
import com.cgtechnodreams.salesassistindia.databasehelper.model.BankEntity;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BankActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    BankAdapter bankAdapter = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ArrayList<Bank> bankList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        toolbar.setTitle("Bank List");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        BankEntity bankEntity = new BankEntity(this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) bankEntity.find(null);
        try {
            objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            bankList = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Bank>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        LinearLayoutManager bankManager = new LinearLayoutManager(BankActivity.this, RecyclerView.VERTICAL, false);
        bankAdapter = new BankAdapter(BankActivity.this, bankList);
        recyclerView.setLayoutManager(bankManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(bankAdapter);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.search_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<Bank> newList = new ArrayList<>();
                for (Bank bank : bankList) {
                    String name = bank.getBankName().toLowerCase();

                    if (name.contains(newText)) {
                        newList.add(bank);
                    }

                    bankAdapter.setFilter(newList);

                }
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                          }
                                      }
        );
        return super.onCreateOptionsMenu(menu);
    }
}
