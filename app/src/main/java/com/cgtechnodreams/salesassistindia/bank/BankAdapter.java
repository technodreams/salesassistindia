package com.cgtechnodreams.salesassistindia.bank;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.bank.model.Bank;
import com.cgtechnodreams.salesassistindia.salesorder.CollectionActivity;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.MyViewHolder>{
    Context mContext;
    PrefManager prefManager;
    ArrayList<Bank> bankList = new ArrayList<>();

    public BankAdapter(Context context, ArrayList<Bank> bankList) {
        this.mContext = context;
        this.bankList = bankList;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public BankAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_bank, null);
        return new BankAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BankAdapter.MyViewHolder holder, int position) {
        Bank bank = bankList.get(position);
        holder.name.setText(bank.getBankName());
    }

    @Override
    public int getItemCount() {
        return bankList.size();
    }

    public void setFilter(ArrayList<Bank> newList) {
        bankList = new ArrayList<>();
        bankList.addAll(newList);
        notifyDataSetChanged();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, CollectionActivity.class);
                    intent.putExtra("BankDetail",bankList.get(getAdapterPosition()));
                    ((Activity) mContext).setResult(Activity.RESULT_OK,intent);
                    ((Activity) mContext).finish();
                }
            });

        }

    }
}
