package com.cgtechnodreams.salesassistindia.bank.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class BankResponse extends BaseResponse {
    @JsonProperty("data")
    ArrayList<Bank> banks;

    public ArrayList<Bank> getBanks() {
        return banks;
    }

    public void setBanks(ArrayList<Bank> banks) {
        this.banks = banks;
    }
}
