package com.cgtechnodreams.salesassistindia.api.interfaces;

/**
 * Created on 27/11/2017.
 *
 * @author Shrestha Bk
 */

public interface OnSuccess {
    public void onSuccess(Object response);
}
