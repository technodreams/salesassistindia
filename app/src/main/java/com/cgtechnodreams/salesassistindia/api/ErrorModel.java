package com.cgtechnodreams.salesassistindia.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created on 21/03/2018.
 *
 * @author Shrestha BK
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorModel {
    private String message;
    private String errors;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    private int code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }
}
