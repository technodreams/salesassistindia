package com.cgtechnodreams.salesassistindia.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 23/03/2018.
 *
 * @author Shrestha BK
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse {
    @JsonProperty("statusCode")
    private int statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("message")
    private String message;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
