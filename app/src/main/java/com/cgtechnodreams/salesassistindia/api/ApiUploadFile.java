package com.cgtechnodreams.salesassistindia.api;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ApiUploadFile {
    private static ApiUploadFile ourInstance;
    private static Retrofit retrofit;
    private APIService apiService;

    public static Retrofit retrofit() {
        if (retrofit == null) {
            ourInstance = new ApiUploadFile();
            return retrofit;
        }
        return retrofit;
    }

    public static APIService getService(Context ctx) {
        ourInstance = new ApiUploadFile(ctx);
        return ourInstance.apiService;
    }

    public ApiUploadFile(Context ctx) {

        final PrefManager mPrefs = new PrefManager(ctx);

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
      loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(400, TimeUnit.SECONDS);
        builder.connectTimeout(400, TimeUnit.SECONDS);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                Request.Builder builder = original.newBuilder()
                        .header("Content-Type", "multipart/form-data")
                        .header("Accept", "application/json")
                        .header("Cache-Control", "no-cache")
                        .method(original.method(), original.body());

                request = builder.build();
                return chain.proceed(request);
            }
        }).addInterceptor(loggingInterceptor);
        OkHttpClient client = builder.build();
        Retrofit.Builder adapterBuilder = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create())
                .client(client)
                .baseUrl(APIConstants.BASE_URL);
        retrofit = adapterBuilder.build();
        this.apiService = retrofit.create(APIService.class);
    }

    public ApiUploadFile() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(120, TimeUnit.SECONDS);
        builder.connectTimeout(120, TimeUnit.SECONDS);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                request = original.newBuilder()
                        .header("Content-Type", "multipart/form-data")
                        .header("Accept", "application/json")
                        .header("Cache-Control", "no-cache")
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });
        OkHttpClient client = builder.build();
        Retrofit.Builder adapterBuilder = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create())
                .client(client)
                .baseUrl(APIConstants.BASE_URL);

        retrofit = adapterBuilder.build();
        this.apiService = retrofit.create(APIService.class);
    }
}