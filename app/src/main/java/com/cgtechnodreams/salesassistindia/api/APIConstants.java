package com.cgtechnodreams.salesassistindia.api;

/**
 * Created on 27/11/2017.
 *
 * @author Shrestha BK
 */

public class APIConstants {
    public static final int ERROR_CODE_DEVICE_MISMATCH = 409;
    public static final int ERROR_CODE_DEVICE_NOT_REGISTERED = 404;
    public static final int ERROR_UNAUTHORIZED = 401;
    public static final int ERROR_BAD_REQUEST = 400;
   public static final String BASE_URL = "https://salesassist.cgfoodsindia.com/api/";
 //  public static final String BASE_URL = "http://192.168.11.26/salesassistindia/api/";
    public static final String USER_MANUAL = BASE_URL + "manual";
    public static final String LOGIN = "login";
    public static final String CHANGE_PASSWORD = "user/changePassword";
    public static final String LOGOUT = "user/logout";
    public static final String REGISTER_DEVICE = "login/post-device-info";
    public static final String USER_ATTENDANCE = "user/attendance";
    public static final String USER_LEAVE = "user/leave";
    public static final String GET_BRAND = "brands";
    public static final String GET_PRODUCT = "products";
    public static final String GET_PRODUCT_GROUP = "product-groups";
    public static final String GET_COMPETITOR_GROUP = "competitorGroups";
    public static final String POST_DAILY_FOOTFALL = "outlets/footfall";
    public static final String MOVEMENT_GPS = "user/gps-movements";
    public static final String PROMOTER_DAILY_SALES = "promoter/daily-sales";
    public static final String PROMOTER_COMPETITOR_SALES = "promoter/competitor-sales";
    public static final String INSPECTION = "allInspections";
    public static final String POST_SURVEY = "postInspections";
    public static final String GET_STOCKIST = "outlets";
    public static final String GET_PROMOTER = "promoters";
    public static final String GET_ATTENDANCE_REPORT = "user/attendance-reports";
    public static final String GET_LEAVE_REPORT = "user/leave-reports";
    public static final String GET_ACHIEVEMENT_REPORT = "user/achievement";
    public static final String GET_DAILY_SALES_REPORT = "user/promoter/daily-activities-reports";
    public static final String POST_USER_ACTIVITIES = "user/activities";
    public static final String GET_TARGET = "user/targets";
    public static final String UPLOAD_IMAGE = "user/image/upload";
    public static final String GET_AREA = "user/areas";
    public static final String GET_PRODUCT_INVENTORIES = "user/inventories";
    public static final String POST_SALES_ORDER = "user/salesman/orderRequest";
    public static final String POST_COLLECTIONS = "user/collection";
    public static final String UPLOAD_COLLECTION_IMAGE = "user/collection/image";
    public static final String GET_SALES_REPORT = "user/salesman/getOrderRequest";
    public static final String POST_STOCKIST_REQUEST = "user/salesman/requestOutlet";
    public static final String GET_30DAYS_SALES_REPORT_OUTLET = "user/outlet/salesReport/{id}";
    public static final String POST_OUTLET_STATUS = "user/salesman/outletVisit";
    public static final String GET_PRODUCT_SUBGROUP = "user/subGroups";
    public static final String POST_OUTLET_STOCK = "outlet/stock";
    public static final String POST_DAMAGE_OUTLET_STOCK = "distributor/damage/stock";
    public static final String GET_OUTLET_ACHIEVEMENT = "user/outletAchievement/{id}";
    public static final String GET_BANKS = "banks";
    public static final String GET_SALESMAN_SALES_REQUEST_REPORT = "getSalesman";
    public static final String GET_SALES_REQUEST_DETAIL_REPORT ="getSalesman/salesRequest/{salesmanId}/{distributorId}";
    public static final String GET_ACTUAL_DISPATCHED_ORDER = "user/distributor/getDispatch/{id}";
    public static final String STOCKIST_DAILY_SALES ="user/salesman/stockist/sales" ;
    public static final String PENDING_SALES_REQUEST = "getChildPendingRequest";
    public static final String PENDING_SALES_REQUEST_DETAIL = "getChildPendingRequest/{id}";
    public static final String UPDATE_PENDING_EACH_SALES_REQUEST = "getPendingRequestUpdateDetail/{id}";
    public static final String GET_PARTIAL_DISPATCHED_ORDER= "user/distributor/getDispatchOrder/{id}";
    public static final String GET_STOCKIST_STOCK = "stockist/stock/{id}}";
    public static final String GET_MARKET_ACTIVITIES_CATEGORY = " user/activitiesCategory";
    public static final String POST_MARKET_ACTIVITIES = " user/marketActivities";
    public static final String POST_MARKET_ACTIVITIES_IMAGES = "user/marketActivities/image";
    public static final String GET_DISTRIBUTOR_COLLECTION_REPORT = "user/getCollections";
    public static final String GET_STOCKIST_DSR_REPORT = "user/getStockistSales";
    public static final String GET_MARKET_ACTIVITIES_REPORT = "user/getMarketActivities";
    public static final String GET_COMPANY_LIST = "companies";
    public static final String GET_ALL_DISPATCHED_REPORT = "user/distributor/getAllDispatch";
    public static final String GET_ALL_PARTIAL_DISPATCHED_REPORT = "user/distributor/getAllDispatchOrders";
    public static final String GET_DISTRIBUTORS = "distributors";
    public static final String POST_DISTRIBUTOR_REQUEST = "user/requestDistributors";
    public static final String POST_DISTRIBUTOR_SALES_ORDER = "user/distributors/orderRequest";
    public static final String POST_DISTRIBUTOR_STOCK = "distributor/stock";
    public static final String  POST_DAMAGE_DISTRIBUTOR_STOCK = "user/distributor/damage/stock";
    public static final String GET_DISTRIBUTOR_STOCK = "distributor/stock/{id}}";
    public static final String DISTRIBUTOR_DAILY_SALES ="user/salesman/distributor/sales" ;
    public static final String DISTRIBUTOR_CLOSING_STOCK ="user/distributors/stock" ;
    public static final String DISTRIBUTOR_PURCHASE_ORDER = "user/stockistSales";

    public static final String GET_MONTHLY_DISTRIBUTOR_STOCK = "distributors/inventories/{id}/search";

    public static final String GET_MONTHLY_DISTRIBUTOR_PURCHASE = "user/stockistSales/{id}";

   // public static final String GET_DISTRIBUTOR_DSR_REPORT = "user/getDistributorSales";user/getStockistSales
}

