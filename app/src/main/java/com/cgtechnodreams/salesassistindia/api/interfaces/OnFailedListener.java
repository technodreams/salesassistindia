package com.cgtechnodreams.salesassistindia.api.interfaces;


import com.cgtechnodreams.salesassistindia.api.ErrorModel;

/**
 * Created on 27/11/2017.
 *
 * @author Shrestha Bk
 */

public interface OnFailedListener {
    public void onFailed(ErrorModel reason);
}

