package com.cgtechnodreams.salesassistindia.api;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static ErrorModel parseError(Response<?> response) {
        Converter<ResponseBody, ErrorModel> converter =
                Api.retrofit()
                        .responseBodyConverter(ErrorModel.class, new Annotation[0]);
        ErrorModel error;
        try {
            error = converter.convert(response.errorBody());
            error.setErrors(response.message());
        } catch (IOException e) {
            return new ErrorModel();
        }

        return error;
    }
}