package com.cgtechnodreams.salesassistindia.api;

import com.cgtechnodreams.salesassistindia.product.model.BrandResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductGroupResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RxApiService {

    @GET(APIConstants.GET_PRODUCT)
    Observable<ProductResponse> product(@Query("token") String token);
    @GET(APIConstants.GET_BRAND)
    Observable<BrandResponse> brand(@Query("token") String token);
    @GET(APIConstants.GET_PRODUCT_GROUP)
    Observable<ProductGroupResponse> productGroup(@Query("token") String token);
}
