package com.cgtechnodreams.salesassistindia.api;


import com.cgtechnodreams.salesassistindia.attendance.AttendanceModel;
import com.cgtechnodreams.salesassistindia.attendance.model.AttendenceResponse;
import com.cgtechnodreams.salesassistindia.attendance.model.LeaveModel;
import com.cgtechnodreams.salesassistindia.attendance.model.PostLeaveResponse;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationResponse;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.model.UserMovementResponse;
import com.cgtechnodreams.salesassistindia.bank.model.BankResponse;
import com.cgtechnodreams.salesassistindia.changepassword.model.ChangePasswordModel;
import com.cgtechnodreams.salesassistindia.dailysales.model.CompetitorSales;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySales;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySalesResponse;
import com.cgtechnodreams.salesassistindia.dailysales.model.LocationModel;
import com.cgtechnodreams.salesassistindia.databasehelper.UserActivity;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchaseResponse;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorDailySales;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchase;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorResponse;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorSalesOrder;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorStockModel;
import com.cgtechnodreams.salesassistindia.dms.model.NewDistributorRequest;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStock;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStockResponse;
import com.cgtechnodreams.salesassistindia.fieldwork.model.ActualDispatchedResponse;
import com.cgtechnodreams.salesassistindia.fieldwork.model.DispatchedOrderResponse;
import com.cgtechnodreams.salesassistindia.fieldwork.model.StockistAchievementResponse;
import com.cgtechnodreams.salesassistindia.footfall.model.FootFall;
import com.cgtechnodreams.salesassistindia.footfall.model.FootFallResponse;
import com.cgtechnodreams.salesassistindia.login.model.DeviceModel;
import com.cgtechnodreams.salesassistindia.login.model.LoginModel;
import com.cgtechnodreams.salesassistindia.login.model.LoginResponse;
import com.cgtechnodreams.salesassistindia.login.model.LogoutModel;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivity;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivityCategoryResponse;
import com.cgtechnodreams.salesassistindia.outlet.model.NewStockistRequest;
import com.cgtechnodreams.salesassistindia.outlet.model.StockistResponse;
import com.cgtechnodreams.salesassistindia.outlet.model.StockistStatus;
import com.cgtechnodreams.salesassistindia.product.model.BrandResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductGroupResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductInventoryResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductSubGroupResponse;
import com.cgtechnodreams.salesassistindia.report.model.AchievementResponse;
import com.cgtechnodreams.salesassistindia.report.model.AttendenceReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.DailySalesReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.DispatchListReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.DistributorCollectionResponse;
import com.cgtechnodreams.salesassistindia.report.model.DistributorDSRResponse;
import com.cgtechnodreams.salesassistindia.report.model.LeaveReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.MarketActivitiesReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.PartialDispatchedResponse;
import com.cgtechnodreams.salesassistindia.report.model.ReportRequest;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.AreaResponse;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesManTargetResponse;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesReportResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionDetailModel;
import com.cgtechnodreams.salesassistindia.salesorder.model.CompanyResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesDetailResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequestResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.SalesOrderWithRemarks;
import com.cgtechnodreams.salesassistindia.salesorder.model.SalesmanSalesOrderReportRepsonse;
import com.cgtechnodreams.salesassistindia.salesorder.model.SalesmanSalesRequestResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.StockistDailySales;
import com.cgtechnodreams.salesassistindia.stock.model.StockistStockModel;
import com.cgtechnodreams.salesassistindia.stock.model.StockistStockResponse;
import com.cgtechnodreams.salesassistindia.survey.SurveyResponse;
import com.cgtechnodreams.salesassistindia.survey.model.AllSurveyResponse;
import com.cgtechnodreams.salesassistindia.survey.model.AnswerModel;
import com.cgtechnodreams.salesassistindia.target.model.TargetResponse;
import com.cgtechnodreams.salesassistindia.user.UserResponse;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created on 27/11/2017.
 *
 * @author Shrestha BK
 */

public interface APIService {
    @POST(APIConstants.LOGIN)
    Call<LoginResponse> doLogIn(@Body LoginModel request);
    @POST(APIConstants.LOGOUT)
    Call<BaseResponse> logout(@Body LogoutModel request,@Query("token") String token);
    @POST(APIConstants.REGISTER_DEVICE)
    Call<LoginResponse> registerDevice(@Body DeviceModel request);
    @POST(APIConstants.USER_ATTENDANCE)
    Call<AttendenceResponse> userAttendence(@Body AttendanceModel request,@Query("token") String token);
    @POST(APIConstants.CHANGE_PASSWORD)
    Call<BaseResponse> changePassword(@Body ChangePasswordModel request, @Query("token") String token);
    @POST(APIConstants.USER_LEAVE)
    Call<PostLeaveResponse> postLeave(@Body LeaveModel request, @Query("token") String token);
    @GET(APIConstants.GET_PRODUCT)
    Call<ProductResponse> getProduct(@Query("token") String token);
    @GET(APIConstants.GET_BRAND)
    Call<BrandResponse> getBrand(@Query("token") String token);
    @GET(APIConstants.GET_PRODUCT_GROUP)
    Call<ProductGroupResponse> getProductGroup(@Query("token") String token);
    @GET(APIConstants.GET_COMPETITOR_GROUP)
    Call<ProductGroupResponse> getCompetitorGroup(@Query("token") String token);
    @POST(APIConstants.PROMOTER_DAILY_SALES)
    Call<DailySalesResponse> postDailysales(@Query("token") String token, @Body ArrayList<DailySales> request);
    @POST(APIConstants.PROMOTER_COMPETITOR_SALES)
    Call<BaseResponse> postCompetitorSalesDetail(@Query("token") String token, @Body ArrayList<CompetitorSales> request);
    @POST(APIConstants.POST_DAILY_FOOTFALL)
    Call<FootFallResponse> postDailyFootfall(@Query("token") String token, @Body FootFall request);
    @POST(APIConstants.MOVEMENT_GPS)
    Call<LocationResponse> movementGPS(@Query("token") String token, @Body ArrayList<LocationModel> request);
    @GET(APIConstants.INSPECTION)
    Call<SurveyResponse> getSurvey(@Query("token") String token);
    @GET(APIConstants.GET_PRODUCT)
    Call<AllSurveyResponse> getAllSurveyDetail(@Query("token") String token);
    @POST(APIConstants.POST_SURVEY)
    Call<BaseResponse> postInspection(@Query("token") String token, @Body ArrayList<AnswerModel> model);
    @GET(APIConstants.GET_STOCKIST)
    Call<StockistResponse> getStockist(@Query("token") String token);
    @GET(APIConstants.GET_PROMOTER)
    Call<UserResponse> getPromoter(@Query("token") String token);
    @POST(APIConstants.GET_ATTENDANCE_REPORT)
    Call<AttendenceReportResponse> getAttendenceReport(@Query("token") String token,@Body ReportRequest model);
    @POST(APIConstants.GET_DAILY_SALES_REPORT)
    Call<DailySalesReportResponse> getDailySalesReport(@Query("token") String token, @Body ReportRequest model);
    @POST(APIConstants.GET_LEAVE_REPORT)
    Call<LeaveReportResponse> getLeaveReport(@Query("token") String token, @Body ReportRequest model);
    @POST(APIConstants.GET_ACHIEVEMENT_REPORT)
    Call<AchievementResponse> getAchievementReport(@Query("token") String token, @Body ReportRequest model);
    @GET(APIConstants.GET_TARGET)
    Call<SalesManTargetResponse> getSalesManTarget(@Query("token") String token);
    @GET(APIConstants.GET_TARGET)
    Call<TargetResponse> getTarget(@Query("token") String token);
    @POST(APIConstants.POST_USER_ACTIVITIES)
    Call<BaseResponse> postActivities(@Query("token") String token, @Body ArrayList<UserActivity> model);

     @Multipart
    @POST(APIConstants.UPLOAD_IMAGE)
    Call<ResponseBody> uploadMultipleFilesDynamic(
            @Query("token") String token,
            @Part("description") RequestBody description,
            @Part("size") RequestBody size,
            @Part List<MultipartBody.Part> files);

    @Multipart
    @POST(APIConstants.UPLOAD_COLLECTION_IMAGE)
    Call<ResponseBody> uploadCollectionMultipleFilesDynamic(
            @Query("token") String token,
            @Part("description") RequestBody description,
            @Part("size") RequestBody size,
            @Part List<MultipartBody.Part> files);

    @GET(APIConstants.GET_AREA)
    Call<AreaResponse> getArea(@Query("token") String token);
    @GET(APIConstants.GET_PRODUCT_INVENTORIES)
    Call<ProductInventoryResponse> getInventories(@Query("token") String token);
    @POST(APIConstants.POST_SALES_ORDER)
    Call<BaseResponse> postSalesOrder(@Query("token") String token, @Body ArrayList<SalesOrderWithRemarks> request);
    @POST(APIConstants.POST_COLLECTIONS)
    Call<BaseResponse> postCollectionDetail(@Query("token") String token, @Body ArrayList<CollectionDetailModel> request);
    @POST(APIConstants.GET_SALES_REPORT)
    Call<SalesReportResponse> getSalesOrderReport(@Query("token") String token, @Body ReportRequest request);
    @GET(APIConstants.GET_SALESMAN_SALES_REQUEST_REPORT)
    Call<SalesmanSalesOrderReportRepsonse> getSalesmanSalesOrderRequestReport(@Query("token") String token);
    @POST(APIConstants.POST_STOCKIST_REQUEST)
    Call<BaseResponse> postOutletRequest(@Query("token") String token, @Body ArrayList<NewStockistRequest> request);
    @GET(APIConstants.GET_30DAYS_SALES_REPORT_OUTLET)
    Call<SalesReportResponse> getOutletSalesResponse(@Path("id") int id,@Query("token") String token);
    @POST(APIConstants.POST_OUTLET_STATUS)
    Call<BaseResponse> postStockistStatus(@Query("token") String token, @Body ArrayList<StockistStatus> request);
    @GET(APIConstants.GET_PRODUCT_SUBGROUP)
    Call<ProductSubGroupResponse> getProductSubGroupResponse(@Query("token") String token);
    @POST(APIConstants.POST_OUTLET_STOCK)
    Call<BaseResponse> postOutletStock(@Query("token") String token, @Body ArrayList<StockistStockModel> request);
    @POST(APIConstants.POST_DAMAGE_OUTLET_STOCK)
    Call<BaseResponse> postDamageStokistStock(@Query("token") String token, @Body ArrayList<StockistStockModel> request);
    @GET(APIConstants.GET_OUTLET_ACHIEVEMENT)
    Call<StockistAchievementResponse> getOutletAchievement(@Path("id") int id, @Query("token") String token);
    @GET(APIConstants.MOVEMENT_GPS)
    Call<UserMovementResponse> getMovementGPS(@Query("token") String token);
    @GET(APIConstants.GET_BANKS)
    Call<BankResponse> getBanks(@Query("token") String token);
    @GET(APIConstants.GET_SALES_REQUEST_DETAIL_REPORT)
    Call<SalesmanSalesRequestResponse> getSalesRequestDetail(@Path("salesmanId") String salesmanId, @Path("distributorId") String distributorId, @Query("token") String token);
    @GET(APIConstants.GET_ACTUAL_DISPATCHED_ORDER)
    Call<ActualDispatchedResponse> getDispatched(@Path("id") int id, @Query("token") String token, @Query("from_date") String fromDate, @Query("to_date") String toDate);
    @POST(APIConstants.STOCKIST_DAILY_SALES)
    Call<BaseResponse> postStockistDailySales(@Query("token") String token, @Body ArrayList<StockistDailySales> request);
    @GET(APIConstants.PENDING_SALES_REQUEST)
    Call<PendingSalesRequestResponse> getPendingSalesRequest(@Query("token") String token);
    @GET(APIConstants.PENDING_SALES_REQUEST_DETAIL)
    Call<PendingSalesDetailResponse> getPendingSalesRequestDetail(@Path("id") int id, @Query("token") String token);
    @POST(APIConstants.UPDATE_PENDING_EACH_SALES_REQUEST)
    Call<BaseResponse> updateEachPendingSalesRequestDetail(@Path("id") int id, @Query("token") String token, @Query("quantity") int qty);
    @GET(APIConstants.GET_PARTIAL_DISPATCHED_ORDER)
    Call<DispatchedOrderResponse> getPartialDispatched(@Path("id") int id, @Query("token") String token);
    @GET(APIConstants.GET_STOCKIST_STOCK)
    Call<StockistStockResponse> getStockistStock(@Path("id") int id, @Query("token") String token, @Query("from") String from, @Query("to") String to);
    @GET(APIConstants.GET_MARKET_ACTIVITIES_CATEGORY)
    Call<MarketActivityCategoryResponse> getMarketActivityCategory( @Query("token") String token);
    @POST(APIConstants.POST_MARKET_ACTIVITIES)
    Call<BaseResponse> postMarketActivities(@Query("token") String token, @Body ArrayList<MarketActivity> request);
    @Multipart
    @POST(APIConstants.POST_MARKET_ACTIVITIES_IMAGES)
    Call<ResponseBody> uploadMarketActivitiesImages(
            @Query("token") String token,
            @Part("description") RequestBody description,
            @Part("size") RequestBody size,
            @Part List<MultipartBody.Part> files);

    @GET(APIConstants.GET_DISTRIBUTOR_COLLECTION_REPORT)
    Call<DistributorCollectionResponse> getDistributorCollectionReport(@Query("token") String token, @Query("distributor_id") int id, @Query("from_date") String from, @Query("to_date") String to);
    @GET(APIConstants.GET_STOCKIST_DSR_REPORT)
    Call<DistributorDSRResponse> getDistributorDSRReport(@Query("token") String token, @Query("distributor_id") int id, @Query("from_date") String from, @Query("to_date") String to);
    @GET(APIConstants.GET_MARKET_ACTIVITIES_REPORT)
    Call<MarketActivitiesReportResponse> getMarketActivitiesReport(@Query("token") String token, @Query("from_date") String from, @Query("to_date") String to);
    @GET(APIConstants.GET_COMPANY_LIST)
    Call<CompanyResponse> getCompanyList(@Query("token") String token);

    @GET(APIConstants.GET_ALL_DISPATCHED_REPORT)
    Call<DispatchListReportResponse> getAllDispatched(@Query("token") String token, @Query("from_date") String fromDate, @Query("to_date") String toDate);
    @GET(APIConstants.GET_ALL_PARTIAL_DISPATCHED_REPORT)
    Call<PartialDispatchedResponse> getAllPartialDispatchedReport(@Query("token") String token,@Query("from_date") String fromDate, @Query("to_date") String toDate);

    @GET(APIConstants.GET_DISTRIBUTORS)
    Call<DistributorResponse> getDistributor(@Query("token") String token);
    @POST(APIConstants.POST_DISTRIBUTOR_REQUEST)
    Call<BaseResponse> postDistributorRequest(@Query("token") String token, @Body ArrayList<NewDistributorRequest> request);


    @POST(APIConstants.POST_DISTRIBUTOR_SALES_ORDER)
    Call<BaseResponse> postDistributorSalesOrder(@Query("token") String token, @Body ArrayList<DistributorSalesOrder> request);
    @POST(APIConstants.POST_DISTRIBUTOR_STOCK)
    Call<BaseResponse> postDistributorStock(@Query("token") String token, @Body ArrayList<DistributorStockModel> request);
    @POST(APIConstants.POST_DAMAGE_DISTRIBUTOR_STOCK)
    Call<BaseResponse> postDamageDistributorStock(@Query("token") String token, @Body ArrayList<DistributorStockModel> request);
    @GET(APIConstants.GET_DISTRIBUTOR_STOCK)
    Call<StockistStockResponse> getDistributorStock(@Path("id") int id, @Query("token") String token, @Query("from") String from, @Query("to") String to);
    @POST(APIConstants.DISTRIBUTOR_DAILY_SALES)
    Call<BaseResponse> postDistributorDailysales(@Query("token") String token, @Body ArrayList<DistributorDailySales> request);

    @POST(APIConstants.DISTRIBUTOR_CLOSING_STOCK)
    Call<BaseResponse> uploadClosingStock(@Query("token") String authKey,  @Body ArrayList<ClosingStock> openingStock);


    @POST(APIConstants.DISTRIBUTOR_PURCHASE_ORDER)
    Call<BaseResponse> uploadDistributorPurchase(@Query("token") String authKey,  @Body ArrayList<DistributorPurchase> purchases);

    @GET(APIConstants.GET_MONTHLY_DISTRIBUTOR_STOCK)
    Call<ClosingStockResponse> getMonthlyDistributorStock( @Path("id") int distributorId,@Query("token") String authKey, @Query("month") String month, @Query("year") String year);

    @GET(APIConstants.GET_MONTHLY_DISTRIBUTOR_PURCHASE)
    Call<DistributorPurchaseResponse> getMonthlyDistributorPurchase(@Path("id") int stockiestId, @Query("token") String authKey, @Query("date_from") String fromDate, @Query("date_to") String toDate, @Query("distributor_id") int distributorId);
}

