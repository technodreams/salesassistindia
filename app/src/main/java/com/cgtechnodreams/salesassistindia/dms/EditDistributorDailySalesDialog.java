package com.cgtechnodreams.salesassistindia.dms;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorDailySales;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditDistributorDailySalesDialog  extends DialogFragment {
    AppCompatEditText productQuantity;
    TextView productName;
    @BindView(R.id.btnUpdate)
    public Button btnUpdate;
    private DistributorDailySales salesOrder;
    public EditDistributorDailySalesDialog newInstance(DistributorDailySales stockistDailySales) {
        Bundle args = new Bundle();
        args.putSerializable("SalesDetail", stockistDailySales);
        this.setArguments(args);
        return this;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_order_update, container, false);
        productQuantity = (AppCompatEditText) view.findViewById(R.id.updateQuantity);
        if (getArguments() != null) {
            salesOrder = (DistributorDailySales) getArguments().getSerializable("SalesDetail");
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        productName = view.findViewById(R.id.productName);
        productName.setText(salesOrder.getProductName());
        productQuantity.setText(salesOrder.getQuantity() + "");
    }

    @OnClick(R.id.btnUpdate)
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (productQuantity.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please enter quantity", Toast.LENGTH_SHORT).show();
                } else{
                    salesOrder.setQuantity(Integer.parseInt(productQuantity.getText().toString()));

                    updateOrder(salesOrder);
                }
        }
    }

    private void updateOrder(DistributorDailySales salesOrder) {
        DistributorSalesOrderEntity salesOrderEntity = new DistributorSalesOrderEntity(getActivity());
        salesOrderEntity.update(salesOrder);
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }
}
