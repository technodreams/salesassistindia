package com.cgtechnodreams.salesassistindia.dms.distributorpurchase.data;

import android.content.Context;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchase;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchaseResponse;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStockResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DistributorPurchaseDataService {

    private Context mContext;
    PrefManager prefManager;
    public DistributorPurchaseDataService(Context mContext) {
        this.mContext = mContext;
        prefManager = new PrefManager(mContext);
    }

    public void uploadDistributorPurchase(ArrayList<DistributorPurchase> distributorPurchases, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).uploadDistributorPurchase(prefManager.getAuthKey(),distributorPurchases);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    Toasty.error(mContext,"Code:" + response.code() +" " +  response.message(), Toast.LENGTH_SHORT,true).show();
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void getClosingStock(String month, String year, int distributorId, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ClosingStockResponse> callBack = Api.getService(mContext).getMonthlyDistributorStock(distributorId,prefManager.getAuthKey(),month,year);
        callBack.enqueue(new Callback<ClosingStockResponse>() {
            @Override
            public void onResponse(Call<ClosingStockResponse> call, Response<ClosingStockResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    Toasty.error(mContext,"Code:" + response.code() +" " +  response.message(), Toast.LENGTH_SHORT,true).show();
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }
            @Override
            public void onFailure(Call<ClosingStockResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void getDistributorPurchase(int stockistId, int distributorId, String fromDate, String toDate, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<DistributorPurchaseResponse> callBack = Api.getService(mContext).getMonthlyDistributorPurchase(stockistId,prefManager.getAuthKey(),fromDate,toDate,distributorId);
        callBack.enqueue(new Callback<DistributorPurchaseResponse>() {
            @Override
            public void onResponse(Call<DistributorPurchaseResponse> call, Response<DistributorPurchaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    Toasty.error(mContext,"Code:" + response.code() +" " +  response.message(), Toast.LENGTH_SHORT,true).show();
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }
            @Override
            public void onFailure(Call<DistributorPurchaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

}
