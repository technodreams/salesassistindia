package com.cgtechnodreams.salesassistindia.dms.productclosing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductClosingEntity;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStock;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductOpeningAdapter extends RecyclerView.Adapter<ProductOpeningAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<ClosingStock> products = new ArrayList<>();
    String date;
    boolean isReport;

    public ProductOpeningAdapter(Context context, ArrayList<ClosingStock> products, boolean isReport) {
        this.mContext = context;
        this.products = products;
        this.isReport = isReport;
        prefManager = new PrefManager(mContext);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_closing_stock, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ClosingStock eachItem = products.get(position);
        holder.name.setText(eachItem.getProductName());
        holder.qty.setText(eachItem.getQuantity() + "");
        if(isReport == true){
            holder.delete.setVisibility(View.GONE);
            holder.edit.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.productName)
        TextView name;
        @BindView(R.id.quantity)
        TextView qty;
        @BindView(R.id.edit)
        ImageView edit;
        @BindView(R.id.delete)
        ImageView delete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm =((AppCompatActivity) mContext).getSupportFragmentManager();
                    UpdateStockDialog dialogFragment = new UpdateStockDialog().newInstance(products.get(getAdapterPosition()));
                    dialogFragment.show(fm, "dialog");
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProductClosingEntity entity = new ProductClosingEntity(mContext);
                    entity.deleteById(products.get(getAdapterPosition()).getDistributorId(),products.get(getAdapterPosition()).getProductId());
                    products.remove(products.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });
        }
    }
}
