package com.cgtechnodreams.salesassistindia.dms.distributorpurchase;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorPurchaseEntity;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchase;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class UpdateDistributorPurchaseDialog extends DialogFragment {
    @BindView(R.id.updateQuantity)
    TextInputEditText productQuantity;
    //    @BindView(R.id.value)
//    TextInputEditText value;
    @BindView(R.id.productName)
    TextView productName;
    @BindView(R.id.btnUpdate)
    public Button btnUpdate;
    @BindView(R.id.amount)
    TextInputEditText amount;
    @BindView(R.id.discountedAmount)
    TextInputEditText discountedAmount;
    private DistributorPurchase distributorPurchase;

    public  UpdateDistributorPurchaseDialog newInstance(DistributorPurchase distributorPurchase) {
        Bundle args = new Bundle();
        args.putSerializable("Purchase", distributorPurchase);
        this.setArguments(args);
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_distributor_purchase_update, container, false);
        if (getArguments() != null) {
            distributorPurchase = (DistributorPurchase) getArguments().getSerializable("Purchase");
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        productName.setText(distributorPurchase.getProductName());
        productQuantity.setText(distributorPurchase.getQuantity() + "");
        amount.setText(distributorPurchase.getAmount());
        discountedAmount.setText(distributorPurchase.getDiscountedAmount());
//        value.setText(closingStock.getValue() + "");
    }

    @OnClick(R.id.btnUpdate)
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (productQuantity.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please enter quantity", Toast.LENGTH_SHORT).show();
                } else {
                    distributorPurchase.setQuantity(Integer.parseInt(productQuantity.getText().toString()));
                    distributorPurchase.setAmount(amount.getText().toString());
                    distributorPurchase.setDiscountedAmount(discountedAmount.getText().toString());

                    updateOrder(distributorPurchase);
                }
        }
    }

    private void updateOrder(DistributorPurchase distributorPurchase) {
        DistributorPurchaseEntity entity = new DistributorPurchaseEntity(getActivity());
        entity.update(distributorPurchase);
        dismiss();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }
}
