package com.cgtechnodreams.salesassistindia.dms;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductEntity;
import com.cgtechnodreams.salesassistindia.dms.adapter.DistributorProductAvailabilityAdapter;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorProductAvailabilityActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    DistributorProductAvailabilityAdapter adapter;
    ArrayList<Product> productList = null;
    Distributor mDistributor = null;
    boolean isDamageStock = false;
    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_avaibility);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Product Stock");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDistributor = (Distributor) getIntent().getSerializableExtra("DistributorDetail");
        int brandId = getIntent().getIntExtra("BrandId", 0);
        isDamageStock = getIntent().getBooleanExtra("IsDamage",false);
        date = getIntent().getStringExtra("Date");

        if(isDamageStock){
            getSupportActionBar().setTitle("Add Damage Stock");
        }
        ProductEntity productEntity = new ProductEntity(DistributorProductAvailabilityActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) productEntity.findByBrand(brandId);


        try {
            productList = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Product>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(DistributorProductAvailabilityActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new DistributorProductAvailabilityAdapter(DistributorProductAvailabilityActivity.this, productList, mDistributor, isDamageStock,date);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }
}