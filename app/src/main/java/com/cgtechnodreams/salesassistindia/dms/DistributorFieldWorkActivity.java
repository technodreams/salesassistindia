package com.cgtechnodreams.salesassistindia.dms;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorFieldWorkActivity extends AppCompatActivity {
    Fragment fragment = null;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Distributor distributor = null;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            int id = item.getItemId();
            if (id == R.id.navigation_dashboard) {
                fragment = DistributorVisitFragment.newInstance(distributor);
            }
//            else if (id == R.id.navigation_received_order) {
//                fragment = DispatchReceivedFragment.newInstance(distributor);
//            }
//            else if(id == R.id.navigation_partial_dipatched){
//                fragment = PartialDispatchedFragment.newInstance(distributor);
//            }
//            else if (id == R.id.navigation_sales_report) {
//                fragment = DistributorSalesReportFragment.newInstance(stockist);
//            } else if (id == R.id.navigation_target) {
//                fragment = DistributorTargetFragment.newInstance(stockist);
//            }
            else if (id == R.id.navigation_info) {
                fragment = DistributorInfoFragment.newInstance(distributor);
            }
            if (fragment != null) {
                FragmentManager fragmentManager = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();
                    return true;
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
            return false;
        }

    };

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_field_work);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        distributor = (Distributor) getIntent().getSerializableExtra(AppConstant.DISTRIBUTOR_DETAIL);
        toolbar.setTitle(distributor.getName());
        Fragment fragment =   fragment = DistributorVisitFragment.newInstance(distributor);
        if (fragment != null) {
            FragmentManager fragmentManager = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();
            } else {
                Log.e(" FieldWork Activity", "Error in creating fragment");
            }
        }
        //getSupportActionBar().setTitle("Field Work");


        BottomNavigationView bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        //disableMenuShiftMode(bottomNavigation);
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
