package com.cgtechnodreams.salesassistindia.dms.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dms.DistributorFieldWorkActivity;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorAdapter extends RecyclerView.Adapter<DistributorAdapter.MyViewHolder> {
    Context mContext;
    PrefManager prefManager;
    ArrayList<Distributor> distributors = new ArrayList<>();

    public DistributorAdapter(Context context, ArrayList<Distributor> distributors) {
        this.mContext = context;
        this.distributors = distributors;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public DistributorAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_distributor, null);
        return new DistributorAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DistributorAdapter.MyViewHolder holder, int position) {
        Distributor distributor = distributors.get(position);
        holder.name.setText(distributor.getName());
        holder.address.setText(distributor.getAddress());
//        holder.businessUnit.setText("Business Unit: " + distributor.getBusinessUnitName());
//        holder.stockist.setText("Stockist: " + distributor.getStockistName());
    }

    @Override
    public int getItemCount() {
        return distributors.size();
    }

    public void setFilter(ArrayList<Distributor> newList) {

        distributors = new ArrayList<>();
        distributors.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
//        @BindView(R.id.businessUnit)
//        TextView businessUnit;
//        @BindView(R.id.stockist)
//        TextView stockist;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, DistributorFieldWorkActivity.class);
                    intent.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributors.get(getAdapterPosition()));
                    ((Activity)mContext).startActivity(intent);
                }
            });

        }

    }
}
