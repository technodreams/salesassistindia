package com.cgtechnodreams.salesassistindia.dms;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.dms.adapter.DistributorStockAdapter;
import com.cgtechnodreams.salesassistindia.dms.data.DistributorDataService;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.stock.model.Stock;
import com.cgtechnodreams.salesassistindia.stock.model.StockistStockResponse;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class DistributorStockActivity extends AppCompatActivity {
    PrefManager prefManager;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    DistributorStockAdapter adapter;

    String fromDate = null;
    String toDate = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_stock);
        prefManager = new PrefManager(this);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Distributor Stock");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        fromDate = getIntent().getStringExtra("From");
        toDate = getIntent().getStringExtra("To");
        Distributor distributor = (Distributor) getIntent().getSerializableExtra("DistributorDetail");
        getDistributorStock(distributor.getDistributorId(),fromDate,toDate);

    }

    private void getDistributorStock(int distributorId, String from, String to) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        DistributorDataService dataService = new DistributorDataService(this);
        dataService.getDistributorStock(prefManager.getAuthKey(),distributorId,from, to, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                StockistStockResponse baseResponse = (StockistStockResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(DistributorStockActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();

                } else { //success case
                    if(baseResponse.getDistributorStock().size()>0){
                        Toasty.success(DistributorStockActivity.this,  baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                        setData(baseResponse.getDistributorStock());
                    }else{
                        Toasty.info(DistributorStockActivity.this, "No Data Found!!", Toast.LENGTH_SHORT,true).show();
                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<Stock> data) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(DistributorStockActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new DistributorStockAdapter(DistributorStockActivity.this, data);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
