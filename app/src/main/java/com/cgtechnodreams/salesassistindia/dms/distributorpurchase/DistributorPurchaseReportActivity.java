package com.cgtechnodreams.salesassistindia.dms.distributorpurchase;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.data.DistributorPurchaseDataService;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchaseResponse;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.google.android.material.textfield.TextInputEditText;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class DistributorPurchaseReportActivity extends AppCompatActivity {


    @BindView(R.id.fromDate)
    TextInputEditText fromDate;
    @BindView(R.id.toDate)
    TextInputEditText toDate;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;


    ArrayList<Product> productList;
    //    String selectedDistributor = null;
//    int selectedDistributorId = 0;
    PrefManager prefManager  = null;
    DistributorPurchaseAdapter adapter;
    Distributor distributor = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_purchase_report);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        distributor = (Distributor) getIntent().getSerializableExtra(AppConstant.DISTRIBUTOR_DETAIL);
        getSupportActionBar().setTitle(distributor.getName() + " Purchase Report");
        getSupportActionBar().setSubtitle(distributor.getStockistName());

    }

    @OnClick({R.id.fromDate,R.id.toDate,R.id.btnGenerateReport})
    public void onButtonClicked(View view) {
        switch (view.getId()) {

            case R.id.btnGenerateReport:
                if(fromDate.getText().toString().equalsIgnoreCase("") ||
                        toDate.getText().toString().equalsIgnoreCase("")){
                    Toast.makeText(DistributorPurchaseReportActivity.this, "Please select the  date!!",Toast.LENGTH_SHORT).show();
                }else{
                    getData();
                }
                break;
            case R.id.fromDate:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        month = month + 1;
                        String tempDay = day + "";
                        String tempMonth = month + "";
                        if (day < 10) {
                            tempDay = "0" + day;
                        }
                        if (month < 10) {
                            tempMonth = "0" + month;
                        }
                        String date = year + "-" + (tempMonth) + "-" + tempDay;
                        fromDate.setText(date);
                    }
                });
                dpd.setVersion(DatePickerDialog.Version.VERSION_2);

                //@TODO Minimum date handle left
                dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                dpd.show(getSupportFragmentManager(), "DatePicker");
                break;
            case R.id.toDate:
                Calendar toCal = Calendar.getInstance();
                DatePickerDialog toDPD = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        month = month + 1;
                        String tempDay = day + "";
                        String tempMonth = month + "";
                        if (day < 10) {
                            tempDay = "0" + day;
                        }
                        if (month < 10) {
                            tempMonth = "0" + month;
                        }
                        String date = year + "-" + (tempMonth) + "-" + tempDay;
                        toDate.setText(date);
                    }
                });
                toDPD.setVersion(DatePickerDialog.Version.VERSION_2);

                //@TODO Minimum date handle left
                toDPD.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                toDPD.show(getSupportFragmentManager(), "DatePicker");
                break;

        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

//            case R.id.ic_syn:
//                Sync sync = new Sync(ProductClosingActivity.this);
//                sync.uploadOpeningStock();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void getData() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Downloading distributor purchase...");
        dialog.setCancelable(false);
        dialog.show();

        DistributorPurchaseDataService service = new DistributorPurchaseDataService(DistributorPurchaseReportActivity.this);
        service.getDistributorPurchase(distributor.getStockistId(),distributor.getDistributorId(),fromDate.getText().toString(),toDate.getText().toString(), new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                DistributorPurchaseResponse baseResponse = (DistributorPurchaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {
                    Toasty.error(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                } else {
                    Toasty.success(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    LinearLayoutManager layoutManager = new LinearLayoutManager(DistributorPurchaseReportActivity.this, LinearLayoutManager.VERTICAL, false);
                    adapter = new DistributorPurchaseAdapter(DistributorPurchaseReportActivity.this, baseResponse.getData(),true);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(adapter);
                }
            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

}