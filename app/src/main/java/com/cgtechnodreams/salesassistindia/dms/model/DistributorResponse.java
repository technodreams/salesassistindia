package com.cgtechnodreams.salesassistindia.dms.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class DistributorResponse extends BaseResponse {

    public ArrayList<Distributor> getDistributors() {
        return distributors;
    }

    public void setDistributors(ArrayList<Distributor> distributors) {
        this.distributors = distributors;
    }

    @JsonProperty("data")
    ArrayList<Distributor> distributors;
}
