package com.cgtechnodreams.salesassistindia.dms.productclosing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClosingStock implements Serializable {
    @JsonProperty("product_id")
    int productId;
    @JsonProperty("product_name")
    String productName;
    @JsonProperty("quantity")
    int quantity;
    @JsonProperty("month")
    String month;
    @JsonProperty("year")
    String year;
    @JsonProperty("stock_captured_date")
    String stockCapturedDate;
    @JsonProperty("distributor_id")
    int distributorId;
    @JsonProperty("distributor_name")
    String distributorName;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getStockCapturedDate() {
        return stockCapturedDate;
    }

    public void setStockCapturedDate(String stockCapturedDate) {
        this.stockCapturedDate = stockCapturedDate;
    }

    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

}
