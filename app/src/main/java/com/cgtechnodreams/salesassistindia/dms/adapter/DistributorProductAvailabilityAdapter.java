package com.cgtechnodreams.salesassistindia.dms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorStockEntity;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorStockModel;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class DistributorProductAvailabilityAdapter extends RecyclerView.Adapter<DistributorProductAvailabilityAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<Product> products = new ArrayList<>();
    Distributor mDistributor;
    boolean isDamageStock;
    String date;

    public DistributorProductAvailabilityAdapter(Context context, ArrayList<Product> products, Distributor distributor, boolean isDamageStock, String date) {
        this.mContext = context;
        this.products = products;
        this.mDistributor = distributor;
        this.isDamageStock = isDamageStock;
        prefManager = new PrefManager(mContext);
        this.date = date;
    }

    @Override
    public DistributorProductAvailabilityAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_availability, null);

        return new DistributorProductAvailabilityAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Product product = products.get(position);
        holder.name.setText(product.getProductName());
        holder.imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!holder.qty.getText().toString().equalsIgnoreCase("")) {
                    DistributorStockModel outletStock = new DistributorStockModel();
                    outletStock.setProductId(product.getProductId());
                    outletStock.setProductName(product.getProductName());
                    outletStock.setDistributorId(mDistributor.getDistributorId());
                    outletStock.setLatitude(Double.parseDouble(prefManager.getLatitude()));
                    outletStock.setLongitude(Double.parseDouble(prefManager.getLongitude()));
                    //outletStock.setDate(Utils.getCurrentDate());
                    outletStock.setDate(date);
                    outletStock.setQuantity(Integer.parseInt(holder.qty.getText().toString()));
                    if(!isDamageStock){
                        DistributorStockEntity distributorStockEntity = new DistributorStockEntity(mContext);
                        long count = distributorStockEntity.getCount(product.getProductId(), mDistributor.getDistributorId());
                        if(count>0){
                            distributorStockEntity.update(outletStock);
                        }else{
                            distributorStockEntity.insert(outletStock);
                        }
                    }else{
                        DistributorDamageStockEntity damageStockEntity = new DistributorDamageStockEntity(mContext);
                        long count = damageStockEntity.getCount(product.getProductId(), mDistributor.getDistributorId());
                        if(count>0){
                            damageStockEntity.update(outletStock);
                        }else{
                            damageStockEntity.insert(outletStock);
                        }
                    }

                    Toasty.success(mContext, "Product stock updated" + "", Toast.LENGTH_SHORT,true).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setFilter(ArrayList<Product> newList) {
        products = new ArrayList<>();
        products.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.productSubGroup)
        TextView name;
        @BindView(R.id.qty)
        AppCompatEditText qty;
        @BindView(R.id.imgSave)
        ImageView imgSave;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);



        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
