package com.cgtechnodreams.salesassistindia.dms;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorEntity;
import com.cgtechnodreams.salesassistindia.dms.adapter.DistributorSalesOrderOutletAdapter;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    DistributorSalesOrderOutletAdapter outLetAdapter = null;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ArrayList<Distributor> distributors = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet);
        ButterKnife.bind(this);
        toolbar.setTitle("Distributor");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DistributorEntity entity = new DistributorEntity(this);
            ObjectMapper objectMapper = new ObjectMapper();
            JSONArray array = (JSONArray) entity.find(null);
            try {
                objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                distributors = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Distributor>>(){});
            } catch (IOException e) {
                e.printStackTrace();
            }
            LinearLayoutManager promoterListManager = new LinearLayoutManager(DistributorActivity.this, LinearLayoutManager.VERTICAL, false);
            outLetAdapter = new DistributorSalesOrderOutletAdapter(DistributorActivity.this, distributors);
            recyclerView.setLayoutManager(promoterListManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(outLetAdapter);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.search_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<Distributor> newList = new ArrayList<>();
                for (Distributor distributor : distributors) {
                    String name = distributor.getName().toLowerCase();
                    String address = distributor.getAddress().toLowerCase();

                    if (name.contains(newText) || address.toLowerCase().contains(newText) ) {
                        newList.add(distributor);
                    }

                    outLetAdapter.setFilter(newList);

                }
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                          }
                                      }
        );
        return super.onCreateOptionsMenu(menu);
    }
}
