package com.cgtechnodreams.salesassistindia.dms;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.DistributorPurchaseActivity;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.DistributorPurchaseReportActivity;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.dms.productclosing.ClosingStockReportActivity;
import com.cgtechnodreams.salesassistindia.dms.productclosing.ProductClosingActivity;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DistributorVisitFragment extends Fragment {

    Distributor distributor = null;
    PrefManager prefManager = null;



    public DistributorVisitFragment() {
        // Required empty public constructor
    }
    public static DistributorVisitFragment newInstance(Distributor distributor) {
        DistributorVisitFragment fragment = new DistributorVisitFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.DISTRIBUTOR_DETAIL, distributor);
        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_distributor_visit, container, false);
        distributor = (Distributor) getArguments().getSerializable(AppConstant.DISTRIBUTOR_DETAIL);
        getActivity().setTitle(distributor.getName());
        prefManager = new PrefManager(getActivity());
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @OnClick({R.id.cardDistributorClosingStock, R.id.cardDistributorPurchase,R.id.cardDistributorPurchaseReport,R.id.cardDistributorStockReport})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.cardDistributorClosingStock:
                Intent closingStock = new Intent(getActivity(), ProductClosingActivity.class);
                closingStock.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributor);
                startActivity(closingStock);
                break;
            case R.id.cardDistributorPurchase:
                Intent distributorPurchase = new Intent(getActivity(), DistributorPurchaseActivity.class);
                distributorPurchase.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributor);
                startActivity(distributorPurchase);
                break;
            case R.id.cardDistributorPurchaseReport:
                Intent distributorPurchaseReport = new Intent(getActivity(), DistributorPurchaseReportActivity.class);
                distributorPurchaseReport.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributor);
                startActivity(distributorPurchaseReport);
                break;

            case R.id.cardDistributorStockReport:
                Intent distributorClosingReport = new Intent(getActivity(), ClosingStockReportActivity.class);
                distributorClosingReport.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributor);
                startActivity(distributorClosingReport);
                break;
//            case R.id.cardAddOrder:
//
//                Intent placeOrder = new Intent(getActivity(), DistributorSalesOrderActivity.class);
//                placeOrder.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributor);
//                startActivity(placeOrder);
//                break;
          //  case R.id.cardDailySales:
//                Intent dailySales = new Intent(getActivity(), DistributorDailySalesActivity.class);
//                dailySales.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributor);
//                startActivity(dailySales);
          //      break;
//            case R.id.cardOutletStock:
//                FragmentManager fm =(getActivity()).getSupportFragmentManager();
//                DistributorStockDialog dialogFragment = new DistributorStockDialog().newInstance(distributor);
//                dialogFragment.show(fm, "dialog");
//                break;
//            case R.id.cardPhoto:
//                Intent photo = new Intent(getActivity(), PhotoActivity.class);
//                photo.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributor);
//                startActivity(photo);
//                break;
//            case R.id.cardCollection:
//                Intent collection = new Intent(getActivity(), CollectionActivity.class);
//                collection.putExtra(AppConstant.DISTRIBUTOR_DETAIL, distributor);
//                startActivity(collection);

        }

    }

    private void showOrderDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_status);
        dialog.show();

        Button btnDismiss = (Button) dialog.findViewById(R.id.btnDismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        // if decline button is clicked, close the custom dialog
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                dialog.dismiss();
            }
        });
    }
//
//    private void showOutletStatusDialog() {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.setContentView(R.layout.dialog_status);
//        dialog.show();
//        RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radioStatus);
//
//        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                switch (checkedId) {
//                    case R.id.radioOpen:
//                        isOpen = true;
//                        break;
//                    case R.id.radioClosed:
//                        isOpen = false;
//                        break;
//                }
//            }
//        });
//
//
//        Button declineButton = (Button) dialog.findViewById(R.id.btnDismiss);
//        declineButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Close dialog
//                dialog.dismiss();
//            }
//        });
//
//        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
//        btnConfirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                StockistStatus stockistStatus = new StockistStatus();
//                if(isOpen)
//                    stockistStatus.setStatus("OPEN");
//                else
//                    stockistStatus.setStatus("CLOSE");
//                stockistStatus.setOutletId(mStockist.getDistributorId());
//                stockistStatus.setDate(Utils.getCurrentDate());
//                OutletStatusEntity outletStatusEntity = new OutletStatusEntity(getActivity());
//                long i = outletStatusEntity.insert(stockistStatus);
//                if(i>0){
//                  //  @TODO set user activity and do needful function
//                    //set user activity;
//                }
//
//                dialog.dismiss();
//            }
//        });
//    }
}
