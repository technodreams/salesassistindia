package com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DistributorPurchaseResponse extends BaseResponse {
    public ArrayList<DistributorPurchase> getData() {
        return data;
    }

    public void setData(ArrayList<DistributorPurchase> data) {
        this.data = data;
    }

    @JsonProperty("data")
    ArrayList<DistributorPurchase> data;
}
