package com.cgtechnodreams.salesassistindia.dms;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DistributorStockDialog extends DialogFragment {
    @BindView(R.id.viewStock)
    TextView viewStock;
    @BindView(R.id.enterDistributorStock)
    public TextView enterDistributorStock;
    @BindView(R.id.lastStock)
    TextView lastStock;
    Distributor mDistributor;
    public DistributorStockDialog newInstance(Distributor stockistDetail) {
        Bundle args = new Bundle();
        args.putSerializable("Detail", stockistDetail);
        this.setArguments(args);
        return this;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_distributor_stock_activities, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        if (getArguments() != null) {
            mDistributor = (Distributor) getArguments().getSerializable("Detail");
        }

    }

    @OnClick({R.id.viewStock,R.id.enterDistributorStock,R.id.lastStock, R.id.enterDamageDistributorStock,R.id.viewDamageStock})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.viewStock:
                Intent viewStock = new Intent(getActivity(), ViewDistributorStockActivity.class);
                viewStock.putExtra("DistributorDetail", mDistributor);
                startActivity(viewStock);
                dismiss();
                break;

            case R.id.viewDamageStock:

                Intent viewDamageStock = new Intent(getActivity(), ViewDistributorStockActivity.class);
                viewDamageStock.putExtra("DistributorDetail", mDistributor);
                viewDamageStock.putExtra("IsDamage",true);
                startActivity(viewDamageStock);
                dismiss();
                break;
            case R.id.enterDistributorStock:

                FragmentManager fm =(getActivity()).getSupportFragmentManager();
                SetStockDateDialog dialogFragment = new SetStockDateDialog().newInstance(mDistributor,"Sellable");
                dialogFragment.show(fm, "dialog");
                dismiss();
                break;
            case R.id.enterDamageDistributorStock:

                FragmentManager damageStock =(getActivity()).getSupportFragmentManager();
                SetStockDateDialog damageStockFragment = new SetStockDateDialog().newInstance(mDistributor,"Damage");
                damageStockFragment.show(damageStock, "dialog");
                dismiss();
                break;
            case R.id.lastStock:
                Intent lastStock = new Intent(getActivity(), DistributorStockActivity.class);
                lastStock.putExtra("DistributorDetail", mDistributor);
                startActivity(lastStock);
                dismiss();
                break;
        }

    }

}
