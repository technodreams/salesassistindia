package com.cgtechnodreams.salesassistindia.dms.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorSalesOrderOutletAdapter extends RecyclerView.Adapter<DistributorSalesOrderOutletAdapter.MyViewHolder> {
    Context mContext;
    PrefManager prefManager;
    ArrayList<Distributor> distributors = new ArrayList<>();

    public DistributorSalesOrderOutletAdapter(Context context, ArrayList<Distributor> distributors) {
        this.mContext = context;
        this.distributors = distributors;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public DistributorSalesOrderOutletAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_distributor, null);
        return new DistributorSalesOrderOutletAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DistributorSalesOrderOutletAdapter.MyViewHolder holder, int position) {
        Distributor distributor = distributors.get(position);
        holder.name.setText(distributor.getName());
        holder.address.setText(distributor.getAddress());
       // holder.businessUnit.setText("Business Unit: " + distributor.getBusinessUnitName());
    }

    @Override
    public int getItemCount() {
        return distributors.size();
    }

    public void setFilter(ArrayList<Distributor> newList) {
        distributors = new ArrayList<>();
        distributors.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
//        @BindView(R.id.businessUnit)
//        TextView businessUnit;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", distributors.get(getAdapterPosition()));
                    ((Activity) mContext).setResult(Activity.RESULT_OK,returnIntent);
                    ((Activity) mContext).finish();
                    //  finish();
                }
            });

        }

    }
}
