package com.cgtechnodreams.salesassistindia.dms.distributorpurchase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorPurchaseEntity;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchase;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorPurchaseAdapter extends RecyclerView.Adapter<DistributorPurchaseAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<DistributorPurchase> purchases = new ArrayList<>();
    String date;
    boolean isReport = false;

    public DistributorPurchaseAdapter(Context context, ArrayList<DistributorPurchase> purchases, boolean isReport) {
        this.mContext = context;
        this.purchases = purchases;
        this.isReport = isReport;
        prefManager = new PrefManager(mContext);

    }

    @Override
    public DistributorPurchaseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_distributor_purchase, null);
        return new DistributorPurchaseAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DistributorPurchaseAdapter.MyViewHolder holder, int position) {
        DistributorPurchase eachItem = purchases.get(position);
        holder.name.setText(eachItem.getProductName());
        holder.qty.setText(eachItem.getQuantity() + "");
        holder.amount.setText(eachItem.getAmount());
        holder.discountedAmount.setText(eachItem.getDiscountedAmount());
        holder.billingNumber.setText("Bill no:" + eachItem.getBillNumber());
        holder.date.setText("Date:" + eachItem.getDate());

        // holder.value.setText(eachItem.getValue() + "");
    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.productName)
        TextView name;
        @BindView(R.id.quantity)
        TextView qty;
        @BindView(R.id.amount)
        TextView amount;
        @BindView(R.id.discountedAmount)
        TextView discountedAmount;
        @BindView(R.id.billingNumber)
        TextView billingNumber;
        @BindView(R.id.date)
        TextView date;

        //        @BindView(R.id.value)
//        TextView value;
        @BindView(R.id.edit)
        ImageView edit;
        @BindView(R.id.delete)
        ImageView delete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if(isReport){
               edit.setVisibility(View.GONE);
               delete.setVisibility(View.GONE);
            }
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm =((AppCompatActivity) mContext).getSupportFragmentManager();
                    UpdateDistributorPurchaseDialog dialogFragment = new UpdateDistributorPurchaseDialog().newInstance(purchases.get(getAdapterPosition()));
                    dialogFragment.show(fm, "dialog");
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DistributorPurchaseEntity entity = new DistributorPurchaseEntity(mContext);
                    entity.deleteById(purchases.get(getAdapterPosition()).getId());
                    purchases.remove(purchases.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });
        }
    }
}
