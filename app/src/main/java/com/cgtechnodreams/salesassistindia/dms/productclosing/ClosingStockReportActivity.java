package com.cgtechnodreams.salesassistindia.dms.productclosing;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.data.DistributorPurchaseDataService;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStockResponse;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;

public class ClosingStockReportActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.input_month)
    MaterialAutoCompleteTextView closingMonth;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.input_year)
    MaterialAutoCompleteTextView spinnerYear;
    PrefManager prefManager = null;
    ProductOpeningAdapter adapter;
    String month = "";
    String year = "";
    @BindView(R.id.btnGenerateReport)
    Button btnGenerateReport;
    Distributor distributor = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closing_stock_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        distributor = (Distributor) getIntent().getSerializableExtra(AppConstant.DISTRIBUTOR_DETAIL);
        getSupportActionBar().setTitle(distributor.getName() + " Closing");
        getSupportActionBar().setSubtitle(distributor.getCity());

        String[] months = getResources().getStringArray(R.array.month);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.drop_down_layout, months);
        closingMonth.setAdapter(adapter);

        String[] years = getResources().getStringArray(R.array.year);
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<>(this, R.layout.drop_down_layout, years);
        spinnerYear.setAdapter(yearAdapter);


        closingMonth.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                month = months[position];

            }
        });

        spinnerYear.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                year = years[position];

            }
        });


        btnGenerateReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(month.equalsIgnoreCase("") || year.equalsIgnoreCase("")){
                    Toast.makeText(ClosingStockReportActivity.this, "Please select year and month", Toast.LENGTH_SHORT).show();
                }else{
                    getReport();
                }

            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

//            case R.id.ic_syn:
//                Sync sync = new Sync(ProductClosingActivity.this);
//                sync.uploadOpeningStock();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void getReport() {

        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Downloading distributor closing stock...");
        dialog.setCancelable(false);
        dialog.show();

        DistributorPurchaseDataService service = new DistributorPurchaseDataService(ClosingStockReportActivity.this);
        service.getClosingStock(month,year,distributor.getDistributorId(), new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                ClosingStockResponse baseResponse = (ClosingStockResponse) response;
                if (baseResponse.getStatusCode() != 200) {

                    Toasty.error(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                } else {
//                        if (baseResponse.getData().size() > 0) {
                            LinearLayoutManager layoutManager = new LinearLayoutManager(ClosingStockReportActivity.this, LinearLayoutManager.VERTICAL, false);
                            adapter = new ProductOpeningAdapter(ClosingStockReportActivity.this, baseResponse.getData(),true);
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(adapter);
                  //      }
                    }

                    Toasty.success(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });

    }


}