package com.cgtechnodreams.salesassistindia.dms;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorSalesOrder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogUpdateDistributorOrder extends DialogFragment {
    //   @BindView(R.id.updateQuantity)
    AppCompatEditText productQuantity;
    TextView productName;
    @BindView(R.id.btnUpdate)
    public Button btnUpdate;
    private DistributorSalesOrder salesOrder;
    public DialogUpdateDistributorOrder newInstance(DistributorSalesOrder fmcgSalesOrder) {
        Bundle args = new Bundle();
        args.putSerializable("SalesDetail", fmcgSalesOrder);
        this.setArguments(args);
        return this;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_order_update, container, false);
        productQuantity = (AppCompatEditText) view.findViewById(R.id.updateQuantity);
        if (getArguments() != null) {
            salesOrder = (DistributorSalesOrder) getArguments().getSerializable("SalesDetail");
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        productName = view.findViewById(R.id.productName);
        productName.setText(salesOrder.getProductName());
        productQuantity.setText(salesOrder.getQuantity() + "");
    }

    @OnClick(R.id.btnUpdate)
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (productQuantity.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please enter quantity", Toast.LENGTH_SHORT).show();
                } else{
                    salesOrder.setQuantity(Integer.parseInt(productQuantity.getText().toString()));
                    salesOrder.setTotalPrice(Double.parseDouble(productQuantity.getText().toString())*salesOrder.getUnitPrice());
                    updateOrder(salesOrder);
                }
        }
    }

    private void updateOrder(DistributorSalesOrder salesOrder) {
        DistributorSalesOrderEntity salesOrderEntity = new DistributorSalesOrderEntity(getActivity());
        Toast.makeText(getActivity(), salesOrder.getQuantity() + "", Toast.LENGTH_SHORT).show();
        salesOrderEntity.update(salesOrder);
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }

}