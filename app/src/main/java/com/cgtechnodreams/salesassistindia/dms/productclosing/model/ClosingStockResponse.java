package com.cgtechnodreams.salesassistindia.dms.productclosing.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClosingStockResponse extends BaseResponse {
    @JsonProperty("data")
    public ArrayList<ClosingStock> getData() {
        return data;
    }
    public void setData(ArrayList<ClosingStock> data) {
        this.data = data;
    }

    ArrayList<ClosingStock> data;
}
