package com.cgtechnodreams.salesassistindia.dms.productclosing;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductClosingEntity;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStock;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateStockDialog extends DialogFragment {
    @BindView(R.id.updateQuantity)
    AppCompatEditText productQuantity;
//    @BindView(R.id.value)
//    TextInputEditText value;
    @BindView(R.id.productName)
    TextView productName;
    @BindView(R.id.month)
    TextView month;
    @BindView(R.id.btnUpdate)
    public Button btnUpdate;
    private ClosingStock closingStock;

    public UpdateStockDialog newInstance(ClosingStock closingStock) {
        Bundle args = new Bundle();
        args.putSerializable("Stock", closingStock);
        this.setArguments(args);
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_distributor_stock_update, container, false);
        if (getArguments() != null) {
            closingStock = (ClosingStock) getArguments().getSerializable("Stock");
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        productName.setText(closingStock.getProductName());
        productQuantity.setText( ""+closingStock.getQuantity());
        month.setText(closingStock.getMonth() +" - " + closingStock.getYear());
    }

    @OnClick(R.id.btnUpdate)
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (productQuantity.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please enter quantity", Toast.LENGTH_SHORT).show();
                } else {
                    closingStock.setQuantity(Integer.parseInt(productQuantity.getText().toString()));

                    updateOrder(closingStock);
                }
        }
    }

    private void updateOrder(ClosingStock distributorStock) {
            ProductClosingEntity distributorStockEntity = new ProductClosingEntity(getActivity());
            distributorStockEntity.update(distributorStock);
            dismiss();
        }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }
}
