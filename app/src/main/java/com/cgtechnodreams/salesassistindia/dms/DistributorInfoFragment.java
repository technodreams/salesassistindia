package com.cgtechnodreams.salesassistindia.dms;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;

/**
 * A simple {@link Fragment} subclass.
 */
public class DistributorInfoFragment extends Fragment  implements OnMapReadyCallback {
    private static final String TAG = DistributorInfoFragment.class.getSimpleName();
    private GoogleMap mMap;
    Distributor mDistributor = null;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.tvStockistName)
    TextView tvStockistName;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.tvContactPerson)
    TextView tvContactPerson;
    @BindView(R.id.tvLandLineNumber)
    TextView tvLandLineNumber;
    @BindView(R.id.tvMobileNumber)
    TextView tvMobileNumber;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.tvPanNumber)
    TextView tvPanNumber;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.btnEdit)
    Button btnEdit;

    public DistributorInfoFragment() {
        // Required empty public constructor
    }
    public static DistributorInfoFragment newInstance(Distributor distributor) {
        DistributorInfoFragment fragment = new DistributorInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.DISTRIBUTOR_DETAIL, distributor);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_outlet_info, container, false);
        ButterKnife.bind(this,rootView);
        mDistributor = (Distributor) getArguments().getSerializable(AppConstant.DISTRIBUTOR_DETAIL);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setDetail(mDistributor);
        return rootView;
    }

    private void setDetail(Distributor distributor) {
        if(distributor.getAddress()!= null)
            tvAddress.setText(distributor.getAddress());
        else
            tvAddress.setText("");
        tvStockistName.setText(distributor.getName());
        tvContactPerson.setText(distributor.getContactPerson());
        tvMobileNumber.setText(distributor.getMobileNumber());
        if(distributor.getEmail()!= null)
            tvEmail.setText(distributor.getEmail());
        else
            tvEmail.setText("");
        if(distributor.getLandLine()!= null)
            tvLandLineNumber.setText(distributor.getLandLine());
        else
            tvLandLineNumber.setText("");
        if(distributor.getPanNumber()!= null)
            tvPanNumber.setText(distributor.getPanNumber());
        else
            tvPanNumber.setText("");
    }

    @OnClick({R.id.btnEdit})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnEdit:
                Intent intent = new Intent(getActivity(), NewDistributorActivity.class);
                intent.putExtra(AppConstant.DISTRIBUTOR_DETAIL, mDistributor);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();

        CameraPosition camera = new CameraPosition.Builder()
                .target(new LatLng(Double.parseDouble(mDistributor.getLatitude()),Double.parseDouble(mDistributor.getLongitude())))
                .zoom(14)  //limite ->21
                .bearing(120) // 0 - 365
                .tilt(45) // limite ->90
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));

        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(mDistributor.getLatitude()),
                Double.parseDouble(mDistributor.getLongitude())))
                .title("Distributor Location"));
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                        (FrameLayout) getActivity().findViewById(R.id.map), false);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());

                return infoWindow;
            }
        });


    }
}
