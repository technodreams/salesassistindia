package com.cgtechnodreams.salesassistindia.dms;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorRequestEntity;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.dms.model.NewDistributorRequest;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MinLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class NewDistributorActivity extends AppCompatActivity implements OnMapReadyCallback {


    private static final String TAG = NewDistributorActivity.class.getSimpleName();
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.etOutletName)
    EditText etOutletName;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.etAddress)
    EditText etAddress;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.etContactPerson)
    EditText etContactPerson;
    @BindView(R.id.etLandLineNumber)
    EditText etLandLineNumber;
    @NotEmpty(messageId = R.string.field_required)
    @MinLength(value = 10, messageId = R.string.validation_valid_mobile_number, order = 2)
    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.etPanNumber)
    EditText etPanNumber;

    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    PrefManager prefManager;
    Distributor distributor = null;
    @BindView(R.id.btnSave)
    Button btnSave;
    double mLatitude = 0.0;
    double mLongitude = 0.0;
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;
    private LatLng mDefaultLocation = null;
    private boolean mLocationPermissionGranted;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_outlet);
        ButterKnife.bind(this);
        distributor = (Distributor) getIntent().getSerializableExtra(AppConstant.DISTRIBUTOR_DETAIL);
        if (distributor != null) {
            setOutletDetail(distributor);
        }

        prefManager = new PrefManager(this);
        mDefaultLocation = new LatLng(Double.parseDouble(prefManager.getLatitude()), Double.parseDouble(prefManager.getLongitude()));
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        toolbar.setTitle("Add Distributor");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setOutletDetail(Distributor distributor) {
        etAddress.setText(distributor.getAddress());
        etOutletName.setText(distributor.getName());
        if(distributor.getMobileNumber()!=null)
            etMobileNumber.setText(distributor.getMobileNumber());
        else{
            etMobileNumber.setText("");
        }
        etContactPerson.setText(distributor.getContactPerson());
        if(distributor.getPanNumber()!=null)
            etPanNumber.setText(distributor.getPanNumber());
        if(distributor.getEmail()!=null)
            etEmail.setText(distributor.getEmail());
        else{
            etEmail.setText("");
        }
        if(distributor.getLandLine()!=null)
            etLandLineNumber.setText(distributor.getLandLine());
        else{
            etLandLineNumber.setText("");
        }

    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @OnClick({R.id.btnSave})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {

                    saveOutletRequest(distributor);
                }
                break;

        }

    }

    private void saveOutletRequest(Distributor distributor) {

        NewDistributorRequest outlet = new NewDistributorRequest();
        if (distributor == null) {
            //new Stockist request
            outlet.setOutletId(0);

        } else {
            outlet.setOutletId(distributor.getDistributorId());
        }
        outlet.setName(etOutletName.getText().toString());
        outlet.setAddress(etAddress.getText().toString());
        outlet.setPanNumber(etPanNumber.getText().toString());
        outlet.setContactPerson(etContactPerson.getText().toString());
        outlet.setLandLine(etLandLineNumber.getText().toString());
        outlet.setMobileNumber(etMobileNumber.getText().toString());
        outlet.setEmail(etMobileNumber.getText().toString());
        outlet.setLatitude(mLatitude + "");
        outlet.setLongitude(mLongitude + "");


        ObjectMapper Obj = new ObjectMapper();

        JSONObject jsonObject = null;
        try {

            String jsonStr = Obj.writeValueAsString(outlet);
            jsonObject = new JSONObject(jsonStr);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        DistributorRequestEntity distributorRequestEntity = new DistributorRequestEntity(this);
        long i = distributorRequestEntity.insert(jsonObject);
        if(i>0){
            Toasty.success(this, "New Distributor Create Request Saved", Toast.LENGTH_SHORT,true).show();
            finish();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Manipulates the map when it's available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.clear();
        showCurrentPlace();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title("Stockist Location");
                mLatitude = latLng.latitude;
                mLongitude = latLng.longitude;

                // Clears the previously touched position
                mMap.clear();

                // Animating to the touched position
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                // Placing a marker on the touched position
                mMap.addMarker(markerOptions);
            }
        });
        // Use a custom info window adapter to handle multiple lines of text in the
        // info window contents.
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                        (FrameLayout) findViewById(R.id.map), false);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());

                return infoWindow;
            }
        });

        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
    }

    private void showCurrentPlace() {
        if (distributor != null) {
            mLongitude = Double.parseDouble(distributor.getLongitude());
            mLatitude = Double.parseDouble(distributor.getLatitude());
        } else {
            if (mLastKnownLocation != null) {
                mLatitude = mLastKnownLocation.getLatitude();
                mLongitude = mLastKnownLocation.getLongitude();

            } else {
                mLatitude = Double.parseDouble(prefManager.getLatitude());
                mLongitude = Double.parseDouble(prefManager.getLongitude());

            }
        }

        mMap.addMarker(new MarkerOptions().position(new LatLng(mLatitude,
                mLongitude))
                .title("Stockist Location"));

    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            prefManager.setLatitude(mLastKnownLocation.getLatitude() + "");
                            prefManager.setLatitude(mLastKnownLocation.getLongitude() + "");
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }
}