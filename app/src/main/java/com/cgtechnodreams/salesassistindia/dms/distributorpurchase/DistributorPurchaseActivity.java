package com.cgtechnodreams.salesassistindia.dms.distributorpurchase;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorPurchaseEntity;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.data.DistributorPurchaseDataService;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchase;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.product.SearchProductActivity;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.textfield.TextInputEditText;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class DistributorPurchaseActivity extends AppCompatActivity implements DialogInterface.OnDismissListener {
    private static final int REQUEST_CODE_SEARCH_PRODUCT = 100;
    Product selectedProduct = null;

    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.input_product)
    TextInputEditText productName;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.qty)
    TextInputEditText qty;
    @BindView(R.id.amount)
    TextInputEditText amount;
    @BindView(R.id.discountedAmount)
    TextInputEditText discountedAmount;
    @BindView(R.id.billingNumber)
    TextInputEditText billNumber;
    @BindView(R.id.btnDate)
    TextInputEditText btnDate;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;


    ArrayList<Product> productList;
//    String selectedDistributor = null;
//    int selectedDistributorId = 0;
    PrefManager prefManager  = null;
    DistributorPurchaseAdapter adapter;

    ArrayList<Distributor> distributorList = new ArrayList<>();
    Distributor distributor = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_purchase);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        distributor = (Distributor) getIntent().getSerializableExtra(AppConstant.DISTRIBUTOR_DETAIL);
        getSupportActionBar().setTitle(distributor.getName() + " Purchase Entry");
        getSupportActionBar().setSubtitle(distributor.getCity());

        ArrayList<DistributorPurchase> purchaseDistributor= getDistributorPurchase(distributor.getDistributorId());
        if(purchaseDistributor.size()>0){
            setProductOpeningDataOutletWise(purchaseDistributor);
        }

    }

    @OnClick({R.id.input_product,R.id.btnDate,R.id.btnSave,R.id.uploadDistributorPurchase})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.input_product:
                Intent intent = new Intent(DistributorPurchaseActivity.this, SearchProductActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SEARCH_PRODUCT);
                break;
            case R.id.btnSave:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    DistributorPurchase distributorPurchase = new DistributorPurchase();
                    distributorPurchase.setProductId(selectedProduct.getProductId());
                    distributorPurchase.setProductName(selectedProduct.getProductName());
                    distributorPurchase.setQuantity(Integer.parseInt(qty.getText().toString()));
                    distributorPurchase.setAmount(amount.getText().toString());
                    distributorPurchase.setDiscountedAmount(discountedAmount.getText().toString());
                    distributorPurchase.setBillNumber(billNumber.getText().toString());
                    distributorPurchase.setDistributorName(distributor.getName());
                    distributorPurchase.setDistributorId(distributor.getDistributorId());
                    distributorPurchase.setDate(btnDate.getText().toString());
                    DistributorPurchaseEntity entity = new DistributorPurchaseEntity(DistributorPurchaseActivity.this);
                    JSONArray jsonArray = (JSONArray) entity.findByProductAndOutletId(distributor.getDistributorId(), selectedProduct.getProductId(),btnDate.getText().toString());
                    if(jsonArray.length()>0){
                        updateConfirmationDialog(distributorPurchase, entity);
                    }else{
                        entity.insert(distributorPurchase);
                        clearData();
                    }
                }
                break;
            case R.id.btnDate:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        month = month + 1;
                        String tempDay = day + "";
                        String tempMonth = month + "";
                        if (day < 10) {
                            tempDay = "0" + day;
                        }
                        if (month < 10) {
                            tempMonth = "0" + month;
                        }
                        String date = year + "-" + (tempMonth) + "-" + tempDay;
                        btnDate.setText(date);
                    }
                });
                dpd.setVersion(DatePickerDialog.Version.VERSION_2);

                //@TODO Minimum date handle left
                dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                dpd.show(getSupportFragmentManager(), "DatePicker");
                break;
            case R.id.uploadDistributorPurchase:
                ArrayList<DistributorPurchase> distributorPurchases = getDistributorPurchase(0); // 0 means find all
                if(distributorPurchases.size()>0){
                    uploadDistributorPurchase(distributorPurchases);
                }else{
                    Toast.makeText(DistributorPurchaseActivity.this,"No stocks left to upload!!",Toast.LENGTH_SHORT).show();
                }
              //  uploadDistributorPurchase(distributorPurchases);
                break;

        }

    }

    private void updateConfirmationDialog(DistributorPurchase purchase, DistributorPurchaseEntity entity) {
        new AlertDialog.Builder(this)
                .setTitle(purchase.getProductName())
                .setMessage(purchase.getDistributorName() + " stock has been already taken for product " +
                        purchase.getProductName() + ". Current stock is " +
                        purchase.getQuantity() + " Do you want to update the purchase"  )
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        entity.update(purchase);
                        clearData();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void uploadDistributorPurchase(ArrayList<DistributorPurchase> distributorPurchases) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Uploading distributor purchase...");
        dialog.setCancelable(false);
        dialog.show();

        DistributorPurchaseDataService service = new DistributorPurchaseDataService(DistributorPurchaseActivity.this);
        service.uploadDistributorPurchase(distributorPurchases, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {
                    Toasty.error(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                } else {
                    Toasty.success(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    DistributorPurchaseEntity entity = new DistributorPurchaseEntity(DistributorPurchaseActivity.this);
                    entity.delete(null);
                    finish();
                }
            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void clearData() {
        productName.setText("");
        selectedProduct = null;
        qty.setText("");
        amount.setText("");
        billNumber.setText("");
        discountedAmount.setText("");
//        value.setText("");
        ArrayList<DistributorPurchase> purchaseDistributor= getDistributorPurchase(distributor.getDistributorId());
        setProductOpeningDataOutletWise(purchaseDistributor);
    }

    private void setProductOpeningDataOutletWise( ArrayList<DistributorPurchase> purchaseDistributor) {
        if(purchaseDistributor.size() > 0){
            LinearLayoutManager favouriteListManager = new LinearLayoutManager(DistributorPurchaseActivity.this, LinearLayoutManager.VERTICAL, false);
            adapter = new DistributorPurchaseAdapter(DistributorPurchaseActivity.this, purchaseDistributor,false);
            recyclerView.setLayoutManager(favouriteListManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }else{
            adapter.notifyDataSetChanged();
        }
    }

    private ArrayList<DistributorPurchase> getDistributorPurchase(int selectedOutletId) {

        ArrayList<DistributorPurchase> distributorPurchases = new ArrayList<>();
        DistributorPurchaseEntity entity = new DistributorPurchaseEntity(DistributorPurchaseActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray purchaseArray = null;
        if(selectedOutletId == 0){
            purchaseArray = (JSONArray) entity.find(null);
        }else{
            purchaseArray = (JSONArray) entity.findByOutletId(selectedOutletId);
        }
        try {
            distributorPurchases = objectMapper.readValue(String.valueOf(purchaseArray), new TypeReference<ArrayList<DistributorPurchase>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  distributorPurchases;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

//            case R.id.ic_syn:
//                Sync sync = new Sync(ProductClosingActivity.this);
//                sync.uploadOpeningStock();
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SEARCH_PRODUCT) {
            if (resultCode == Activity.RESULT_OK) {
                selectedProduct = (Product) data.getSerializableExtra("Result");
                productName.setText(selectedProduct.getProductName());
            }
            if (resultCode == Activity.RESULT_CANCELED) {
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        ArrayList<DistributorPurchase> distributorPurchase= getDistributorPurchase(distributor.getDistributorId());
        setProductOpeningDataOutletWise(distributorPurchase);
    }
}