package com.cgtechnodreams.salesassistindia.dms.productclosing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductClosingEntity;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.dms.productclosing.data.ProductClosingDataService;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStock;
import com.cgtechnodreams.salesassistindia.product.SearchProductActivity;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class ProductClosingActivity extends AppCompatActivity implements DialogInterface.OnDismissListener {

    private static final int REQUEST_CODE_SEARCH_PRODUCT = 100;
    Product selectedProduct = null;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.input_product_name)
    TextInputEditText productName;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.qty)
    TextInputEditText qty;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.input_month)
    MaterialAutoCompleteTextView closingMonth;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.input_fiscal_year)
    MaterialAutoCompleteTextView spinnerYear;
    ArrayList<Product> productList;
    PrefManager prefManager  = null;
    ProductOpeningAdapter adapter;
    String month ="";
    String year= "";
    ArrayList<Distributor> distributorList = new ArrayList<>();
    Distributor distributor = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_closing);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        prefManager = new PrefManager(this);
        distributor = (Distributor) getIntent().getSerializableExtra(AppConstant.DISTRIBUTOR_DETAIL);
        getSupportActionBar().setTitle(distributor.getName() + " Closing");
        getSupportActionBar().setSubtitle(distributor.getCity());

        ArrayList<ClosingStock> closingStocks= getClosingList(distributor.getDistributorId());
        if(closingStocks.size()>0){
            setProductOpeningDataOutletWise(closingStocks);
        }
        String[] months = getResources().getStringArray(R.array.month);
        ArrayAdapter<String> adapter =  new ArrayAdapter<>(this, R.layout.drop_down_layout, months);
        closingMonth.setAdapter(adapter);

        String[] years = getResources().getStringArray(R.array.year);
        ArrayAdapter<String> yearAdapter =  new ArrayAdapter<>(this, R.layout.drop_down_layout, years);
        spinnerYear.setAdapter(yearAdapter);


        closingMonth.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                month = months[position];
                // Toast.makeText(getApplicationContext(),month[position],Toast.LENGTH_SHORT).show();
               // _toMonth = position+1;
            }
        });

        spinnerYear.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                year = years[position];

            }
        });
    }
    @OnClick({R.id.input_product_name,R.id.btnSave,R.id.uploadClosingStock})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.input_product_name:
                Intent intent = new Intent(ProductClosingActivity.this, SearchProductActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SEARCH_PRODUCT);
                break;
            case R.id.btnSave:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    ClosingStock closingStock = new ClosingStock();
                    closingStock.setProductId(selectedProduct.getProductId());
                    closingStock.setProductName(selectedProduct.getProductName());
                    closingStock.setQuantity(Integer.parseInt(qty.getText().toString()));
                    closingStock.setMonth(month);
                    closingStock.setYear(year);
                    closingStock.setDistributorName(distributor.getName());
                    closingStock.setDistributorId(distributor.getDistributorId());
                    closingStock.setStockCapturedDate(Utils.getCurrentDate());
                    closingStock.setDistributorId(distributor.getDistributorId());
                    closingStock.setDistributorName(distributor.getDistrictName());
                    ProductClosingEntity entity = new ProductClosingEntity(ProductClosingActivity.this);
                    JSONArray jsonArray = (JSONArray) entity.findByProductAndOutletId(distributor.getDistributorId(), selectedProduct.getProductId());
                   if(jsonArray.length()>0){
                       updateConfirmationDialog(closingStock, entity);

                   }else{
                       entity.insert(closingStock);
                       clearData();
                   }
                }
                break;
            case R.id.uploadClosingStock:
                ArrayList<ClosingStock> closingStocks = getClosingList(0); // 0 means find all
                if(closingStocks.size()>0){
                    uploadClosingStock(closingStocks);
                }else{
                    Toast.makeText(ProductClosingActivity.this,"No stocks left to upload!!",Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    private void updateConfirmationDialog(ClosingStock stock, ProductClosingEntity entity) {
        new AlertDialog.Builder(this)
                .setTitle(stock.getProductName())
                .setMessage(stock.getDistributorName() + " stock has been already taken for product " +
                        stock.getProductName() + ". Current stock is " +
                        stock.getQuantity() + " Do you want to update the product stock?"  )
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        entity.update(stock);
                        clearData();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void uploadClosingStock(ArrayList<ClosingStock> closingStocks) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Uploading closing stock...");
        dialog.setCancelable(false);
        dialog.show();

        ProductClosingDataService service = new ProductClosingDataService(ProductClosingActivity.this);
        service.uploadClosingStock(closingStocks, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {
                        Toasty.error(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                } else {
                    Toasty.success(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    ProductClosingEntity productOpeningEntity = new ProductClosingEntity(ProductClosingActivity.this);
                    productOpeningEntity.delete(null);
                    finish();
                }
            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void clearData() {
        productName.setText("");
        selectedProduct = null;
        qty.setText("");
//        value.setText("");
        ArrayList<ClosingStock> closingStocks= getClosingList(distributor.getDistributorId());
        setProductOpeningDataOutletWise(closingStocks);
    }

    private void setProductOpeningDataOutletWise( ArrayList<ClosingStock> closingStocks) {

        if(closingStocks.size() > 0){
            LinearLayoutManager favouriteListManager = new LinearLayoutManager(ProductClosingActivity.this, LinearLayoutManager.VERTICAL, false);
            adapter = new ProductOpeningAdapter(ProductClosingActivity.this, closingStocks,false);
            recyclerView.setLayoutManager(favouriteListManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }else{
            //@TODO show dowloand button to pull.
        }
    }

    private ArrayList<ClosingStock> getClosingList(int selectedOutletId) {

        ArrayList<ClosingStock> openingStocks = new ArrayList<>();
        ProductClosingEntity productClosingEntity = new ProductClosingEntity(ProductClosingActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray stockArray = null;
        if(selectedOutletId == 0){
            stockArray = (JSONArray) productClosingEntity.find(null);
        }else{
            stockArray = (JSONArray) productClosingEntity.findByOutletId(selectedOutletId);
        }
        try {
            openingStocks = objectMapper.readValue(String.valueOf(stockArray), new TypeReference<ArrayList<ClosingStock>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  openingStocks;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

//            case R.id.ic_syn:
//                Sync sync = new Sync(ProductClosingActivity.this);
//                sync.uploadOpeningStock();
            default:
                return super.onOptionsItemSelected(item);
        }

    }
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == REQUEST_CODE_SEARCH_PRODUCT) {
        if (resultCode == Activity.RESULT_OK) {
            selectedProduct = (Product) data.getSerializableExtra("Result");
            productName.setText(selectedProduct.getProductName());
        }
        if (resultCode == Activity.RESULT_CANCELED) {
        }
    }
}

    @Override
    public void onDismiss(DialogInterface dialog) {
        ArrayList<ClosingStock> closingStock= getClosingList(distributor.getDistributorId());
        setProductOpeningDataOutletWise(closingStock);
    }
}