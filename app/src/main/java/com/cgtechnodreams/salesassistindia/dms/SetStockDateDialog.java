package com.cgtechnodreams.salesassistindia.dms;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.product.BrandActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;


public class SetStockDateDialog extends DialogFragment {
    String stockType;
    Distributor mDistributor;
    @NotEmpty(messageId = R.string.error_field_required)
    @BindView(R.id.stockDate)
    EditText stockDate;

    public SetStockDateDialog newInstance(Distributor distributorDetail, String stockType) {
        Bundle args = new Bundle();
        args.putSerializable("Detail", distributorDetail);
        args.putString("StockType", stockType);
        this.setArguments(args);
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_set_stock_date, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            mDistributor = (Distributor) getArguments().getSerializable("Detail");
            stockType = getArguments().getString("StockType");

        }

    }

    @OnClick({R.id.stockDate, R.id.btnNext})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.stockDate:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        month = month + 1;
                        String tempDay = day + "";
                        String tempMonth = month + "";
                        if (day < 10) {
                            tempDay = "0" + day;
                        }
                        if (month < 10) {
                            tempMonth = "0" + month;
                        }
                        String date = year + "-" + (tempMonth) + "-" + tempDay;
                        stockDate.setText(date);
                    }
                });
                dpd.setVersion(DatePickerDialog.Version.VERSION_2);

                //@TODO Minimum date handle left
                dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                dpd.show(getActivity().getSupportFragmentManager(), "DatePicker");

                break;

            case R.id.btnNext:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(getActivity(), true))) {
                    Intent viewStock = new Intent(getActivity(), BrandActivity.class);
                    viewStock.putExtra("DistributorDetail", mDistributor);
                    viewStock.putExtra("Date",stockDate.getText().toString());
                    if (stockType.equalsIgnoreCase("Damage")) {
                        viewStock.putExtra("IsDamage", true);
                    }
                    startActivity(viewStock);
                    dismiss();
                }


                break;
        }
    }
}
