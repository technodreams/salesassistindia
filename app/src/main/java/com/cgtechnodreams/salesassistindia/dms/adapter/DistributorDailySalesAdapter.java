package com.cgtechnodreams.salesassistindia.dms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorDailySalesEntity;
import com.cgtechnodreams.salesassistindia.dms.EditDistributorDailySalesDialog;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorDailySales;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DistributorDailySalesAdapter extends RecyclerView.Adapter<DistributorDailySalesAdapter.MyViewHolder>{

    Context mContext;
    PrefManager prefManager;
    ArrayList<DistributorDailySales> dailySales = new ArrayList<>();

    public DistributorDailySalesAdapter(Context context, ArrayList<DistributorDailySales> dailySales) {
        this.mContext = context;
        this.dailySales = dailySales;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public DistributorDailySalesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_distributor_daily_sales, null);

        return new DistributorDailySalesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DistributorDailySalesAdapter.MyViewHolder holder, int position) {
        DistributorDailySales eachSales = dailySales.get(position);
        holder.productName.setText(eachSales.getProductName());
        holder.quantity.setText( "Qty: " + eachSales.getQuantity() + "");

    }

    @Override
    public int getItemCount() {
        return dailySales.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.edit)
        ImageView imgEdit;
        @BindView(R.id.delete)
        ImageView imgDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm =((AppCompatActivity) mContext).getSupportFragmentManager();
                    EditDistributorDailySalesDialog dialogFragment = new EditDistributorDailySalesDialog().newInstance(dailySales.get(getAdapterPosition()));
                    dialogFragment.show(fm, "dialog");

                }
            });
            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DistributorDailySalesEntity stockistDailySalesEntity = new DistributorDailySalesEntity(mContext);
                    stockistDailySalesEntity.deleteById(dailySales.get(getAdapterPosition()).getId());
                    dailySales.remove(dailySales.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });


        }
    }
}
