package com.cgtechnodreams.salesassistindia.dms;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorStockEntity;
import com.cgtechnodreams.salesassistindia.dms.adapter.ViewDistributorStockAdapter;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorStockModel;
import com.cgtechnodreams.salesassistindia.salesorder.IMethodCaller;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewDistributorStockActivity extends AppCompatActivity implements DialogInterface.OnDismissListener, IMethodCaller {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ViewDistributorStockAdapter adapter;
    PrefManager prefManager;
    ArrayList<DistributorStockModel> distributorStock;
    Distributor distributor;
    boolean isDamage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_distributor_stock);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Captured Stock");
        distributor = (Distributor) getIntent().getSerializableExtra("DistributorDetail");
        isDamage = getIntent().getBooleanExtra("IsDamage",false);

        getStock(isDamage);
        setData();

    }

    private void setData() {
        LinearLayoutManager salesOrderManager = new LinearLayoutManager(ViewDistributorStockActivity.this, RecyclerView.VERTICAL, false);
        adapter = new ViewDistributorStockAdapter(ViewDistributorStockActivity.this, distributorStock, isDamage);
        recyclerView.setLayoutManager(salesOrderManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void getStock(boolean isDamage) {
        if(!isDamage){
            DistributorStockEntity distributorStockEntity = new DistributorStockEntity(this);
            JSONArray jsonArray = (JSONArray) distributorStockEntity.findById(distributor.getDistributorId());

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                distributorStock = objectMapper.readValue(String.valueOf(jsonArray), new TypeReference<ArrayList<DistributorStockModel>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            DistributorDamageStockEntity distributorDamageStockEntity = new DistributorDamageStockEntity(this);
            JSONArray jsonArray = (JSONArray) distributorDamageStockEntity.findById(distributor.getDistributorId());

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                distributorStock = objectMapper.readValue(String.valueOf(jsonArray), new TypeReference<ArrayList<DistributorStockModel>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        getStock(isDamage);
        setData();

    }

    @Override
    public void onDeleteOrder() {

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
