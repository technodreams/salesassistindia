package com.cgtechnodreams.salesassistindia.dms;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorStockEntity;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorStockModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateDistributorStockDialog extends DialogFragment {
    AppCompatEditText productQuantity;
    TextView productName;
    @BindView(R.id.btnUpdate)
    public Button btnUpdate;
    private DistributorStockModel distributorStock;
    boolean isDamage;

    public UpdateDistributorStockDialog newInstance(DistributorStockModel distributorStock, boolean isDamage) {
        Bundle args = new Bundle();
        args.putSerializable("Stock", distributorStock);
        args.putBoolean("IsDamage",isDamage);
        this.setArguments(args);
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_distributor_stock_update, container, false);
        productQuantity = (AppCompatEditText) view.findViewById(R.id.updateQuantity);
        if (getArguments() != null) {
            distributorStock = (DistributorStockModel) getArguments().getSerializable("Stock");
        }
        isDamage = getArguments().getBoolean("IsDamage");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        productName = view.findViewById(R.id.productName);
        productName.setText(distributorStock.getProductName());
        productQuantity.setText(distributorStock.getQuantity() + "");
    }

    @OnClick(R.id.btnUpdate)
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (productQuantity.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please enter quantity", Toast.LENGTH_SHORT).show();
                } else {
                    distributorStock.setQuantity(Integer.parseInt(productQuantity.getText().toString()));
                    updateOrder(distributorStock,isDamage);
                }
        }
    }

    private void updateOrder(DistributorStockModel distributorStock, boolean isDamage) {
        if(!isDamage){
            DistributorStockEntity distributorStockEntity = new DistributorStockEntity(getActivity());
            distributorStockEntity.update(distributorStock);
            dismiss();
        }else{
            DistributorDamageStockEntity distributorDamageStockEntity = new DistributorDamageStockEntity(getActivity());
            distributorDamageStockEntity.update(distributorStock);
            dismiss();
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }
}
