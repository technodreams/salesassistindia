package com.cgtechnodreams.salesassistindia.dms.productclosing.data;

import android.content.Context;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStock;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductClosingDataService {
    private Context mContext;
    PrefManager prefManager;
    public ProductClosingDataService(Context mContext) {
        this.mContext = mContext;
        prefManager = new PrefManager(mContext);
    }

    public void uploadClosingStock(ArrayList<ClosingStock> closingStock, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).uploadClosingStock(prefManager.getAuthKey(),closingStock);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    Toasty.error(mContext,"Code:" + response.code() +" " +  response.message(), Toast.LENGTH_SHORT,true).show();
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
}
