package com.cgtechnodreams.salesassistindia.footfall.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class FootFall implements Serializable {
    @JsonProperty("date")
    private String date;
    @JsonProperty("visitor")
    private String numberOfVisitor;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNumberOfVisitor() {
        return numberOfVisitor;
    }

    public void setNumberOfVisitor(String numberOfVisitor) {
        this.numberOfVisitor = numberOfVisitor;
    }
}
