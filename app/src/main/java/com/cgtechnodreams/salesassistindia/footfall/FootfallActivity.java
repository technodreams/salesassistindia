package com.cgtechnodreams.salesassistindia.footfall;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.footfall.dataservice.FootFallDataService;
import com.cgtechnodreams.salesassistindia.footfall.model.FootFall;
import com.cgtechnodreams.salesassistindia.footfall.model.FootFallResponse;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class FootfallActivity extends AppCompatActivity {
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.etFootfall)
    EditText etFootfall;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.etFootfallDate)
    EditText etFootfallDate;
    Calendar cal;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    int day,year,month;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_footfall);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        toolbar.setTitle("Daily Footfall");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        LocationUtils locationUtils = new LocationUtils(FootfallActivity.this);
        locationUtils.requestLocationUpdates();
    }
    @OnClick({R.id.etFootfallDate,R.id.btnPostFootfall})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.etFootfallDate:

               DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        String date = year + "-" + (month + 1) + "-" + day;
                        etFootfallDate.setText(date);
                    }
                });


                dpd.setVersion(DatePickerDialog.Version.VERSION_2);
                dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                dpd.show(getSupportFragmentManager(), "DatePicker");
                break;
            case R.id.btnPostFootfall:
                if(FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))){
                    postFootfall();
                }
                break;

        }

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    private void postFootfall() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        final FootFall request = new FootFall();
        request.setNumberOfVisitor(etFootfall.getText().toString());
        request.setDate(etFootfallDate.getText().toString());
        FootFallDataService footFallDataService = new FootFallDataService(FootfallActivity.this);
        footFallDataService.postFootfall(prefManager.getAuthKey(),request, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                FootFallResponse footFallResponse = (FootFallResponse) response;
                if (footFallResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(FootfallActivity.this, "Footfall", footFallResponse.getMessage());
                    Toasty.error(FootfallActivity.this, footFallResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    if(footFallResponse.getStatusCode()== APIConstants.ERROR_UNAUTHORIZED ){
                        Intent intent = new Intent(FootfallActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PrefManager preferenceManager = new PrefManager(FootfallActivity.this);
                        preferenceManager.clear();
                        startActivity(intent);
                        finish();
                    }
                }else { //success case
                    LogUtils.setUserActivity(FootfallActivity.this, "FootFall", "Footfall uploaded sucessfully");
                    Toasty.success(FootfallActivity.this,"Footfall uploaded successfully!!",Toast.LENGTH_SHORT,true).show();

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }
}
