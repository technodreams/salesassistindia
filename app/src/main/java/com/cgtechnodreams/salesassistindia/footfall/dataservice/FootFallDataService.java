package com.cgtechnodreams.salesassistindia.footfall.dataservice;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.footfall.model.FootFall;
import com.cgtechnodreams.salesassistindia.footfall.model.FootFallResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FootFallDataService {
    private Context mContext;

    public FootFallDataService(Context mContext) {
        this.mContext = mContext;
    }

    public void postFootfall(String token, FootFall footFall, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<FootFallResponse> callBack = Api.getService(mContext).postDailyFootfall(token,footFall);
        callBack.enqueue(new Callback<FootFallResponse>() {
            @Override
            public void onResponse(Call<FootFallResponse> call, Response<FootFallResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));


                }
            }

            @Override
            public void onFailure(Call<FootFallResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
}