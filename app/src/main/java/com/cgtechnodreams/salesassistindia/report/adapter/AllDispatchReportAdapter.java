package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.fieldwork.model.ActualDispatched;

import java.util.List;

public class AllDispatchReportAdapter  extends RecyclerView.Adapter<AllDispatchReportAdapter.ViewHolder> {

    private final List<ActualDispatched> mValues;
    private final Context mContext;

    public AllDispatchReportAdapter(List<ActualDispatched> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public AllDispatchReportAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_all_dispatch_report, parent, false);
        return new AllDispatchReportAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final AllDispatchReportAdapter.ViewHolder holder, int position) {
        holder.dispatchQuantity.setText(mValues.get(position).getQuantity());
        holder.warehouse.setText("Warehouse: " + mValues.get(position).getWarehouseName());
        holder.productName.setText(mValues.get(position).getProductName());
        holder.date.setText(mValues.get(position).getMiti());
        holder.unitPrice.setText(mValues.get(position).getProductPrice());
        holder.total.setText(mValues.get(position).getTotal());
        holder.distributor.setText(mValues.get(position).getDistributorName());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView total;
        public final TextView unitPrice;
        public final TextView dispatchQuantity;
        public final TextView productName;
        public final TextView warehouse;
        public final TextView date;
        public final TextView distributor;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            total = (TextView) view.findViewById(R.id.total);
            dispatchQuantity = (TextView) view.findViewById(R.id.dispatchQty);
            unitPrice = (TextView) view.findViewById(R.id.unitPrice);
            productName = (TextView)view.findViewById(R.id.productName);
            date = (TextView) view.findViewById(R.id.orderDate);
            warehouse = (TextView) view.findViewById(R.id.warehouse);
            distributor = (TextView) view.findViewById(R.id.stockist);
        }

    }
}