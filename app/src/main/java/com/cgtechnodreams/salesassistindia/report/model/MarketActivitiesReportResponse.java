package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketActivitiesReportResponse extends BaseResponse {
    public ArrayList<MarketActivitiesReportData> getData() {
        return data;
    }

    public void setData(ArrayList<MarketActivitiesReportData> data) {
        this.data = data;
    }

    @JsonProperty("data")
    ArrayList<MarketActivitiesReportData> data;
}
