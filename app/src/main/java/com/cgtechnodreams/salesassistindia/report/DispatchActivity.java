package com.cgtechnodreams.salesassistindia.report;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.fieldwork.model.ActualDispatched;
import com.cgtechnodreams.salesassistindia.report.adapter.DispatchListAdapter;
import com.cgtechnodreams.salesassistindia.report.model.DispatchListReportModel;
import com.cgtechnodreams.salesassistindia.report.model.DispatchListReportResponse;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.data.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class DispatchActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    DispatchListAdapter adapter;
    ProgressBar dialog;
    PrefManager prefManager;
    @BindView(R.id.dateRange)
    TextView dateRange;
   ArrayList<DispatchListReportModel> dispatchListReportModels = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dispatched Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        String from= getIntent().getStringExtra("From");
        String to = getIntent().getStringExtra("To");
        dateRange.setText("Report Generated From " + from +" to " + to);

        getAllDispatched(from, to);
    }

    private void getAllDispatched(String fromDate, String toDate) {
        ProgressDialog dialog = new ProgressDialog(DispatchActivity.this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        SalesOrderDataService dataService = new SalesOrderDataService(DispatchActivity.this);
        dataService.getAllDispatched(prefManager.getAuthKey(),fromDate,toDate, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                DispatchListReportResponse baseResponse = (DispatchListReportResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(DispatchActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();

                } else { //success case
                    if(baseResponse.getData().size()>0){
                        Toasty.success(DispatchActivity.this,  baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();

                        setData(baseResponse.getData());
                    }else{
                        Toasty.info(DispatchActivity.this, "No Data Found!!", Toast.LENGTH_SHORT,true).show();
                    }
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<DispatchListReportModel> data) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(DispatchActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new DispatchListAdapter( data,DispatchActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(DispatchActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(DispatchActivity.this, DispatchDetailReportActivity.class);
                ArrayList<ActualDispatched> dispatchList = data.get(position).getActualDispatchedList();
                intent.putExtra("DispatchedReport",dispatchList);
                startActivity(intent);
            }
            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}