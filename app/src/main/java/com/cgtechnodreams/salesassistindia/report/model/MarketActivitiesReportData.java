package com.cgtechnodreams.salesassistindia.report.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketActivitiesReportData implements Serializable {
    @JsonProperty("id")
    int id;
    @JsonProperty("activities_category")
    int activitiesCategory;
    @JsonProperty("title")
    String title;
    @JsonProperty("description")
    String description;
    @JsonProperty("file_name")
    String filePath;
    @JsonProperty("date_np")
    String miti;
    @JsonProperty("date")
    String date;
    @JsonProperty("user_name")
    String userName;
    @JsonProperty("activities_category_name")
    String activitiesCategoryName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActivitiesCategory() {
        return activitiesCategory;
    }

    public void setActivitiesCategory(int activitiesCategory) {
        this.activitiesCategory = activitiesCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMiti() {
        return miti;
    }

    public void setMiti(String miti) {
        this.miti = miti;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getActivitiesCategoryName() {
        return activitiesCategoryName;
    }

    public void setActivitiesCategoryName(String activitiesCategoryName) {
        this.activitiesCategoryName = activitiesCategoryName;
    }
}
