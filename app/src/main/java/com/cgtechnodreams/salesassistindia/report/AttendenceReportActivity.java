package com.cgtechnodreams.salesassistindia.report;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.report.adapter.AttendenceReportAdapter;
import com.cgtechnodreams.salesassistindia.report.dataservice.ReportDataService;
import com.cgtechnodreams.salesassistindia.report.model.AttendenceReportData;
import com.cgtechnodreams.salesassistindia.report.model.AttendenceReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.ReportRequest;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class AttendenceReportActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    AttendenceReportAdapter adapter;
    ProgressBar dialog;
    PrefManager prefManager;
    @BindView(R.id.dateRange)
    TextView dateRange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        String from= getIntent().getStringExtra("From");
        String to = getIntent().getStringExtra("To");
        dateRange.setText("Report Generated From " + from +" to " + to);
        ReportRequest report = new ReportRequest();
        report.setFrom(from);
        report.setTo(to);
        getAttendenceReport(report);


    }

    private void getAttendenceReport(ReportRequest reportRequest) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        ReportDataService dataService = new ReportDataService(this);
        dataService.getAttendenceReport(prefManager.getAuthKey(), reportRequest, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                AttendenceReportResponse baseResponse = (AttendenceReportResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(AttendenceReportActivity.this, "Attendance Report", baseResponse.getMessage());
                    Toasty.error(AttendenceReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    if(baseResponse.getStatusCode()== APIConstants.ERROR_UNAUTHORIZED ){
                        Intent intent = new Intent(AttendenceReportActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PrefManager preferenceManager = new PrefManager(AttendenceReportActivity.this);
                        preferenceManager.clear();
                        startActivity(intent);
                        finish();
                    }
                    finish();
                }else { //success case
                    Toasty.success(AttendenceReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    setData(baseResponse.getAttendenceReportList());
//                    SurveyAnswerEntity surveyAnswerEntity = new SurveyAnswerEntity(SyncFileActivity.this);
//                    surveyAnswerEntity.delete(null);
//                    Toasty.makeText(AttendenceReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<AttendenceReportData> attendenceReportDataList) {
        LinearLayoutManager favouriteListManager = new LinearLayoutManager(AttendenceReportActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new AttendenceReportAdapter(AttendenceReportActivity.this, attendenceReportDataList);
        recyclerView.setLayoutManager(favouriteListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
