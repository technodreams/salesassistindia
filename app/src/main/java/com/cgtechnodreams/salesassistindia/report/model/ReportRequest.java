package com.cgtechnodreams.salesassistindia.report.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ReportRequest implements Serializable {
    @JsonProperty("from")
    String from;
    @JsonProperty("to")
    String to;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
