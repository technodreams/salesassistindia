package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.AchievementReportData;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<AchievementReportData> achievementReportData = new ArrayList<>();

    public AchievementAdapter(Context context, ArrayList<AchievementReportData> achievementReportData) {
        this.mContext = context;
        this.achievementReportData = achievementReportData;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public AchievementAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_achievement_report, null);
        return new AchievementAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AchievementAdapter.MyViewHolder holder, int position) {
        AchievementReportData eachData = achievementReportData.get(position) ;
        holder.targetQty.setText(("Target Qty: " +eachData.getQuantity()));
        holder.targetAmount.setText(("Target Amount: " +eachData.getAmount()));
        holder.amountAchieved.setText(("Amt Achieved: " +eachData.getAmountAchieved()+"%"));
        holder.qtyAchieved.setText(("Qty Achieved: " +eachData.getQuantityAchieved() +"%"));
        holder.amountSold.setText(("Amt Sold: " +eachData.getAmountSold()));
        holder.qtySold.setText(("Qty Sold: " + eachData.getQuantitySold()));
        holder.month.setText(("Month: " +eachData.getMonth()));
        holder.year.setText(("Year: " +eachData.getYear()));
        holder.pse.setText("PSE Index: " + eachData.getPse());

    }

    @Override
    public int getItemCount() {
        return achievementReportData.size();
    }

//    public void setFilter(ArrayList<Product> newList) {
//        brandList = new ArrayList<>();
//        productList.addAll(newList);
//        notifyDataSetChanged();
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.segmentName)
        TextView pse;
        @BindView(R.id.targetQty)
        TextView targetQty;
        @BindView(R.id.targetAmount)
        TextView targetAmount;
        @BindView(R.id.month)
        TextView month;
        @BindView(R.id.year)
        TextView year;
        @BindView(R.id.amountSold)
        TextView amountSold;
        @BindView(R.id.amountAchieved)
        TextView amountAchieved;
        @BindView(R.id.qtySold)
        TextView qtySold;
        @BindView(R.id.qtyAchieved)
        TextView qtyAchieved;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}