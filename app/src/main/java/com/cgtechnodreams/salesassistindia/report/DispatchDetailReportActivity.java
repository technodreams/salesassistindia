package com.cgtechnodreams.salesassistindia.report;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.fieldwork.model.ActualDispatched;
import com.cgtechnodreams.salesassistindia.report.adapter.AllDispatchReportAdapter;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DispatchDetailReportActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    AllDispatchReportAdapter adapter;
    PrefManager prefManager;
    ArrayList<ActualDispatched> actualDispatcheds;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch_detail_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dispatched Detail Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        actualDispatcheds= (ArrayList<ActualDispatched>)getIntent().getSerializableExtra("DispatchedReport");
        LinearLayoutManager layoutManager = new LinearLayoutManager(DispatchDetailReportActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new AllDispatchReportAdapter( actualDispatcheds,DispatchDetailReportActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}