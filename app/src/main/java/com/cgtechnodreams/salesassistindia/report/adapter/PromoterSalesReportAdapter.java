package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.DailySalesReportList;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PromoterSalesReportAdapter extends RecyclerView.Adapter<PromoterSalesReportAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<DailySalesReportList> dailySalesReportData = new ArrayList<>();

    public PromoterSalesReportAdapter(Context context, ArrayList<DailySalesReportList> dailySalesReportData) {
        this.mContext = context;
        this.dailySalesReportData = dailySalesReportData;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public PromoterSalesReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_daily_sales_report, null);
        return new PromoterSalesReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PromoterSalesReportAdapter.MyViewHolder holder, int position) {
        DailySalesReportList eachData = dailySalesReportData.get(position) ;
        holder.group.setText((" " +eachData.getProductGroup()));
        holder.brand.setText((eachData.getBrand()));
        holder.model.setText((" " +eachData.getModelNumber()));
        holder.mrp.setText(("MRP: " +eachData.getMrp()));
        holder.mop.setText(("MOP: " +eachData.getMop()));
        holder.totalAmount.setText(("Selling Price: " + eachData.getAmount()));
        holder.date.setText(("Date: " +eachData.getDate()));
        holder.outletName.setText(("Stockist Name: " +eachData.getStockistDetails().getName()));
        holder.pseIndex.setText("PSE Index: " + eachData.getPse());

    }

    @Override
    public int getItemCount() {
        return dailySalesReportData.size();
    }

//    public void setFilter(ArrayList<Product> newList) {
//        brandList = new ArrayList<>();
//        productList.addAll(newList);
//        notifyDataSetChanged();
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.group)
        TextView group;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.brand)
        TextView brand;
        @BindView(R.id.model)
        TextView model;
        @BindView(R.id.mrp)
        TextView mrp;
        @BindView(R.id.mop)
        TextView mop;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.totalAmount)
        TextView totalAmount;
        @BindView(R.id.name)
        TextView outletName;
        @BindView(R.id.pseIndex)
        TextView pseIndex;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}