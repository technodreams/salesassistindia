package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DistributorDSRResponse extends BaseResponse {
    @JsonProperty("data")
    ArrayList<DistributorDSR> data;

    public ArrayList<DistributorDSR> getData() {
        return data;
    }

    public void setData(ArrayList<DistributorDSR> data) {
        this.data = data;
    }
}
