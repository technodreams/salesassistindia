package com.cgtechnodreams.salesassistindia.report.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class LeaveReportData implements Serializable {
    @JsonProperty("date_np")
    private String date;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("type")
    private String leaveType;
    @JsonProperty("name")
    private String leaveName;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getLeaveName() {
        return leaveName;
    }

    public void setLeaveName(String leaveName) {
        this.leaveName = leaveName;
    }
}

