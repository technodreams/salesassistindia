package com.cgtechnodreams.salesassistindia.report;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.report.adapter.LeaveReportAdapter;
import com.cgtechnodreams.salesassistindia.report.dataservice.ReportDataService;
import com.cgtechnodreams.salesassistindia.report.model.LeaveReportData;
import com.cgtechnodreams.salesassistindia.report.model.LeaveReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.ReportRequest;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class LeaveReportActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    LeaveReportAdapter adapter = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    PrefManager prefManager = null;
    @BindView(R.id.dateRange)
    TextView dateRange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        String from= getIntent().getStringExtra("From");
        String to = getIntent().getStringExtra("To");
        dateRange.setText("Report Generated From " + from +" to " + to);
        ReportRequest report = new ReportRequest();
        report.setFrom(from);
        report.setTo(to);
        getLeaveReport(report);
    }

    private void getLeaveReport(ReportRequest report) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        ReportDataService dataService = new ReportDataService(this);
        dataService.getLeaveReport(prefManager.getAuthKey(), report, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                LeaveReportResponse leaveReportResponse = (LeaveReportResponse) response;
                if (leaveReportResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(LeaveReportActivity.this, "Leave Report", leaveReportResponse.getMessage());
                    Toasty.error(LeaveReportActivity.this, leaveReportResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    if(leaveReportResponse.getStatusCode()== APIConstants.ERROR_UNAUTHORIZED ){
                        Intent intent = new Intent(LeaveReportActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PrefManager preferenceManager = new PrefManager(LeaveReportActivity.this);
                        preferenceManager.clear();
                        startActivity(intent);
                        finish();
                    }
                    finish();
                } else { //success case
                    Toasty.success(LeaveReportActivity.this, leaveReportResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    setData(leaveReportResponse.getLeaveReportData());
//                    SurveyAnswerEntity surveyAnswerEntity = new SurveyAnswerEntity(SyncFileActivity.this);
//                    surveyAnswerEntity.delete(null);
//                    Toasty.makeText(AttendenceReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void setData(ArrayList<LeaveReportData> leaveReportData) {
        LinearLayoutManager favouriteListManager = new LinearLayoutManager(LeaveReportActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new LeaveReportAdapter(LeaveReportActivity.this, leaveReportData);
        recyclerView.setLayoutManager(favouriteListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

}
