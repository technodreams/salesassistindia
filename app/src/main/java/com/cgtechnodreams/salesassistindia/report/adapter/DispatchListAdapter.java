package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.DispatchListReportModel;

import java.util.List;

public class DispatchListAdapter  extends RecyclerView.Adapter<DispatchListAdapter.ViewHolder> {

    private final List<DispatchListReportModel> mValues;
    private final Context mContext;

    public DispatchListAdapter(List<DispatchListReportModel> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public DispatchListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_dispatch_list, parent, false);
        return new DispatchListAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final DispatchListAdapter.ViewHolder holder, int position) {
        holder.date.setText(mValues.get(position).getNepaliDate());
        holder.stockist.setText(mValues.get(position).getDistributorName());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView date;
        public final TextView stockist;

        public ViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.orderDate);
            stockist = (TextView) view.findViewById(R.id.stockist);
        }

    }

}

