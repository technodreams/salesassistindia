package com.cgtechnodreams.salesassistindia.report.dataservice;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.report.model.AchievementResponse;
import com.cgtechnodreams.salesassistindia.report.model.AttendenceReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.DailySalesReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.DistributorCollectionResponse;
import com.cgtechnodreams.salesassistindia.report.model.DistributorDSRResponse;
import com.cgtechnodreams.salesassistindia.report.model.LeaveReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.MarketActivitiesReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.ReportRequest;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportDataService {
    private Context mContext;

    public ReportDataService(Context mContext) {
        this.mContext = mContext;
    }

    public void getAttendenceReport(String token, ReportRequest reqeust,final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<AttendenceReportResponse> callBack = Api.getService(mContext).getAttendenceReport(token, reqeust);
        callBack.enqueue(new Callback<AttendenceReportResponse>() {
            @Override
            public void onResponse(Call<AttendenceReportResponse> call, Response<AttendenceReportResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<AttendenceReportResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getLeaveReport(String token,  ReportRequest request,final OnSuccess onSuccessListener,final OnFailedListener onErrorListener) {
        Call<LeaveReportResponse> callBack = Api.getService(mContext).getLeaveReport(token,request);
        callBack.enqueue(new Callback<LeaveReportResponse>() {
            @Override
            public void onResponse(Call<LeaveReportResponse> call, Response<LeaveReportResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<LeaveReportResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getDailySalesReport(String token, ReportRequest request,final OnSuccess onSuccessListener,final OnFailedListener onErrorListener) {
        Call<DailySalesReportResponse> callBack = Api.getService(mContext).getDailySalesReport(token,request);
        callBack.enqueue(new Callback<DailySalesReportResponse>() {
            @Override
            public void onResponse(Call<DailySalesReportResponse> call, Response<DailySalesReportResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<DailySalesReportResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getAchievementReport(String token, ReportRequest request,final OnSuccess onSuccessListener,final OnFailedListener onErrorListener) {
        Call<AchievementResponse> callBack = Api.getService(mContext).getAchievementReport(token,request);
        callBack.enqueue(new Callback<AchievementResponse>() {
            @Override
            public void onResponse(Call<AchievementResponse> call, Response<AchievementResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<AchievementResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getDistributorCollectionReport(String token, int id, String from, String to,final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<DistributorCollectionResponse> callBack = Api.getService(mContext).getDistributorCollectionReport(token, id,from,to);
        callBack.enqueue(new Callback<DistributorCollectionResponse>() {
            @Override
            public void onResponse(Call<DistributorCollectionResponse> call, Response<DistributorCollectionResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<DistributorCollectionResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getMarketActivitiesReport(String token,String from, String to,final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<MarketActivitiesReportResponse> callBack = Api.getService(mContext).getMarketActivitiesReport(token, from,to);
        callBack.enqueue(new Callback<MarketActivitiesReportResponse>() {
            @Override
            public void onResponse(Call<MarketActivitiesReportResponse> call, Response<MarketActivitiesReportResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<MarketActivitiesReportResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getDistributorDSRReport(String token, int id,String from, String to,final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<DistributorDSRResponse> callBack = Api.getService(mContext).getDistributorDSRReport(token, id, from,to);
        callBack.enqueue(new Callback<DistributorDSRResponse>() {
            @Override
            public void onResponse(Call<DistributorDSRResponse> call, Response<DistributorDSRResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<DistributorDSRResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
}

