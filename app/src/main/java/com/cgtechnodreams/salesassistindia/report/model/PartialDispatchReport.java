package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.fieldwork.model.DispatchedOrder;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PartialDispatchReport implements Serializable {
    @JsonProperty("distributor_name")
    String distributorName;
    @JsonProperty("date")
    String date;
    @JsonProperty("date_np")
    String nepaliDate;
    @JsonProperty("dispatches")
    private ArrayList<DispatchedOrder> data;

    public ArrayList<DispatchedOrder> getData() {
        return data;
    }

    public void setData(ArrayList<DispatchedOrder> data) {
        this.data = data;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNepaliDate() {
        return nepaliDate;
    }

    public void setNepaliDate(String nepaliDate) {
        this.nepaliDate = nepaliDate;
    }
}
