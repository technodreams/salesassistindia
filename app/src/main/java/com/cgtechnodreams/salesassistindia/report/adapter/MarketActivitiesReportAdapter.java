package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.MarketActivitiesReportData;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MarketActivitiesReportAdapter  extends RecyclerView.Adapter<MarketActivitiesReportAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<MarketActivitiesReportData> marketActivitiesReportDataArrayList = new ArrayList<>();
   public MarketActivitiesReportAdapter(Context context, ArrayList<MarketActivitiesReportData> marketActivitiesReportDataArrayList) {
        this.mContext = context;
        this.marketActivitiesReportDataArrayList = marketActivitiesReportDataArrayList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public MarketActivitiesReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_market_activities_report, null);
        return new MarketActivitiesReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MarketActivitiesReportAdapter.MyViewHolder holder, int position) {
        MarketActivitiesReportData eachData = marketActivitiesReportDataArrayList.get(position);
        holder.date.setText(eachData.getMiti());
        holder.type.setText(eachData.getActivitiesCategoryName());
        holder.title.setText(eachData.getTitle());
        holder.description.setText( eachData.getDescription());
        String url = eachData.getFilePath();
        if(!url.equalsIgnoreCase("") && url !=null){
            Glide.with(mContext)
                    .load(url)
                    .centerCrop()
                    .into(holder.imgMarketActivities);
        }


    }

    @Override
    public int getItemCount() {
        return marketActivitiesReportDataArrayList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.category)
        TextView type;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.imageMarketActivities)
        ImageView imgMarketActivities;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

