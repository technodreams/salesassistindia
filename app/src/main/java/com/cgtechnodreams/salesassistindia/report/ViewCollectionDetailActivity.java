package com.cgtechnodreams.salesassistindia.report;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.StockistCollectionReport;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewCollectionDetailActivity extends AppCompatActivity {
    @BindView(R.id.bankName)
    TextView bankName;
    @BindView(R.id.paymentMethod)
    TextView paymentMethod;
    @BindView(R.id.chequeOrAccountNumber)
    TextView chequeAccountNumber;
    @BindView(R.id.amount)
    TextView amount;
    @BindView(R.id.remarks)
    TextView remarks;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.stockist)
    TextView stockist;
    StockistCollectionReport stockistCollectionReport;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_collection_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Collection Detail");

        stockistCollectionReport = (StockistCollectionReport) getIntent().getSerializableExtra("CollectionDetail");
       if(stockistCollectionReport.getFilepath().equalsIgnoreCase("") || stockistCollectionReport.getFilepath() == null){
           image.setVisibility(View.GONE);
       }else{
           Glide.with(this).load(stockistCollectionReport.getFilepath()).into(image);
       }

        amount.setText(String.valueOf(stockistCollectionReport.getAmount()));
        remarks.setText(stockistCollectionReport.getRemarks());
        stockist.setText(stockistCollectionReport.getDistributorName());
        paymentMethod.setText(stockistCollectionReport.getCollectionMethod());
        if(stockistCollectionReport.getCollectionMethod().equalsIgnoreCase("Deposit")){
            chequeAccountNumber.setText(stockistCollectionReport.getDepositNumber());
        }else{
            chequeAccountNumber.setText(stockistCollectionReport.getChequeNumber());
        }

        bankName.setText(stockistCollectionReport.getBankName());

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
