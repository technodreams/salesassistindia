package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.ViewCollectionDetailActivity;
import com.cgtechnodreams.salesassistindia.report.model.StockistCollectionReport;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorCollectionAdapter  extends RecyclerView.Adapter<DistributorCollectionAdapter.MyViewHolder> {

        Context mContext;
        PrefManager prefManager;
        ArrayList<StockistCollectionReport> collections = new ArrayList<>();

        public DistributorCollectionAdapter(Context context, ArrayList<StockistCollectionReport> collections) {
            this.mContext = context;
            this.collections = collections;
            prefManager = new PrefManager(mContext);
        }

        @Override
        public DistributorCollectionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_distributor_collection_report, null);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(DistributorCollectionAdapter.MyViewHolder holder, int position) {
            StockistCollectionReport collectionDetailModel = collections.get(position);
            holder.bankName.setText(collectionDetailModel.getBankName());
            holder.amount.setText( "Amount: " + collectionDetailModel.getAmount() + "");
            holder.paymentMethod.setText( "Collection Method: " +collectionDetailModel.getCollectionMethod());
            String url = collectionDetailModel.getFilepath();
            if(!url.equalsIgnoreCase("") && url !=null){
                Glide.with(mContext)
                        .load(url)
                        .centerCrop()
                        .into(holder.imgCheque);
            }


        }

        @Override
        public int getItemCount() {
            return collections.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.bankName)
            TextView bankName;
            @BindView(R.id.amount)
            TextView amount;
            @BindView(R.id.imgCheque)
            ImageView imgCheque;
            @BindView(R.id.paymentMethod)
            TextView paymentMethod;

            public MyViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, ViewCollectionDetailActivity.class);
                        intent.putExtra("CollectionDetail",collections.get(getAdapterPosition()));
                        ((AppCompatActivity)mContext).startActivity(intent);
                    }
                });

            }
        }
    }

