package com.cgtechnodreams.salesassistindia.report;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.outlet.StockistActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.report.adapter.ReportAdapter;
import com.cgtechnodreams.salesassistindia.report.model.ReportData;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.SalesmanSalesReportActivity;
import com.cgtechnodreams.salesassistindia.stock.DistributorStockActivity;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class ReportActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fromDate)
    EditText fromDate;
    @BindView(R.id.toDate)
    EditText toDate;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    ReportAdapter mAdapter;
    PrefManager prefManager = null;
    List<ReportData> data = null;
    Calendar from = Calendar.getInstance();
    int fromYear, fromMonth, fromDay;
    Stockist mStockist = null;

    public static final int REQUEST_CODE_OUTLETS_COLLECTION = 101;
    public static final int REQUEST_CODE_OUTLETS_MARKET_ACTIVITIES = 102;
    public static final int REQUEST_CODE_OUTLETS_DSR= 103;
    public static final int REQUEST_CODE_DISTRIBUTOR_STOCK= 104;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Generate Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        prefManager = new PrefManager(this);

        String reportData = getReportData(prefManager.getRole());
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            data = (List<ReportData>) objectMapper.readValue(reportData, new TypeReference<List<ReportData>>(){});

        } catch (IOException e) {
            e.printStackTrace();
        }

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mAdapter = new ReportAdapter(data,this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(ReportActivity.this, mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if(fromDate.getText().toString().equalsIgnoreCase("") || toDate.getText().toString().equalsIgnoreCase("")){
                            Toasty.info(ReportActivity.this, "Date missing",Toast.LENGTH_SHORT,true).show();
                        }else{

                            changeActivity(data.get(position).getActivityName());
                        }
                    }
                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                }));

    }
    private void changeActivity(String activityName) {
        if(activityName.equalsIgnoreCase("AttendenceReport")){
            Intent intent = new Intent(ReportActivity.this, AttendenceReportActivity.class);
            intent.putExtra("From", fromDate.getText().toString());
            intent.putExtra("To", toDate.getText().toString());
             startActivity(intent);
        }
        else if(activityName.equalsIgnoreCase("LeaveReport")){
            Intent intent = new Intent(ReportActivity.this, LeaveReportActivity.class);
            intent.putExtra("From", fromDate.getText().toString());
            intent.putExtra("To", toDate.getText().toString());
            startActivity(intent);
        }
        else if(activityName.equalsIgnoreCase("DailySalesReport")){
            Intent intent = new Intent(ReportActivity.this, DailySalesReportActivity.class);
            intent.putExtra("From", fromDate.getText().toString());
            intent.putExtra("To", toDate.getText().toString());
            startActivity(intent);
        }
        else if(activityName.equalsIgnoreCase("SalesOrderReport")){
            Intent intent = new Intent(ReportActivity.this, SalesmanSalesReportActivity.class);
            intent.putExtra("From", fromDate.getText().toString());
            intent.putExtra("To", toDate.getText().toString());
            startActivity(intent);
        }
        else if(activityName.equalsIgnoreCase("AchievementReport")){
            Intent intent = new Intent(ReportActivity.this, AchievementReportActivity.class);
            intent.putExtra("From", fromDate.getText().toString());
            intent.putExtra("To", toDate.getText().toString());
            startActivity(intent);
        }else if(activityName.equalsIgnoreCase("DistributorCollectionReportActivity")){
            Intent intent = new Intent(ReportActivity.this, StockistActivity.class);
            startActivityForResult(intent,REQUEST_CODE_OUTLETS_COLLECTION);

        }else if(activityName.equalsIgnoreCase("DistributorDailySalesReportActivity")){
            Intent intent = new Intent(ReportActivity.this, StockistActivity.class);
            startActivityForResult(intent,REQUEST_CODE_OUTLETS_DSR);
        }else if(activityName.equalsIgnoreCase("MarketActivitiesReportActivity")){
            Intent intent = new Intent(ReportActivity.this, MarketActivitiesReportActivity.class);
            intent.putExtra("From", fromDate.getText().toString());
            intent.putExtra("To", toDate.getText().toString());
            startActivity(intent);
        }else if(activityName.equalsIgnoreCase("DispatchActivity")){
            Intent intent = new Intent(ReportActivity.this, DispatchActivity.class);
            intent.putExtra("From", fromDate.getText().toString());
            intent.putExtra("To", toDate.getText().toString());
            startActivity(intent);
        }
        else if(activityName.equalsIgnoreCase("PartialDispatchedActivity")){
            Intent intent = new Intent(ReportActivity.this, PartialDispatchedActivity.class);
            intent.putExtra("From", fromDate.getText().toString());
            intent.putExtra("To", toDate.getText().toString());
            startActivity(intent);
        }
        else if (activityName.equalsIgnoreCase("DistributorStockActivity")){
            Intent intent = new Intent(ReportActivity.this, StockistActivity.class);
            startActivityForResult(intent,REQUEST_CODE_DISTRIBUTOR_STOCK);
        }
    }
    @OnClick({R.id.fromDate, R.id.toDate})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.toDate:
                if(fromDate.getText().toString().equalsIgnoreCase("")){
                    Toasty.info(ReportActivity.this,"Please select from date first", Toast.LENGTH_SHORT,true).show();
                }else{
                    Calendar now = Calendar.getInstance();
                    DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                            month = month + 1;
                            String tempDay = day + "";
                            String tempMonth = month + "";
                            if (day < 10) {
                                tempDay = "0" + day;
                            }
                            if (month < 10) {
                                tempMonth = "0" + month;
                            }
                            String date = year + "-" + (tempMonth) + "-" + tempDay;
                            toDate.setText(date);

                        }
                    });
                    dpd.setVersion(DatePickerDialog.Version.VERSION_2);

                    //@TODO Minimum date handle left
                    dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                    dpd.show(getSupportFragmentManager(), "DatePicker");

                }

                // dpd.setMaxDate(dateConverter.getTodayNepaliDate());
                break;
            case R.id.fromDate:

                from = Calendar.getInstance();
                DatePickerDialog fromDPD = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        month = month + 1;
                        String tempDay = day + "";
                        String tempMonth = month + "";
                        if (day < 10) {
                            tempDay = "0" + day;
                        }
                        if (month < 10) {
                            tempMonth = "0" + month;
                        }
                        String date = year + "-" + (tempMonth) + "-" + tempDay;
                        fromDate.setText(date);
                    }
                });
                fromDPD.setVersion(DatePickerDialog.Version.VERSION_2);
                fromDPD.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                fromDPD.show(getSupportFragmentManager(), "DatePicker");

                break;


        }

    }
    private String getReportData(String role) {
        String reportData = null;
      //  if(role.equalsIgnoreCase("salesman") || role.equalsIgnoreCase("Area Sales Manager")){
            reportData = readAssetFile("salesman_report.json");

      //  }
        return reportData;
    }
    private String readAssetFile(String fileName) {
        String json = null;
        try {
            InputStream is = getResources().getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_OUTLETS_COLLECTION:
                    this.mStockist = (Stockist) data.getSerializableExtra("result");
                    Intent intent = new Intent(ReportActivity.this, DistributorCollectionReportActivity.class);
                    intent.putExtra("From", fromDate.getText().toString());
                    intent.putExtra("To", toDate.getText().toString());
                    intent.putExtra("Id", this.mStockist.getDistributorId());
                    startActivity(intent);
                   // stockist.setText(distributorObj.getName());
                    break;
                case REQUEST_CODE_OUTLETS_DSR:
                    Stockist mStockist = (Stockist) data.getSerializableExtra("result");
                    Intent dsr = new Intent(ReportActivity.this, DistributorDailySalesReportActivity.class);
                    dsr.putExtra("From", fromDate.getText().toString());
                    dsr.putExtra("To", toDate.getText().toString());
                    dsr.putExtra(("Id"), mStockist.getDistributorId());
                    startActivity(dsr);
                    break;

                case REQUEST_CODE_DISTRIBUTOR_STOCK:
                    Stockist stockist = (Stockist) data.getSerializableExtra("result");
                    Intent ds = new Intent(ReportActivity.this, DistributorStockActivity.class);
                    ds.putExtra("From", fromDate.getText().toString());
                    ds.putExtra("To", toDate.getText().toString());
                    ds.putExtra(("DistributorDetail"), stockist);
                    startActivity(ds);
                    break;

            }
        }

    }
}
