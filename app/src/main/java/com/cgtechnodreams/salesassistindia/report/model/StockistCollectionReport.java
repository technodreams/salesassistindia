package com.cgtechnodreams.salesassistindia.report.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StockistCollectionReport implements Serializable {
    @JsonProperty("id")
    int id;
    @JsonProperty("outlet_id")
    int distributorId;
    @JsonProperty("outlet_name")
    String distributorName;
    @JsonProperty("bank_id")
    int bankId;
    @JsonProperty("bank_name")
    String bankName;
    @JsonProperty("division_id")
    int divisionId;
    @JsonProperty("division_name")
    String divisionName;
    @JsonProperty("salesman_id")
    int salesmanId;
    @JsonProperty("salesman_name")
    String salesmanName;
    @JsonProperty("collection_method")
    String collectionMethod;
    @JsonProperty("cheque_number")
    String chequeNumber;
    @JsonProperty("deposit_number")
    String depositNumber;
    @JsonProperty("amount")
    double amount;
    @JsonProperty("remarks")
    String remarks;
    @JsonProperty("cash_in_date")
    String cashInDate;
    @JsonProperty("cash_in_date_np")
    String cashInMiti;
    @JsonProperty("filename")
    String filepath;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public int getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(int salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getCollectionMethod() {
        return collectionMethod;
    }

    public void setCollectionMethod(String collectionMethod) {
        this.collectionMethod = collectionMethod;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCashInDate() {
        return cashInDate;
    }

    public void setCashInDate(String cashInDate) {
        this.cashInDate = cashInDate;
    }

    public String getCashInMiti() {
        return cashInMiti;
    }

    public void setCashInMiti(String cashInMiti) {
        this.cashInMiti = cashInMiti;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }


//     "id": 121,
//             "outlet_id": 1,
//             "outlet_name": "Bhat Bhateni Super Market",
//             "bank_id": 11,
//             "bank_name": "Citizens Bank International Ltd",
//             "division_id": 1,
//             "division_name": "Noodles",
//             "salesman_id": 11,
//             "salesman_name": "Pawan KC",
//             "collection_method": "Cheque",
//             "cheque_number": null,
//             "deposit_number": "",
//             "amount": 25000,
//             "remarks": null,

}
