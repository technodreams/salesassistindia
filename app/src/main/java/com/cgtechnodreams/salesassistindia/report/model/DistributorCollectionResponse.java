package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DistributorCollectionResponse extends BaseResponse {
    @JsonProperty("data")
    ArrayList<StockistCollectionReport> data;

    public ArrayList<StockistCollectionReport> getData() {
        return data;
    }

    public void setData(ArrayList<StockistCollectionReport> data) {
        this.data = data;
    }
}
