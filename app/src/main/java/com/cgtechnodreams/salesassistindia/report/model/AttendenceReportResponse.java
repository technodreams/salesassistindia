package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AttendenceReportResponse extends BaseResponse {
    @JsonProperty("data")
    ArrayList<AttendenceReportData> attendenceReportList;

    public ArrayList<AttendenceReportData> getAttendenceReportList() {
        return attendenceReportList;
    }

    public void setAttendenceReportList(ArrayList<AttendenceReportData> attendenceReportList) {
        this.attendenceReportList = attendenceReportList;
    }
}
