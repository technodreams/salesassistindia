package com.cgtechnodreams.salesassistindia.report;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.report.adapter.MarketActivitiesReportAdapter;
import com.cgtechnodreams.salesassistindia.report.dataservice.ReportDataService;
import com.cgtechnodreams.salesassistindia.report.model.MarketActivitiesReportData;
import com.cgtechnodreams.salesassistindia.report.model.MarketActivitiesReportResponse;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class MarketActivitiesReportActivity extends AppCompatActivity {
    PrefManager prefManager = null;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    MarketActivitiesReportAdapter adapter;
    ProgressDialog dialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_activiites_report);
        ButterKnife.bind(this);
        dialog = new ProgressDialog(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        String from= getIntent().getStringExtra("From");
        String to = getIntent().getStringExtra("To");
     //   int distributorId = getIntent().getIntExtra("Id",0);
        getSupportActionBar().setTitle("Market Activities");
        getMarketActivitiesReport(from,to);
    }

    private void getMarketActivitiesReport( String from, String to) {  dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        ReportDataService dataService = new ReportDataService(this);
        dataService.getMarketActivitiesReport(prefManager.getAuthKey(),from,to, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                MarketActivitiesReportResponse baseResponse = (MarketActivitiesReportResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(MarketActivitiesReportActivity.this, "Market Activities Report", baseResponse.getMessage());
                    Toasty.error(MarketActivitiesReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    if(baseResponse.getStatusCode()== APIConstants.ERROR_UNAUTHORIZED ){
                        Intent intent = new Intent(MarketActivitiesReportActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PrefManager preferenceManager = new PrefManager(MarketActivitiesReportActivity.this);
                        preferenceManager.clear();
                        startActivity(intent);
                        finish();
                    }
                    finish();
                } else { //success case
                    Toasty.success(MarketActivitiesReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    setData(baseResponse.getData());
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<MarketActivitiesReportData> marketActivitiesReportData) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MarketActivitiesReportActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new MarketActivitiesReportAdapter(MarketActivitiesReportActivity.this, marketActivitiesReportData);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
