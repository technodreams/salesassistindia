package com.cgtechnodreams.salesassistindia.report.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class AchievementReportData implements Serializable {
    @JsonProperty("quantity")
    private int quantity;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("pse")
    private String pse;
    @JsonProperty("month")
    private String month;
    @JsonProperty("year")
    private String year;
    @JsonProperty("quantity_sold")
    private float quantitySold;
    @JsonProperty("amount_sold")
    private float amountSold;
    @JsonProperty("quantity_sold_percentage")
    private float quantityAchieved;
    @JsonProperty("amount_sold_percentage")
    private float amountAchieved;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPse() {
        return pse;
    }

    public void setPse(String pse) {
        this.pse = pse;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public float getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(float quantitySold) {
        this.quantitySold = quantitySold;
    }

    public float getAmountSold() {
        return amountSold;
    }

    public void setAmountSold(float amountSold) {
        this.amountSold = amountSold;
    }

    public float getQuantityAchieved() {
        return quantityAchieved;
    }

    public void setQuantityAchieved(float quantityAchieved) {
        this.quantityAchieved = quantityAchieved;
    }

    public float getAmountAchieved() {
        return amountAchieved;
    }

    public void setAmountAchieved(float amountAchieved) {
        this.amountAchieved = amountAchieved;
    }
}
