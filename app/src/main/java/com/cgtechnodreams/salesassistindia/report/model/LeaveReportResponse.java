package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeaveReportResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<LeaveReportData> leaveReportData;
    public void setLeaveReportData(ArrayList<LeaveReportData> leaveReportData) {
        this.leaveReportData = leaveReportData;
    }

    public ArrayList<LeaveReportData> getLeaveReportData() {
        return leaveReportData;
    }






}
