package com.cgtechnodreams.salesassistindia.report.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DailySalesData implements Serializable {
    @JsonProperty("sales_detail")
    private ArrayList<DailySalesReportList> dailySalesReportData;
    @JsonProperty("total_amount")
    int totalAmount;
    @JsonProperty("total_quantity")
    int totalQuantity;

    public ArrayList<DailySalesReportList> getDailySalesReportData() {
        return dailySalesReportData;
    }

    public void setDailySalesReportData(ArrayList<DailySalesReportList> dailySalesReportData) {
        this.dailySalesReportData = dailySalesReportData;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }
}
