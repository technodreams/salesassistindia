package com.cgtechnodreams.salesassistindia.report;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.fieldwork.PartialDispatchedAdapter;
import com.cgtechnodreams.salesassistindia.fieldwork.model.DispatchedOrder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PartialDispatchDetailReportActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    PartialDispatchedAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partial_dispatch_detail_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ArrayList<DispatchedOrder> partialDispatched = ( ArrayList<DispatchedOrder>) getIntent().getSerializableExtra("PartialDispatched");

        if(partialDispatched!=null){
            getSupportActionBar().setTitle(partialDispatched.get(0).getDistributorName() + " Partial Dispatch");
            LinearLayoutManager layoutManager = new LinearLayoutManager(PartialDispatchDetailReportActivity.this, LinearLayoutManager.VERTICAL, false);
            adapter = new PartialDispatchedAdapter(partialDispatched,PartialDispatchDetailReportActivity.this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}