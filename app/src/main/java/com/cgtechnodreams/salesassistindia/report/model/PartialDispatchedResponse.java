package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PartialDispatchedResponse extends BaseResponse implements Serializable {
    ArrayList<PartialDispatchReport> data;
    public ArrayList<PartialDispatchReport> getData() {
        return data;
    }

    public void setData(ArrayList<PartialDispatchReport> data) {
        this.data = data;
    }
}
