package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.DistributorDSR;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorDSRReportAdapter  extends RecyclerView.Adapter<DistributorDSRReportAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<DistributorDSR> dsrList = new ArrayList<>();

    public DistributorDSRReportAdapter(Context context, ArrayList<DistributorDSR> dsrList) {
        this.mContext = context;
        this.dsrList = dsrList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public DistributorDSRReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_distributor_dsr, null);
        return new DistributorDSRReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DistributorDSRReportAdapter.MyViewHolder holder, int position) {
        DistributorDSR eachData = dsrList.get(position);
        holder.date.setText("Miti: " + eachData.getMiti());
        holder.qty.setText("Quantity: " + eachData.getQuantity());
        holder.product.setText( eachData.getProductName());
        holder.salesmanName.setText( eachData.getSalesmanName());

    }

    @Override
    public int getItemCount() {
        return dsrList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.product)
        TextView product;
        @BindView(R.id.qty)
        TextView qty;
        @BindView(R.id.salesmanName)
        TextView salesmanName;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

