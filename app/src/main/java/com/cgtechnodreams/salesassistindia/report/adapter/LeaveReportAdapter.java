package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.LeaveReportData;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LeaveReportAdapter extends RecyclerView.Adapter<LeaveReportAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<LeaveReportData> leaveReportList = new ArrayList<>();

    public LeaveReportAdapter(Context context, ArrayList<LeaveReportData> leaveReportList) {
        this.mContext = context;
        this.leaveReportList = leaveReportList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public LeaveReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_leave_report, null);
        return new LeaveReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LeaveReportAdapter.MyViewHolder holder, int position) {
        LeaveReportData eachData = leaveReportList.get(position);
        holder.date.setText("Date:" + eachData.getDate());
        holder.type.setText("Leave Type:" + eachData.getLeaveType());
        holder.leaveName.setText("Leave Name:" + eachData.getLeaveName());
        holder.remarks.setText( eachData.getRemarks());

    }

    @Override
    public int getItemCount() {
        return leaveReportList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.type)
        TextView type;
        @BindView(R.id.leaveName)
        TextView leaveName;
        @BindView(R.id.remarks)
        TextView remarks;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
