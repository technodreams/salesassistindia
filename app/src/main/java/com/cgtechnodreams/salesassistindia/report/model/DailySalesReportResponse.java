package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DailySalesReportResponse extends BaseResponse {
    @JsonProperty("data")
    private DailySalesData dailySalesData;

    public DailySalesData getDailySalesData() {
        return dailySalesData;
    }

    public void setDailySalesData(DailySalesData dailySalesData) {
        this.dailySalesData = dailySalesData;
    }
}
