package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AchievementResponse extends BaseResponse {
    public ArrayList<AchievementReportData> getAchievementReportData() {
        return achievementReportData;
    }

    public void setAchievementReportData(ArrayList<AchievementReportData> achievementReportData) {
        this.achievementReportData = achievementReportData;
    }
    @JsonProperty("data")
    ArrayList<AchievementReportData> achievementReportData;
}
