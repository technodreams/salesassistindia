package com.cgtechnodreams.salesassistindia.report.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ReportData implements Serializable {
    @JsonProperty("Title")
    String title;
    @JsonProperty("Image")
    String image;
    @JsonProperty("Activity")
    String activityName;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
}
