package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.PartialDispatchReport;

import java.util.List;

public class PartialDispatchedReportAdapter extends RecyclerView.Adapter<PartialDispatchedReportAdapter.ViewHolder> {

    private final List<PartialDispatchReport> mValues;
    private final Context mContext;

    public PartialDispatchedReportAdapter(List<PartialDispatchReport> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public PartialDispatchedReportAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_dispatch_list, parent, false);
        return new PartialDispatchedReportAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final PartialDispatchedReportAdapter.ViewHolder holder, int position) {
        holder.date.setText(mValues.get(position).getNepaliDate());
        holder.stockist.setText(mValues.get(position).getDistributorName());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView date;
        public final TextView stockist;

        public ViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.orderDate);
            stockist = (TextView) view.findViewById(R.id.stockist);
        }

    }
}
