package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DispatchListReportResponse extends BaseResponse {
    public ArrayList<DispatchListReportModel> getData() {
        return data;
    }

    public void setData(ArrayList<DispatchListReportModel> data) {
        this.data = data;
    }

    ArrayList<DispatchListReportModel> data;
}
