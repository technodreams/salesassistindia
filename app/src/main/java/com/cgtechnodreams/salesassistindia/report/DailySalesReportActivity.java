package com.cgtechnodreams.salesassistindia.report;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.report.adapter.PromoterSalesReportAdapter;
import com.cgtechnodreams.salesassistindia.report.dataservice.ReportDataService;
import com.cgtechnodreams.salesassistindia.report.model.DailySalesData;
import com.cgtechnodreams.salesassistindia.report.model.DailySalesReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.ReportRequest;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class DailySalesReportActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    PrefManager prefManager = null;
    PromoterSalesReportAdapter adapter = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.dateRange)
    TextView dateRange;
    @BindView(R.id.totalSalesAmount)
    TextView totalSalesAmount;
    @BindView(R.id.totalQuantity)
    TextView totalQuantity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_sales_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        String from= getIntent().getStringExtra("From");
        String to = getIntent().getStringExtra("To");
        dateRange.setText("Report Generated From " + from +" to " + to);
        ReportRequest report = new ReportRequest();
        report.setFrom(from);
        report.setTo(to);
        getDailySalesReport(report);
    }

    private void getDailySalesReport(ReportRequest report) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        ReportDataService dataService = new ReportDataService(this);
        dataService.getDailySalesReport(prefManager.getAuthKey(), report, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                DailySalesReportResponse baseResponse = (DailySalesReportResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(DailySalesReportActivity.this, "Daily Sales Report", baseResponse.getMessage());
                    Toasty.error(DailySalesReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    if(baseResponse.getStatusCode()== APIConstants.ERROR_UNAUTHORIZED ){
                        Intent intent = new Intent(DailySalesReportActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PrefManager preferenceManager = new PrefManager(DailySalesReportActivity.this);
                        preferenceManager.clear();
                        startActivity(intent);
                        finish();
                    }
                    finish();
                } else { //success case
                    setData(baseResponse.getDailySalesData());
//                    SurveyAnswerEntity surveyAnswerEntity = new SurveyAnswerEntity(SyncFileActivity.this);
//                    surveyAnswerEntity.delete(null);
//                    Toasty.makeText(AttendenceReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(DailySalesData dailySalesReportData) {
        totalSalesAmount.setText("Total Amount Sold Rs." + dailySalesReportData.getTotalAmount());
        totalQuantity.setText("Total Sold Quantity: " + dailySalesReportData.getTotalQuantity());
        LinearLayoutManager favouriteListManager = new LinearLayoutManager(DailySalesReportActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new PromoterSalesReportAdapter(DailySalesReportActivity.this, dailySalesReportData.getDailySalesReportData());
        recyclerView.setLayoutManager(favouriteListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
