package com.cgtechnodreams.salesassistindia.report.model;

import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DailySalesReportList implements Serializable {
    @JsonProperty("date_np")
    private String date;
    @JsonProperty("product_model")
    private String modelNumber;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("product_group")
    private String productGroup;
    @JsonProperty("mrp")
    private String mrp;
    @JsonProperty("mop")
    private String mop;
    @JsonProperty("distributor_details")
    private Stockist stockistDetails;
    @JsonProperty("pse")
    private String pse;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("serial_number")
    private String serailNumber;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getMop() {
        return mop;
    }

    public void setMop(String mop) {
        this.mop = mop;
    }

    public Stockist getStockistDetails() {
        return stockistDetails;
    }

    public void setStockistDetails(Stockist stockistDetails) {
        this.stockistDetails = stockistDetails;
    }

    public String getPse() {
        return pse;
    }

    public void setPse(String pse) {
        this.pse = pse;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSerailNumber() {
        return serailNumber;
    }

    public void setSerailNumber(String serailNumber) {
        this.serailNumber = serailNumber;
    }
}
