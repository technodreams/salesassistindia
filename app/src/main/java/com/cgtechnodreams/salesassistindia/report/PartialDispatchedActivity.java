package com.cgtechnodreams.salesassistindia.report;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.fieldwork.model.DispatchedOrder;
import com.cgtechnodreams.salesassistindia.report.adapter.PartialDispatchedReportAdapter;
import com.cgtechnodreams.salesassistindia.report.model.DispatchListReportModel;
import com.cgtechnodreams.salesassistindia.report.model.PartialDispatchReport;
import com.cgtechnodreams.salesassistindia.report.model.PartialDispatchedResponse;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.data.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class PartialDispatchedActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    PartialDispatchedReportAdapter adapter;
    ProgressBar dialog;
    PrefManager prefManager;
    @BindView(R.id.dateRange)
    TextView dateRange;
    ArrayList<DispatchListReportModel> dispatchListReportModels = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partial_dispatched);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Partial Dispatched Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        String from= getIntent().getStringExtra("From");
        String to = getIntent().getStringExtra("To");
        dateRange.setText("Report Generated From " + from +" to " + to);

        if(Utils.isConnectionAvailable(PartialDispatchedActivity.this)){

            getDispatchedOrder(from,to);
        }else{
            Toasty.info(PartialDispatchedActivity.this,"No internet connection to fetch Sales Report!!", Toast.LENGTH_SHORT,true).show();
        }
    }

    private void getDispatchedOrder(String from, String to) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        SalesOrderDataService dataService = new SalesOrderDataService(this);
        dataService.getAllPartialDispatchedOrderReport(prefManager.getAuthKey(),from, to, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                PartialDispatchedResponse baseResponse = (PartialDispatchedResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(PartialDispatchedActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();

                } else { //success case
                    if(baseResponse.getData().size()>0){
                        Toasty.success(PartialDispatchedActivity.this,  baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                        setData(baseResponse.getData());
                    }else{
                        Toasty.info(PartialDispatchedActivity.this, "No Data Found!!", Toast.LENGTH_SHORT,true).show();
                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<PartialDispatchReport> data) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(PartialDispatchedActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new PartialDispatchedReportAdapter(data,PartialDispatchedActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(PartialDispatchedActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(PartialDispatchedActivity.this, PartialDispatchDetailReportActivity.class);
                ArrayList<DispatchedOrder> dispatchList = data.get(position).getData();
                intent.putExtra("PartialDispatched",dispatchList);
                startActivity(intent);
            }
            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}