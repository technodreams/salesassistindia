package com.cgtechnodreams.salesassistindia.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.report.model.AttendenceReportData;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AttendenceReportAdapter extends RecyclerView.Adapter<AttendenceReportAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<AttendenceReportData> attendenceReportList = new ArrayList<>();

    public AttendenceReportAdapter(Context context, ArrayList<AttendenceReportData> attendenceReportList) {
        this.mContext = context;
        this.attendenceReportList = attendenceReportList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public AttendenceReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_attendence_report, null);
        return new AttendenceReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AttendenceReportAdapter.MyViewHolder holder, int position) {
        AttendenceReportData report = attendenceReportList.get(position);
        holder.checkIn.setText("Check In: " + report.getCheckInTime());
        holder.checkOut.setText("Check Out: " + report.getCheckOutTime());
        holder.date.setText("Date: " + report.getDate());
    }

    @Override
    public int getItemCount() {
        return attendenceReportList.size();
    }

//    public void setFilter(ArrayList<Product> newList) {
//        brandList = new ArrayList<>();
//        productList.addAll(newList);
//        notifyDataSetChanged();
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.check_in)
        TextView checkIn;
        @BindView(R.id.check_out)
        TextView checkOut;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}