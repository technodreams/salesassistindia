package com.cgtechnodreams.salesassistindia.product.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.product.model.Brand;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrandListAdapter extends RecyclerView.Adapter<BrandListAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<Brand> brandLists = new ArrayList<>();

    public BrandListAdapter(Context context, ArrayList<Brand> brandLists) {
        this.mContext = context;
        this.brandLists = brandLists;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public BrandListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_brand, null);

        return new BrandListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BrandListAdapter.MyViewHolder holder, int position) {
        Brand brand = brandLists.get(position);
        holder.name.setText(brand.getName());
    }

    @Override
    public int getItemCount() {
        return brandLists.size();
    }

    public void setFilter(ArrayList<Brand> newList) {
            brandLists = new ArrayList<>();
            brandLists.addAll(newList);
            notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}