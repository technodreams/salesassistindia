package com.cgtechnodreams.salesassistindia.product;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.BrandEnity;
import com.cgtechnodreams.salesassistindia.dms.DistributorProductAvailabilityActivity;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.fieldwork.ProductAvailabilityActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.product.adapter.BrandListAdapter;
import com.cgtechnodreams.salesassistindia.product.model.Brand;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrandActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    BrandListAdapter adapter;
    ArrayList<Brand> brand = null;
    Stockist mStockist = null;
    Distributor mDistributor = null;
    boolean isDamageStock = false;
    String date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Select Brand");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mStockist = (Stockist)getIntent().getSerializableExtra("OutletDetail");
        mDistributor = (Distributor) getIntent().getSerializableExtra("DistributorDetail");
        isDamageStock = getIntent().getBooleanExtra("IsDamage",false);
        date = getIntent().getStringExtra("Date");
        BrandEnity brandEnity = new BrandEnity(BrandActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) brandEnity.find(null);
        try {
            brand = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Brand>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(BrandActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new BrandListAdapter(BrandActivity.this, brand);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(BrandActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = null;
                        if(mStockist!=null){
                            intent = new Intent(BrandActivity.this, ProductAvailabilityActivity.class);

                            intent.putExtra("OutletDetail", mStockist);

                        }else{
                            intent = new Intent(BrandActivity.this, DistributorProductAvailabilityActivity.class);

                            intent.putExtra("DistributorDetail", mDistributor);
                        }
                        intent.putExtra("BrandId", brand.get(position).getId());
                        intent.putExtra("IsDamage",isDamageStock);
                        intent.putExtra("Date",date);
                        startActivity(intent);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                }));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            searchView = (SearchView) item.getActionView();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<Brand> newList = new ArrayList<>();
                for (Brand eachBrand : brand) {
                    String name = eachBrand.getName().toLowerCase();
                    if (name.contains(newText)) {
                        newList.add(eachBrand);
                    }

                    adapter.setFilter(newList);

                }
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                          }
                                      }
        );
        return super.onCreateOptionsMenu(menu);
    }
}
