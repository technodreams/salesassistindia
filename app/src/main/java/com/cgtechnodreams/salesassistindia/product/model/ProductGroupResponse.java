package com.cgtechnodreams.salesassistindia.product.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ProductGroupResponse extends BaseResponse {
    public ArrayList<ProductGroup> getProductGroups() {
        return productGroups;
    }

    public void setProductGroups(ArrayList<ProductGroup> productGroups) {
        this.productGroups = productGroups;
    }
    @JsonProperty("data")
    private ArrayList<ProductGroup> productGroups;
}
