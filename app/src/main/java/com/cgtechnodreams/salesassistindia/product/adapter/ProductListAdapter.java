package com.cgtechnodreams.salesassistindia.product.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<Product> productList = new ArrayList<>();
    int etProdcutIndex =0;

    public ProductListAdapter(Context context, ArrayList<Product> productList, int index) {
        this.mContext = context;
        this.productList = productList;
        prefManager = new PrefManager(mContext);
        this.etProdcutIndex = index;
    }

    @Override
    public ProductListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_list, null);

        return new ProductListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductListAdapter.MyViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.name.setText(product.getProductName());
        holder.group.setText(product.getGroupName());
        holder.brand.setText(product.getBrandName());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void setFilter(ArrayList<Product> newList) {
        productList = new ArrayList<>();
        productList.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.brand)
        TextView brand;
        @BindView(R.id.group)
        TextView group;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("Result",productList.get(getAdapterPosition()));
                    returnIntent.putExtra("Index", etProdcutIndex);
                    ((Activity)mContext).setResult(Activity.RESULT_OK,returnIntent);
                    ((Activity)mContext).finish();
                }
            });


        }
    }
}
