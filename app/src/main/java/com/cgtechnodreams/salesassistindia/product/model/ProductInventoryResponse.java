package com.cgtechnodreams.salesassistindia.product.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductInventoryResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<ProductInventory> productInventories;

    public ArrayList<ProductInventory> getProductInventories() {
        return productInventories;
    }

    public void setProductInventories(ArrayList<ProductInventory> productInventories) {
        this.productInventories = productInventories;
    }
}
