package com.cgtechnodreams.salesassistindia.product.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.product.model.ProductGroup;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductGroupAdapter extends RecyclerView.Adapter<ProductGroupAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<ProductGroup> productGroups = new ArrayList<>();

    public ProductGroupAdapter(Context context, ArrayList<ProductGroup> productGroups) {
        this.mContext = context;
        this.productGroups = productGroups;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public ProductGroupAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_brand, null);

        return new ProductGroupAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductGroupAdapter.MyViewHolder holder, int position) {
        ProductGroup product = productGroups.get(position);
        holder.name.setText(product.getName());
    }

    @Override
    public int getItemCount() {
        return productGroups.size();
    }

    public void setFilter(ArrayList<ProductGroup> newList) {
        productGroups = new ArrayList<>();
        productGroups.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}
