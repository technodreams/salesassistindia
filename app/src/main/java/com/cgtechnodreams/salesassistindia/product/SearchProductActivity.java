package com.cgtechnodreams.salesassistindia.product;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductEntity;
import com.cgtechnodreams.salesassistindia.product.adapter.ProductListAdapter;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class SearchProductActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    ProductListAdapter adapter;
    ArrayList<Product> productList= null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product_group);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Product List");
        int businessUnitId = getIntent().getIntExtra("BusinessUnitId",0);
        int priceGroupId = getIntent().getIntExtra("PriceGroupId",0);
        ProductEntity product = new ProductEntity(SearchProductActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = null;
        if(businessUnitId == 0){
            array = (JSONArray) product.find(null);
        }else if(priceGroupId!=0 && businessUnitId!=0){
            array = (JSONArray) product.findByPriceGroupAndBusinessUnit(businessUnitId,priceGroupId);
        }else{
            array = (JSONArray) product.findByBusinessUnitId(businessUnitId);
        }


        int index = getIntent().getIntExtra("Index",0);
        if (!((LocationManager) this.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            Toasty.info(SearchProductActivity.this, "Please turn on your gps.", Toast.LENGTH_SHORT,true).show();
            LocationUtils locationUtils = new LocationUtils(SearchProductActivity.this);
            locationUtils.requestLocationUpdates(null);
        }
        try {
            productList = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Product>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager favouriteListManager = new LinearLayoutManager(SearchProductActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new ProductListAdapter(SearchProductActivity.this, productList,index);
        recyclerView.setLayoutManager(favouriteListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.search_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<Product> newList = new ArrayList<>();
                for (Product product : productList) {
                    String name = product.getProductName().toLowerCase();

                    if (name.contains(newText) || product.getBrandName().toLowerCase().contains(newText) || product.getGroupName().toLowerCase().contains(newText)) {
                        newList.add(product);
                    }

                    adapter.setFilter(newList);

                }
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                          }
                                      }
        );
        return super.onCreateOptionsMenu(menu);
    }

}
