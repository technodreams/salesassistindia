package com.cgtechnodreams.salesassistindia.product.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.fieldwork.ProductAvailabilityActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.product.model.ProductSubGroup;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductSubGroupAdapter extends RecyclerView.Adapter<ProductSubGroupAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<ProductSubGroup> productSubGroups = new ArrayList<>();
    Stockist mStockist;
    public ProductSubGroupAdapter(Context context, ArrayList<ProductSubGroup> productSubGroups, Stockist stockist) {
        this.mContext = context;
        this.productSubGroups = productSubGroups;
        prefManager = new PrefManager(mContext);
        this.mStockist = stockist;
    }

    @Override
    public ProductSubGroupAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sub_group, null);

        return new ProductSubGroupAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductSubGroupAdapter.MyViewHolder holder, int position) {
        ProductSubGroup subGroup = productSubGroups.get(position);
        holder.name.setText(subGroup.getSubGroupName());
        holder.code.setText(subGroup.getCode());
    }

    @Override
    public int getItemCount() {
        return productSubGroups.size();
    }

    public void setFilter(ArrayList<ProductSubGroup> newList) {
        productSubGroups = new ArrayList<>();
        productSubGroups.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.code)
        TextView code;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ProductAvailabilityActivity.class);
                    intent.putExtra("SubGroupId", productSubGroups.get(getAdapterPosition()).getId());
                    intent.putExtra("OutletDetail", mStockist);
                    ((Activity)mContext).startActivity(intent);
//                    Intent intent = new Intent(mContext, FieldWorkActivity.class);
//                    intent.putExtra("OutletDetail",outlets.get(getAdapterPosition()));
//                    ((Activity)mContext).startActivity(intent);
                }
            });

        }
    }
}
