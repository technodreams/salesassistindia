package com.cgtechnodreams.salesassistindia.product.dataservice;


import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.product.model.BrandResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductGroupResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductInventoryResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductSubGroupResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDataService {
    private Context mContext;

    public ProductDataService(Context mContext) {
        this.mContext = mContext;
    }

    public void getProduct(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ProductResponse> callBack = Api.getService(mContext).getProduct(token);
        callBack.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getBrand(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BrandResponse> callBack = Api.getService(mContext).getBrand(token);
        callBack.enqueue(new Callback<BrandResponse>() {
            @Override
            public void onResponse(Call<BrandResponse> call, Response<BrandResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BrandResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getProductGroup(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ProductGroupResponse> callBack = Api.getService(mContext).getProductGroup(token);
        callBack.enqueue(new Callback<ProductGroupResponse>() {
            @Override
            public void onResponse(Call<ProductGroupResponse> call, Response<ProductGroupResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());

                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<ProductGroupResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getProductInventory(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ProductInventoryResponse> callBack = Api.getService(mContext).getInventories(token);
        callBack.enqueue(new Callback<ProductInventoryResponse>() {
            @Override
            public void onResponse(Call<ProductInventoryResponse> call, Response<ProductInventoryResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<ProductInventoryResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getSubGroup(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ProductSubGroupResponse> callBack = Api.getService(mContext).getProductSubGroupResponse(token);
        callBack.enqueue(new Callback<ProductSubGroupResponse>() {
            @Override
            public void onResponse(Call<ProductSubGroupResponse> call, Response<ProductSubGroupResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());

                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));


                }
            }

            @Override
            public void onFailure(Call<ProductSubGroupResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getCompetitorGroup(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ProductGroupResponse> callBack = Api.getService(mContext).getCompetitorGroup(token);
        callBack.enqueue(new Callback<ProductGroupResponse>() {
            @Override
            public void onResponse(Call<ProductGroupResponse> call, Response<ProductGroupResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());

                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<ProductGroupResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

}