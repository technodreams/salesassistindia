package com.cgtechnodreams.salesassistindia.product.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Brand implements Serializable {

    @JsonProperty("id")//should be same as database name
    private int id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("division_id")
    private int divisionId;
    @JsonProperty("business_unit_id")
    private int businessUnitId;
    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
