package com.cgtechnodreams.salesassistindia.product.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ProductResponse extends BaseResponse {

    public ProductResponse() {
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public ProductResponse(ArrayList<Product> products) {
        this.products = products;
    }

    @JsonProperty("data")
    ArrayList<Product> products;
}
