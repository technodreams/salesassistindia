package com.cgtechnodreams.salesassistindia.product.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSubGroupResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<ProductSubGroup> data;

    public ArrayList<ProductSubGroup> getData() {
        return data;
    }

    public void setData(ArrayList<ProductSubGroup> data) {
        this.data = data;
    }
}

