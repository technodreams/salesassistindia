package com.cgtechnodreams.salesassistindia.product;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;



import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductSubGroupEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.product.adapter.ProductSubGroupAdapter;
import com.cgtechnodreams.salesassistindia.product.model.ProductSubGroup;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductSubGroupActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    ProductSubGroupAdapter adapter;
    ArrayList<ProductSubGroup> productSubGroups = null;
    Stockist mStockist = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_sub_group);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Product Sub Group");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mStockist = (Stockist)getIntent().getSerializableExtra("OutletDetail");
        ProductSubGroupEntity subGroup = new ProductSubGroupEntity(ProductSubGroupActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) subGroup.find(null);


        try {
            productSubGroups = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<ProductSubGroup>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager favouriteListManager = new LinearLayoutManager(ProductSubGroupActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new ProductSubGroupAdapter(ProductSubGroupActivity.this, productSubGroups, mStockist);
        recyclerView.setLayoutManager(favouriteListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

//        recyclerView.addOnItemTouchListener(
//                new RecyclerItemClickListener(ProductSubGroupActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        Intent intent = new Intent(ProductSubGroupActivity.this, ProductAvailabilityActivity.class);
//                        intent.putExtra("SubGroupId", productSubGroups.get(position).getId());
//                        intent.putExtra("OutletDetail",mStockist);
//                        startActivity(intent);
//                        //  setResult(Activity.RESULT_OK, intent);
//                     //   finish();
//                    }
//
//                    @Override
//                    public void onLongItemClick(View view, int position) {
//                        // do whatever
//                    }
//                }));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            searchView = (SearchView) item.getActionView();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<ProductSubGroup> newList = new ArrayList<>();
                for (ProductSubGroup subGroup : productSubGroups) {
                    String name = subGroup.getSubGroupName().toLowerCase();
                    String code = subGroup.getCode().toLowerCase();
                    if (name.contains(newText) || code.contains(newText)) {
                        newList.add(subGroup);
                    }

                    adapter.setFilter(newList);

                }
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                  }
              }
        );
        return super.onCreateOptionsMenu(menu);
    }


}