package com.cgtechnodreams.salesassistindia.product.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product  implements Serializable {
    @JsonProperty("id")
    private int productId;
    @JsonProperty("name")
    private String productName;
    @JsonProperty("code")
    private String productCode;
    @JsonProperty("product_description")
    private String productDetail;
    @JsonProperty("image")
    private String productImage;
    @JsonProperty("wholesale_price")
    private String wholesalePrice;
    @JsonProperty("special_price")
    private String specialPrice;
    @JsonProperty("dealer_price")
    private String dealerPrice;
    @JsonProperty("price_group_id")
    private int priceGroupId;
    @JsonProperty("price_group_name")
    private String priceGroupName;
    @JsonProperty("price")
    private String priceGroupWise;
    @JsonProperty("retail_price")
    private String retailPrice;
    @JsonProperty("excise_duty")
    private String exciseDuty;
    @JsonProperty("vat")
    private String vat;
    @JsonProperty("segment_name")
    private String segmentName;
    @JsonProperty("segment_id")
    private String segmentId;
    @JsonProperty("brand_id")
    private int brandId;
    @JsonProperty("brand_name")
    private String brandName;
    @JsonProperty("group_id")
    private int groupId;
    @JsonProperty("group_name")
    private String groupName;
    @JsonProperty("sub_group_name")
    private String subGroupName;
    @JsonProperty("sub_group_id")
    private String subGroupId;
    @JsonProperty("division_id")
    private int divisionId;
    @JsonProperty("division_name")
    private String divisionName;
    @JsonProperty("business_unit_id")
    private int businessUnitId;
    @JsonProperty("business_unit_name")
    private String businessUnitName;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(String wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getDealerPrice() {
        return dealerPrice;
    }

    public void setDealerPrice(String dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getExciseDuty() {
        return exciseDuty;
    }

    public void setExciseDuty(String exciseDuty) {
        this.exciseDuty = exciseDuty;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getSubGroupName() {
        return subGroupName;
    }

    public void setSubGroupName(String subGroupName) {
        this.subGroupName = subGroupName;
    }

    public String getSubGroupId() {
        return subGroupId;
    }

    public void setSubGroupId(String subGroupId) {
        this.subGroupId = subGroupId;
    }


    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public int getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(int businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public int getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(int priceGroupId) {
        this.priceGroupId = priceGroupId;
    }

    public String getPriceGroupName() {
        return priceGroupName;
    }

    public void setPriceGroupName(String priceGroupName) {
        this.priceGroupName = priceGroupName;
    }
    public String getPriceGroupWise() {
        return priceGroupWise;
    }

    public void setPriceGroupWise(String priceGroupWise) {
        this.priceGroupWise = priceGroupWise;
    }
}
