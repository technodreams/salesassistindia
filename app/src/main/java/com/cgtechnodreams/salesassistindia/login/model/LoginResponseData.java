package com.cgtechnodreams.salesassistindia.login.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by user on 06/05/2018.
 *
 * {"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIyLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4xMS40Ni9pc20vcHVibGljL2FwaS9sb2dpbi9wb3N0LWRldmljZS1pbmZvIiwiaWF0IjoxNTQ3NjI4NTI1LCJleHAiOjE1NDc2MzIxMjUsIm5iZiI6MTU0NzYyODUyNSwianRpIjoic0NIcjBvaVBCdU54SG0xSiJ9.Ewx1motjt_2T0k3x5gwEiddU7I5z3j5XaMMA86oI8o0",
 * "name":"Bk Dai",
 * "email":"abc@gmail.comsa",
 * "mobile":"9841278788",
 * "role":"Promoter"}}
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponseData implements Serializable{
    public LoginResponseData(){}
    @JsonProperty("name")
    private String name;
    @JsonProperty("token")
    private String token;
    @JsonProperty("email")
    private String email;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("role")
    private String userType;
    @JsonProperty("is_supervisor")
    private boolean isSupervisor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public boolean isSupervisor() {
        return isSupervisor;
    }

    public void setSupervisor(boolean supervisor) {
        isSupervisor = supervisor;
    }
}
