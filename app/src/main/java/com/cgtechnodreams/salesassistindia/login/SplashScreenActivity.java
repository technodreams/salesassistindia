package com.cgtechnodreams.salesassistindia.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUpdatesService;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.home.MainActivity;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.google.firebase.analytics.FirebaseAnalytics;

public class SplashScreenActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
//    private static final String TAG = LoginActivity.class.getSimpleName();
//    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
//    /**
//     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
//     */
//    private static final long UPDATE_INTERVAL = 1000 * 60 * 2; // Every 60 seconds.
//
//    /**
//     * The fastest rate for active location updates. Updates will never be more frequent
//     * than this value, but they may be less frequent.
//     */
//    private static final long FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL / 2; // Every 30 seconds
//
//    /**
//     * The max time before batched results are delivered by location services. Results may be
//     * delivered sooner than this interval.
//     */
//    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 5; // Every 5 minutes.
//    private static final int REQUEST_CHECK_SETTINGS = 100;
    PrefManager prefManager = null;
//    LocationUtils locationUtils = null;
    TextView versionNumber;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
//    private LocationRequest mLocationRequest;
    /**
     * Provides access to the Fused Location Provider API.
     */
//    private FusedLocationProviderClient mFusedLocationClient;
//    private SettingsClient mSettingsClient;
//    private LocationSettingsRequest mLocationSettingsRequest;
    private FirebaseAnalytics mFirebaseAnalytics;

    ProgressBar progressBar;


    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;

    // A reference to the service used to get location updates.
  //  private LocationUpdatesService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    // Monitors the state of the connection to the service.
//    private final ServiceConnection mServiceConnection = new ServiceConnection() {
//
//        @Override
//        public void onServiceConnected(ComponentName name, IBinder service) {
//            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
//            mService = binder.getService();
//            mBound = true;
//        }
//
//        @Override
//        public void onServiceDisconnected(ComponentName name) {
//            mService = null;
//            mBound = false;
//        }
//    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        myReceiver = new MyReceiver();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        prefManager = new PrefManager(this);
        versionNumber = findViewById(R.id.version);
      // versionNumber.setText("Version: " + getResources().getString(R.string.version_number));
        progressBar = findViewById(R.id.progressbar);
        SQLiteDbHelper.getDbAdapter(this);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }


//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//        mSettingsClient = LocationServices.getSettingsClient(this);
//
//        locationUtils = new LocationUtils(SplashScreenActivity.this);
//        if (!((LocationManager) SplashScreenActivity.this.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            locationUtils.requestLocationUpdates(null);//goes to failure case and show location enalbe dialog
//            Toasty.info(SplashScreenActivity.this, "Please turn on your gps", Toast.LENGTH_SHORT).show();
//        }
//
//
//        locationUtils.requestAllPermission();
//        if (!locationUtils.checkPermissions()) {
//            locationUtils.requestPermissions();
//        }else{
//            locationUtils.requestLocationUpdates(null);
//           // locationUtils.setLastKnownLocation();
//        }

//        if (ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                locationUtils.requestAllPermission();
//
//        }

        findViewById(R.id.startApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!((LocationManager) SplashScreenActivity.this.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//                    locationUtils.requestLocationUpdates(null);
//                    Toasty.info(SplashScreenActivity.this, "Please turn on your gps", Toast.LENGTH_SHORT).show();
//                }
//                else {
//                    if (ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                            && ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                         locationUtils.requestAllPermission();
//
//                    } else {
//                        if (!checkPermissions()) {
//                            requestPermissions();
//                        } else {
//                            mService.requestLocationUpdates();
//                            startApp();
//                        }
//
//                    }
//                }
                startApp();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

//        PreferenceManager.getDefaultSharedPreferences(this)
//                .registerOnSharedPreferenceChangeListener(this);
//        // Bind to the service. If the service is in foreground mode, this signals to the service
//        // that since this activity is in the foreground, the service can exit foreground mode.
//        bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection,
//                Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onStop() {
//        if (mBound) {
//            // Unbind from the service. This signals to the service that this activity is no longer
//            // in the foreground, and the service can respond by promoting itself to a foreground
//            // service.
//            unbindService(mServiceConnection);
//            mBound = false;
//        }
//        PreferenceManager.getDefaultSharedPreferences(this)
//                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    private void startApp() {
        progressBar.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */


            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                if(prefManager.getAuthKey()==null || prefManager.getAuthKey().equals("")){

                    startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                    finish();
                }else{
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                    finish();
                }

            }
        }, 3000);
    }

    /**
     * Returns the current state of the permissions needed.
     */
//    private boolean checkPermissions() {
//        return  PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION);
//    }
//
//    private void requestPermissions() {
//        boolean shouldProvideRationale =
//                ActivityCompat.shouldShowRequestPermissionRationale(this,
//                        Manifest.permission.ACCESS_FINE_LOCATION);
//
//        // Provide an additional rationale to the user. This would happen if the user denied the
//        // request previously, but didn't check the "Don't ask again" checkbox.
//        if (shouldProvideRationale) {
//            Log.i(TAG, "Displaying permission rationale to provide additional context.");
//            Snackbar.make(
//                    findViewById(R.id.activity_main),
//                    R.string.permission_rationale,
//                    Snackbar.LENGTH_INDEFINITE)
//                    .setAction(R.string.ok, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            // Request permission
//                            ActivityCompat.requestPermissions(SplashScreenActivity.this,
//                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                                    REQUEST_PERMISSIONS_REQUEST_CODE);
//                        }
//                    })
//                    .show();
//        } else {
//            Log.i(TAG, "Requesting permission");
//            // Request permission. It's possible this can be auto answered if device policy
//            // sets the permission in a given state or the user denied the permission
//            // previously and checked "Never ask again".
//            ActivityCompat.requestPermissions(SplashScreenActivity.this,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                    REQUEST_PERMISSIONS_REQUEST_CODE);
//        }
//    }

    /**
     * Callback received when a permissions request has been completed.
     */
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        Log.i(TAG, "onRequestPermissionResult");
//        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
//            if (grantResults.length <= 0) {
//                // If user interaction was interrupted, the permission request is cancelled and you
//                // receive empty arrays.
//                Log.i(TAG, "User interaction was cancelled.");
//            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // Permission was granted.
//                mService.requestLocationUpdates();
//            } else {
//                // Permission denied.
//                // setButtonsState(false);
//                Snackbar.make(
//                        findViewById(R.id.activity_main),
//                        R.string.permission_denied_explanation,
//                        Snackbar.LENGTH_INDEFINITE)
//                        .setAction(R.string.settings, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                // Build intent that displays the App settings screen.
//                                Intent intent = new Intent();
//                                intent.setAction(
//                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                Uri uri = Uri.fromParts("package",
//                                        BuildConfig.APPLICATION_ID, null);
//                                intent.setData(uri);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
//                            }
//                        })
//                        .show();
//            }
//        }
//    }
//

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
        if (s.equals(LocationUtils.KEY_REQUESTING_LOCATION_UPDATES)) {
            // setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES, false));
        }
    }


    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(SplashScreenActivity.this, LocationUtils.getLocationText(location),
//                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}