package com.cgtechnodreams.salesassistindia.login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUpdatesService;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.home.MainActivity;
import com.cgtechnodreams.salesassistindia.login.data.LoginDataService;
import com.cgtechnodreams.salesassistindia.login.model.DeviceModel;
import com.cgtechnodreams.salesassistindia.login.model.LoginModel;
import com.cgtechnodreams.salesassistindia.login.model.LoginResponse;
import com.cgtechnodreams.salesassistindia.utils.CustomEditText;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MinLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class  LoginActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {



    @MinLength(value = 10, messageId = R.string.validation_valid_mobile_number, order = 2)
    @BindView(R.id.input_mobile_number)
    public CustomEditText inputMobileNumber;
    @NotEmpty(messageId = R.string.password_empty)
    @BindView(R.id.input_password)
    public CustomEditText inputPassword;

    ProgressDialog dialog;
    PrefManager prefManager = null;


    private static final String TAG = LoginActivity.class.getSimpleName();
    LocationUtils locationUtils = null;
    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    // The BroadcastReceiver used to listen from broadcasts from the service.
 //   private MyReceiver myReceiver;

    // A reference to the service used to get location updates.
 //   private LocationUpdatesService mService = null;

    // Tracks the bound state of the service.
  //  private boolean mBound = false;

    // Monitors the state of the connection to the service.
  /*  private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

      //  myReceiver = new MyReceiver();
       // locationUtils = new LocationUtils(LoginActivity.this);
        prefManager = new PrefManager(this);
        FormValidator.startLiveValidation(this, findViewById(R.id.container), new SimpleErrorPopupCallback(this, false));
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

//        locationUtils.requestAllPermission();
//        if (!locationUtils.checkPermissions()) {
//            locationUtils.requestPermissions();
//        }else{
//            locationUtils.setLastKnownLocation();
//        }



    }

    @OnClick({R.id.btnLogin})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    doLogin();
                }
                break;

        }

    }

    private void doLogin() {
        attemptLogin();
     //   if (((LocationManager) this.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        //    if (Utils.isConnectionAvailable(LoginActivity.this) ){
//                LocationUtils locationUtils = new LocationUtils(LoginActivity.this);
//                locationUtils.requestLocationUpdates(null);
//                if(prefManager.getLatitude()==null || prefManager.getLatitude().equalsIgnoreCase("")||prefManager.getLongitude() == null||prefManager.getLongitude().equalsIgnoreCase("")){
//                    Toasty.info(LoginActivity.this,"Unable to get your current location please check your gps and try again!!", Toasty.LENGTH_LONG).show();
//                }else{
//                    attemptLogin();
//                }
//
//            }
//            else
//                Toasty.info(LoginActivity.this, "Please check your internet connection!!", Toast.LENGTH_SHORT,true).show();
//        }else{
//
//            locationUtils.requestLocationUpdates(null);
//            Toasty.info(LoginActivity.this, "Please turn on your gps.", Toast.LENGTH_SHORT,true).show();
//        }
    }

    private void attemptLogin() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Authenticating your credential...");
        dialog.setCancelable(false);
        dialog.show();
        final LoginModel request = new LoginModel();
        request.setPassword(inputPassword.getText().toString());
        request.setMobile(inputMobileNumber.getText().toString());
        request.setDeviceId(Utils.getDeviceIMEI(LoginActivity.this));
        request.setLatitude(prefManager.getLatitude());
        request.setLongitude(prefManager.getLongitude());
        LoginDataService loginService = new LoginDataService(LoginActivity.this);
        loginService.login(request, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse.getStatusCode() != 200) {//failure case
                    if (loginResponse.getStatusCode() == APIConstants.ERROR_CODE_DEVICE_NOT_REGISTERED) {
                        showDeviceRegistration();
                    } else if (loginResponse.getStatusCode() == APIConstants.ERROR_CODE_DEVICE_MISMATCH) {
                       // locationUtils.removeLocationUpdates(null);
                        Toasty.error(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                        return;
                    } else {
                      //  locationUtils.removeLocationUpdates(null);
                        Toasty.error(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    }
                } else { //success case
                    String appVersionName="";
                    int appVersionNumber=0;
                    try {
                        PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        appVersionName = pInfo.versionName;
                        appVersionName = pInfo.versionName;
                        appVersionNumber = pInfo.versionCode;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    LogUtils.setUserActivity(LoginActivity.this, "Login", "Login Successfull. App Vesrion: " + appVersionName +"App Version Code: " + appVersionNumber);
                    Toasty.success(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                    if (!checkPermissions()) {
//                        requestPermissions();
//                    } else {
//                        mService.requestLocationUpdates();
//                    }

                    prefManager.setAuthKey(loginResponse.getData().getToken());
                    prefManager.setEmail(loginResponse.getData().getEmail());
                    prefManager.setName(loginResponse.getData().getName());
                    prefManager.setMobile(loginResponse.getData().getMobile());
                    prefManager.setRole(loginResponse.getData().getUserType());
                    prefManager.setIsSupervisor(loginResponse.getData().isSupervisor());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("DownloadRequire",true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            //    locationUtils.removeLocationUpdates(null);
            }
        });

    }

    private void showDeviceRegistration() {
        new AlertDialog.Builder(this)
                .setTitle("Device Registration")
                .setMessage("No device has been registered for this user. Do you want to register "
                        + Build.MANUFACTURER + " " + Build.MODEL + " with " + "IMEI ID:" + Utils.getDeviceIMEI(this)
                )
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        updateDeviceDetail();
                        // Toasty.makeText(LoginActivity.this, "Yes", Toast.LENGTH_SHORT,true).show();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void updateDeviceDetail() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        final DeviceModel request = new DeviceModel();
        request.setPassword(inputPassword.getText().toString());
        request.setMobile(inputMobileNumber.getText().toString());
        request.setDeviceId(Utils.getDeviceIMEI(LoginActivity.this));
        request.setOsVersion("Version:" + Build.VERSION.RELEASE + " SDK:" +Build.VERSION.SDK_INT );
        request.setDeviceModel(Build.MANUFACTURER + " " + Build.MODEL);
        LoginDataService deviceRegister = new LoginDataService(LoginActivity.this);
        deviceRegister.registerDevice(request, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse.getStatusCode() != 200) {//failure case
                    locationUtils.removeLocationUpdates(null);
                    if (loginResponse.getStatusCode() == APIConstants.ERROR_CODE_DEVICE_NOT_REGISTERED) {
                        showDeviceRegistration();
                    } else if (loginResponse.getStatusCode() == APIConstants.ERROR_CODE_DEVICE_MISMATCH) {
                        Toasty.error(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    } else {
                        Toasty.error(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    }
                } else { //success case
                    Toasty.success(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    prefManager.setAuthKey(loginResponse.getData().getToken());
                    prefManager.setEmail(loginResponse.getData().getEmail());
                    prefManager.setName(loginResponse.getData().getName());
                    prefManager.setMobile(loginResponse.getData().getMobile());
                    prefManager.setRole(loginResponse.getData().getUserType());
                    prefManager.setIsSupervisor(loginResponse.getData().isSupervisor());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("Role", loginResponse.getData().getUserType());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toasty.error(LoginActivity.this, reason.getErrors(), Toast.LENGTH_SHORT,true).show();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
//
//        PreferenceManager.getDefaultSharedPreferences(this)
//                .registerOnSharedPreferenceChangeListener(this);
//        // Bind to the service. If the service is in foreground mode, this signals to the service
//        // that since this activity is in the foreground, the service can exit foreground mode.
//        bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection,
//                Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
//                new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    @Override
    protected void onPause() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onStop() {
//        if (mBound) {
//            // Unbind from the service. This signals to the service that this activity is no longer
//            // in the foreground, and the service can respond by promoting itself to a foreground
//            // service.
//            unbindService(mServiceConnection);
//            mBound = false;
//        }
//        PreferenceManager.getDefaultSharedPreferences(this)
//                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    /**
     * Returns the current state of the permissions needed.
     */
//    private boolean checkPermissions() {
//        return  PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION);
//    }
//
//    private void requestPermissions() {
//        boolean shouldProvideRationale =
//                ActivityCompat.shouldShowRequestPermissionRationale(this,
//                        Manifest.permission.ACCESS_FINE_LOCATION);
//
//        // Provide an additional rationale to the user. This would happen if the user denied the
//        // request previously, but didn't check the "Don't ask again" checkbox.
//        if (shouldProvideRationale) {
//            Log.i(TAG, "Displaying permission rationale to provide additional context.");
//            Snackbar.make(
//                    findViewById(R.id.activity_main),
//                    R.string.permission_rationale,
//                    Snackbar.LENGTH_INDEFINITE)
//                    .setAction(R.string.ok, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            // Request permission
//                            ActivityCompat.requestPermissions(LoginActivity.this,
//                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                                    REQUEST_PERMISSIONS_REQUEST_CODE);
//                        }
//                    })
//                    .show();
//        } else {
//            Log.i(TAG, "Requesting permission");
//            // Request permission. It's possible this can be auto answered if device policy
//            // sets the permission in a given state or the user denied the permission
//            // previously and checked "Never ask again".
//            ActivityCompat.requestPermissions(LoginActivity.this,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                    REQUEST_PERMISSIONS_REQUEST_CODE);
//        }
//    }
//
//    /**
//     * Callback received when a permissions request has been completed.
//     */
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        Log.i(TAG, "onRequestPermissionResult");
//        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
//            if (grantResults.length <= 0) {
//                // If user interaction was interrupted, the permission request is cancelled and you
//                // receive empty arrays.
//                Log.i(TAG, "User interaction was cancelled.");
//            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // Permission was granted.
//                mService.requestLocationUpdates();
//            } else {
//                // Permission denied.
//                // setButtonsState(false);
//                Snackbar.make(
//                        findViewById(R.id.activity_main),
//                        R.string.permission_denied_explanation,
//                        Snackbar.LENGTH_INDEFINITE)
//                        .setAction(R.string.settings, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                // Build intent that displays the App settings screen.
//                                Intent intent = new Intent();
//                                intent.setAction(
//                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                Uri uri = Uri.fromParts("package",
//                                        BuildConfig.APPLICATION_ID, null);
//                                intent.setData(uri);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
//                            }
//                        })
//                        .show();
//            }
//        }
//    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
        if (s.equals(LocationUtils.KEY_REQUESTING_LOCATION_UPDATES)) {
            // setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES, false));
        }
    }


    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
//    private class MyReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
//            if (location != null) {
////                Toast.makeText(LoginActivity.this, LocationUtils.getLocationText(location),
////                        Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
}
