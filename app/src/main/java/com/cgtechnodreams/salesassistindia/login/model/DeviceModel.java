package com.cgtechnodreams.salesassistindia.login.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by user on 16/01/2019.
 */

public class DeviceModel extends LoginModel{
    @JsonProperty("device_model")
    private String deviceModel;
    @JsonProperty("os_version")
    private String osVersion;
    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }


    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel =deviceModel;
    }


}
