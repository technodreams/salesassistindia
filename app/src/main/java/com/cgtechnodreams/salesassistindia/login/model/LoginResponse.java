package com.cgtechnodreams.salesassistindia.login.model;


import com.cgtechnodreams.salesassistindia.api.BaseResponse;

import java.io.Serializable;

/**
 * Created by user on 06/05/2018.
 */

public class LoginResponse extends BaseResponse implements Serializable {
    public LoginResponse(){}
    private LoginResponseData data;
    public LoginResponseData getData() {
        return data;
    }
    public void setData(LoginResponseData data) {
        this.data = data;
    }
}
