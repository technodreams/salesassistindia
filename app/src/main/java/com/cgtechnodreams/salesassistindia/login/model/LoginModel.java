package com.cgtechnodreams.salesassistindia.login.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by user on 30/01/2018.
 */

public class LoginModel {
    public LoginModel(){}
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("password")
    private String password;
    @JsonProperty("device_id")
    private String deviceId;
    @JsonProperty("latitude")
    private String latitude;
    @JsonProperty("longitude")
    private String longitude;
    @JsonProperty("is_supervisor")
    private String isSupervisor;
    public String getIsSupervisor() {
        return isSupervisor;
    }

    public void setIsSupervisor(String isSupervisor) {
        this.isSupervisor = isSupervisor;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
