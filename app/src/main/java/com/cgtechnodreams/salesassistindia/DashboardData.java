package com.cgtechnodreams.salesassistindia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by user on 03/01/2019.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardData {
    @JsonProperty("Title")
    String title;
    @JsonProperty("Image")
    String image;
    @JsonProperty("Activity")
    String activityName;

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
