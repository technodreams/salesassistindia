package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SalesOrderEntity;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesOrder;
import com.cgtechnodreams.salesassistindia.sync.Sync;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PendingSyncActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    PendingSyncAdapter adapter;
    ArrayList<SalesOrder> salesOrders = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_sync);
        ButterKnife.bind(this);
        toolbar.setTitle("Pending Sync");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //int productId = getIntent().getIntExtra("ProductId", 0);
       loadData();
    }

    private void loadData() {
        SalesOrderEntity salesOrderEntity = new SalesOrderEntity(PendingSyncActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) salesOrderEntity.find(null);
        try {
            salesOrders = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<SalesOrder>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutmanager = new LinearLayoutManager(PendingSyncActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new PendingSyncAdapter(PendingSyncActivity.this, salesOrders);
        recyclerView.setLayoutManager(layoutmanager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(PendingSyncActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
//                        Intent returnIntent = new Intent();
//                      //  returnIntent.putExtra("Result", salesOrders.get(position));
//                      //  setResult(Activity.RESULT_OK, returnIntent);
//                        finish();
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.menu_pending_sync, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<SalesOrder> newList = new ArrayList<>();
                for (SalesOrder saleOrder : salesOrders) {
                    String outletName = saleOrder.getOutletName().toLowerCase();
                    String warehouse = saleOrder.getWarehouseName().toLowerCase();
                    String productName = saleOrder.getProductName().toLowerCase();

                    if (outletName.contains(newText) || warehouse.contains(newText) || productName.contains(newText)) {
                        newList.add(saleOrder);
                    }

                    adapter.setFilter(newList);

                }
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {

              }
          }
        );
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_sync:
                Sync sync = new Sync(PendingSyncActivity.this);
                sync.uploadSalesOrder();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}