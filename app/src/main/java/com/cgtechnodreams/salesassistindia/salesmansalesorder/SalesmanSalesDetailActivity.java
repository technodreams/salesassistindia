package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesmanSalesDetail;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesmanSalesDetailActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SalesmanSalesDetailAdapter adapter = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    PrefManager prefManager = null;
    ArrayList<SalesmanSalesDetail> salesmanSalesDetails = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesman_sales_detail);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        salesmanSalesDetails = (ArrayList<SalesmanSalesDetail>) getIntent().getSerializableExtra("SalesDetail");

        setData(salesmanSalesDetails);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void setData(ArrayList<SalesmanSalesDetail> salesmanSalesDetails) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(SalesmanSalesDetailActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new SalesmanSalesDetailAdapter(SalesmanSalesDetailActivity.this, salesmanSalesDetails);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
}

