package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SalesOrderEntity;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesOrder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class ConfirmSalesOrderActivity extends AppCompatActivity {
    @BindView(R.id.discountPercentage)
    TextView tvDiscountPercentage;
    @BindView(R.id.productName)
    TextView tvProductName;
    @BindView(R.id.productQuantity)
    TextView productQuantity;
    @BindView(R.id.warehouse)
    TextView tvWareHouse;
    @BindView(R.id.stockist)
    TextView tvStockist;
    SalesOrder salesOrder = null;
    @BindView(R.id.orderType)
    RadioGroup radioGroupOrderType;
    @BindView(R.id.remarks)
    EditText remarks;
    boolean isUpdate = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_sales_order);
        ButterKnife.bind(this);
        salesOrder = (SalesOrder) getIntent().getSerializableExtra("SalesOrder");
        isUpdate = getIntent().getBooleanExtra("IsUpdate",false);
        LocationUtils locationUtils = new LocationUtils(ConfirmSalesOrderActivity.this);
        locationUtils.requestLocationUpdates();
        if(salesOrder!=null){
            tvDiscountPercentage.setText("Discount Percentage: " + salesOrder.getDiscountPercentage() );
            tvProductName.setText(salesOrder.getProductName());
            tvStockist.setText(salesOrder.getOutletName());
            tvWareHouse.setText(salesOrder.getWarehouseName());
            productQuantity.setText("Quantity: " + salesOrder.getQuantity());
            salesOrder.setVisitPhone("By Visit"); // default value
        }else{

        }
        radioGroupOrderType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    salesOrder.setVisitPhone(checkedRadioButton.getText().toString());

                }
            }
        });

    }
    @OnClick({R.id.btnConfirm,R.id.btnEdit})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnConfirm:
                if(!isUpdate){
                    salesOrder.setRemarks(remarks.getText().toString());
                    SalesOrderEntity salesOrderEntity = new SalesOrderEntity(ConfirmSalesOrderActivity.this);
                    long result = salesOrderEntity.insert(salesOrder);
                    if(result>=1){
                        Toasty.info(ConfirmSalesOrderActivity.this,"Sales order placed successfully",Toast.LENGTH_SHORT,true).show();
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("IsUpdate",isUpdate);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }else{
                        Toasty.error(ConfirmSalesOrderActivity.this,"Unable to insert the sales order",Toast.LENGTH_SHORT,true).show();
                        finish();
                    }
                }else{
                    salesOrder.setRemarks(remarks.getText().toString());
                    SalesOrderEntity salesOrderEntity = new SalesOrderEntity(ConfirmSalesOrderActivity.this);
                    long result = salesOrderEntity.update(salesOrder);
                    if(result == 0){
                        Toasty.error(ConfirmSalesOrderActivity.this,"Unable to update the order",Toast.LENGTH_SHORT,true).show();
                         finish();
                    }else{
                        Toasty.success(ConfirmSalesOrderActivity.this,"Updated successfully",Toast.LENGTH_SHORT,true).show();
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Update",true);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                }

                break;

            case R.id.btnEdit:
                finish();
                break;

        }

    }
}
