package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesOrderTarget;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesOrderTargetAdapter extends RecyclerView.Adapter<SalesOrderTargetAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<SalesOrderTarget> targets = new ArrayList<>();

    public SalesOrderTargetAdapter(Context context, ArrayList<SalesOrderTarget> targets) {
        this.mContext = context;
        this.targets = targets;
    }

    @Override
    public SalesOrderTargetAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sales_order_target, null);

        return new SalesOrderTargetAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SalesOrderTargetAdapter.MyViewHolder holder, int position) {
        SalesOrderTarget eachTarget = targets.get(position);
        holder.tvAmount.setText("Amount: " + eachTarget.getAmount());
        holder.tvMonth.setText("Month: " +  eachTarget.getMonth());
        holder.tvQuantity.setText("Qty: " + eachTarget.getQuantity());
        holder.tvYear.setText("Year: " +  eachTarget.getYear());

    }

    @Override
    public int getItemCount() {
        return targets.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.year)
        TextView tvYear;
        @BindView(R.id.month)
        TextView tvMonth;
        @BindView(R.id.amount)
        TextView tvAmount;
        @BindView(R.id.quantity)
        TextView tvQuantity;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);



        }
    }
}

