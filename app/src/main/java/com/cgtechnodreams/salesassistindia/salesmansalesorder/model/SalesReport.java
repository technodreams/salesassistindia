package com.cgtechnodreams.salesassistindia.salesmansalesorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesReport implements Serializable {
    @JsonProperty("id")
    private int id;
    @JsonProperty("outlet_id")
    private int outletId;
    @JsonProperty("outlet_name")
    private String outletName;
    @JsonProperty("salesman_id")
    private int salesmanId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("date_np")
    private String date;
    @JsonProperty("pending")
    private String pendingDate;
    @JsonProperty("rejected")
    private String rejectedDate;
    @JsonProperty("approved")
    private String approvedDate;
    @JsonProperty("delivered")
    private String deliveredDate;
    @JsonProperty("created_at")
    private String createdDate;
    @JsonProperty("updated_at")
    private String updateDate;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("grand_total")
    private double grandTotal;
    @JsonProperty("taxable_amount")
    private String taxableAmount;
    @JsonProperty("sales_details")
    private ArrayList<SalesmanSalesDetail> salesDetail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOutletId() {
        return outletId;
    }

    public void setOutletId(int outletId) {
        this.outletId = outletId;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public int getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(int salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPendingDate() {
        return pendingDate;
    }

    public void setPendingDate(String pendingDate) {
        this.pendingDate = pendingDate;
    }

    public String getRejectedDate() {
        return rejectedDate;
    }

    public void setRejectedDate(String rejectedDate) {
        this.rejectedDate = rejectedDate;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(String deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(String taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public ArrayList<SalesmanSalesDetail> getSalesDetail() {
        return salesDetail;
    }

    public void setSalesDetail(ArrayList<SalesmanSalesDetail> salesDetail) {
        this.salesDetail = salesDetail;
    }
}
