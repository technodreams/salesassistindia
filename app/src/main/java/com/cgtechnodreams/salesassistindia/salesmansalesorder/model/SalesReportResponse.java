package com.cgtechnodreams.salesassistindia.salesmansalesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesReportResponse extends BaseResponse implements Serializable {
    @JsonProperty("data")
    private ArrayList<SalesReport> salesReportData;

    public ArrayList<SalesReport> getSalesReportData() {
        return salesReportData;
    }

    public void setSalesReportData(ArrayList<SalesReport> salesReportData) {
        this.salesReportData = salesReportData;
    }
}
