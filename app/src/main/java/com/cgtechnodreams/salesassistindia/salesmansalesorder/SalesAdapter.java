package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesReport;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.List;

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.MyViewHolder> {
    Context mContext;
    List<SalesReport> mData;
    public SalesAdapter(List<SalesReport> data, Context ctx) {
        this.mData = data;
        this.mContext = ctx;

    }

    @NonNull
    @Override
    public SalesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sales_report, parent, false);
        return new SalesAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesAdapter.MyViewHolder holder, int position) {
       if(mData.get(position).getStatus().equalsIgnoreCase("Pending")){
           holder.view.setBackgroundColor(Color.parseColor("#25aae1"));
       }else if(mData.get(position).getStatus().equalsIgnoreCase("Approved")){
           holder.view.setBackgroundColor(Color.parseColor("#FF0FB939"));
       }
        holder.stockist.setText(mData.get(position).getOutletName());
        holder.status.setText(mData.get(position).getStatus());
        holder.requestedDate.setText(mData.get(position).getDate());
        holder.grandTotal.setText(Utils.round(mData.get(position).getGrandTotal(),2)+"");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView stockist;
        TextView grandTotal;
        TextView requestedDate;
        TextView status;
        View view;
        public MyViewHolder(View itemView) {
            super(itemView);
            stockist = itemView.findViewById(R.id.stockist);
            grandTotal = itemView.findViewById(R.id.grandTotal);
            requestedDate = itemView.findViewById(R.id.requestedDate);
            status = itemView.findViewById(R.id.status);
            view= itemView.findViewById(R.id.statusColor);

        }
    }

}

