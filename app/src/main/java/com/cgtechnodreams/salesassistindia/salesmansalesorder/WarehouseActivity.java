package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductInventoryEntity;
import com.cgtechnodreams.salesassistindia.product.model.ProductInventory;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WarehouseActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    WarehouseAdapter adapter;
    ArrayList<ProductInventory> inventories = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warehouse);
        ButterKnife.bind(this);
        toolbar.setTitle("Select Warehouse");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        int productId = getIntent().getIntExtra("ProductId",0);
        ProductInventoryEntity inventory = new ProductInventoryEntity(WarehouseActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) inventory.findByProductId(productId);


        try {
            inventories = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<ProductInventory>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutmanager = new LinearLayoutManager(WarehouseActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new WarehouseAdapter(WarehouseActivity.this, inventories);
        recyclerView.setLayoutManager(layoutmanager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(WarehouseActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("Result", inventories.get(position));
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.search_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<ProductInventory> newList = new ArrayList<>();
                for (ProductInventory warehouse : inventories) {
                    String name = warehouse.getWarehouseName().toLowerCase();

                    if (name.contains(newText)) {
                        newList.add(warehouse);
                    }

                    adapter.setFilter(newList);

                }
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {

              }
          }
        );
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
