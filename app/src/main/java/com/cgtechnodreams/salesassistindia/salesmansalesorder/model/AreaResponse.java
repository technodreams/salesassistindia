package com.cgtechnodreams.salesassistindia.salesmansalesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AreaResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<Area> data;

    public ArrayList<Area> getData() {
        return data;
    }

    public void setData(ArrayList<Area> data) {
        this.data = data;
    }
}
