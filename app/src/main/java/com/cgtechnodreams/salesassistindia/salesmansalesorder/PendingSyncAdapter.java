package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SalesOrderEntity;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesOrder;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PendingSyncAdapter extends RecyclerView.Adapter<PendingSyncAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<SalesOrder> salesOrders = new ArrayList<>();

    public PendingSyncAdapter(Context context, ArrayList<SalesOrder> salesOrders) {
        this.mContext = context;
        this.salesOrders = salesOrders;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public PendingSyncAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pending_sync, null);

        return new PendingSyncAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PendingSyncAdapter.MyViewHolder holder, int position) {
        SalesOrder eachOrder = salesOrders.get(position);
        holder.stockist.setText(eachOrder.getOutletName());
        holder.productName.setText(eachOrder.getProductName());
        holder.tvQuantity.setText("Qty" + eachOrder.getQuantity());
        holder.tvWarehouse.setText(eachOrder.getWarehouseName());

    }

    @Override
    public int getItemCount() {
        return salesOrders.size();
    }


    public void setFilter(ArrayList<SalesOrder> newList) {
        salesOrders = new ArrayList<>();
        salesOrders.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.stockist)
        TextView stockist;
        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.warehouse)
        TextView tvWarehouse;

        @BindView(R.id.quantity)
        TextView tvQuantity;
        @BindView(R.id.edit)
        ImageView imgEdit;
        @BindView(R.id.delete)
        ImageView imgDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext,SalesOrderActivity.class);
                    intent.putExtra("SalesOrder",salesOrders.get(getAdapterPosition()));
                    mContext.startActivity(intent);
                }
            });
            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SalesOrderEntity salesOrderEntity = new SalesOrderEntity(mContext);
                    salesOrderEntity.deleteById(salesOrders.get(getAdapterPosition()).getOrderId());
                    salesOrders.remove(salesOrders.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });


        }
    }
}
