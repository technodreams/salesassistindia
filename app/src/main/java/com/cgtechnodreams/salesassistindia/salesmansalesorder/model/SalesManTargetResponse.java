package com.cgtechnodreams.salesassistindia.salesmansalesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesManTargetResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<SalesOrderTarget> salesOrderTargetArrayList;

    public ArrayList<SalesOrderTarget> getSalesOrderTargetArrayList() {
        return salesOrderTargetArrayList;
    }

    public void setSalesOrderTargetArrayList(ArrayList<SalesOrderTarget> salesOrderTargetArrayList) {
        this.salesOrderTargetArrayList = salesOrderTargetArrayList;
    }
}
