package com.cgtechnodreams.salesassistindia.salesmansalesorder.data;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ApiUploadFile;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorSalesOrder;
import com.cgtechnodreams.salesassistindia.fieldwork.model.ActualDispatchedResponse;
import com.cgtechnodreams.salesassistindia.fieldwork.model.DispatchedOrderResponse;
import com.cgtechnodreams.salesassistindia.report.model.DispatchListReportResponse;
import com.cgtechnodreams.salesassistindia.report.model.PartialDispatchedResponse;
import com.cgtechnodreams.salesassistindia.report.model.ReportRequest;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesReportResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionDetailModel;
import com.cgtechnodreams.salesassistindia.salesorder.model.StockistDailySales;
import com.cgtechnodreams.salesassistindia.salesorder.model.SalesOrderWithRemarks;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesOrderDataService {
    private Context mContext;
    public SalesOrderDataService(Context mContext) {
        this.mContext = mContext;
    }
    //FMCGSAlesORder is repalcew ith SAlesOrderWithRemarks class
    public void postSalesOrder(String token, ArrayList<SalesOrderWithRemarks> salesOrders, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postSalesOrder(token, salesOrders);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void postCollection(String token, ArrayList<CollectionDetailModel> collectionDetailModels, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postCollectionDetail(token, collectionDetailModels);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getSalesOrderReport(String token, ReportRequest request,  final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<SalesReportResponse> callBack = Api.getService(mContext).getSalesOrderReport(token, request);
        callBack.enqueue(new Callback<SalesReportResponse>() {
            @Override
            public void onResponse(Call<SalesReportResponse> call, Response<SalesReportResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));


                }
            }

            @Override
            public void onFailure(Call<SalesReportResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getOutletSalesOrderReport(String token, int outletId, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<SalesReportResponse> callBack = Api.getService(mContext).getOutletSalesResponse(outletId, token);
        callBack.enqueue(new Callback<SalesReportResponse>() {
            @Override
            public void onResponse(Call<SalesReportResponse> call, Response<SalesReportResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<SalesReportResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getActualDispatched(String token, int outletId, String fromDate, String toDate, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ActualDispatchedResponse> callBack = Api.getService(mContext).getDispatched(outletId,token,fromDate,toDate);
        callBack.enqueue(new Callback<ActualDispatchedResponse>() {
            @Override
            public void onResponse(Call<ActualDispatchedResponse> call, Response<ActualDispatchedResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<ActualDispatchedResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getAllDispatched(String token,String fromDate, String toDate, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<DispatchListReportResponse> callBack = Api.getService(mContext).getAllDispatched(token,fromDate,toDate);
        callBack.enqueue(new Callback<DispatchListReportResponse>() {
            @Override
            public void onResponse(Call<DispatchListReportResponse> call, Response<DispatchListReportResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<DispatchListReportResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getPartialDispatchedOrder(String token, int outletId, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<DispatchedOrderResponse> callBack = Api.getService(mContext).getPartialDispatched(outletId, token);
        callBack.enqueue(new Callback<DispatchedOrderResponse>() {
            @Override
            public void onResponse(Call<DispatchedOrderResponse> call, Response<DispatchedOrderResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<DispatchedOrderResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void getAllPartialDispatchedOrderReport(String token,String from, String to, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<PartialDispatchedResponse> callBack = Api.getService(mContext).getAllPartialDispatchedReport(token,from,to);
        callBack.enqueue(new Callback<PartialDispatchedResponse>() {
            @Override
            public void onResponse(Call<PartialDispatchedResponse> call, Response<PartialDispatchedResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<PartialDispatchedResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void postDistributorDailySales(String token, ArrayList<StockistDailySales> dailySales, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postStockistDailySales(token,dailySales);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);

            }
        });
    }
    public void uploadImages(String token, RequestBody description, RequestBody size, List<MultipartBody.Part> files, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ResponseBody> callBack = ApiUploadFile.getService(mContext).uploadCollectionMultipleFilesDynamic(token,description, size,files);
        callBack.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));


                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void postDistributorSalesOrder(String token, ArrayList<DistributorSalesOrder> salesOrders, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postDistributorSalesOrder(token, salesOrders);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }


}
