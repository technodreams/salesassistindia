package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.outlet.StockistActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.product.SearchProductActivity;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.product.model.ProductInventory;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesOrder;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class SalesOrderActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_SEARCH_PRODUCT = 100;
    private static final int REQUEST_CODE_SEARCH_OUTLET = 101;
    private static final int REQUEST_CODE_SEARCH_WAREHOUSE = 102;
    private static final int REQUEST_CODE_CONFIRM_SALES_ORDER = 103 ;
    boolean isUpdate = false;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.stockist)
    EditText stockist;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.product)
    EditText product;
    //    @BindView(R.id.productGroup)
//    EditText productGroup;
    @BindView(R.id.unitPrice)
    TextView unitPrice;
    @BindView(R.id.discountPercentage)
    EditText discountPercentage;
    @BindView(R.id.totalPrice)
    TextView totalPrice;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.quantity)
    EditText quantity;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.warehouse)
    EditText etWarehouse;
    @BindView(R.id.excise)
    TextView excise;
    @BindView(R.id.vat)
    TextView vat;
    @BindView(R.id.orderContainer)
    LinearLayout container;
    @BindView(R.id.discountAmount)
    EditText etDiscountedAmount;
    @BindView(R.id.grandTotal)
    TextView grandTotal;
    @BindView(R.id.taxableAmount)
    TextView tvTaxableAmt;
    Product selectedProduct;
    Stockist selectedStockist;
    ProductInventory selectedWarehouse;
    PrefManager prefManager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    int salesOrderId = 0;
    //float totalAmount, taxableAmount, discountedPercentage, discountedAmount, exciseAmount, vatPercentage;
    Double totalAmountIncludingTax=0.0,totalAmount=0.0, taxableAmount=0.0, discountedPercentage=0.0, discountedAmount =0.0, exciseAmount=0.0, vatPercentage=0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_order);
        ButterKnife.bind(this);
        toolbar.setTitle("Sales Order");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        SalesOrder updateSalesOrder = (SalesOrder)getIntent().getSerializableExtra("SalesOrder");
        LocationUtils locationUtils = new LocationUtils(SalesOrderActivity.this);
        locationUtils.requestLocationUpdates();
        if(updateSalesOrder !=null){
            setData(updateSalesOrder);

        }
        Stockist mStockist =(Stockist) getIntent().getSerializableExtra("OutletDetail");
        if(mStockist !=null){
            selectedStockist = mStockist;
            stockist.setEnabled(false);
            stockist.setText(selectedStockist.getName());
        }
        prefManager = new PrefManager(this);
        quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equalsIgnoreCase("")) {
                    double quantity = Double.parseDouble(s.toString());
                    totalAmount = quantity * Double.parseDouble(unitPrice.getText().toString());
                    totalPrice.setText(String.format("%.2f", totalAmount));
                    exciseAmount = totalAmount *( Double.parseDouble(selectedProduct.getExciseDuty()) / 100);
                    if(!discountPercentage.getText().toString().equalsIgnoreCase("")){
                        discountedPercentage = Double.parseDouble(s.toString());
                        discountedAmount = (discountedPercentage / 100) * totalAmount;
                        etDiscountedAmount.setText(String.format("%.2f", discountedAmount));
                    }
//                    if(!etDiscountedAmount.getText().toString().equalsIgnoreCase(""))
//                        discountedAmount = Double.parseDouble(etDiscountedAmount.getText().toString());
                    taxableAmount = totalAmount + exciseAmount - discountedAmount;
                    tvTaxableAmt.setText(String.format("%.2f", taxableAmount));
                    totalAmountIncludingTax = Double.parseDouble(taxableAmount * (((vatPercentage + 100) / 100))+"");
                    DecimalFormat df = new DecimalFormat("#.##");
                    df.setRoundingMode(RoundingMode.CEILING);

                    Double d = totalAmountIncludingTax.doubleValue();
                    grandTotal.setText(df.format(d));

                }else{
                    double quantity = 0;
                    totalAmount = 0.0;
                    totalPrice.setText(String.format("%.2f", totalAmount));
                    exciseAmount = 0.0;
                }


            }
        });
        discountPercentage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equalsIgnoreCase("")) {
                    discountedPercentage = Double.parseDouble(s.toString());
                    discountedAmount = (discountedPercentage / 100) * totalAmount;
                    etDiscountedAmount.setText(String.format("%.2f", discountedAmount));
                    taxableAmount = totalAmount + exciseAmount - discountedAmount;
                    tvTaxableAmt.setText(String.format("%.2f", taxableAmount));
                    totalAmountIncludingTax = Double.parseDouble(taxableAmount * (((vatPercentage + 100) / 100))+"");
                    DecimalFormat df = new DecimalFormat("#.##");
                    df.setRoundingMode(RoundingMode.CEILING);
                    Double d = totalAmountIncludingTax.doubleValue();
                    grandTotal.setText(df.format(d));

                }else{
                    discountedPercentage = 0.0;
                    discountedAmount = (discountedPercentage / 100) * totalAmount;
                    etDiscountedAmount.setText(discountedAmount + "");
                    taxableAmount = totalAmount + exciseAmount - discountedAmount;
                    totalAmountIncludingTax = Double.parseDouble(taxableAmount * (((vatPercentage + 100) / 100))+"");
                    DecimalFormat df = new DecimalFormat("#.##");
                    df.setRoundingMode(RoundingMode.CEILING);
                    Double d = totalAmountIncludingTax.doubleValue();
                    grandTotal.setText(df.format(d));
                }


            }
        });
    }

    private void setData(SalesOrder updateSalesOrder) {
        if(selectedWarehouse == null){
            selectedWarehouse = new ProductInventory();
        }
        if(selectedProduct ==null){
            selectedProduct = new Product();
        }
        if(selectedStockist == null){
            selectedStockist = new Stockist();
        }
        salesOrderId = updateSalesOrder.getOrderId();
        selectedWarehouse.setWareHouseId(updateSalesOrder.getWarehouseId());
        selectedWarehouse.setWarehouseName(updateSalesOrder.getWarehouseName());
        selectedProduct.setProductId(updateSalesOrder.getProductId());
        selectedProduct.setProductName(updateSalesOrder.getProductName());
        selectedProduct.setExciseDuty(updateSalesOrder.getExcise()+"");
        selectedProduct.setVat(updateSalesOrder.getVat() + "");
        selectedProduct.setDealerPrice(updateSalesOrder.getUnitPrice());
        selectedStockist.setName(updateSalesOrder.getOutletName());
        selectedStockist.setDistributorId(updateSalesOrder.getOutletId());
        stockist.setText(updateSalesOrder.getOutletName());
        product.setText(updateSalesOrder.getProductName());
        unitPrice.setText(updateSalesOrder.getUnitPrice());
        discountPercentage.setText(updateSalesOrder.getDiscountPercentage() + "");
        totalPrice.setText(updateSalesOrder.getTotalPrice() + "");
        grandTotal.setText(updateSalesOrder.getGrandTotal() + "");
        quantity.setText(updateSalesOrder.getQuantity() + "");
        etWarehouse.setText(updateSalesOrder.getWarehouseName());
        excise.setText(updateSalesOrder.getExcise() + "");
        vat.setText(updateSalesOrder.getVat()+"");
        etDiscountedAmount.setText(updateSalesOrder.getDiscountAmount() + "");
        tvTaxableAmt.setText(updateSalesOrder.getTaxableAmount() +"");
        container.setVisibility(View.VISIBLE);

        totalAmountIncludingTax=updateSalesOrder.getGrandTotal();
        totalAmount=updateSalesOrder.getTotalPrice();
        taxableAmount=updateSalesOrder.getTaxableAmount();
        discountedPercentage=updateSalesOrder.getDiscountPercentage();
        discountedAmount =updateSalesOrder.getDiscountAmount();
        exciseAmount=updateSalesOrder.getExcise();
        vatPercentage=updateSalesOrder.getVat();

        isUpdate = true;
    }

    @OnClick({R.id.stockist, R.id.product, R.id.btnSave,R.id.warehouse})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.stockist:
                Intent intent = new Intent(SalesOrderActivity.this, StockistActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SEARCH_OUTLET);
                clearData();
                break;
            case R.id.product:
                if(stockist.getText().toString().equalsIgnoreCase("")){
                    Toasty.info(SalesOrderActivity.this,"Please select outlet first!!",Toast.LENGTH_SHORT,true).show();

                }else{
                    Intent product = new Intent(SalesOrderActivity.this, SearchProductActivity.class);
                    startActivityForResult(product, REQUEST_CODE_SEARCH_PRODUCT);
                }
                break;
//            case R.id.productGroup:
//
//                break;
            case R.id.btnSave:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    if(quantity.getText().toString().equalsIgnoreCase("0")){
                        Toasty.info(SalesOrderActivity.this,"Quantity cannot be 0",Toast.LENGTH_SHORT,true).show();
                    }

                        confirmOrder();
                }

                break;

            case R.id.warehouse:
                Intent warehouse = new Intent(SalesOrderActivity.this, WarehouseActivity.class);
                warehouse.putExtra("ProductId",selectedProduct.getProductId());
                startActivityForResult(warehouse, REQUEST_CODE_SEARCH_WAREHOUSE);

                break;

        }

    }

    private void confirmOrder() {

        SalesOrder salesOrder = new SalesOrder();
        if(isUpdate)
            salesOrder.setOrderId(salesOrderId);
        salesOrder.setProductId(selectedProduct.getProductId());
        salesOrder.setProductName(selectedProduct.getProductName());
        salesOrder.setOutletId(selectedStockist.getDistributorId());
        salesOrder.setOutletName(selectedStockist.getName());
        salesOrder.setWarehouseId(selectedWarehouse.getWareHouseId());
        salesOrder.setWarehouseName(selectedWarehouse.getWarehouseName());
        salesOrder.setUnitPrice(selectedProduct.getDealerPrice());
        salesOrder.setQuantity(Integer.parseInt(quantity.getText().toString()));
        if(discountPercentage.getText().toString().equalsIgnoreCase(""))
            discountPercentage.setText("0");
        salesOrder.setDiscountPercentage(Double.parseDouble(discountPercentage.getText().toString()));
        salesOrder.setDiscountAmount(Double.parseDouble(etDiscountedAmount.getText().toString()));
        salesOrder.setTotalPrice(Double.parseDouble(totalPrice.getText().toString()));
        salesOrder.setExcise(Double.parseDouble(selectedProduct.getExciseDuty()));
        salesOrder.setVat(Double.parseDouble(selectedProduct.getVat()));
        salesOrder.setTaxableAmount(Double.parseDouble(tvTaxableAmt.getText().toString()));
        salesOrder.setGrandTotal(Double.parseDouble(grandTotal.getText().toString()));
        salesOrder.setLatitude(Double.parseDouble(prefManager.getLatitude()));
        salesOrder.setLongitude(Double.parseDouble(prefManager.getLongitude()));
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormatCr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        salesOrder.setDateTime(simpleDateFormatCr.format(calendar.getTime()));

        Intent intent = new Intent(SalesOrderActivity.this,ConfirmSalesOrderActivity.class);
        intent.putExtra("SalesOrder",salesOrder);
        intent.putExtra("IsUpdate",isUpdate);
        startActivityForResult(intent,REQUEST_CODE_CONFIRM_SALES_ORDER);


    }
    private void clearData() {
        totalAmountIncludingTax=0.0;
        totalAmount=0.0;
        taxableAmount=0.0;
        discountedPercentage=0.0;
        discountedAmount =0.0;
        exciseAmount=0.0;
        vatPercentage=0.0;
        product.setText("");
        etDiscountedAmount.setText("");
        unitPrice.setText("");
        totalPrice.setText("");
        excise.setText("");
        tvTaxableAmt.setText("");
        grandTotal.setText("");
        quantity.setText("");
        discountPercentage.setText("");
        etWarehouse.setText("");
        container.setVisibility(View.GONE);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE_SEARCH_PRODUCT) {
                if (resultCode == Activity.RESULT_OK) {
                    selectedProduct = (Product) data.getSerializableExtra("Result");
                    if (selectedProduct != null) {
                        product.setText(selectedProduct.getProductName());
                        unitPrice.setText(selectedProduct.getDealerPrice());
                        excise.setText(selectedProduct.getExciseDuty());
                        vat.setText(selectedProduct.getVat());
                        container.setVisibility(View.VISIBLE);
                        vatPercentage = Double.parseDouble(selectedProduct.getVat());
                        quantity.setText("");
                        discountPercentage.setText("");
                        etDiscountedAmount.setText("");
                        totalPrice.setText("");
                        tvTaxableAmt.setText("");
                        grandTotal.setText("");
                        etWarehouse.setText("");
                    }


                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            } else if (requestCode == REQUEST_CODE_SEARCH_OUTLET) {
                if (resultCode == Activity.RESULT_OK) {
                    selectedStockist = (Stockist) data.getSerializableExtra("result");
                    stockist.setText(selectedStockist.getName());
                    clearData();
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            } else if (requestCode == REQUEST_CODE_SEARCH_WAREHOUSE) {
                if (resultCode == Activity.RESULT_OK) {
                    selectedWarehouse = (ProductInventory) data.getSerializableExtra("Result");
                    etWarehouse.setText(selectedWarehouse.getWarehouseName() + " (" + selectedWarehouse.getStock() + " )");
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            } else if (requestCode == REQUEST_CODE_CONFIRM_SALES_ORDER) {
                if (resultCode == Activity.RESULT_OK) {
                    boolean isUpdated = data.getBooleanExtra("IsUpdate", false);
                    clearData();
                    if (isUpdated) {
                        finish();
                    }

                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            }
        }
}
