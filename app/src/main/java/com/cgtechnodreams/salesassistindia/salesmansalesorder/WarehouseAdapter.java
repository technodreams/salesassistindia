package com.cgtechnodreams.salesassistindia.salesmansalesorder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.product.model.ProductInventory;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WarehouseAdapter extends RecyclerView.Adapter<WarehouseAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<ProductInventory> inventories = new ArrayList<>();

    public WarehouseAdapter(Context context, ArrayList<ProductInventory> inventories) {
        this.mContext = context;
        this.inventories = inventories;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public WarehouseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_warehouse, null);

        return new WarehouseAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WarehouseAdapter.MyViewHolder holder, int position) {
        ProductInventory productInventory = inventories.get(position);
        holder.name.setText(productInventory.getWarehouseName());
        holder.quantity.setText("Available Stock: " + productInventory.getStock());
    }

    @Override
    public int getItemCount() {
        return inventories.size();
    }

    public void setFilter(ArrayList<ProductInventory> newList) {
        inventories = new ArrayList<>();
        inventories.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.quantity)
        TextView quantity;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}
