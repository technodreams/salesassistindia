package com.cgtechnodreams.salesassistindia.survey;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OutletSurveyListAdapter extends RecyclerView.Adapter<OutletSurveyListAdapter.MyViewHolder> {
    Context mContext;
    PrefManager prefManager;
    ArrayList<Stockist> stockists = new ArrayList<>();


    public OutletSurveyListAdapter(Context context, ArrayList<Stockist> stockists) {
        this.mContext = context;
        this.stockists = stockists;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public OutletSurveyListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_outlet, null);
        return new OutletSurveyListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OutletSurveyListAdapter.MyViewHolder holder, int position) {
        Stockist stockist = stockists.get(position);
        holder.name.setText(stockist.getName());
        holder.address.setText(stockist.getAddress());
    }

    @Override
    public int getItemCount() {
        return stockists.size();
    }

    public void setFilter(ArrayList<Stockist> newList) {

        stockists = new ArrayList<>();
        stockists.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", stockists.get(getAdapterPosition()));
                    ((Activity)mContext).setResult(Activity.RESULT_OK,returnIntent);
                    ((Activity)mContext).finish();
                }
            });

        }

    }
}
