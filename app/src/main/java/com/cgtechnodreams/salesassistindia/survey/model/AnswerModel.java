package com.cgtechnodreams.salesassistindia.survey.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnswerModel implements Serializable {
    @JsonProperty("survey_id")
    private int surveyId;
    @JsonProperty("latitude")
    private String latitude;
    @JsonProperty("longitude")
    private String longitude;
    @JsonProperty("ref_id_one") // created to indiccate the whose survey is taken
    private int refIdOne;
    @JsonProperty("ref_id_two")
    private int refIdTwo; // // created to indiccate the whose survey is taken
    @JsonProperty("answer_data")
    private ArrayList<AnswerData> answerDataList;

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public ArrayList<AnswerData> getAnswerDataList() {
        return answerDataList;
    }

    public void setAnswerDataList(ArrayList<AnswerData> answerDataList) {
        this.answerDataList = answerDataList;
    }

    public int getRefIdOne() {
        return refIdOne;
    }

    public void setRefIdOne(int refIdOne) {
        this.refIdOne = refIdOne;
    }

    public int getRefIdTwo() {
        return refIdTwo;
    }

    public void setRefIdTwo(int refIdTwo) {
        this.refIdTwo = refIdTwo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
