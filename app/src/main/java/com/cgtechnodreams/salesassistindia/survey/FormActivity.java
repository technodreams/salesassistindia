package com.cgtechnodreams.salesassistindia.survey;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.CompoundButtonCompat;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SurveyAnswerEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SurveyEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.survey.model.AnswerData;
import com.cgtechnodreams.salesassistindia.survey.model.AnswerModel;
import com.cgtechnodreams.salesassistindia.survey.model.Section;
import com.cgtechnodreams.salesassistindia.user.model.User;
import com.cgtechnodreams.salesassistindia.utils.AppConfig;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class FormActivity extends AppCompatActivity {
    ArrayList<Section> sectionList = new ArrayList<Section>();
    int surveyId;
    String surveyType = null;
    PrefManager prefManager;
    @BindView(R.id.tvSurveyType)
    TextView tvSurveyType;
    @BindView(R.id.etSurveyType)
    EditText etSurveyType;
    @BindView(R.id.parentContainer)
    LinearLayout parentContainer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    List<View> allViewInstance = new ArrayList<View>();
    AnswerModel answerModel = new AnswerModel();
    final int REQUEST_CODE_PROMOTER = 100;
    final int REQUEST_CODE_OUTLETS = 101;
    User user = null;
    Stockist stockist = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        ButterKnife.bind(this);
        toolbar.setTitle("Survey");
        prefManager  = new PrefManager(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        surveyId = getIntent().getIntExtra("Id", 0);
        surveyType =getIntent().getStringExtra("SurveyType");
        LocationUtils locationUtils = new LocationUtils(FormActivity.this);
        locationUtils.requestLocationUpdates();
        SurveyEntity surveyEntity = new SurveyEntity(this);
        String surveyDetail = surveyEntity.getSurveyDetail(surveyId);
        if(surveyType.equalsIgnoreCase("") || surveyType  == null){

        }else if(surveyType.equalsIgnoreCase("promoter")){
            tvSurveyType.setText("Select Promoter");
            etSurveyType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FormActivity.this,SurveyDatasetActivity.class);
                    intent.putExtra("SurveyType", surveyType);
                    startActivityForResult(intent,REQUEST_CODE_PROMOTER);
                }
            });
        }else if(surveyType.equalsIgnoreCase("new stockist")){
            etSurveyType.setVisibility(View.GONE);
            tvSurveyType.setVisibility(View.GONE);

        }else if(surveyType.equalsIgnoreCase("old stockist")){
            tvSurveyType.setText("Select Stockist");
            etSurveyType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FormActivity.this,SurveyDatasetActivity.class);
                    intent.putExtra("SurveyType", surveyType);
                    startActivityForResult(intent,REQUEST_CODE_OUTLETS);
                }
            });
        }
//        try {
//            JSONArray jsonArray = new JSONArray(surveyDetail);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            sectionList = objectMapper.readValue(String.valueOf(surveyDetail), new TypeReference<ArrayList<Section>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (sectionList.size() > 0) {
            for (int i = 0; i < sectionList.size(); i++) {
                if (!sectionList.get(i).getSectionName().equalsIgnoreCase("none")) {
                    TextView sectionTitle = new TextView(this);
                    sectionTitle.setText(sectionList.get(i).getSectionName());
                    sectionTitle.setTypeface(null, Typeface.BOLD);
                    parentContainer.addView(sectionTitle);
                }
                for (int j = 0; j < sectionList.get(i).getQuestions().size(); j++) {
                    TextView question = new TextView(this);
                    question.setText(sectionList.get(i).getQuestions().get(j).getQuestionName());
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 10, 10, 10);
                    question.setLayoutParams(params);
                    parentContainer.addView(question);
                    if (sectionList.get(i).getQuestions().get(j).getType().equalsIgnoreCase("Checkbox")) {
                        for (int chk = 0; chk < sectionList.get(i).getQuestions().get(j).getOptions().size(); chk++) {
                            CheckBox cb = new CheckBox(getApplicationContext());
                            cb.setText(sectionList.get(i).getQuestions().get(j).getOptions().get(chk).getOptionName());
                            // if (!(checkBoxJSONOpt.getJSONObject(j).getString("variant_name").equalsIgnoreCase("NO"))) {
                            allViewInstance.add(cb);
                            cb.setTag("option_" + sectionList.get(i).getQuestions().get(j).getOptions().get(chk).getId());
                            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            params1.topMargin = 3;
                            params1.bottomMargin = 3;
                            String optionString = sectionList.get(i).getQuestions().get(j).getOptions().get(chk).getOptionName();
                            cb.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String variant_name = v.getTag().toString();
                                  //  Toasty.info(getApplicationContext(), variant_name + "", Toasty.LENGTH_LONG).show();
                                }
                            });
                            cb.setTextColor(getResources().getColor(R.color.colorPrimary));
                            ColorStateList colorStateList = new ColorStateList(
                                    new int[][]{
                                            new int[]{-android.R.attr.state_checked}, // unchecked
                                            new int[]{android.R.attr.state_checked}, // checked
                                    },
                                    new int[]{
                                            Color.parseColor("#25aae1"),
                                            Color.parseColor("#25aae1"),
                                    }
                            );

                            CompoundButtonCompat.setButtonTintList(cb, colorStateList);

                            parentContainer.addView(cb);
                        }
                    } else if (sectionList.get(i).getQuestions().get(j).getType().equalsIgnoreCase("Radio")) {

                        final RadioButton[] rb = new RadioButton[sectionList.get(i).getQuestions().get(j).getOptions().size()];
                        RadioGroup rg = new RadioGroup(this); //create the RadioGroup
                        allViewInstance.add(rg);
                        rg.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL
                        for (int rbCount = 0; rbCount < sectionList.get(i).getQuestions().get(j).getOptions().size(); rbCount++) {
                            rb[rbCount] = new RadioButton(this);
                            rb[rbCount].setText(sectionList.get(i).getQuestions().get(j).getOptions().get(rbCount).getOptionName());
                            rb[rbCount].setTag("Option_" + sectionList.get(i).getQuestions().get(j).getOptions().get(rbCount).getId());
                            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                                @Override
                                public void onCheckedChanged(RadioGroup group, int checkedId) {

                                    View radioButton = group.findViewById(checkedId);
                                    String variant_name = radioButton.getTag().toString();
                                   // Toasty.info(getApplicationContext(), variant_name + "", Toasty.LENGTH_LONG).show();
                                }
                            });
                            rg.addView(rb[rbCount]);
                        }
                        parentContainer.addView(rg);

                    } else if (sectionList.get(i).getQuestions().get(j).getType().equalsIgnoreCase("TextArea")) {
                        EditText editText = new EditText(this);
                        allViewInstance.add(editText);
                        parentContainer.addView(editText);
                        editText.setTag("et_"+sectionList.get(i).getQuestions().get(j).getId());

                    } else if (sectionList.get(i).getQuestions().get(j).getType().equalsIgnoreCase("TextBox")) {
                        EditText editText = new EditText(this);
                        allViewInstance.add(editText);
                        parentContainer.addView(editText);

                        editText.setTag("et_"+sectionList.get(i).getQuestions().get(j).getId());
                    } else {

                    }
                }

            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_PROMOTER) {
                user = (User) data.getSerializableExtra("result");
                etSurveyType.setText(user.getName());
                // Toasty.makeText(FormActivity.this, user.getName(),Toast.LENGTH_SHORT,true).show();
            } else if (requestCode == REQUEST_CODE_OUTLETS) {
                stockist = (Stockist) data.getSerializableExtra("result");
                etSurveyType.setText(stockist.getName());
            }

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.survey_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_save:
                if(etSurveyType.getText().toString().equalsIgnoreCase("")){
                    if(null ==surveyType || surveyType.equalsIgnoreCase("")|| surveyType.equalsIgnoreCase("new stockist")){

                    }else{
                        etSurveyType.setError("Required Field");
                       return true;
                    }
                }
                ArrayList<AnswerData> answerData = getSelectedData();
                if(answerData!=null){
                    saveSurvey(answerData);
                }
               break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void saveSurvey(ArrayList<AnswerData> answerData) {
        SurveyAnswerEntity answerEntity = new SurveyAnswerEntity(this);
        AnswerModel answerModel = new AnswerModel();
        if(user!=null){
            answerModel.setRefIdOne(user.getId());
        }else if(stockist !=null){
            answerModel.setRefIdOne(stockist.getDistributorId()
            );
        }

        answerModel.setLatitude(prefManager.getLatitude());
        answerModel.setLongitude(prefManager.getLongitude());
        answerModel.setSurveyId(surveyId);
        answerModel.setAnswerDataList(answerData);

        //  @TODO promoter stockist survey and new stockist request;
        JSONObject jsonAnswerDetail = new JSONObject();
        ObjectMapper objectMapper = new ObjectMapper();
        // objectMapper.setDateFormat(dateFormat);


        try {
            String jsonString = objectMapper.writeValueAsString(answerModel);
            jsonAnswerDetail = new JSONObject(jsonString);
            //jsonAnswerDetail.put(SurveyAnswerEntity.COL_ANSWER,jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        answerEntity.insert(jsonAnswerDetail);
        finish();
        Toasty.success(FormActivity.this, "Survey Saved", Toast.LENGTH_SHORT,true).show();
    }

    private ArrayList<AnswerData> getSelectedData() {
        int elementIndex = 0;
        ArrayList<AnswerData> answerData = new ArrayList<AnswerData>();
            for (int noOfSection = 0; noOfSection < sectionList.size(); noOfSection++)
                for (int numberOfQuestion = 0; numberOfQuestion < sectionList.get(noOfSection).getQuestions().size(); numberOfQuestion++) {
                    AnswerData eachAnswerData = new AnswerData();

                    if (sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getType().equalsIgnoreCase("Radio")) {
                        RadioGroup radioGroup = (RadioGroup) allViewInstance.get(elementIndex);
                        RadioButton selectedRadioBtn = (RadioButton) findViewById(radioGroup.getCheckedRadioButtonId());
                        eachAnswerData.setQuestionId(sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getId());
                        eachAnswerData.setQuestionType(AppConfig.QUESTION_TYPE_RADIO);
                        // @TODO check for validataion
                        if(selectedRadioBtn!=null){
                            int radioId = Integer.parseInt(selectedRadioBtn.getTag().toString().split("_")[1]);
                            eachAnswerData.setRadioId(radioId);
                            answerData.add(eachAnswerData);
                        }else{
                            Toasty.info(FormActivity.this,"Required Field is missing!!",Toast.LENGTH_SHORT,true).show();
                            return null;
                        }


                        elementIndex++;
                    } else if (sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getType().equalsIgnoreCase("Checkbox")) {
                        //int checkboxId[] = new int[sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getOptions().size()];
                        ArrayList<Integer> checkboxId = new ArrayList<>();
                        eachAnswerData.setQuestionType(AppConfig.QUESTION_TYPE_CHECK);
                        eachAnswerData.setQuestionId(sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getId());
                        for (int i = 0; i < sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getOptions().size(); i++) {
                            CheckBox tempChkBox = (CheckBox) allViewInstance.get(elementIndex);
                            if (tempChkBox.isChecked()) {
                                checkboxId.add(sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getOptions().get(i).getId());

                            }
                            elementIndex++;
                        }
                        if(checkboxId.size() == 0){
                            Toasty.info(FormActivity.this,"Required Field is missing!!",Toast.LENGTH_SHORT,true).show();
                            return null;
                        }else{
                            eachAnswerData.setCheckboxId(checkboxId);
                            answerData.add(eachAnswerData);

                        }


                    } else if (sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getType().equalsIgnoreCase("TextArea")) {
                        EditText editText = (EditText) allViewInstance.get(elementIndex);
                        eachAnswerData.setQuestionType(AppConfig.QUESTION_TYPE_TEXT);
                        eachAnswerData.setQuestionId(sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getId());
                        // @TODO check for validataion
                        if(editText.getText().toString().equalsIgnoreCase("")){
                            editText.setError("Required field missing");
                            return null;
                        }else{
                            eachAnswerData.setAnswer(editText.getText().toString());
                            answerData.add(eachAnswerData);
                        }

                        elementIndex++;
                    } else if (sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getType().equalsIgnoreCase("TextBox")) {
                        EditText editText = (EditText) allViewInstance.get(elementIndex);
                        eachAnswerData.setQuestionType(AppConfig.QUESTION_TYPE_TEXT);
                        eachAnswerData.setQuestionId(sectionList.get(noOfSection).getQuestions().get(numberOfQuestion).getId());
                        // @TODO check for validataion
                        if(editText.getText().toString().equalsIgnoreCase("")){
                            editText.setError("Required field missing");
                            return null;
                        }else {
                            eachAnswerData.setAnswer(editText.getText().toString());
                            answerData.add(eachAnswerData);
                        }
                        elementIndex++;
                    }
                }
                return answerData;
            }

    public static class OutletAdapter {
    }

}

