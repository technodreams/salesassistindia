package com.cgtechnodreams.salesassistindia.survey.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SavedSurveyModel implements Serializable {
    @JsonProperty("id")
    private int id;
    @JsonProperty("survey_name")
    private String surveyName;
    @JsonProperty("sections")
    private String sections;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    public String getSections() {
        return sections;
    }

    public void setSections(String sections) {
        this.sections = sections;
    }
}
