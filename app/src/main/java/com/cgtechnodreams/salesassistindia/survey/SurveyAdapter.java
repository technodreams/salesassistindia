package com.cgtechnodreams.salesassistindia.survey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.survey.model.SurveyModel;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<SurveyModel> surveyList = new ArrayList<>();

    public SurveyAdapter(Context context, ArrayList<SurveyModel> surveyList) {
        this.mContext = context;
        this.surveyList = surveyList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public SurveyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_list, null);
        return new SurveyAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SurveyAdapter.MyViewHolder holder, int position) {
        SurveyModel survey = surveyList.get(position);
        holder.name.setText(survey.getSurveyName());

    }

    @Override
    public int getItemCount() {
        return surveyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}