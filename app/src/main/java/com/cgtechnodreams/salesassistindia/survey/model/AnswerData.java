package com.cgtechnodreams.salesassistindia.survey.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnswerData implements Serializable {
    @JsonProperty("check_option")
    private  ArrayList<Integer> checkboxId;
    @JsonProperty("radio_option")
    private int radioId;
    @JsonProperty("question_id")
    private int questionId;

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    @JsonProperty("question_type")
    private String questionType;
    @JsonProperty("answer")
    private String answer;

    public ArrayList<Integer> getCheckboxId() {
        return checkboxId;
    }

    public void setCheckboxId( ArrayList<Integer> checkboxId) {
        this.checkboxId = checkboxId;
    }
    public int getRadioId() {
        return radioId;
    }

    public void setRadioId(int radioId) {
        this.radioId = radioId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
