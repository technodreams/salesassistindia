package com.cgtechnodreams.salesassistindia.survey;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.UserEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.user.model.User;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SurveyDatasetActivity extends AppCompatActivity {
    String surveyType;
    PromoterAdapter promoterAdapter =null;
    OutletSurveyListAdapter outLetAdapter = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    ArrayList<User> users= null;
    ArrayList<Stockist> stockists = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_dataset);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        surveyType = getIntent().getStringExtra("SurveyType");
        Log.i("SurveyType",surveyType);

        if(surveyType.equalsIgnoreCase("promoter")){
            UserEntity userEntity = new UserEntity(this);
            ObjectMapper objectMapper = new ObjectMapper();
            JSONArray array = (JSONArray)userEntity.find(null);
            try {
                objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                users = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<User>>(){});
            } catch (IOException e) {
                e.printStackTrace();
            }
            LinearLayoutManager promoterListManager = new LinearLayoutManager(SurveyDatasetActivity.this, LinearLayoutManager.VERTICAL, false);
            promoterAdapter = new PromoterAdapter(SurveyDatasetActivity.this, users);
            recyclerView.setLayoutManager(promoterListManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(promoterAdapter);

        }
        else if(surveyType.equalsIgnoreCase("old stockist")){
            StockistEntity stockistEntity = new StockistEntity(this);


            ObjectMapper objectMapper = new ObjectMapper();
            JSONArray array = (JSONArray) stockistEntity.find(null);
            try {
                objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                stockists = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Stockist>>(){});
            } catch (IOException e) {
                e.printStackTrace();
            }
            LinearLayoutManager promoterListManager = new LinearLayoutManager(SurveyDatasetActivity.this, LinearLayoutManager.VERTICAL, false);
            outLetAdapter = new OutletSurveyListAdapter(SurveyDatasetActivity.this, stockists);
            recyclerView.setLayoutManager(promoterListManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(outLetAdapter);
//
        }
        else if(surveyType.equalsIgnoreCase("new stockist")){

        }else{

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.search_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                if(surveyType.equalsIgnoreCase("old stockist")){
                    ArrayList<Stockist> newList = new ArrayList<>();
                    for (Stockist stockist : stockists) {
                        String name = stockist.getName().toLowerCase();
                        String address = stockist.getAddress().toLowerCase();

                        if (name.contains(newText) || address.toLowerCase().contains(newText) ) {
                            newList.add(stockist);
                        }

                        outLetAdapter.setFilter(newList);

                    }
                }else if(surveyType.equalsIgnoreCase("promoter")) {
                    ArrayList<User> newList = new ArrayList<>();
                    for (User user : users) {
                        String name = user.getName().toLowerCase();

                        if (name.contains(newText)) {
                            newList.add(user);
                        }

                        promoterAdapter.setFilter(newList);
                    }
                }

                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                          }
                                      }
        );
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
