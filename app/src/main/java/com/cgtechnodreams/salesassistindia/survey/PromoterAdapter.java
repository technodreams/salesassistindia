package com.cgtechnodreams.salesassistindia.survey;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.user.model.User;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PromoterAdapter extends RecyclerView.Adapter<PromoterAdapter.MyViewHolder> {
    Context mContext;
    PrefManager prefManager;
    ArrayList<User> promoterList = new ArrayList<>();

    public PromoterAdapter(Context context, ArrayList<User> promoterList) {
        this.mContext = context;
        this.promoterList = promoterList;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public PromoterAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_user, null);
        return new PromoterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PromoterAdapter.MyViewHolder holder, int position) {
        User user = promoterList.get(position);
        holder.name.setText(user.getName());
    }

    @Override
    public int getItemCount() {
        return promoterList.size();
    }
    public void setFilter(ArrayList<User> newList) {

        promoterList = new ArrayList<>();
        promoterList.addAll(newList);
        notifyDataSetChanged();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result",promoterList.get(getAdapterPosition()));
                    ((Activity)mContext).setResult(Activity.RESULT_OK,returnIntent);
                    ((Activity)mContext).finish();

                }
            });

        }

    }
}
