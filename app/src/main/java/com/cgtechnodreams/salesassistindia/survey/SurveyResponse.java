package com.cgtechnodreams.salesassistindia.survey;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.survey.model.SurveyModel;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class SurveyResponse extends BaseResponse {

    @JsonProperty("data")
    ArrayList<SurveyModel> data;

    public ArrayList<SurveyModel> getData() {
        return data;
    }

    public void setData(ArrayList<SurveyModel> data) {
        this.data = data;
    }
}
