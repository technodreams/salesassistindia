package com.cgtechnodreams.salesassistindia.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by user on 15/01/2019.
 */

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    //shared pref mode
    int PRIVATE_MODE = 0;

    //Shared preferences file name
    public static final String PREF_NAME = "sales-assist-foods";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String AUTH_KEY = "AuthKey";
    private static final String EMAIL = "Email";
    private static final String MOBILE = "Mobile";
    private static final String NAME = "Name";
    private static final String LATITUDE = "Latitude";
    private static final String LONGITUDE = "Longitude";
    private static final String IS_SUPERVISOR = "IsSupervisor";


    private static final String ROLE = "Role";

    public PrefManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void setFirstTimeLaunch(boolean isFirstTime){
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch(){
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setIsSupervisor(boolean isSupervisor){
        editor.putBoolean(IS_SUPERVISOR, isSupervisor);
        editor.commit();
    }

    public boolean getIsSupervisor(){
        return pref.getBoolean(IS_SUPERVISOR, false);
    }
    public void setAuthKey(String authKey){
        editor.putString(AUTH_KEY, authKey);
        editor.commit();
    }

    public String getAuthKey(){
        return pref.getString(AUTH_KEY, null);
    }
    public String getEmail(){
        return pref.getString(EMAIL, null);
    }
    public void setEmail(String email){
        editor.putString(EMAIL, email);
        editor.commit();
    }
    public String getName(){
        return pref.getString(NAME, null);
    }
    public void setName(String authKey){
        editor.putString(NAME, authKey);
        editor.commit();
    }
    public String getMobile(){
        return pref.getString(MOBILE, null);
    }
    public void setMobile(String authKey){
        editor.putString(MOBILE, authKey);
        editor.commit();
    }
    public String getRole(){
        return pref.getString(ROLE, "");
    }
    public void setRole(String role){
        editor.putString(ROLE, role);
        editor.commit();
    }
    public String getLongitude(){
        return pref.getString(LONGITUDE, "");
    }
    public void setLongitude(String longitude){
        editor.putString(LONGITUDE, longitude);
        editor.commit();
    }
    public String getLatitude(){
        return pref.getString(LATITUDE, "");
    }
    public void setLatitude(String latitude){
        editor.putString(LATITUDE, latitude);
        editor.commit();
    }
    public void clear(){
        editor.clear();
        editor.apply();
    }
}
