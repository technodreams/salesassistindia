package com.cgtechnodreams.salesassistindia.utils;

/**
 * Created by user on 11/01/2019.
 */

public class Log {


    public static final void d(String TAG, String message) {
        if (AppConfig.APP_ENV == AppConfig.AppEnvironment.DEBUG) {
            android.util.Log.d(TAG, message);
        }

    }

    public static final void i(String TAG, String message) {
        if (AppConfig.APP_ENV == AppConfig.AppEnvironment.DEBUG) {
            android.util.Log.i(TAG, message);
        }
    }

    public static final void e(String TAG, String message) {
        if (AppConfig.APP_ENV == AppConfig.AppEnvironment.DEBUG) {
            android.util.Log.e(TAG, message);
        }
    }

    public static final void e(String TAG, String message, Exception e) {
        if (AppConfig.APP_ENV == AppConfig.AppEnvironment.DEBUG) {
            android.util.Log.e(TAG, message, e);
        }
    }

}
