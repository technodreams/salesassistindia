package com.cgtechnodreams.salesassistindia.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

/**
 * Created by user on 01/06/2018.
 */


public class ImageUtils {
    public static Bitmap getBitmapFromUri(Activity activity, Uri uri) {
        Bitmap orgImage = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(activity.getApplicationContext()
                            .getContentResolver().openInputStream(uri), null,
                    options);
            int minValue = (options.outHeight <= options.outWidth) ? options.outHeight
                    : options.outWidth;
            options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            for (; ((minValue / (options.inSampleSize + 1)) > 524); ) {
                options.inSampleSize += 1;
            }
            options.inJustDecodeBounds = false;
            options.inPurgeable = true;
            orgImage = BitmapFactory.decodeStream(activity
                    .getApplicationContext().getContentResolver()
                    .openInputStream(uri), null, options);
        } catch (FileNotFoundException e) {
            Log.e("Error", e.getMessage());
        }
        return orgImage;
    }

    public static String getBase64Image(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos); //bm is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();
        //return "data:image/jpeg;base64," + Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }
}
