package com.cgtechnodreams.salesassistindia.utils;

/**
 * Created by user on 08/01/2019.
 */

public class AppConstant {
    public static final int MEDIA_TYPE_IMAGE = 147;
    public static final int MEDIA_TYPE_IMAGE_COLLECTION = 148;
    public static final int MEDIA_TYPE_IMAGE_MARKET_ACITIVITIES = 149;
    public static final int MEDIA_TYPE_VID = 258;
    public static final int COMPETITOR_BRAND = 1;
    public static final int OWN_BRAND = 0;
    // Image and Video file extensions
    public static final String IMAGE_EXTENSION = "jpg";
    public static final String VIDEO_EXTENSION = "mp4";
    // Gallery directory name to store the images or videos
    public static final String GALLERY_DIRECTORY_NAME = "Camera";
    public static final String IMAGES_DIRECTORY = "Images";

    public static final String STOCKIST_DETAIL = "StockistDetail";
    public static final String DISTRIBUTOR_DETAIL= "DistributorDetail";


}
