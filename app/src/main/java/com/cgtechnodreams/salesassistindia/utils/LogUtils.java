package com.cgtechnodreams.salesassistindia.utils;

import android.content.Context;
import android.os.Environment;

import com.cgtechnodreams.salesassistindia.databasehelper.UserActivity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.UserActivityEntity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by user on 08/01/2019.
 */


public class LogUtils {


    private static final String DIST_LOG_DIR = "CG_LOG" + File.separator + getCurrentDate();
    private static final String DIST_LOG_FILE = "CG_LOG" + File.separator + getCurrentDate() + File.separator + "log.txt";
    public static StringBuilder mEodLog;
    private static String mLogReport = null;

    public static void initializeLogBuilder() {
        if (mEodLog == null) mEodLog = new StringBuilder();
        mEodLog.append("-------------------------").append(getCurrentDateTime()).append("-------------------------\n\n");
    }

    //
    public synchronized static String saveLog() {

        final File logDirectory = new File(Environment.getExternalStorageDirectory().getPath() + File.separator, DIST_LOG_DIR);
        final File logFile = new File(Environment.getExternalStorageDirectory().getPath() + File.separator, DIST_LOG_FILE);

//        if (!logDirectory.exists())
//            if (logDirectory.mkdirs()) {
//                if (!logFile.exists())
//                    try {
//                        logFile.createNewFile();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//            }


//        try {
//            writeLogFile(logFile);
//            mLogReport = readLogFile(logFile);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return "";
    }

    private static String readLogFile(File installation) throws IOException {
        RandomAccessFile f = new RandomAccessFile(installation, "r");
        byte[] bytes = new byte[(int) f.length()];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }

    private static void writeLogFile(File installation) throws IOException {
        FileOutputStream out = new FileOutputStream(installation, true);
        mEodLog.append("\n\n-------------------------").append(getCurrentDateTime()).append("-------------------------\n\n");
        out.write(mEodLog.toString().getBytes());
        out.close();
    }

    private static String getCurrentDate() {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        return simpleDateFormat.format(calendar.getTime());
    }

    private static String getCurrentDateTime() {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.getDefault());
        return simpleDateFormat.format(calendar.getTime());
    }

    public static void setUserActivity(Context context , String activity, String activityDetail){
        PrefManager prefManager = new PrefManager(context);
        UserActivity userActivity = new UserActivity();
        userActivity.setUserActivity(activity);
        userActivity.setDetail(activityDetail);
        userActivity.setDatetime(getCurrentDateTime());
        userActivity.setLatitude(prefManager.getLatitude());
        userActivity.setLongitude(prefManager.getLongitude());
        UserActivityEntity userActivityEntity = new UserActivityEntity(context);
        userActivityEntity.insert(userActivity);

    }


}
