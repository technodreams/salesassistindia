package com.cgtechnodreams.salesassistindia.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;

import java.io.IOException;

import es.dmoral.toasty.Toasty;

public class ErrorHandler {
    public static void httpRequestFailure(Context mContext, Throwable t){
        LogUtils.setUserActivity(mContext,"Error",t.getMessage());
        if (t instanceof IOException) {
            Toasty.error(mContext, "Network error :( " + t.getMessage() + " Please try later!!", Toast.LENGTH_SHORT,true ).show();
            //Toasty.makeText(mContext, "Network error :( " + t.getMessage() + " Please try later!!", Toast.LENGTH_SHORT,true).show();
            // logging probably not necessary
        }else {
            Toasty.error(mContext, "Conversion issue! :( Please contact your support team", Toast.LENGTH_SHORT,true ).show();
           // Toasty.makeText(mContext, "Conversion issue! :( Please contact your support team", Toast.LENGTH_SHORT,true).show();
            // todo log to some central bug tracking service
        }
    }

    public static void handleSyncError(Context mContext, int statusCode, String message){
       // Toasty.makeText(mContext, statusCode+ ": " + message, Toast.LENGTH_SHORT,true).show();
        Toasty.info(mContext, statusCode+ ": " + message, Toast.LENGTH_SHORT,true ).show();
        if(statusCode== APIConstants.ERROR_UNAUTHORIZED ){ //LogOut User if user is unauthorized
            Intent intent = new Intent(mContext, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PrefManager preferenceManager = new PrefManager(mContext);
            preferenceManager.clear();
            mContext.startActivity(intent);
            ((Activity)mContext).finish();
        }
        LogUtils.setUserActivity(mContext ,"Error", "Status Code: " + statusCode +  " Error Message " + message);
    }
}
