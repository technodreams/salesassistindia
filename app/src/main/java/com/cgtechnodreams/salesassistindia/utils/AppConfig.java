package com.cgtechnodreams.salesassistindia.utils;

/**
 * Created by user on 11/01/2019.
 */



    public class AppConfig{
        public static final String QUESTION_TYPE_RADIO = "R";
        public static final String QUESTION_TYPE_CHECK = "C";
        public static final String QUESTION_TYPE_TEXT = "T";

    public enum AppContext {
        ONLINE, OFFLINE
    }

        public static final AppContext APP_CONTEXT = AppContext.OFFLINE;


    /*
     ------------------------------------------------------------------------------
     | Application Development environment
     |
     | Changing the Environment to Release will hide the logging of
     | the application to the android monitor, thus hiding the vulnerable web service requests.
     ------------------------------------------------------------------------------
    */
    public enum AppEnvironment {
        DEBUG, RELEASE
    }

        public static final AppEnvironment APP_ENV = AppEnvironment.DEBUG;


}