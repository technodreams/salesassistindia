package com.cgtechnodreams.salesassistindia.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

/**
 * Created by user on 08/01/2019.
 */

public class Utils {

    private static final String TAG = Utils.class.getSimpleName() + "==> ";
    private static String version = null;

    /**
     * Round up the large decimal value into decimal with digits count equal to the decimalCount
     * after the decimal
     *
     * @param largeDecimal the large decimal value to round up
     * @param decimalCount the digits count after the decimal value
     * @return Rounded up decimal value
     */
    public static double decimal(double largeDecimal, int decimalCount) {
        try {
            if (decimalCount < 1) {
                return largeDecimal;
            }
            //
            double divider = Math.pow(10, decimalCount);
            double val = largeDecimal * divider;

            return ((int) val) / divider;

        } catch (Exception e) {
            return largeDecimal;
        }
    }

    /**
     * @param doubleValue the large decimal value to round up
     * @return
     */
    public static String getDecimalFormatted(String doubleValue) {
        if (doubleValue.equalsIgnoreCase("0"))
            return "0";

        DecimalFormat df = new DecimalFormat("#,###.00");
        df.setRoundingMode(RoundingMode.DOWN);
        try {
            return df.format(Double.parseDouble(doubleValue));
        } catch (NumberFormatException e) {
            return doubleValue;
        }
    }

    /**
     * Highlight the needle string in the haystack string
     * Will return Html formatted string, use <b>Html.fromHtml()</b> to display the highlighted string.
     *
     * @param needle   the needle string to search in the string
     * @param hayStack the Html formatted haystack with highlighted needle equivalent string
     * @return
     */
    public static String getStringHighlighted(String needle, String hayStack) {

        Pattern pattern = Pattern.compile("(?i)" + needle);
        Matcher matcher = pattern.matcher(hayStack);
        if (matcher.find()) {
            return hayStack.replaceAll("(?i)" + needle, "<b>" + matcher.group(0) + "</b>");
        }

        return hayStack;

    }

    /**
     * Format the distance according to the distance value.
     *
     * @param distance distance value in double
     * @return return distance meters if less than 1000 or else the equivalent distance in km.
     */
    public static String formatDistance(double distance) {
        if (distance < 1000) {
            return decimal(distance, 2) + " meters";
        } else {
            return decimal(distance / 1000, 2) + " km";
        }
    }

    /**
     * Method to get the Uri path of the file to be returned by the camera action.
     *
     * @param type The type of media file requested. either Image(MEDIA_IMAGE) or Video(MEDIA_VIDEO)
     * @return The uri of the file of the specific media type
     */
    public static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Creates a file to be used by the camera intent action.
     *
     * @param type The type of media file requested. either Image(AppConstant.MEDIA_TYPE_IMG)
     *             or Video(AppConstant.MEDIA_TYPE_VID)
     * @return File for the specific media type
     */
    private static File getOutputMediaFile(int type) {
        final File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Distribution" +
                File.separator + new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()).format(new Date()));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "getOutputMediaFile: Failed to create directory " + mediaStorageDir.getAbsolutePath());
                return null;
            }
        }

        //create a media file name
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == AppConstant.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timestamp + ".jpg");
        } else if (type == AppConstant.MEDIA_TYPE_VID) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timestamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static boolean isMyServiceRunning(Activity activity, Class<?> serviceClass) {
        try {
            ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
            return false;

        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check if internet connection is available.
     *
     * @param context
     * @return
     */
    public static boolean isConnectionAvailable(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static AlertDialog.Builder getNewAlertDialog(Activity activity) {

        return new AlertDialog.Builder(activity).setCancelable(true);

    }

    /**
     * Use this function to display toast from doInBackground() method of the AsyncTask class
     *
     * @param activity
     * @param message
     * @param length
     */
    public static final void backgroundToast(final Activity activity, final String message, final int length) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (length > 0) {
                    Toasty.info(activity, message, Toasty.LENGTH_LONG).show();
                } else {
                    Toasty.info(activity, message, Toast.LENGTH_SHORT,true).show();
                }

            }
        });

    }

    public static String implode(String separator, String... data) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length - 1; i++) {
            //data.length - 1 => to not add separator at the end
            if (!data[i].matches(" *")) {//empty string are ""; " "; "  "; and so on
                sb.append(data[i]);
                sb.append(separator);
            }
        }
        sb.append(data[data.length - 1].trim());
        return sb.toString();
    }

    public static float convertPixelToDp(Context context, float px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return Math.round(dp);
    }

    public static float convertDpToPixel(Context context, float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static void getConnectionToast(final Context context) {
        try {
            Toasty.info(context, "Internet connection not available!", Toast.LENGTH_SHORT,true).show();
        } catch (Exception e) {
            Log.e(TAG, "getConnectionToast: " + e.getMessage());
        }
    }

    /**
     * Get the device IMEI number.
     *
     * @param context The context of the calling class
     * @return IMEI number of the device, else 0
     */
    @SuppressLint("HardwareIds")
    public static String getDeviceIMEI(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return "";
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (telephonyManager.getPhoneCount() > 1) {
                        return telephonyManager.getImei(0) + "##" + telephonyManager.getImei(1);
                    } else return telephonyManager.getImei();

                } else {
                    if (telephonyManager.getPhoneCount() > 1) {
                        return telephonyManager.getDeviceId(0) + "##" + telephonyManager.getDeviceId(1);
                    } else return telephonyManager.getDeviceId();
                }

            } else return telephonyManager.getDeviceId();

        } catch (Exception e) {
            return "0";
        }


    }
    public static JSONObject mergeJSONObjects(JSONObject... objects) {
        if (objects.length > 1) {
            JSONObject first = objects[0];

            for (int i = 1; i < objects.length; i++) {
                JSONObject later = objects[i];
                Iterator<String> keys = later.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    try {
                        first.put(key, later.get(key));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG + "mergeJSONObjects", e.getMessage());
                    }
                }
            }
            return first;
        } else if (objects.length == 1) {
            return objects[0];
        } else {
            return null;
        }
    }
    public static boolean isTimeNetworkProvided(Context context) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            return (Settings.System.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1 &&
                    Settings.System.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0) == 1);
        } else {
            return (Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1 &&
                    Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0) == 1);
        }
    }
    public static void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "sales_assist");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
//            Toasty.makeText(context, "Saved", Toast.LENGTH_SHORT,true).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static Long getTimeInMilliSec() {

        Calendar cal = Calendar.getInstance();
        final String currentDate = cal.get(Calendar.DAY_OF_MONTH) + "-"
                + cal.get(Calendar.MONTH) + "-"
                + cal.get(Calendar.YEAR) + " "
                + cal.get(Calendar.HOUR_OF_DAY) + ":"
                + cal.get(Calendar.MINUTE) + ":"
                + cal.get(Calendar.SECOND) + "."
                + cal.get(Calendar.MILLISECOND);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss.SSSSS", Locale.getDefault());
        Date date = null;

        try {
            date = sdf.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        cal.setTime(date);
        return cal.getTimeInMillis();
    }
    public static String getCurrentDate(){
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormatCr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return simpleDateFormatCr.format(calendar.getTime());
    }
    public static String getNumberFormatted(double value) {
        if (value == 0)
            return "0";

        DecimalFormat df = new DecimalFormat("#,###.00");
        df.setRoundingMode(RoundingMode.DOWN);
        try {
            return df.format(value);
        } catch (NumberFormatException e) {
            return String.valueOf(value);
        }
    }
    private static String getCallerCallerClassName() {
        StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
        StringBuilder callBuilder = new StringBuilder();
        for (StackTraceElement ste : stElements) {
            if (ste != null)
                callBuilder.append(ste.getClassName()).append("\n");
        }
        return callBuilder.toString();
    }
    public static ByteArrayOutputStream compressImage(String filePath) {

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 1920×1080
        float maxHeight = 1620.0f;
        float maxWidth = 1080.0f;
//        float maxHeight = 816.0f;
//        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }


        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        return out;

    }
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
    public static String getAppVersion(Context context) {
        try {
            version = (version == null) ? (context.getPackageManager().getPackageInfo(context.getPackageName(), 0)).versionName : version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }
    private static boolean isSystemPackage(PackageInfo pkgInfo) {
        return (pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    public static String getCurrentDateTime() {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.getDefault());
        return simpleDateFormat.format(calendar.getTime());
    }


}
