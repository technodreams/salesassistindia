package com.cgtechnodreams.salesassistindia.utils.scanner;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cgtechnodreams.salesassistindia.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import es.dmoral.toasty.Toasty;

public class BarcodeScannerActivity extends AppCompatActivity {

    String scanContent;
    String scanFormat;
    TextView textView;
    Button button,btnBarCodeScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_scanner);
        textView = (TextView) findViewById(R.id.textView);
       // button = (Button) findViewById(R.id.button);
        btnBarCodeScanner = (Button) findViewById(R.id.btnBarCodeScanner);
        btnBarCodeScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toasty.makeText(BarcodeScannerActivity.this,"this is test",Toast.LENGTH_SHORT,true).show();
                IntentIntegrator scanIntegrator = new IntentIntegrator(BarcodeScannerActivity.this);
                scanIntegrator.setPrompt("Please place the bar code or QR code inside the box");
                scanIntegrator.setBeepEnabled(false);
                scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                scanIntegrator.setBeepEnabled(true);
                scanIntegrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
                scanIntegrator.setOrientationLocked(true);
                scanIntegrator.setBarcodeImageEnabled(true);
                scanIntegrator.initiateScan();
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanningResult != null) {
            if (scanningResult.getContents() != null) {
                scanContent = scanningResult.getContents().toString();
                scanFormat = scanningResult.getFormatName().toString();
            }

          //  Toasty.makeText(this, scanContent + "   type:" + scanFormat, Toast.LENGTH_SHORT,true).show();

            textView.setText(scanContent + "    type:" + scanFormat);

        } else {
            Toasty.info(this, "Nothing scanned", Toast.LENGTH_SHORT,true).show();
        }
    }


}
