package com.cgtechnodreams.salesassistindia.stock.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StockistStockResponse extends BaseResponse {
    @JsonProperty("data")
    ArrayList<Stock> distributorStock;

    public ArrayList<Stock> getDistributorStock() {
        return distributorStock;
    }

    public void setDistributorStock(ArrayList<Stock> distributorStock) {
        this.distributorStock = distributorStock;
    }
}
