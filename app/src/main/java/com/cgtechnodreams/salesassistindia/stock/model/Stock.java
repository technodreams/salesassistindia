package com.cgtechnodreams.salesassistindia.stock.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Stock implements Serializable {
    @JsonProperty("id")
    int id;
    @JsonProperty("product_code")
    String productCode;
    @JsonProperty("quantity")
    int quantity;
    @JsonProperty("outlet_id")
    int outletId;
    @JsonProperty("product_name")
    String productName;
    @JsonProperty("update_date")
    String updateDate;
    @JsonProperty("product_id")
    int productId;
    @JsonProperty("division_id")
    int divisionId;
    @JsonProperty("division_name")
    String divisionName;

    @JsonProperty("non_saleable_product")
    int nonSalableStock;
    @JsonProperty("saleable_product")
    int saleableStock;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getOutletId() {
        return outletId;
    }

    public void setOutletId(int outletId) {
        this.outletId = outletId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public int getNonSalableStock() {
        return nonSalableStock;
    }

    public void setNonSalableStock(int nonSalableStock) {
        this.nonSalableStock = nonSalableStock;
    }

    public int getSaleableStock() {
        return saleableStock;
    }

    public void setSaleableStock(int saleableStock) {
        this.saleableStock = saleableStock;
    }

//    public int getSystemCalculatedStock() {
//        return systemCalculatedStock;
//    }
//
//    public void setSystemCalculatedStock(int systemCalculatedStock) {
//        this.systemCalculatedStock = systemCalculatedStock;
//    }

}
