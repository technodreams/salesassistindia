package com.cgtechnodreams.salesassistindia.stock.data;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.stock.model.StockistStockResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StockDataService {
    private Context mContext;
    public StockDataService(Context mContext) {
        this.mContext = mContext;
    }
   public void getDistributorStock(String token, int distributorId,String from,String to, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<StockistStockResponse> callBack = Api.getService(mContext).getStockistStock(distributorId, token,from,to);
        callBack.enqueue(new Callback<StockistStockResponse>() {
            @Override
            public void onResponse(Call<StockistStockResponse> call, Response<StockistStockResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<StockistStockResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

}
