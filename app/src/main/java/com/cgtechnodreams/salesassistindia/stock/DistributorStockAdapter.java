package com.cgtechnodreams.salesassistindia.stock;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.stock.model.Stock;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributorStockAdapter extends RecyclerView.Adapter<DistributorStockAdapter.MyViewHolder> {
    Context mContext;
    PrefManager prefManager;
    ArrayList<Stock> stocks = new ArrayList<>();

    public DistributorStockAdapter(Context context, ArrayList<Stock> stocks) {
        this.mContext = context;
        this.stocks = stocks;
        prefManager = new PrefManager(mContext);
    }

    @NonNull
    @Override
    public DistributorStockAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_view_stock, null);
        return new DistributorStockAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DistributorStockAdapter.MyViewHolder holder, int position) {
        Stock eachStock = stocks.get(position);
        holder.name.setText(eachStock.getProductName());
       // holder.quantity.setText("Qty: " + eachStock.getQuantity());
        holder.salableStock.setText(eachStock.getSaleableStock() + "");
        holder.autoCalculatedStock.setText(eachStock.getQuantity() + "");
        holder.nonSalableStock.setText(eachStock.getNonSalableStock()+"");

        holder.date.setText("Updated Date: " + eachStock.getUpdateDate());
    }

    @Override
    public int getItemCount() {
        return stocks.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.productName)
        TextView name;
//        @BindView(R.id.productQuantity)
//        TextView quantity;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.salableStock)
        TextView salableStock;
        @BindView(R.id.nonSalableStock)
        TextView nonSalableStock;
        @BindView(R.id.autoCalculatedStock)
        TextView autoCalculatedStock;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }
}

