package com.cgtechnodreams.salesassistindia.stock;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistStockEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.salesorder.IMethodCaller;
import com.cgtechnodreams.salesassistindia.stock.model.StockistStockModel;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewDistributorStockActivity extends AppCompatActivity implements DialogInterface.OnDismissListener, IMethodCaller {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ViewStockAdapter adapter;
    PrefManager prefManager;
    ArrayList<StockistStockModel> distributorStock;
    Stockist stockist;
    boolean isDamage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_distributor_stock);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Captured Stock");
        stockist = (Stockist) getIntent().getSerializableExtra("DistributorDetail");
        isDamage = getIntent().getBooleanExtra("IsDamage",false);

        getStock(isDamage);
        setData();

    }

    private void setData() {
        LinearLayoutManager salesOrderManager = new LinearLayoutManager(ViewDistributorStockActivity.this, RecyclerView.VERTICAL, false);
        adapter = new ViewStockAdapter(ViewDistributorStockActivity.this, distributorStock, isDamage);
        recyclerView.setLayoutManager(salesOrderManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void getStock(boolean isDamage) {
        if(!isDamage){
            StockistStockEntity stockistStockEntity = new StockistStockEntity(this);
            JSONArray jsonArray = (JSONArray) stockistStockEntity.findById(stockist.getDistributorId());

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                distributorStock = objectMapper.readValue(String.valueOf(jsonArray), new TypeReference<ArrayList<StockistStockModel>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            StockistDamageStockEntity stockistDamageStockEntity = new StockistDamageStockEntity(this);
            JSONArray jsonArray = (JSONArray) stockistDamageStockEntity.findById(stockist.getDistributorId());

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                distributorStock = objectMapper.readValue(String.valueOf(jsonArray), new TypeReference<ArrayList<StockistStockModel>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        getStock(isDamage);
        setData();

    }

    @Override
    public void onDeleteOrder() {

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
