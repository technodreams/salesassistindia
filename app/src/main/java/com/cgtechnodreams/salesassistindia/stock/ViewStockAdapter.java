package com.cgtechnodreams.salesassistindia.stock;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistStockEntity;
import com.cgtechnodreams.salesassistindia.stock.model.StockistStockModel;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewStockAdapter extends RecyclerView.Adapter<ViewStockAdapter.MyViewHolder> {
    Context mContext;
    PrefManager prefManager;
    boolean isDamage;
    ArrayList<StockistStockModel> distributorStock = new ArrayList<>();
    public ViewStockAdapter(Context context, ArrayList<StockistStockModel> distributorStock, boolean isDamage) {
        this.mContext = context;
        this.distributorStock = distributorStock;
        this.isDamage = isDamage;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public ViewStockAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_view_edit_stock, null);
        return new ViewStockAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewStockAdapter.MyViewHolder holder, int position) {
        StockistStockModel stockistStockModel = distributorStock.get(position);

        holder.productName.setText(stockistStockModel.getProductName());
        holder.productStock.setText("Qty: " + stockistStockModel.getQuantity() );
        holder.date.setText("Date: " + stockistStockModel.getDate());
    }

    @Override
    public int getItemCount() {
        return distributorStock.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.quantity)
        TextView productStock;
        @BindView(R.id.name)
        TextView productName;
        @BindView(R.id.edit)
        AppCompatImageView imgEdit;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.delete)
        AppCompatImageView imgDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm =((AppCompatActivity) mContext).getSupportFragmentManager();
                    UpdateStockDialog dialogFragment = new UpdateStockDialog().newInstance(distributorStock.get(getAdapterPosition()), isDamage);
                    dialogFragment.show(fm, "dialog");
                }
            });

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isDamage){
                        StockistStockEntity entity = new StockistStockEntity(mContext);
                        entity.deleteById(distributorStock.get(getAdapterPosition()).getDistributorId(),distributorStock.get(getAdapterPosition()).getProductId());
                        distributorStock.remove(distributorStock.get(getAdapterPosition()));
                        notifyDataSetChanged();
                    }else{
                        StockistDamageStockEntity entity = new StockistDamageStockEntity(mContext);
                        entity.deleteById(distributorStock.get(getAdapterPosition()).getDistributorId(),distributorStock.get(getAdapterPosition()).getProductId());
                        distributorStock.remove(distributorStock.get(getAdapterPosition()));
                        notifyDataSetChanged();
                    }

                }
            });

        }
    }
}
