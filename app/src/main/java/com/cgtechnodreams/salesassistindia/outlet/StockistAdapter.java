package com.cgtechnodreams.salesassistindia.outlet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.fieldwork.FieldWorkActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StockistAdapter extends RecyclerView.Adapter<StockistAdapter.MyViewHolder> {
    Context mContext;
    PrefManager prefManager;
    ArrayList<Stockist> stockists = new ArrayList<>();

    public StockistAdapter(Context context, ArrayList<Stockist> stockists) {
        this.mContext = context;
        this.stockists = stockists;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public StockistAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_outlet, null);
        return new StockistAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StockistAdapter.MyViewHolder holder, int position) {
        Stockist stockist = stockists.get(position);
        holder.name.setText(stockist.getName());
        holder.address.setText(stockist.getAddress());
        holder.businessUnit.setText("Business Unit: " + stockist.getBusinessUnitName());
    }

    @Override
    public int getItemCount() {
        return stockists.size();
    }

    public void setFilter(ArrayList<Stockist> newList) {

        stockists = new ArrayList<>();
        stockists.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.businessUnit)
        TextView businessUnit;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, FieldWorkActivity.class);
                    intent.putExtra(AppConstant.STOCKIST_DETAIL, stockists.get(getAdapterPosition()));
                    ((Activity)mContext).startActivity(intent);
                }
            });

        }

    }
}
