package com.cgtechnodreams.salesassistindia.outlet.model;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Stockist implements Serializable {
    @JsonProperty("distributor_id")
    private int distributorId;
    @JsonProperty("distributor_name")
    private String name;
    @JsonProperty("mobile_number")
    private String mobileNumber;
    @Nullable
    @JsonProperty("landline_number")
    private String landLine;
    @Nullable
    @JsonProperty("email")
    private String email;
    @JsonProperty("address")
    private String address;
    @JsonProperty("contact_person")
    private String contactPerson;
    @Nullable
    @JsonProperty("pan_number")
    private String panNumber;
    @JsonProperty("latitude")
    private String latitude;
    @JsonProperty("longitude")
    private String longitude;
    @JsonProperty("area_id")
    private int areaId;
    @JsonProperty("area_name")
    private String areaName;
    @JsonProperty("district_id")
    private int districtId;
    @JsonProperty("price_group_id")
    private int priceGroupId;
    @JsonProperty("price_group_name")
    private String priceGroupName;
    @JsonProperty("district_name")
    private String districtName;
    @JsonProperty("business_unit_id")
    private int businessUnitId;
    @JsonProperty("business_unit_name")
    private String businessUnitName;
    @JsonProperty("local_administrative_name")
    private String localAdministrativeName;
    @JsonProperty("bg_amount")
    private double bgAmount;


    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Nullable
    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(@Nullable String landLine) {
        this.landLine = landLine;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    @Nullable
    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(@Nullable String panNumber) {
        this.panNumber = panNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public int getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(int businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public String getBusinessUnitName() {
        return businessUnitName;
    }

    public void setBusinessUnitName(String businessUnitName) {
        this.businessUnitName = businessUnitName;
    }

    public double getBgAmount() {
        return bgAmount;
    }

    public void setBgAmount(double bgAmount) {
        this.bgAmount = bgAmount;
    }
    public String getLocalAdministrativeName() {
        return localAdministrativeName;
    }

    public void setLocalAdministrativeName(String localAdministrativeName) {
        this.localAdministrativeName = localAdministrativeName;
    }

    public int getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(int priceGroupId) {
        this.priceGroupId = priceGroupId;
    }

    public String getPriceGroupName() {
        return priceGroupName;
    }

    public void setPriceGroupName(String priceGroupName) {
        this.priceGroupName = priceGroupName;
    }
}
