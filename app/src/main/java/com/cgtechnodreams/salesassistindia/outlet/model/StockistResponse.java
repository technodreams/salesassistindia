package com.cgtechnodreams.salesassistindia.outlet.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class StockistResponse extends BaseResponse {


    public ArrayList<Stockist> getStockists() {
        return stockists;
    }

    public void setStockists(ArrayList<Stockist> stockists) {
        this.stockists = stockists;
    }

    @JsonProperty("data")
    ArrayList<Stockist> stockists;
}
