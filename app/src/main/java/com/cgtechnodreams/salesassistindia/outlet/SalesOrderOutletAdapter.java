package com.cgtechnodreams.salesassistindia.outlet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesOrderOutletAdapter extends RecyclerView.Adapter<SalesOrderOutletAdapter.MyViewHolder> {
    Context mContext;
    PrefManager prefManager;
    ArrayList<Stockist> stockists = new ArrayList<>();

    public SalesOrderOutletAdapter(Context context, ArrayList<Stockist> stockists) {
        this.mContext = context;
        this.stockists = stockists;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public SalesOrderOutletAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_outlet, null);
        return new SalesOrderOutletAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesOrderOutletAdapter.MyViewHolder holder, int position) {
        Stockist stockist = stockists.get(position);
        holder.name.setText(stockist.getName());
        holder.address.setText(stockist.getAddress());
        holder.businessUnit.setText("Business Unit: " + stockist.getBusinessUnitName());
    }

    @Override
    public int getItemCount() {
        return stockists.size();
    }

    public void setFilter(ArrayList<Stockist> newList) {

        stockists = new ArrayList<>();
        stockists.addAll(newList);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.businessUnit)
        TextView businessUnit;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", stockists.get(getAdapterPosition()));
                    ((Activity) mContext).setResult(Activity.RESULT_OK,returnIntent);
                    ((Activity) mContext).finish();
                    //  finish();
                }
            });

        }

    }
}
