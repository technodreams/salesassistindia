package com.cgtechnodreams.salesassistindia.outlet;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StockistListActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    StockistAdapter stockistAdapter = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ArrayList<Stockist> stockists = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StockistListActivity.this, NewStockistActivity.class);
                startActivity(intent);
            }
        });
        ButterKnife.bind(this);
        toolbar.setTitle("Stockist");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        StockistEntity stockistEntity = new StockistEntity(this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) stockistEntity.find(null);
        try {
            objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            stockists = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Stockist>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        LinearLayoutManager promoterListManager = new LinearLayoutManager(StockistListActivity.this, LinearLayoutManager.VERTICAL, false);
        stockistAdapter = new StockistAdapter(StockistListActivity.this, stockists);
        recyclerView.setLayoutManager(promoterListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(stockistAdapter);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.search_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<Stockist> newList = new ArrayList<>();
                for (Stockist stockist : stockists) {
                    String name = stockist.getName().toLowerCase();
                    String address = stockist.getAddress().toLowerCase();

                    if (name.contains(newText) || address.toLowerCase().contains(newText) ) {
                        newList.add(stockist);
                    }

                    stockistAdapter.setFilter(newList);

                }
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                          }
                                      }
        );
        return super.onCreateOptionsMenu(menu);
    }
}