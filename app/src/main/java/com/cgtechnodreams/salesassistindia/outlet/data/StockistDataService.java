package com.cgtechnodreams.salesassistindia.outlet.data;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.fieldwork.model.StockistAchievementResponse;
import com.cgtechnodreams.salesassistindia.outlet.model.StockistStatus;
import com.cgtechnodreams.salesassistindia.stock.model.StockistStockModel;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.NewStockistRequest;
import com.cgtechnodreams.salesassistindia.outlet.model.StockistResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockistDataService {
    private Context mContext;
    PrefManager prefManager;

    public StockistDataService(Context mContext) {
        this.mContext = mContext;
        prefManager = new PrefManager(mContext);
    }

    public void getStockist(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<StockistResponse> callBack = Api.getService(mContext).getStockist(token);
        callBack.enqueue(new Callback<StockistResponse>() {
            @Override
            public void onResponse(Call<StockistResponse> call, Response<StockistResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                    if(response.code() == APIConstants.ERROR_UNAUTHORIZED ){
                        Intent intent = new Intent(mContext,LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PrefManager preferenceManager = new PrefManager(mContext);
                        preferenceManager.clear();
                        (mContext).startActivity(intent);
                        ((Activity) mContext).finish();
                    }

                }
            }

            @Override
            public void onFailure(Call<StockistResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getStockistAchievement(String token, int id, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<StockistAchievementResponse> callBack = Api.getService(mContext).getOutletAchievement(id,token);
        callBack.enqueue(new Callback<StockistAchievementResponse>() {
            @Override
            public void onResponse(Call<StockistAchievementResponse> call, Response<StockistAchievementResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));


                }
            }

            @Override
            public void onFailure(Call<StockistAchievementResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void postOutletRequest(String token, ArrayList<NewStockistRequest> stockistRequests, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postOutletRequest(token,stockistRequests);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void postStockistStatus(String token, ArrayList<StockistStatus> stockistStatuses, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postStockistStatus(token, stockistStatuses);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);

                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void postOutletStock(String token, ArrayList<StockistStockModel> model, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postOutletStock(token,model);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void postStockistDamageStock(String token, ArrayList<StockistStockModel> model, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postDamageStokistStock(token,model);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

}
