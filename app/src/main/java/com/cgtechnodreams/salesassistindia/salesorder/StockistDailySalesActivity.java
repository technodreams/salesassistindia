package com.cgtechnodreams.salesassistindia.salesorder;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistDailySalesEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.product.SearchProductActivity;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.salesorder.adapter.StockistDailySalesAdapter;
import com.cgtechnodreams.salesassistindia.salesorder.model.StockistDailySales;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class StockistDailySalesActivity extends AppCompatActivity  implements DialogInterface.OnDismissListener {

    private static final int REQUEST_CODE_SEARCH_PRODUCT = 100;
    LocationUtils locationUtils;
    @NotEmpty(messageId = R.string.product_empty)
    @BindView(R.id.productName) AppCompatEditText productName;

    @NotEmpty(messageId = R.string.distributor_empty)
    @BindView(R.id.name) TextView distributorName;
    @NotEmpty(messageId = R.string.quantity_empty)
    @BindView(R.id.quantity)  AppCompatEditText quantity;

    @BindView(R.id.date)  AppCompatEditText date;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    PrefManager prefManager = null;
    int day, month, year;
    Stockist stockist;
    Product selectedProduct = null;
    StockistDailySalesAdapter adapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor_daily_sales);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Stockist Daily Sales");

        locationUtils = new LocationUtils(StockistDailySalesActivity.this);
        locationUtils.requestLocationUpdates();
        stockist = (Stockist) getIntent().getSerializableExtra(AppConstant.STOCKIST_DETAIL);

        if (stockist != null) {
            distributorName.setText(stockist.getName() + " ( " + stockist.getBusinessUnitName()+ " )");
            getOrderSummary();
        }
    }

    @OnClick({R.id.btnSave,R.id.date,R.id.productName})
    public void onButtonClicked(View view){
        switch (view.getId()) {
            case R.id.btnSave:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    btnSave();
                }
                break;
            case R.id.date:

                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        month = month + 1;
                        String tempDay = day + "";
                        String tempMonth = month + "";
                        if (day < 10) {
                            tempDay = "0" + day;
                        }
                        if (month < 10) {
                            tempMonth = "0" + month;
                        }
                        String tempDate = year + "-" + (tempMonth) + "-" + tempDay;
                        date.setText(tempDate);
                    }
                });
                dpd.setVersion(DatePickerDialog.Version.VERSION_2);
                dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                dpd.show(getSupportFragmentManager(), "DatePicker");
                break;
            case R.id.productName:
                Intent product = new Intent(StockistDailySalesActivity.this, SearchProductActivity.class);
                product.putExtra("BusinessUnitId", stockist.getBusinessUnitId());
                startActivityForResult(product, REQUEST_CODE_SEARCH_PRODUCT);

        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SEARCH_PRODUCT) {
            if (resultCode == Activity.RESULT_OK) {
                selectedProduct = (Product) data.getSerializableExtra("Result");
                if (selectedProduct != null) {
                    productName.setText(selectedProduct.getProductName());
                    quantity.setText("");
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

private void getOrderSummary() {
    ArrayList<StockistDailySales> dailySales = null;
    StockistDailySalesEntity dailySalesEntity = new StockistDailySalesEntity(StockistDailySalesActivity.this);
    ObjectMapper objectMapper = new ObjectMapper();
    JSONArray array = (JSONArray)dailySalesEntity.findById(stockist.getDistributorId());
    try {
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        dailySales = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<StockistDailySales>>(){});
    } catch (IOException e) {
        e.printStackTrace();
    }
    LinearLayoutManager salesOrderManager = new LinearLayoutManager(StockistDailySalesActivity.this, RecyclerView.VERTICAL, false);
    adapter = new StockistDailySalesAdapter(StockistDailySalesActivity.this, dailySales);
    recyclerView.setLayoutManager(salesOrderManager);
    recyclerView.setHasFixedSize(true);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.setAdapter(adapter);
    adapter.notifyDataSetChanged();
}
    private void btnSave() {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormatCr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        StockistDailySalesEntity dailySalesEntity = new StockistDailySalesEntity(StockistDailySalesActivity.this);
        StockistDailySales stockistDailySales = new StockistDailySales();
        stockistDailySales.setProductId(selectedProduct.getProductId());
        stockistDailySales.setQuantity(Integer.parseInt(quantity.getText().toString()));
        stockistDailySales.setProductName(selectedProduct.getProductName());
        stockistDailySales.setMiti(date.getText().toString());
        stockistDailySales.setDate(simpleDateFormatCr.format(calendar.getTime()));
        stockistDailySales.setDistributorId(stockist.getDistributorId());
        stockistDailySales.setDivisionId(stockist.getBusinessUnitId());
        stockistDailySales.setDistributorName(stockist.getName());
        stockistDailySales.setDivisionName(stockist.getBusinessUnitName());
        stockistDailySales.setLatitude(Double.parseDouble(prefManager.getLatitude()));
        stockistDailySales.setLongitude(Double.parseDouble(prefManager.getLongitude()));

        long result = dailySalesEntity.insert(stockistDailySales);
        if(result>0){
            quantity.setText("");
            getOrderSummary();
            productName.setText("");
            selectedProduct = null;
            Toasty.info(StockistDailySalesActivity.this,"Daily Sales Saved",Toasty.LENGTH_SHORT).show();
        }


    }
    @Override
    public void onDismiss(DialogInterface dialog) {
        getOrderSummary();
    }

//    @Override
//    public void onDeleteOrder() {
//        getOrderSummary();
//    }
}
