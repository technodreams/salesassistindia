package com.cgtechnodreams.salesassistindia.salesorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistDailySalesEntity;
import com.cgtechnodreams.salesassistindia.salesorder.EditDailySalesDialog;
import com.cgtechnodreams.salesassistindia.salesorder.model.StockistDailySales;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StockistDailySalesAdapter extends RecyclerView.Adapter<StockistDailySalesAdapter.MyViewHolder>{

    Context mContext;
    PrefManager prefManager;
    ArrayList<StockistDailySales> dailySales = new ArrayList<>();

    public StockistDailySalesAdapter(Context context, ArrayList<StockistDailySales> dailySales) {
        this.mContext = context;
        this.dailySales = dailySales;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public StockistDailySalesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_distributor_daily_sales, null);

        return new StockistDailySalesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StockistDailySalesAdapter.MyViewHolder holder, int position) {
        StockistDailySales eachSales = dailySales.get(position);
        holder.productName.setText(eachSales.getProductName());
        holder.quantity.setText( "Qty: " + eachSales.getQuantity() + "");

    }

    @Override
    public int getItemCount() {
        return dailySales.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.edit)
        ImageView imgEdit;
        @BindView(R.id.delete)
        ImageView imgDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm =((AppCompatActivity) mContext).getSupportFragmentManager();
                    EditDailySalesDialog dialogFragment = new EditDailySalesDialog().newInstance(dailySales.get(getAdapterPosition()));
                    dialogFragment.show(fm, "dialog");

                }
            });
            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StockistDailySalesEntity stockistDailySalesEntity = new StockistDailySalesEntity(mContext);
                    stockistDailySalesEntity.deleteById(dailySales.get(getAdapterPosition()).getId());
                    dailySales.remove(dailySales.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });


        }
    }
}
