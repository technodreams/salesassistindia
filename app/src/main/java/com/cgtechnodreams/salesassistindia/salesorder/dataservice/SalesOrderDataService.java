package com.cgtechnodreams.salesassistindia.salesorder.dataservice;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesDetailResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequestResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.SalesmanSalesOrderReportRepsonse;
import com.cgtechnodreams.salesassistindia.salesorder.model.SalesmanSalesRequestResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesOrderDataService {
    private Context mContext;

    public SalesOrderDataService(Context mContext) {
        this.mContext = mContext;
    }
    public void getSalesmanSalesOrderRequestReport(String token,  final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<SalesmanSalesOrderReportRepsonse> callBack = Api.getService(mContext).getSalesmanSalesOrderRequestReport(token);
        callBack.enqueue(new Callback<SalesmanSalesOrderReportRepsonse>() {
            @Override
            public void onResponse(Call<SalesmanSalesOrderReportRepsonse> call, Response<SalesmanSalesOrderReportRepsonse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));


                }
            }

            @Override
            public void onFailure(Call<SalesmanSalesOrderReportRepsonse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getSalesmanSalesOrderRequestDetailReport(final String salesmanId, final String distributorId,String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<SalesmanSalesRequestResponse> callBack = Api.getService(mContext).getSalesRequestDetail(salesmanId,distributorId,token);
        callBack.enqueue(new Callback<SalesmanSalesRequestResponse>() {
            @Override
            public void onResponse(Call<SalesmanSalesRequestResponse> call, Response<SalesmanSalesRequestResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<SalesmanSalesRequestResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getSalesmanPendingOrderRequest(String token,  final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<PendingSalesRequestResponse> callBack = Api.getService(mContext).getPendingSalesRequest(token);
        callBack.enqueue(new Callback<PendingSalesRequestResponse>() {
            @Override
            public void onResponse(Call<PendingSalesRequestResponse> call, Response<PendingSalesRequestResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<PendingSalesRequestResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getPendingSalesDetail(final int id, String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<PendingSalesDetailResponse> callBack = Api.getService(mContext).getPendingSalesRequestDetail(id,token);
        callBack.enqueue(new Callback<PendingSalesDetailResponse>() {
            @Override
            public void onResponse(Call<PendingSalesDetailResponse> call, Response<PendingSalesDetailResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<PendingSalesDetailResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void updateEachPendingSalesDetail(final int id, final int quantity,String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).updateEachPendingSalesRequestDetail(id,token,quantity);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
}
