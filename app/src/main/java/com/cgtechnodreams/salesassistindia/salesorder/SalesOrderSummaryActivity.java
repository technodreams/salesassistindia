package com.cgtechnodreams.salesassistindia.salesorder;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.FMCGSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.salesorder.adapter.SalesOrderSummaryAdapter;
import com.cgtechnodreams.salesassistindia.salesorder.model.StockistPendingSyncTotal;
import com.cgtechnodreams.salesassistindia.sync.Sync;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesOrderSummaryActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    SalesOrderSummaryAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_order_summary);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pending Stockist's Order Summary");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(this);
        ArrayList<StockistPendingSyncTotal> stockistPendingSyncTotals = salesOrderEntity.getOutletPendingSyncTotal();
        setData(stockistPendingSyncTotals);
    }

    private void setData(ArrayList<StockistPendingSyncTotal> stockistPendingSyncTotals) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(SalesOrderSummaryActivity.this, RecyclerView.VERTICAL, false);
        adapter = new SalesOrderSummaryAdapter(SalesOrderSummaryActivity.this, stockistPendingSyncTotals);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(SalesOrderSummaryActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(SalesOrderSummaryActivity.this, PendingSyncOrderActivity.class);
                intent.putExtra("OutletName", stockistPendingSyncTotals.get(position).getOutletName());
                intent.putExtra("OutletId", stockistPendingSyncTotals.get(position).getOutletId());

                startActivity(intent);
            }
            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(this);
        ArrayList<StockistPendingSyncTotal> stockistPendingSyncTotals = salesOrderEntity.getOutletPendingSyncTotal();
        setData(stockistPendingSyncTotals);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.order_summary, menu);

        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_sync:
                Sync sync = new Sync(SalesOrderSummaryActivity.this);
                sync.uploadSalesOrder();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
