package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

public class PendingSalesRequestResponse extends BaseResponse implements Serializable {
    @JsonProperty("data")
    ArrayList<PendingSalesRequest> pendingSalesRequest;


    public ArrayList<PendingSalesRequest> getPendingSalesRequest() {
        return pendingSalesRequest;
    }

    public void setPendingSalesRequest(ArrayList<PendingSalesRequest> pendingSalesRequest) {
        this.pendingSalesRequest = pendingSalesRequest;
    }
}
