package com.cgtechnodreams.salesassistindia.salesorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesorder.model.SalesmanSalesOrderReprot;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesmanSalesOrderAdapter extends RecyclerView.Adapter<SalesmanSalesOrderAdapter.MyViewHolder>{

    Context mContext;
    PrefManager prefManager;
    ArrayList<SalesmanSalesOrderReprot> salesRequestList = new ArrayList<>();

    public SalesmanSalesOrderAdapter(Context context, ArrayList<SalesmanSalesOrderReprot> salesRequestList) {
        this.mContext = context;
        this.salesRequestList = salesRequestList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public SalesmanSalesOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_salesorder_request_report, null);

        return new SalesmanSalesOrderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SalesmanSalesOrderAdapter.MyViewHolder holder, int position) {
        SalesmanSalesOrderReprot eachSalesOrderReport = salesRequestList.get(position);
        holder.distributorAddress.setText(eachSalesOrderReport.getDistributorArea());
        holder.distributorName.setText(eachSalesOrderReport.getDistributorName());
        holder.salesmanName.setText(eachSalesOrderReport.getSalesmanName());
    }

    @Override
    public int getItemCount() {
        return salesRequestList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.distributorAddress)
        TextView distributorAddress;
        @BindView(R.id.name)
        TextView distributorName;
        @BindView(R.id.salesmanName)
        TextView salesmanName;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
//            imgEdit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });



        }
    }
}


