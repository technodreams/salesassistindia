package com.cgtechnodreams.salesassistindia.salesorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesmanSalesDetail;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesmanSalesDetailAdapter extends RecyclerView.Adapter<SalesmanSalesDetailAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<SalesmanSalesDetail> mData = new ArrayList<>();

    public SalesmanSalesDetailAdapter(Context context, ArrayList<SalesmanSalesDetail> data) {
        this.mContext = context;
        this.mData = data;
    }

    @Override
    public SalesmanSalesDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_salesorder_detail, null);

        return new SalesmanSalesDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SalesmanSalesDetailAdapter.MyViewHolder holder, int position) {
            holder.name.setText(mData.get(position).getProductName());

            holder.quantity.setText(mData.get(position).getQuantity() + "");
            holder.dealerPrice.setText(Utils.round(mData.get(position).getDealerPrice(),2)+"");
            holder.total.setText(Utils.round(mData.get(position).getTotal(),2) + "");

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.warehouse)
        TextView warehouse;
        @BindView(R.id.dealerPrice)
        TextView dealerPrice;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.total)
        TextView total;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);



        }
    }
}

