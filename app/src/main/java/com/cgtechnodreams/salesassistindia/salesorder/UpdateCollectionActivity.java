package com.cgtechnodreams.salesassistindia.salesorder;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.andy6804tw.zoomimageviewlibrary.ZoomImageView;
import com.cgtechnodreams.salesassistindia.BaseApplication;
import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.bank.BankActivity;
import com.cgtechnodreams.salesassistindia.bank.model.Bank;
import com.cgtechnodreams.salesassistindia.company.CompanyListActivity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CollectionEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CollectionImageEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionDetailModel;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionImages;
import com.cgtechnodreams.salesassistindia.salesorder.model.Company;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.CameraUtils;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.PathUtil;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.adapter.image.impl.GlideAdapter;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

import static com.cgtechnodreams.salesassistindia.utils.AppConstant.MEDIA_TYPE_IMAGE;
import static com.cgtechnodreams.salesassistindia.utils.AppConstant.MEDIA_TYPE_IMAGE_COLLECTION;

public class UpdateCollectionActivity extends AppCompatActivity {
    public static final int COLLECTION_IMAGE_REQUEST_CODE = 105;
    private static final int REQUEST_CODE_SEARCH_BANK = 100;
    private static final int REQUEST_CODE_SEARCH_COMPANY = 102;
    private static String imageStoragePath = "";
    String imageName = "";
    Uri fileUri;
    @BindView(R.id.collectionImage)
    ZoomImageView collectionImage;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.stockist)
    EditText stockist;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.bank)
    EditText bank;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.company)
    EditText company;
    @BindView(R.id.chequeNumber)
    EditText chequeNumber;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.amount)
    EditText chequeAmount;
    @BindView(R.id.remarks)
    EditText remarks;
    @BindView(R.id.chequeNumberContainer)
    LinearLayout checkNumberContainer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.containerAcountNumber)
    LinearLayout containerAccountNumber;
    @BindView(R.id.accountNumber)
    EditText accountNumber;
    @BindView(R.id.takePhoto)
    TextView takePhoto;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.date)
    EditText cashInDate;
    @BindView(R.id.tvCashDate)
    TextView tvCashDate;
    Stockist selectedStockist;
    Bank selectedBank;
    Company selectedCompany;
    String paymentMethod = "Cheque";
    String chequeStatus = "Present Date";
    PrefManager prefManager = null;
    CollectionDetailModel mCollection = null;
    @BindView(R.id.radioBankDeposit)
    RadioButton radioBankDeposit;
    @BindView(R.id.radioCheque)
    RadioButton radioCheque;
    @BindView(R.id.presentDate)
    RadioButton presentDate;
    @BindView(R.id.postDate)
    RadioButton postDate;
    @BindView(R.id.checkPostGroupContainer)
    LinearLayout checkPostGroupContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_detail);
        ButterKnife.bind(this);
        toolbar.setTitle("Update Collection Detail");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        mCollection = (CollectionDetailModel) getIntent().getSerializableExtra("CollectionDetail");

        setCollectionData(mCollection);
        RadioGroup rg = findViewById(R.id.paymentMethod);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioBankDeposit:
                        checkNumberContainer.setVisibility(View.GONE);
                        paymentMethod = "Bank Deposit";
                        chequeNumber.setText("");
                        tvCashDate.setText("Deposit Date");
                        checkPostGroupContainer.setVisibility(View.GONE);
                        containerAccountNumber.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioCheque:
                        checkNumberContainer.setVisibility(View.VISIBLE);
                        containerAccountNumber.setVisibility(View.GONE);
                        checkPostGroupContainer.setVisibility(View.VISIBLE);
                        paymentMethod = "Cheque";
                        tvCashDate.setText("Cheque Cash Date");
                        accountNumber.setText("");
                        break;

                }
            }
        });
        RadioGroup checkPostGroup = findViewById(R.id.checkPostGroup);
        checkPostGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.postDate:
                        chequeStatus = "Post Date";
                        break;
                    case R.id.presentDate:
                        chequeStatus = "Present Date";
                        break;
                }
            }
        });
    }

    private void setCollectionData(CollectionDetailModel mCollection) {
        stockist.setText(mCollection.getDistributorName());
        company.setText(mCollection.getCompanyName());
        bank.setText(mCollection.getBankName());
        chequeAmount.setText(String.valueOf(mCollection.getCollectionAmount()));

        cashInDate.setText(mCollection.getCashInDate());
        remarks.setText(mCollection.getRemarks());
        if(mCollection.getPaymentMethod().equalsIgnoreCase("Cheque")){
            checkNumberContainer.setVisibility(View.VISIBLE);
            containerAccountNumber.setVisibility(View.GONE);
            checkPostGroupContainer.setVisibility(View.VISIBLE);
            paymentMethod = "Cheque";
            tvCashDate.setText("Cheque Cash Date");
            accountNumber.setText("");
            radioCheque.setChecked(true);
            chequeNumber.setText(mCollection.getChequeNumber());

        }else{
            checkNumberContainer.setVisibility(View.GONE);
            paymentMethod = "Bank Deposit";
            chequeNumber.setText("");
            tvCashDate.setText("Deposit Date");
            checkPostGroupContainer.setVisibility(View.GONE);
            containerAccountNumber.setVisibility(View.VISIBLE);
            radioBankDeposit.setChecked(true);
            accountNumber.setText(mCollection.getAccountNumber());

        }
        if(mCollection.getChequeStatus().equalsIgnoreCase("Present Date")){
            chequeStatus = "Present Date";
            presentDate.setChecked(true);
        }else{
            chequeStatus = "Post Date";
            postDate.setChecked(true);
        }

        imageStoragePath = mCollection.getPhotoPath();
        fileUri = Uri.parse(mCollection.getFileUri());
        imageName = mCollection.getFileName();
        previewCapturedImage(collectionImage);

    }


    @OnClick({R.id.bank, R.id.btnSave, R.id.takePhoto, R.id.date, R.id.takePhotoFromGallery, R.id.company})
    public void onButtonClicked(View view) {
        switch (view.getId()) {

            case R.id.bank:
                if (stockist.getText().toString().equalsIgnoreCase("")) {
                    Toasty.info(UpdateCollectionActivity.this, "Please select stockist first!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    Intent product = new Intent(UpdateCollectionActivity.this, BankActivity.class);
                    startActivityForResult(product, REQUEST_CODE_SEARCH_BANK);
                }
                break;
            case R.id.company:
                Intent product = new Intent(UpdateCollectionActivity.this, CompanyListActivity.class);
                startActivityForResult(product, REQUEST_CODE_SEARCH_COMPANY);
                break;
            case R.id.btnSave:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    // Toast.makeText(CollectionActivity.this,paymentMethod,Toast.LENGTH_SHORT).show();
                    if (paymentMethod.equalsIgnoreCase("Cheque") && chequeNumber.getText().toString().equals("")) {
                        chequeNumber.setError("Field Required");
                    } else if ((paymentMethod.equalsIgnoreCase("Cheque") && chequeStatus.equalsIgnoreCase(""))) {
                        Toast.makeText(UpdateCollectionActivity.this, "Please select post date or present date", Toast.LENGTH_SHORT).show();

                    } else if (paymentMethod.equalsIgnoreCase("Bank Deposit") && accountNumber.getText().toString().equals("")) {
                        accountNumber.setError("Field Required");
                    } else {
                        updateCollectionDetail();
                    }

                }
                break;
            case R.id.takePhotoFromGallery:
                FishBun.with(this)
                        .setImageAdapter(new GlideAdapter())
                        .setIsUseDetailView(false)
                        .setMaxCount(1)
                        .setMinCount(1)
                        .setPickerSpanCount(6)
                        .setActionBarColor(R.color.colorPrimary, R.color.colorPrimaryDark, false)
                        .setActionBarTitleColor(Color.parseColor("#ffffff"))
                        .setAlbumSpanCount(2, 4)
                        .setButtonInAlbumActivity(false)
                        .setCamera(true)
                        .setReachLimitAutomaticClose(true)
                        .setAllViewTitle("All")
                        .setActionBarTitle("Image Library")
                        .textOnImagesSelectionLimitReached("Limit Reached!")
                        .textOnNothingSelected("Nothing Selected")
                        .startAlbum();
                break;
            case R.id.takePhoto:
                if (CameraUtils.checkPermissions(getApplicationContext())) {
                    captureImage(COLLECTION_IMAGE_REQUEST_CODE);
                } else {
                    requestCameraPermission(MEDIA_TYPE_IMAGE, COLLECTION_IMAGE_REQUEST_CODE);
                }
                break;
            case R.id.date:

                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        month = month + 1;
                        String tempDay = day + "";
                        String tempMonth = month + "";
                        if (day < 10) {
                            tempDay = "0" + day;
                        }
                        if (month < 10) {
                            tempMonth = "0" + month;
                        }
                        String date = year + "-" + (tempMonth) + "-" + tempDay;
                        cashInDate.setText(date);
                    }
                });
                dpd.setVersion(DatePickerDialog.Version.VERSION_2);
                dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                dpd.show(getSupportFragmentManager(), "DatePicker");
                break;
        }
    }

    private void updateCollectionDetail() {
        if ((imageStoragePath.equalsIgnoreCase("") || imageName.equalsIgnoreCase(""))) {
            Toast.makeText(UpdateCollectionActivity.this, "Please take photo of deposit slip or cheque", Toast.LENGTH_SHORT).show();
            return;
        }
        CollectionDetailModel collectionDetail = new CollectionDetailModel();
        collectionDetail.setCollectionId(mCollection.getCollectionId());
        CollectionImages collectionImages = new CollectionImages();
        if (paymentMethod.equalsIgnoreCase("Cheque")) {
            collectionDetail.setChequeStatus(chequeStatus);
        }
        if(selectedBank ==null){
            collectionDetail.setBankId(mCollection.getBankId());
            collectionDetail.setBankName(mCollection.getBankName());
        }else{
            collectionDetail.setBankId(selectedBank.getBankId());
            collectionDetail.setBankName(selectedBank.getBankName());
        }
        if(selectedCompany == null){
            collectionDetail.setCompanyId(mCollection.getCompanyId());
            collectionDetail.setCompanyName(mCollection.getCompanyName());
        }else {
            collectionDetail.setCompanyId(selectedCompany.getId());
            collectionDetail.setCompanyName(selectedCompany.getName());
        }
        if(selectedStockist == null){
            collectionDetail.setDistributorId(mCollection.getDistributorId());
            collectionDetail.setBusinessUnitiId(mCollection.getBusinessUnitiId());
            collectionDetail.setDistributorName(mCollection.getDistributorName());
        }else{
            collectionDetail.setDistributorId(selectedStockist.getDistributorId());
            collectionDetail.setBusinessUnitiId(selectedStockist.getBusinessUnitId());
            collectionDetail.setDistributorName(selectedStockist.getName());
        }
        collectionDetail.setChequeNumber((chequeNumber.getText().toString()));
        collectionDetail.setAccountNumber(accountNumber.getText().toString());
        collectionDetail.setPaymentMethod(paymentMethod);
        collectionDetail.setCollectionAmount(Double.parseDouble(chequeAmount.getText().toString()));
        collectionDetail.setRemarks(remarks.getText().toString());
        collectionDetail.setFileName(imageName);
        collectionDetail.setPhotoPath(imageStoragePath);
        collectionDetail.setLatitude(Double.parseDouble(prefManager.getLatitude()));
        collectionDetail.setLongitude(Double.parseDouble(prefManager.getLongitude()));
        collectionDetail.setCashInDate(cashInDate.getText().toString());
        collectionImages.setFileName(imageName);

        if (fileUri == null) {
            collectionDetail.setFileUri("");
            collectionImages.setFileUri("");
            collectionImages.setPhotoPath("");
        } else {
            collectionDetail.setFileUri(fileUri.toString());
            collectionImages.setFileUri(fileUri.toString());
            collectionImages.setPhotoPath(imageStoragePath);
        }

        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormatCr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        collectionDetail.setDateTime(simpleDateFormatCr.format(calendar.getTime()));
        collectionDetail.setRemarks(remarks.getText().toString());
        CollectionEntity collectionEntity = new CollectionEntity(UpdateCollectionActivity.this);

        long result = collectionEntity.update(collectionDetail);
        Log.i("Result",result +"");
        if (fileUri != null) {
            collectionImages.setId(result);
            CollectionImageEntity collectionImageEntity = new CollectionImageEntity(UpdateCollectionActivity.this);
            collectionImageEntity.update(collectionImages);
        }
        Toasty.info(UpdateCollectionActivity.this, "Collection Updated", Toasty.LENGTH_SHORT).show();
        finish();
    }

    public File getOutputMediaFile(int type) {
        String IMAGE_PATH = Environment.getExternalStorageDirectory()
                + File.separator + BaseApplication.APP_FOLDER_NAME;
        // External sdcard location
        File mediaStorageDir = new File(
                IMAGE_PATH,
                AppConstant.IMAGES_DIRECTORY);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e(AppConstant.IMAGES_DIRECTORY, "Oops! Failed create "
                        + AppConstant.IMAGES_DIRECTORY + " directory");
                return null;
            }
        }

        // Preparing media file naming convention
        // adds timestampMG_COL
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == AppConstant.MEDIA_TYPE_IMAGE_COLLECTION) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "COL_" + mCollection.getDistributorId() + "_" + timeStamp + "." + AppConstant.IMAGE_EXTENSION);
        } else {
            return null;
        }

        return mediaFile;
    }

    private void requestCameraPermission(final int type, int resultCode) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage(resultCode);
                            }
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(UpdateCollectionActivity.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage(int requestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputMediaFile(MEDIA_TYPE_IMAGE_COLLECTION);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
            imageName = file.getName();
        }
        fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, requestCode);
    }

    private void previewImage(int resultCode, ZoomImageView imageView) {
        if (resultCode == RESULT_OK) {
            CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
            downSizeImage(imageStoragePath);
            previewCapturedImage(imageView);

        } else if (resultCode == RESULT_CANCELED) {
            fileUri = null;
            imageStoragePath = "";
            imageName = "";
            // user cancelled Image capture
            Toasty.info(getApplicationContext(),
                    "User cancelled image capture", Toast.LENGTH_SHORT, true)
                    .show();
        } else {
            // failed to capture image
            Toasty.info(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT, true)
                    .show();
        }
    }

    private void downSizeImage(String imageStoragePath) {
        ByteArrayOutputStream byteArrayOutputStream = Utils.compressImage(imageStoragePath);
        try (OutputStream outputStream = new FileOutputStream(imageStoragePath)) {
            byteArrayOutputStream.writeTo(outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Display image from gallery
     */
    private void previewCapturedImage(ZoomImageView imageView) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            final Bitmap bitmap = BitmapFactory.decodeFile(Uri.parse(imageStoragePath).getPath(),
                    options);
            imageView.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SEARCH_BANK) {
            if (resultCode == Activity.RESULT_OK) {
                selectedBank = (Bank) data.getSerializableExtra("BankDetail");
                if (selectedBank != null) {
                    bank.setText(selectedBank.getBankName());
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (requestCode == REQUEST_CODE_SEARCH_COMPANY) {
            if (resultCode == Activity.RESULT_OK) {
                selectedCompany = (Company) data.getSerializableExtra("CompanyDetail");
                if (selectedCompany != null) {
                    company.setText(selectedCompany.getName());
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (requestCode == COLLECTION_IMAGE_REQUEST_CODE) {
            previewImage(resultCode, collectionImage);
        } else if (requestCode == FishBun.FISHBUN_REQUEST_CODE) {
            // path = imageData.getStringArrayListExtra(Define.INTENT_PATH);
            // you can get an image path(ArrayList<String>) on <0.6.2
            if (data != null) {
                ArrayList<Uri> path = data.getParcelableArrayListExtra(FishBun.INTENT_PATH);
                // you can get an image path(ArrayList<Uri>) on 0.6.2 and later

                if (!path.isEmpty()) {
                    try {
                        fileUri = path.get(0);
                        imageStoragePath = PathUtil.getPath(this, fileUri);
                        String[] tempArray = imageStoragePath.split("/");
                        imageName = tempArray[tempArray.length - 1];
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    // start cropping activity for pre-acquired image saved on the device
                    previewImage(resultCode, collectionImage);
                }
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putString("ImagePath", imageStoragePath);
        // etc.
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.

        imageStoragePath = savedInstanceState.getString("ImagePath");
    }
}

