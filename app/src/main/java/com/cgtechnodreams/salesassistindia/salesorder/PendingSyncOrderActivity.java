package com.cgtechnodreams.salesassistindia.salesorder;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.FMCGSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.salesorder.adapter.PendingSyncStockistOrderAdapter;
import com.cgtechnodreams.salesassistindia.salesorder.model.FMCGSalesOrder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PendingSyncOrderActivity extends AppCompatActivity implements DialogInterface.OnDismissListener {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    PendingSyncStockistOrderAdapter adapter;
    ArrayList<FMCGSalesOrder> salesOrders = null;
   int outletId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_sync);
        ButterKnife.bind(this);
        toolbar.setTitle("Pending Sync");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        String outletName = getIntent().getStringExtra("OutletName");
        outletId = getIntent().getIntExtra("OutletId", 0);
       loadData();
    }

    private void loadData() {
        FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(PendingSyncOrderActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) salesOrderEntity.findByOutletID(outletId);
        try {
            salesOrders = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<FMCGSalesOrder>>() {});
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutmanager = new LinearLayoutManager(PendingSyncOrderActivity.this, RecyclerView.VERTICAL, false);
        adapter = new PendingSyncStockistOrderAdapter(PendingSyncOrderActivity.this, salesOrders);
        recyclerView.setLayoutManager(layoutmanager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
//
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        loadData();
    }
}