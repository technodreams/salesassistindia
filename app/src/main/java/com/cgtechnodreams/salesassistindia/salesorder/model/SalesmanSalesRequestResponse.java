package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesmanSalesRequestResponse  extends BaseResponse implements Serializable {
    public ArrayList<SalesmanSalesRequest> getData() {
        return data;
    }

    public void setData(ArrayList<SalesmanSalesRequest> data) {
        this.data = data;
    }

    @JsonProperty("data")
    ArrayList<SalesmanSalesRequest> data;
}
