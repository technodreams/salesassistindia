package com.cgtechnodreams.salesassistindia.salesorder;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.salesorder.dataservice.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequestDetail;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class UpdatePendingOrderDialog extends DialogFragment {
    EditDialogListener editDialogListener;
    @BindView(R.id.btnUpdate)
    public Button btnUpdate;
    @BindView(R.id.updateQuantity)
    public AppCompatEditText productQuantity;
    @BindView(R.id.productName)
    TextView productName;
    PrefManager prefManager = null;
    private PendingSalesRequestDetail pendingSalesOrder;
    public UpdatePendingOrderDialog newInstance(PendingSalesRequestDetail pendingSalesOrder) {
        Bundle args = new Bundle();
        args.putSerializable("SalesDetail", pendingSalesOrder);
        this.setArguments(args);
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_order_update,container,false);
        prefManager = new PrefManager(getActivity());
        if (getArguments() != null) {
            pendingSalesOrder = (PendingSalesRequestDetail) getArguments().getSerializable("SalesDetail");
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        productName.setText(pendingSalesOrder.getProductName());
        productQuantity.setText(pendingSalesOrder.getQuantity() + "");
    }

    @OnClick(R.id.btnUpdate)
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (productQuantity.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please enter quantity", Toast.LENGTH_SHORT).show();
                } else{
                    pendingSalesOrder.setQuantity(Integer.parseInt(productQuantity.getText().toString()));
                    pendingSalesOrder.setTotal(Double.parseDouble(productQuantity.getText().toString())* pendingSalesOrder.getDealerPrice());
                    updateOrder(pendingSalesOrder);

                }
        }
    }

    private void updateOrder(PendingSalesRequestDetail pendingSalesOrder) {
        ProgressDialog dialog;

        if (Utils.isConnectionAvailable(getActivity())) {

            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Updating salesman's' pending sales detail request...");
            dialog.setCancelable(false);
            dialog.show();

            SalesOrderDataService dataService = new SalesOrderDataService(getActivity());
            dataService.updateEachPendingSalesDetail(pendingSalesOrder.getSalesDetailId(),pendingSalesOrder.getQuantity(),prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    BaseResponse baseResponse = (BaseResponse) response;
                    if (baseResponse.getStatusCode() != 200) {
                        Toasty.error(getActivity(), baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(getActivity(), baseResponse.getStatusCode(), baseResponse.getMessage());
                    } else {

                    }
                    EditDialogListener activity = (EditDialogListener) getActivity();
                    activity.updateResult(true);
                    dismiss();


                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(getActivity(), "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public interface EditDialogListener {
        void updateResult(boolean isFetch);
    }
}
