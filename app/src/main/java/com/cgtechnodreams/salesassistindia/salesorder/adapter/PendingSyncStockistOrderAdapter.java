package com.cgtechnodreams.salesassistindia.salesorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.FMCGSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.salesorder.IMethodCaller;
import com.cgtechnodreams.salesassistindia.salesorder.UpdateOrderDialog;
import com.cgtechnodreams.salesassistindia.salesorder.model.FMCGSalesOrder;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PendingSyncStockistOrderAdapter extends RecyclerView.Adapter<PendingSyncStockistOrderAdapter.MyViewHolder>{

    Context mContext;
    PrefManager prefManager;
    ArrayList<FMCGSalesOrder> salesOrders = new ArrayList<>();

    public PendingSyncStockistOrderAdapter(Context context, ArrayList<FMCGSalesOrder> salesOrders) {
        this.mContext = context;
        this.salesOrders = salesOrders;
        prefManager = new PrefManager(mContext);
    }
    @Override
    public PendingSyncStockistOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_fmcg_salesorder_summary, null);
        return new PendingSyncStockistOrderAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(PendingSyncStockistOrderAdapter.MyViewHolder holder, int position) {
        FMCGSalesOrder eachOrder = salesOrders.get(position);
        holder.productName.setText(eachOrder.getProductName());
        holder.tvQuantity.setText("Qty: " + eachOrder.getQuantity());
        holder.totalAmount.setText("Total: " + eachOrder.getTotalPrice());
    }
    @Override
    public int getItemCount() {
        return salesOrders.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.quantity)
        TextView tvQuantity;
        @BindView(R.id.totalAmount)
        TextView totalAmount;
        @BindView(R.id.edit)
        ImageView imgEdit;
        @BindView(R.id.delete)
        ImageView imgDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm =((AppCompatActivity) mContext).getSupportFragmentManager();
                    UpdateOrderDialog dialogFragment = new UpdateOrderDialog().newInstance(salesOrders.get(getAdapterPosition()));
                    dialogFragment.show(fm, "dialog");
//
                }
            });
            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(mContext);
                    salesOrderEntity.deleteById(salesOrders.get(getAdapterPosition()).getOrderId());
                    salesOrders.remove(salesOrders.get(getAdapterPosition()));
//                    ((AppCompatActivity)mContext).getOrderSummary();
                    if (mContext instanceof IMethodCaller) {
                        ((IMethodCaller) mContext).onDeleteOrder();
                    }
                    notifyDataSetChanged();
                }
            });


        }
    }
}
