package com.cgtechnodreams.salesassistindia.salesorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequest;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PendingSalesRequestAdapter extends RecyclerView.Adapter<PendingSalesRequestAdapter.MyViewHolder>{

    Context mContext;
    PrefManager prefManager;
    ArrayList<PendingSalesRequest> pendingSalesRequest = new ArrayList<>();

    public PendingSalesRequestAdapter(Context context, ArrayList<PendingSalesRequest> pendingSalesRequest) {
        this.mContext = context;
        this.pendingSalesRequest = pendingSalesRequest;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public PendingSalesRequestAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pending_sales_request, null);
        return new PendingSalesRequestAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PendingSalesRequestAdapter.MyViewHolder holder, int position) {
        PendingSalesRequest eachSales = pendingSalesRequest.get(position);
        holder.salesmanName.setText(eachSales.getSalesmanName());
        holder.stockist.setText( eachSales.getDistributorName() + "");
        holder.division.setText("     ( " +eachSales.getDivisionName() + " )");
        holder.bgAmount.setText(Utils.round(eachSales.getBgAmount(),2) + "");
        holder.grandTotal.setText(Utils.round(eachSales.getGrandTotal(),2) + "");
        holder.miti.setText(eachSales.getMiti());
    }

    @Override
    public int getItemCount() {
        return pendingSalesRequest.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.salesmanName)
        TextView salesmanName;
        @BindView(R.id.stockist)
        TextView stockist;
        @BindView(R.id.division)
        TextView division;
        @BindView(R.id.bgAmount)
        TextView bgAmount;
        @BindView(R.id.grandTotal)
        TextView grandTotal;
        @BindView(R.id.miti)
        TextView miti;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
