package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class DOReceived implements Serializable {
    @JsonProperty("date")
    private String date;


    @JsonProperty("productCode")
    private String productCode;
    @JsonProperty("product_name")
    private String productName;
    @JsonProperty("order_quantity")
    private String orderQuantity;
    @JsonProperty("approved_quantity")
    private String approvedQuantity;
    @JsonProperty("dispatch_quantity")
    private String dispatchQuantity;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getApprovedQuantity() {
        return approvedQuantity;
    }

    public void setApprovedQuantity(String approvedQuantity) {
        this.approvedQuantity = approvedQuantity;
    }

    public String getDispatchQuantity() {
        return dispatchQuantity;
    }

    public void setDispatchQuantity(String dispatchQuantity) {
        this.dispatchQuantity = dispatchQuantity;
    }
}
