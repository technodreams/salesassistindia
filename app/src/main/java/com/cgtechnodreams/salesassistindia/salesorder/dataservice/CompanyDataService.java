package com.cgtechnodreams.salesassistindia.salesorder.dataservice;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.salesorder.model.CompanyResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyDataService {

    private Context mContext;

    public CompanyDataService(Context mContext) {
        this.mContext = mContext;
    }
    public void getCompanyList(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<CompanyResponse> callBack = Api.getService(mContext).getCompanyList(token);
        callBack.enqueue(new Callback<CompanyResponse>() {
            @Override
            public void onResponse(Call<CompanyResponse> call, Response<CompanyResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<CompanyResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
}

