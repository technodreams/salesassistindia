package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesmanSalesRequest implements Serializable {
    @JsonProperty("id")
    int id;
    @JsonProperty("product_id")
    int productId;
    @JsonProperty("product_name")
    String productName;
    @JsonProperty("quantity")
    int quantity;
    @JsonProperty("datetime")
    String datetime;
    @JsonProperty("date_np")
    String dateNp;
    @JsonProperty("location")
    String location;
    @JsonProperty("google_coordinates")
    String  googleCordinates;
    @JsonProperty("remarks")
    String remarks;
    @JsonProperty("total")
    double total;
    @JsonProperty("dealer_price")
    double dealerPrice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDateNp() {
        return dateNp;
    }

    public void setDateNp(String dateNp) {
        this.dateNp = dateNp;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGoogleCordinates() {
        return googleCordinates;
    }

    public void setGoogleCordinates(String googleCordinates) {
        this.googleCordinates = googleCordinates;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDealerPrice() {
        return dealerPrice;
    }

    public void setDealerPrice(double dealerPrice) {
        this.dealerPrice = dealerPrice;
    }
}
