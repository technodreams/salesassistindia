package com.cgtechnodreams.salesassistindia.salesorder;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.salesorder.adapter.SalesmanSalesRequestDetailAdapter;
import com.cgtechnodreams.salesassistindia.salesorder.dataservice.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesDetailResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequest;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequestDetail;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class SalesmanSalesDetailReportActivity extends AppCompatActivity implements UpdatePendingOrderDialog.EditDialogListener {
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    PrefManager prefManager = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    PendingSalesRequest pendingSalesRequest = null;
    SalesmanSalesRequestDetailAdapter adapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesman_sales_detail_report);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        toolbar.setTitle("Pending Sales Request Detail");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        pendingSalesRequest = (PendingSalesRequest) getIntent().getSerializableExtra("PendingSalesRequestDetail");
        getPendingSalesDetail(pendingSalesRequest.getId());
    }
    private void getPendingSalesDetail(int id) {
        ProgressDialog dialog;
        if (Utils.isConnectionAvailable(SalesmanSalesDetailReportActivity.this)) {
            dialog = new ProgressDialog(SalesmanSalesDetailReportActivity.this);
            dialog.setMessage("Fetching salesman's' pending sales detail request...");
            dialog.setCancelable(false);
            dialog.show();

            SalesOrderDataService dataService = new SalesOrderDataService(SalesmanSalesDetailReportActivity.this);
            dataService.getPendingSalesDetail(id,prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    PendingSalesDetailResponse baseResponse = (PendingSalesDetailResponse) response;
                    if (baseResponse.getStatusCode() != 200) {
                        Toasty.error(SalesmanSalesDetailReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(SalesmanSalesDetailReportActivity.this, baseResponse.getStatusCode(), baseResponse.getMessage());
                    } else {
                        Toasty.success(SalesmanSalesDetailReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        setData(baseResponse.getData());
                    }


                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(SalesmanSalesDetailReportActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    private void setData(ArrayList<PendingSalesRequestDetail> data) {
        LinearLayoutManager layoutmanager = new LinearLayoutManager(SalesmanSalesDetailReportActivity.this, RecyclerView.VERTICAL, false);
        adapter = new SalesmanSalesRequestDetailAdapter(SalesmanSalesDetailReportActivity.this, data);
        recyclerView.setLayoutManager(layoutmanager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    @Override
    public void updateResult(boolean isFetch) {
        if(isFetch){
            getPendingSalesDetail(pendingSalesRequest.getId());
        }
    }
}
