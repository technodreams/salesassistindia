package com.cgtechnodreams.salesassistindia.salesorder.model;

public class StockistPendingSyncTotal {
    int outletId;
    String outletName;
    double total;

    public int getOutletId() {
        return outletId;
    }

    public void setOutletId(int orderId) {
        this.outletId = orderId;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
