package com.cgtechnodreams.salesassistindia.salesorder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.report.model.ReportRequest;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.SalesAdapter;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.data.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesReport;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesReportResponse;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesmanSalesDetail;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class SalesmanSalesReportActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SalesAdapter adapter = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    PrefManager prefManager = null;
    @BindView(R.id.dateRange)
    TextView dateRange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesman_sales_report);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Sales Report");
        prefManager = new PrefManager(this);
        String from= getIntent().getStringExtra("From");
        String to = getIntent().getStringExtra("To");
        dateRange.setText("Report Generated From " + from +" to " + to);
        ReportRequest report = new ReportRequest();
        report.setFrom(from);
        report.setTo(to);
        getSalesReport(report);
    }

    private void getSalesReport(ReportRequest report) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        SalesOrderDataService dataService = new SalesOrderDataService(this);
        dataService.getSalesOrderReport(prefManager.getAuthKey(), report, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                SalesReportResponse salesReport = (SalesReportResponse) response;
                if (salesReport.getStatusCode() != 200) {//failure case
                    Toasty.error(SalesmanSalesReportActivity.this, salesReport.getMessage(), Toast.LENGTH_SHORT,true).show();
                    ErrorHandler.handleSyncError(SalesmanSalesReportActivity.this,salesReport.getStatusCode(),salesReport.getMessage());
                    finish();
                } else { //success case
                    setData(salesReport.getSalesReportData());
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void setData(ArrayList<SalesReport> salesReports) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(SalesmanSalesReportActivity.this, RecyclerView.VERTICAL, false);
        adapter = new SalesAdapter(salesReports, SalesmanSalesReportActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(SalesmanSalesReportActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(SalesmanSalesReportActivity.this, SalesmanSalesDetailActivity.class);
                ArrayList<SalesmanSalesDetail> salesmanSalesDetails = salesReports.get(position).getSalesDetail();
                intent.putExtra("SalesDetail",salesmanSalesDetails);
                startActivity(intent);
            }
            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));
    }

}
