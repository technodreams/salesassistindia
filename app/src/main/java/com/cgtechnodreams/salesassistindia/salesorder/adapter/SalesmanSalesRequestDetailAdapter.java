package com.cgtechnodreams.salesassistindia.salesorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesorder.UpdatePendingOrderDialog;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequestDetail;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesmanSalesRequestDetailAdapter extends RecyclerView.Adapter<SalesmanSalesRequestDetailAdapter.MyViewHolder>{

    Context mContext;
    PrefManager prefManager;
    ArrayList<PendingSalesRequestDetail> salesRequestList = new ArrayList<>();

    public SalesmanSalesRequestDetailAdapter(Context context, ArrayList<PendingSalesRequestDetail> salesRequestList) {
        this.mContext = context;
        this.salesRequestList = salesRequestList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public SalesmanSalesRequestDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_salesman_sales_request_detail, null);

        return new SalesmanSalesRequestDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SalesmanSalesRequestDetailAdapter.MyViewHolder holder, int position) {
        PendingSalesRequestDetail eachSalesOrderReport = salesRequestList.get(position);
        holder.productName.setText(eachSalesOrderReport.getProductName());
        holder.quantity.setText(eachSalesOrderReport.getQuantity() +"");
        holder.total.setText(String.valueOf(Utils.round(eachSalesOrderReport.getTotal(),2)));
        holder.dealerPrice.setText(String.valueOf(Utils.round(eachSalesOrderReport.getDealerPrice(),2)));
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm =((AppCompatActivity) mContext).getSupportFragmentManager();
                UpdatePendingOrderDialog dialogFragment = new UpdatePendingOrderDialog().newInstance(eachSalesOrderReport);
                dialogFragment.show(fm, "dialog");
            }
        });

    }

    @Override
    public int getItemCount() {
        return salesRequestList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.dealerPrice)
        TextView dealerPrice;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.total)
        TextView total;
        @BindView(R.id.imgEdit)
        ImageView edit;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}


