package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyResponse extends BaseResponse {
    @JsonProperty("data")
    ArrayList<Company> data;
    public ArrayList<Company> getData() {
        return data;
    }
    public void setData(ArrayList<Company> data) {
        this.data = data;
    }

}
