package com.cgtechnodreams.salesassistindia.salesorder;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.OrderRemarksCurrentBalanceEntity;
import com.cgtechnodreams.salesassistindia.salesorder.model.FMCGSalesOrder;
import com.cgtechnodreams.salesassistindia.salesorder.model.OrderRemarksCurrentBalance;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CurrentBalanceOrderRemarkDialog extends DialogFragment {

    AppCompatEditText currentBalance;
    AppCompatEditText orderRemarks;
    OrderRemarksCurrentBalance orderRemarksCurrentBalance = new OrderRemarksCurrentBalance();

    @BindView(R.id.btnConfirmOrder)
    public Button btnConfirmOrder;
    private FMCGSalesOrder salesOrder;

    public CurrentBalanceOrderRemarkDialog newInstance(FMCGSalesOrder fmcgSalesOrder) {
        Bundle args = new Bundle();
        args.putSerializable("SalesDetail", fmcgSalesOrder);
        this.setArguments(args);
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_current_balance_order_remarks, container, false);
        currentBalance = (AppCompatEditText) view.findViewById(R.id.currentBalance);
        orderRemarks = (AppCompatEditText) view.findViewById(R.id.orderRemarks);
        if (getArguments() != null) {
            salesOrder = (FMCGSalesOrder) getArguments().getSerializable("SalesDetail");

        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        currentBalance = view.findViewById(R.id.currentBalance);
        orderRemarks = view.findViewById(R.id.orderRemarks);
    }

    @OnClick(R.id.btnConfirmOrder)
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnConfirmOrder:
                orderRemarksCurrentBalance.setCurrentBalance(currentBalance.getText().toString());
                orderRemarksCurrentBalance.setTransactionRemarks(orderRemarks.getText().toString());
                orderRemarksCurrentBalance.setBusinessUnitId(salesOrder.getBusinessUnitId());
                orderRemarksCurrentBalance.setDate(Utils.getCurrentDate());
                orderRemarksCurrentBalance.setDistributorId(salesOrder.getDistributorId());
                orderRemarksCurrentBalance.setDistributorName(salesOrder.getDistributorName());
                updateOrder(orderRemarksCurrentBalance);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }
    private void updateOrder(OrderRemarksCurrentBalance orderRemarksCurrentBalance) {
        OrderRemarksCurrentBalanceEntity currentBalanceEntity = new OrderRemarksCurrentBalanceEntity(getActivity());
        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<OrderRemarksCurrentBalance> orderRemarksCurrentBalances = new ArrayList<>();
        JSONArray array = (JSONArray) currentBalanceEntity.findById(salesOrder.getDistributorId(), salesOrder.getBusinessUnitId());
        try {
            objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            orderRemarksCurrentBalances = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<OrderRemarksCurrentBalance>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(orderRemarksCurrentBalances.size() > 0){
            currentBalanceEntity.update(orderRemarksCurrentBalance);
        }else{
            currentBalanceEntity.insert(orderRemarksCurrentBalance);
        }

        dismiss();
    }
}