package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PendingSalesRequest implements Serializable {

    @JsonProperty("id")
    private int id;
    @JsonProperty("distributor_id")
    private int distributorId;
    @JsonProperty("distributor_name")
    private String distributorName;
    @JsonProperty("salesman_name")
    private String salesmanName;
    @JsonProperty("salesman_id")
    private int salesmanId;
    @JsonProperty("date")
    private String date;
    @JsonProperty("date_np")
    private  String miti;
    @JsonProperty("status")
    private String status;
    @JsonProperty("rejected")
    private String rejectedDate;
    @JsonProperty("approved")
    private String approvedDate;
    @JsonProperty("delivered")
    private String deliveredDate;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("division_id")
    private int divisionId;
    @JsonProperty("division_name")
    private String divisionName;
    @JsonProperty("bg_amount")
    private double bgAmount;
    @JsonProperty("grand_total")
    private double grandTotal;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public int getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(int salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMiti() {
        return miti;
    }

    public void setMiti(String miti) {
        this.miti = miti;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectedDate() {
        return rejectedDate;
    }

    public void setRejectedDate(String rejectedDate) {
        this.rejectedDate = rejectedDate;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(String deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public double getBgAmount() {
        return bgAmount;
    }

    public void setBgAmount(double bgAmount) {
        this.bgAmount = bgAmount;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }
}
