package com.cgtechnodreams.salesassistindia.salesorder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.salesorder.adapter.PendingSalesRequestAdapter;
import com.cgtechnodreams.salesassistindia.salesorder.dataservice.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequest;
import com.cgtechnodreams.salesassistindia.salesorder.model.PendingSalesRequestResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class SalesmanSalesOrderReportActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    PendingSalesRequestAdapter adapter = null;
    PrefManager prefManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesman_sales_order_report);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        toolbar.setTitle("Salesman Sales Report");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSalesmanSalesRequest();
    }

    public void getSalesmanSalesRequest() {
        ProgressDialog dialog;
        if (Utils.isConnectionAvailable(SalesmanSalesOrderReportActivity.this)) {
            dialog = new ProgressDialog(SalesmanSalesOrderReportActivity.this);
            dialog.setMessage("Fetching salesman's' sales request...");
            dialog.setCancelable(false);
            dialog.show();

            SalesOrderDataService dataService = new SalesOrderDataService(SalesmanSalesOrderReportActivity.this);
            dataService.getSalesmanPendingOrderRequest(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    PendingSalesRequestResponse baseResponse = (PendingSalesRequestResponse) response;
                    if (baseResponse.getStatusCode() != 200) {
                        Toasty.error(SalesmanSalesOrderReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(SalesmanSalesOrderReportActivity.this, baseResponse.getStatusCode(), baseResponse.getMessage());
                    } else {
                        Toasty.success(SalesmanSalesOrderReportActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        setData(baseResponse.getPendingSalesRequest());
                    }


                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(SalesmanSalesOrderReportActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

        private void setData(ArrayList<PendingSalesRequest> pendingSalesRequestList) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(SalesmanSalesOrderReportActivity.this, RecyclerView.VERTICAL, false);
        adapter = new PendingSalesRequestAdapter(SalesmanSalesOrderReportActivity.this, pendingSalesRequestList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(SalesmanSalesOrderReportActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(SalesmanSalesOrderReportActivity.this, SalesmanSalesDetailReportActivity.class);
                intent.putExtra("PendingSalesRequestDetail",pendingSalesRequestList.get(position));
//                intent.putExtra("DistributorId",pendingSalesRequestList.get(position).getDistributorId());
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
