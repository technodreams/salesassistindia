package com.cgtechnodreams.salesassistindia.salesorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesorder.model.StockistPendingSyncTotal;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesOrderSummaryAdapter extends RecyclerView.Adapter<SalesOrderSummaryAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<StockistPendingSyncTotal> outletTotals = new ArrayList<>();

    public SalesOrderSummaryAdapter(Context context, ArrayList<StockistPendingSyncTotal> outletTotals) {
        this.mContext = context;
        this.outletTotals = outletTotals;
    }

    @Override
    public SalesOrderSummaryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_order_summary, null);

        return new SalesOrderSummaryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SalesOrderSummaryAdapter.MyViewHolder holder, int position) {
        StockistPendingSyncTotal eachOutlet = outletTotals.get(position);
        holder.name.setText(eachOutlet.getOutletName());
        holder.total.setText("Total: " + Utils.round(eachOutlet.getTotal(),2));

    }

    @Override
    public int getItemCount() {
        return outletTotals.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.total)
        TextView total;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }
}

