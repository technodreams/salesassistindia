package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesmanSalesOrderReportRepsonse extends BaseResponse {
    @JsonProperty("data")
    ArrayList<SalesmanSalesOrderReprot> salesmanSalesOrderReprots;


    public ArrayList<SalesmanSalesOrderReprot> getSalesmanSalesOrderReprots() {
        return salesmanSalesOrderReprots;
    }

    public void setSalesmanSalesOrderReprots(ArrayList<SalesmanSalesOrderReprot> salesmanSalesOrderReprots) {
        this.salesmanSalesOrderReprots = salesmanSalesOrderReprots;
    }
}
