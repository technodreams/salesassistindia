package com.cgtechnodreams.salesassistindia.salesorder;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.databasehelper.model.FMCGSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.OrderRemarksCurrentBalanceEntity;
import com.cgtechnodreams.salesassistindia.outlet.StockistActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.product.SearchProductActivity;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.salesorder.adapter.PendingSyncStockistOrderAdapter;
import com.cgtechnodreams.salesassistindia.salesorder.model.FMCGSalesOrder;
import com.cgtechnodreams.salesassistindia.salesorder.model.OrderRemarksCurrentBalance;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class StockistSalesOrderActivity extends AppCompatActivity implements DialogInterface.OnDismissListener, IMethodCaller {

    private static final int REQUEST_CODE_SEARCH_PRODUCT = 100;
    private static final int REQUEST_CODE_SEARCH_OUTLET = 101;
    private static final int REQUEST_CODE_CONFIRM_SALES_ORDER = 103;
    boolean isUpdate = false;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.stockist)
    EditText stockist;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.product)
    EditText product;
    @BindView(R.id.unitPrice)
    TextView unitPrice;
    @BindView(R.id.totalPrice)
    TextView totalPrice;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.quantity)
    EditText quantity;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.grandTotal)
    TextView grandTotal;
    @BindView(R.id.bgAmount)
    TextView bgAmount;
    Product selectedProduct;
    Stockist selectedStockist;
    PrefManager prefManager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.remarks)
    TextView remarks;
    @BindView(R.id.btnCurrentBalance)
    Button btnCurrentBalance;
    @BindView(R.id.containerRemarks)
    LinearLayout llContainerRemarks;
    @BindView(R.id.currentBalance)
    TextView currentBalance;
    @BindView(R.id.orderRemarks)
    TextView orderRemarks;

    int salesOrderId = 0;
    PendingSyncStockistOrderAdapter pendingSyncStockistOrderAdapter = null;

    //float totalAmount, taxableAmount, discountedPercentage, discountedAmount, exciseAmount, vatPercentage;
    Double totalAmountIncludingTax = 0.0, totalAmount = 0.0, taxableAmount = 0.0, discountedPercentage = 0.0, discountedAmount = 0.0, exciseAmount = 0.0, vatPercentage = 0.0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sales_order_activity);
        ButterKnife.bind(this);
        toolbar.setTitle("Sales Order");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        FMCGSalesOrder updateSalesOrder = (FMCGSalesOrder) getIntent().getSerializableExtra("SalesOrder");
        LocationUtils locationUtils = new LocationUtils(StockistSalesOrderActivity.this);
        locationUtils.requestLocationUpdates();

        Stockist mStockist = (Stockist) getIntent().getSerializableExtra(AppConstant.STOCKIST_DETAIL);
        if (mStockist != null) {
            selectedStockist = mStockist;
            getOrderSummary();
            stockist.setEnabled(false);
            stockist.setText(selectedStockist.getName());
            bgAmount.setText(selectedStockist.getBgAmount() + "");
        }
        prefManager = new PrefManager(this);
        quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equalsIgnoreCase("")) {
                    double quantity = Double.parseDouble(s.toString());
                    if (!unitPrice.getText().toString().equalsIgnoreCase(""))
                        totalAmount = quantity * Double.parseDouble(unitPrice.getText().toString());
                    else
                        totalAmount = 0.0;
                    totalPrice.setText(String.format("%.2f", totalAmount));
                    DecimalFormat df = new DecimalFormat("#.##");
                    df.setRoundingMode(RoundingMode.CEILING);
                    Double d = totalAmount.doubleValue();
                    totalPrice.setText(df.format(d));

                } else {
                    double quantity = 0;
                    totalAmount = 0.0;
                    totalPrice.setText(String.format("%.2f", totalAmount));
                }
            }
        });
    }

    private void setData(FMCGSalesOrder updateSalesOrder) {
        if (selectedProduct == null) {
            selectedProduct = new Product();
        }
        if (selectedStockist == null) {
            selectedStockist = new Stockist();
        }
        salesOrderId = updateSalesOrder.getOrderId();
        selectedProduct.setProductId(updateSalesOrder.getProductId());
        selectedProduct.setProductName(updateSalesOrder.getProductName());
        selectedProduct.setDealerPrice(updateSalesOrder.getUnitPrice() + "");
        selectedProduct.setDivisionId(updateSalesOrder.getBusinessUnitId());
        selectedStockist.setName(updateSalesOrder.getDistributorName());
        selectedStockist.setDistributorId(updateSalesOrder.getDistributorId());
        stockist.setText(updateSalesOrder.getDistributorName());
        product.setText(updateSalesOrder.getProductName());
        unitPrice.setText(updateSalesOrder.getUnitPrice() + "");
        totalPrice.setText(updateSalesOrder.getTotalPrice() + "");
        quantity.setText(updateSalesOrder.getQuantity() + "");
        isUpdate = true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SEARCH_PRODUCT) {
            if (resultCode == Activity.RESULT_OK) {
                selectedProduct = (Product) data.getSerializableExtra("Result");
                if (selectedProduct != null) {
                    product.setText(selectedProduct.getProductName());
                    if (selectedProduct.getPriceGroupId() == 0) {//price group not set
                        unitPrice.setText(selectedProduct.getDealerPrice());
                    } else {
                        unitPrice.setText(selectedProduct.getPriceGroupWise());
                    }

                    quantity.setText("");
                    totalPrice.setText("");
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (requestCode == REQUEST_CODE_SEARCH_OUTLET) {
            if (resultCode == Activity.RESULT_OK) {
                selectedStockist = (Stockist) data.getSerializableExtra("result");
                stockist.setText(selectedStockist.getName());
                clearData();
                getOrderSummary();
                bgAmount.setText(selectedStockist.getBgAmount() + "");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void clearData() {
        totalAmountIncludingTax = 0.0;
        totalAmount = 0.0;
        vatPercentage = 0.0;
        product.setText("");
        unitPrice.setText("");
        totalPrice.setText("");
        quantity.setText("");
        ;
        remarks.setText("");
    }

    @OnClick({R.id.stockist, R.id.product, R.id.btnSave, R.id.btnCurrentBalance})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.stockist:
                Intent intent = new Intent(StockistSalesOrderActivity.this, StockistActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SEARCH_OUTLET);
                clearData();
                break;
            case R.id.product:
                if (stockist.getText().toString().equalsIgnoreCase("")) {
                    Toasty.info(StockistSalesOrderActivity.this, "Please select stockist first!!", Toast.LENGTH_SHORT, true).show();
                } else {
                    Intent product = new Intent(StockistSalesOrderActivity.this, SearchProductActivity.class);
                    product.putExtra("BusinessUnitId", selectedStockist.getBusinessUnitId());
                    product.putExtra("PriceGroupId", selectedStockist.getPriceGroupId());
                    startActivityForResult(product, REQUEST_CODE_SEARCH_PRODUCT);
                }
                break;
            case R.id.btnSave:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    if (quantity.getText().toString().equalsIgnoreCase("0")) {
                        Toasty.info(StockistSalesOrderActivity.this, "Quantity cannot be 0", Toast.LENGTH_SHORT, true).show();
                    }
                    confirmOrder();
                }
                break;
            case R.id.btnCurrentBalance:
                ArrayList<FMCGSalesOrder> fmcgSalesOrders = null;
                FMCGSalesOrderEntity saleOrderEntity = new FMCGSalesOrderEntity(StockistSalesOrderActivity.this);
                ObjectMapper objectMapper = new ObjectMapper();
                JSONArray array = (JSONArray) saleOrderEntity.findById(selectedStockist.getDistributorId(), selectedStockist.getBusinessUnitId());
                try {
                    objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                    fmcgSalesOrders = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<FMCGSalesOrder>>() {
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

                FragmentManager fm = getSupportFragmentManager();
                CurrentBalanceOrderRemarkDialog dialogFragment = new CurrentBalanceOrderRemarkDialog().newInstance(fmcgSalesOrders.get(0));
                dialogFragment.show(fm, "dialog");

        }

    }

    private void confirmOrder() {
        FMCGSalesOrder salesOrder = new FMCGSalesOrder();
        if (isUpdate)
            salesOrder.setOrderId(salesOrderId);
        salesOrder.setProductId(selectedProduct.getProductId());
        salesOrder.setProductName(selectedProduct.getProductName());
        salesOrder.setDistributorId(selectedStockist.getDistributorId());
        salesOrder.setDistributorName(selectedStockist.getName());
        salesOrder.setUnitPrice(Double.parseDouble(selectedProduct.getDealerPrice()));
        salesOrder.setQuantity(Integer.parseInt(quantity.getText().toString()));
        salesOrder.setBusinessUnitId(selectedProduct.getBusinessUnitId());
        salesOrder.setTotalPrice(Double.parseDouble(totalPrice.getText().toString()));
        salesOrder.setLatitude(Double.parseDouble(prefManager.getLatitude()));
        salesOrder.setLongitude(Double.parseDouble(prefManager.getLongitude()));
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormatCr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        salesOrder.setDateTime(simpleDateFormatCr.format(calendar.getTime()));

        salesOrder.setRemarks(remarks.getText().toString());
        FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(StockistSalesOrderActivity.this);
        long result = salesOrderEntity.insert(salesOrder);
        if (result > 0) {
            clearData();
            getOrderSummary();
        }
    }

    private void getOrderSummary() {
        FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(this);
        double total = salesOrderEntity.getTotalSalesByOutletAndBusinessUnit(selectedStockist.getDistributorId(), selectedStockist.getBusinessUnitId());
        // grandTotal.setText(Utils.round(total,2)+ "");
        grandTotal.setText(total + "");
        ArrayList<FMCGSalesOrder> fmcgSalesOrders = new ArrayList<>();
        FMCGSalesOrderEntity saleOrderEntity = new FMCGSalesOrderEntity(StockistSalesOrderActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) saleOrderEntity.findById(selectedStockist.getDistributorId(), selectedStockist.getBusinessUnitId());
        try {
            objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            fmcgSalesOrders = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<FMCGSalesOrder>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (fmcgSalesOrders.size() > 0) {
            setOrderRemarks();
            LinearLayoutManager salesOrderManager = new LinearLayoutManager(StockistSalesOrderActivity.this, RecyclerView.VERTICAL, false);
            pendingSyncStockistOrderAdapter = new PendingSyncStockistOrderAdapter(StockistSalesOrderActivity.this, fmcgSalesOrders);
            recyclerView.setLayoutManager(salesOrderManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(pendingSyncStockistOrderAdapter);
            pendingSyncStockistOrderAdapter.notifyDataSetChanged();
            llContainerRemarks.setVisibility(View.VISIBLE);
            btnCurrentBalance.setVisibility(View.VISIBLE);
        } else {
            OrderRemarksCurrentBalanceEntity orderRemarksCurrentBalance = new OrderRemarksCurrentBalanceEntity(this);
            orderRemarksCurrentBalance.delete(selectedStockist.getDistributorId(), selectedStockist.getBusinessUnitId());

            llContainerRemarks.setVisibility(View.GONE);
            btnCurrentBalance.setVisibility(View.GONE);
        }

    }

    private void setOrderRemarks() {
        ArrayList<OrderRemarksCurrentBalance> orderRemarksCurrentBalances = new ArrayList<>();
        OrderRemarksCurrentBalanceEntity entity = new OrderRemarksCurrentBalanceEntity(StockistSalesOrderActivity.this);
        ObjectMapper objectMapper1 = new ObjectMapper();
        JSONArray array1 = (JSONArray) entity.findById(selectedStockist.getDistributorId(), selectedStockist.getBusinessUnitId());
        try {
            objectMapper1.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            orderRemarksCurrentBalances = objectMapper1.readValue(String.valueOf(array1), new TypeReference<ArrayList<OrderRemarksCurrentBalance>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(orderRemarksCurrentBalances.size()>0){
            currentBalance.setText(orderRemarksCurrentBalances.get(0).getCurrentBalance());
            orderRemarks.setText(orderRemarksCurrentBalances.get(0).getTransactionRemarks());
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        getOrderSummary();
    }

    @Override
    public void onDeleteOrder() {
        getOrderSummary();

    }
}
