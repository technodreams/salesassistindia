package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CollectionDetailModel implements Serializable {
    @JsonProperty("id")
    private int collectionId;
    @JsonProperty("distributor_id")
    private int distributorId;
    @JsonProperty("distributor_name")
    private String distributorName;
    @JsonProperty("bank_id")
    private int bankId;
    @JsonProperty("bank_name")
    private String bankName;
    @JsonProperty("cheque_number")
    private String chequeNumber;
    @JsonProperty("collection_amount")
    private double collectionAmount;
    @JsonProperty("latitude")
    private double latitude;
    @JsonProperty("longitude")
    private double longitude;
    @JsonProperty("date_time")
    private String dateTime;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("business_unit_id")
    private int businessUnitiId;
    @JsonProperty("payment_method")
    private String paymentMethod;
    @JsonProperty("account_number")
    private String accountNumber;
    @JsonProperty("photo_path")
    private String photoPath;
    @JsonProperty("file_name")
    private String fileName;
    @JsonProperty("file_uri")
    private String fileUri;
    @JsonProperty("cash_in_date")
    private String cashInDate;
    @JsonProperty("company_id")
    private int companyId;

    @JsonProperty("cheque_status")
    private String chequeStatus;
    @JsonProperty("company_name")
    private String companyName;


    public String getChequeStatus() {
        return chequeStatus;
    }

    public void setChequeStatus(String chequeStatus) {
        this.chequeStatus = chequeStatus;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    public String getCashInDate() {
        return cashInDate;
    }

    public void setCashInDate(String cashInDate) {
        this.cashInDate = cashInDate;
    }

    public int getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(int collectionId) {
        this.collectionId = collectionId;
    }

    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public double getCollectionAmount() {
        return collectionAmount;
    }

    public void setCollectionAmount(double collectionAmount) {
        this.collectionAmount = collectionAmount;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getBusinessUnitiId() {
        return businessUnitiId;
    }

    public void setBusinessUnitiId(int businessUnitiId) {
        this.businessUnitiId = businessUnitiId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }
}
