package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CollectionImages implements Serializable {
    @JsonProperty("photo_path")
    String photoPath;
    @JsonProperty("file_name")
    String fileName;
    @JsonProperty("file_uri")
    String fileUri;
    @JsonProperty("collection_id")
    long id;

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
