package com.cgtechnodreams.salesassistindia.salesorder.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CollectionEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CollectionImageEntity;
import com.cgtechnodreams.salesassistindia.salesorder.UpdateCollectionActivity;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionDetailModel;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CollectionAdapter extends RecyclerView.Adapter<CollectionAdapter.MyViewHolder>{

    Context mContext;
    PrefManager prefManager;
    ArrayList<CollectionDetailModel> collections = new ArrayList<>();

    public CollectionAdapter(Context context, ArrayList<CollectionDetailModel> collections) {
        this.mContext = context;
        this.collections = collections;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public CollectionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_collection_detail, null);

        return new CollectionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CollectionAdapter.MyViewHolder holder, int position) {
        CollectionDetailModel collectionDetailModel = collections.get(position);
        holder.bankName.setText(collectionDetailModel.getBankName());
        holder.amount.setText( collectionDetailModel.getCollectionAmount() + "");

    }

    @Override
    public int getItemCount() {
        return collections.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.bankName)
        TextView bankName;
        @BindView(R.id.amount)
        TextView amount;
        @BindView(R.id.edit)
        ImageView imgEdit;
        @BindView(R.id.delete)
        ImageView imgDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, UpdateCollectionActivity.class);
                    intent.putExtra("CollectionDetail",collections.get(getAdapterPosition()));
                    mContext.startActivity(intent);

                }
            });
            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CollectionEntity collectionEntity = new CollectionEntity(mContext);
                    CollectionImageEntity collectionImageEntity = new CollectionImageEntity(mContext);
                    collectionImageEntity.deleteById(collections.get(getAdapterPosition()).getCollectionId());
                    collectionEntity.deleteById(collections.get(getAdapterPosition()).getCollectionId());
                    collections.remove(collections.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });


        }
    }
}

