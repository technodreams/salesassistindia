package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SalesmanSalesOrderReprot extends BaseResponse {
    //"user_id": 3,
//            "user_name": "BK Dai",
//            "distributor_name": "Ram Store",
//            "distributor_area": "Kathmandu",
//            "distributor_id": 2
    @JsonProperty("salesman_id")
    String salesmanId;
    @JsonProperty("salesman_name")
    String salesmanName;
    @JsonProperty("distributor_name")
    String distributorName;
    @JsonProperty("distributor_area")
    String distributorArea;
    @JsonProperty("distributor_id")
    String distributorId;

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorArea() {
        return distributorArea;
    }

    public void setDistributorArea(String distributorArea) {
        this.distributorArea = distributorArea;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }
}
