package com.cgtechnodreams.salesassistindia.salesorder.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PendingSalesDetailResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<PendingSalesRequestDetail> data;

    public ArrayList<PendingSalesRequestDetail> getData() {
        return data;
    }

    public void setData(ArrayList<PendingSalesRequestDetail> data) {
        this.data = data;
    }
}
