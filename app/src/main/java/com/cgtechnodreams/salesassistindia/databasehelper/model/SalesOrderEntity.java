package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesOrder;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class SalesOrderEntity extends BaseModel {
    private SQLiteDbHelper adapter;
    private Context context;


    private static final String TAG = SalesOrderEntity.class.getName();
    public static final String TABLE_NAME = "SalesOrder";
    public static final String COL_ID= "id";
    public static final String COL_OUTLET_ID = "outlet_id";
    public static final String COL_OUTLET_NAME = "outlet_name";
    public static final String COL_PRODUCT_ID = "product_id";
    public static final String COL_PRODUCT_NAME = "product_name";
    public static final String COL_WAREHOUSE_ID = "warehouse_id";
    public static final String COL_WAREHOUSE_NAME = "warehouse_name";
    public static final String COL_DEALER_PRICE = "dealer_price";
    public static final String COL_QUANTITY = "quantity";
    public static final String COL_TOTAL_PRICE= "total_price";
    public static final String COL_DISCOUNT_PERCENTAGE = "discount_percentage";
    public static final String COL_DISCOUNT_AMOUNT = "discount_amount";
    public static final String COL_EXCISE = "excise";
    public static final String COL_TAXABLE_AMOUNT = "taxable_amount";
    public static final String COL_VAT= "vat";
    public static final String COL_GRAND_TOTAL = "grand_total";
    public static final String COL_LATITUDE= "latitude";
    public static final String COL_LONGITUDE= "longitude";
    public static final String COL_DATE_TIME= "date_time";
    public static final String COL_PHONE_VISIT= "phone_visit";
    public static final String COL_REMARKS= "remarks";


    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_OUTLET_ID + " INTEGER NOT NULL, " +
            COL_OUTLET_NAME + " TEXT NOT NULL, " +
            COL_PRODUCT_ID + " INTEGER NOT NULL, " +
            COL_PRODUCT_NAME + " TEXT NOT NULL, " +
            COL_WAREHOUSE_ID + " INTEGER NOT NULL, " +
            COL_WAREHOUSE_NAME + " TEXT NOT NULL, " +
            COL_DEALER_PRICE + " REAL NOT NULL, " +
            COL_QUANTITY + " INTEGER NOT NULL, " +
            COL_TOTAL_PRICE + " REAL NOT NULL, " +
            COL_DISCOUNT_PERCENTAGE + " REAL NOT NULL, " +
            COL_DISCOUNT_AMOUNT + " REAL NOT NULL, " +
            COL_EXCISE + " REAL NOT NULL, " +
            COL_TAXABLE_AMOUNT + " REAL NOT NULL, " +
            COL_VAT + " REAL NOT NULL, " +
            COL_GRAND_TOTAL + " REAL NOT NULL, " +
            COL_LATITUDE + " DOUBLE NOT NULL, " +
            COL_LONGITUDE+ " DOUBLE NOT NULL, " +
            COL_PHONE_VISIT+ " TEXT NOT NULL, " +
            COL_REMARKS+ " TEXT, " +

            COL_DATE_TIME+ " TEXT NOT NULL " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;


    public SalesOrderEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);

    }
    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }


    @Override
    public  long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                if(!key.equalsIgnoreCase("id"))
                    values.put(key, obj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            LogUtils.setUserActivity(context ,"Sales Order" , "Sales Order saved locally");

            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG+ " Error JSONException: ; insert()", e.getMessage());
            this.adapter.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error SQLException: Area; insert()", e.getMessage());
            this.adapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error Exception: Area; insert()", e.getMessage());
            this.adapter.close();
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, obj.getString(key));
            }
            SalesOrder salesOrder = (SalesOrder) object;
            long result = this.adapter.getDatabase().update(TABLE_NAME, values, "id="+salesOrder.getOrderId(),null);
            if (-1 == result) {
                LogUtils.setUserActivity(context ,"Sales Order Update", "Sales Order update failes");
                throw new Exception("Unable to update");

            }
            // return 1;
            LogUtils.setUserActivity(context ,"Sales Order Update", "Sales Order updated locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        }
        return 0;
    }


    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }


    public int deleteById(int id) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_ID + "=" + id);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }



    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }

}
