package com.cgtechnodreams.salesassistindia.databasehelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.cgtechnodreams.salesassistindia.BaseApplication;
import com.cgtechnodreams.salesassistindia.databasehelper.model.AreaEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.BankEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.BrandEnity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CollectionEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CollectionImageEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CompanyEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CompetitorSalesModel;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DailySalesDetailModel;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DailySalesModel;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorDailySalesEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorPurchaseEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorRequestEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductClosingEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistDailySalesEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistRequestEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.FMCGSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.LocationModelEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivitiesImageEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivityCategoryEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivityEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.OrderRemarksCurrentBalanceEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.OutletStatusEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductGroupModel;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductInventoryEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductSubGroupEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SalesOrderEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SurveyAnswerEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SurveyEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SyncEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.UserActivityEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.UserEntity;
import com.cgtechnodreams.salesassistindia.photo.model.PhotoEntity;

import java.io.File;

/**
 * Created by user on 14/01/2019.
 */

public class SQLiteDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_SALES_ASSIST = "sales_assist.db";

    private final static String DATABASE_PATH = Environment.getExternalStorageDirectory()
            + File.separator + BaseApplication.APP_FOLDER_NAME
            + File.separator + DATABASE_SALES_ASSIST;
    private static SQLiteDbHelper databaseAdapter = null;
    protected SQLiteDatabase database;
    String TAG = SQLiteDbHelper.class.getName() + "==>";
    Context mContext;
    private SQLiteDbHelper mDbAdapter;

    public SQLiteDbHelper(Context context) {
      //  super(context, DATABASE_PATH, null, DATABASE_VERSION);
        super(context, DATABASE_SALES_ASSIST, null, DATABASE_VERSION);
        this.mContext = context;
    }

    public static SQLiteDbHelper getDbAdapter(Context context) {
        if (null == databaseAdapter) {
            databaseAdapter = new SQLiteDbHelper(context);
        }
        return databaseAdapter;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LocationModelEntity.TABLE_SCRIPT);
        db.execSQL(AreaEntity.TABLE_SCRIPT);
        db.execSQL(BrandEnity.TABLE_SCRIPT);
        db.execSQL(ProductEntity.TABLE_SCRIPT);
        db.execSQL(ProductGroupModel.TABLE_SCRIPT);
        db.execSQL(DailySalesModel.TABLE_SCRIPT);
        db.execSQL(DailySalesDetailModel.TABLE_SCRIPT);
        db.execSQL(UserActivityEntity.TABLE_SCRIPT);
        db.execSQL(CompetitorSalesModel.TABLE_SCRIPT);
        db.execSQL(PhotoEntity.TABLE_SCRIPT);
        db.execSQL(SyncEntity.TABLE_SCRIPT);
        db.execSQL(SurveyEntity.TABLE_SCRIPT);
        db.execSQL(SurveyAnswerEntity.TABLE_SCRIPT);
        db.execSQL(UserEntity.TABLE_SCRIPT);
        db.execSQL(StockistEntity.TABLE_SCRIPT);
        db.execSQL(SalesOrderEntity.TABLE_SCRIPT);
        db.execSQL(ProductInventoryEntity.TABLE_SCRIPT);
        db.execSQL(OutletStatusEntity.TABLE_SCRIPT);
        db.execSQL(StockistRequestEntity.TABLE_SCRIPT);
        db.execSQL(ProductSubGroupEntity.TABLE_SCRIPT);
        db.execSQL(StockistStockEntity.TABLE_SCRIPT);
        db.execSQL(FMCGSalesOrderEntity.TABLE_SCRIPT);
        db.execSQL(CollectionEntity.TABLE_SCRIPT);
        db.execSQL(BankEntity.TABLE_SCRIPT);
        db.execSQL(StockistDailySalesEntity.TABLE_SCRIPT);
        db.execSQL(CollectionImageEntity.TABLE_SCRIPT);
        db.execSQL(MarketActivityCategoryEntity.TABLE_SCRIPT);
        db.execSQL(MarketActivityEntity.TABLE_SCRIPT);
        db.execSQL(MarketActivitiesImageEntity.TABLE_SCRIPT);
        db.execSQL(StockistDamageStockEntity.TABLE_SCRIPT);
        db.execSQL(CompanyEntity.TABLE_SCRIPT);
        db.execSQL(OrderRemarksCurrentBalanceEntity.TABLE_SCRIPT);
        db.execSQL(DistributorEntity.TABLE_SCRIPT);
        db.execSQL(DistributorSalesOrderEntity.TABLE_SCRIPT);

        db.execSQL(DistributorDamageStockEntity.TABLE_SCRIPT);
        db.execSQL(DistributorRequestEntity.TABLE_SCRIPT);
        db.execSQL(DistributorStockEntity.TABLE_SCRIPT);
        db.execSQL(DistributorDailySalesEntity.TABLE_SCRIPT);
        db.execSQL(ProductClosingEntity.TABLE_SCRIPT);
        db.execSQL(DistributorPurchaseEntity.TABLE_SCRIPT);
    }

    public void deleteDatabase(android.database.sqlite.SQLiteDatabase db) {
        db.execSQL(AreaEntity.DROP_TABLE_SCRIPT);
        db.execSQL(LocationModelEntity.DROP_TABLE_SCRIPT);
        db.execSQL(BrandEnity.DROP_TABLE_SCRIPT);
        db.execSQL(ProductEntity.DROP_TABLE_SCRIPT);
        db.execSQL(ProductGroupModel.DROP_TABLE_SCRIPT);
        db.execSQL(DailySalesModel.DROP_TABLE_SCRIPT);
        db.execSQL(DailySalesDetailModel.DROP_TABLE_SCRIPT);
        db.execSQL(UserActivityEntity.DROP_TABLE_SCRIPT);
        db.execSQL(CompetitorSalesModel.DROP_TABLE_SCRIPT);
        db.execSQL(PhotoEntity.DROP_TABLE_SCRIPT);
        db.execSQL(SyncEntity.DROP_TABLE_SCRIPT);
        db.execSQL(SurveyEntity.DROP_TABLE_SCRIPT);
        db.execSQL(SurveyAnswerEntity.DROP_TABLE_SCRIPT);
        db.execSQL(UserEntity.DROP_TABLE_SCRIPT);
        db.execSQL(StockistEntity.DROP_TABLE_SCRIPT);
        db.execSQL(SalesOrderEntity.DROP_TABLE_SCRIPT);
        db.execSQL(ProductInventoryEntity.DROP_TABLE_SCRIPT);
        db.execSQL(OutletStatusEntity.DROP_TABLE_SCRIPT);
        db.execSQL(StockistRequestEntity.DROP_TABLE_SCRIPT);
        db.execSQL(ProductSubGroupEntity.DROP_TABLE_SCRIPT);
        db.execSQL(StockistStockEntity.DROP_TABLE_SCRIPT);
        db.execSQL(FMCGSalesOrderEntity.DROP_TABLE_SCRIPT);
        db.execSQL(CollectionEntity.DROP_TABLE_SCRIPT);
        db.execSQL(BankEntity.DROP_TABLE_SCRIPT);
        db.execSQL(StockistDailySalesEntity.DROP_TABLE_SCRIPT);
        db.execSQL(CollectionImageEntity.DROP_TABLE_SCRIPT);
        db.execSQL(MarketActivityCategoryEntity.DROP_TABLE_SCRIPT);
        db.execSQL(MarketActivityEntity.DROP_TABLE_SCRIPT);
        db.execSQL(MarketActivitiesImageEntity.DROP_TABLE_SCRIPT);
        db.execSQL(StockistDamageStockEntity.DROP_TABLE_SCRIPT);
        db.execSQL(CompanyEntity.DROP_TABLE_SCRIPT);
        db.execSQL(OrderRemarksCurrentBalanceEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorDamageStockEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorDailySalesEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorStockEntity.DROP_TABLE_SCRIPT);
        db.execSQL(FMCGSalesOrderEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorSalesOrderEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorDamageStockEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorRequestEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorStockEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorDailySalesEntity.DROP_TABLE_SCRIPT);
        db.execSQL(ProductClosingEntity.DROP_TABLE_SCRIPT);
        db.execSQL(DistributorPurchaseEntity.DROP_TABLE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.i("Old Database Version", oldVersion + "");
        Log.i("New Database Version", newVersion + "");

//       deleteDatabase(db);
//       onCreate(db);
    }

    public SQLiteDatabase openDatabase() {
        //this.database = this.getWritableDatabase(DATABASE_PASSPHRASE);
        this.database = this.getWritableDatabase();
        try {
            this.database.execSQL("PRAGMA automatic_index = off;");
        } catch (Exception e) {
            Log.e(TAG, "openDatabase: " + e.getMessage());
        }
        return this.database;
    }

    public SQLiteDatabase getDatabase() {
        if (null == this.database || !this.database.isOpen()) {
            throw new SQLiteException("Database has not been opened yet. Call openDatabase() first before calling getDatabase().");
        }

        return this.database;
    }

    public void deleteIfDatabaseExists() {
        File database = new File(DATABASE_PATH + DATABASE_SALES_ASSIST);
//        File database = new File(DATABASE_PATH + File.separator +
//                (BuildConfig.FLAVOR_configuration.equalsIgnoreCase("branding") ? BRD_DATABASE_NAME : DIST_DATABASE_NAME));
//        File databaseJournal = new File(DATABASE_PATH + File.separator + DIST_DATABASE_NAME + "-journal");

        if (database.exists()) {
//            boolean deleted = database.delete();
//            Log.i("Database", "Found and deleted " + deleted);
        }

        //commented for attempt to write on readonly error
//        if (databaseJournal.exists()) {
//            boolean deleted = databaseJournal.delete();
//            Log.i("Database", "Found and deleted " + deleted);
//        }
    }
}