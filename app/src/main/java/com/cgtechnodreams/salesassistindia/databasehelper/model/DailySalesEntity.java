package com.cgtechnodreams.salesassistindia.databasehelper.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DailySalesEntity {

//    @JsonProperty("product_id")
//    private int productId;
//    @JsonProperty("model")
//    private String modelNumber;
//    @JsonProperty("mrp")
//    private String mrp;
//    @JsonProperty("mop")
//    private String mop;
//    @JsonProperty("brand_id")
//    private int brandId;
//    @JsonProperty("brand_name")
//    private String brandName;
//    @JsonProperty("group_id")
//    private int groupId;
//    @JsonProperty("group_name")
//    private String groupName;
    @JsonProperty("customer_name")
    private String customerName;
    @JsonProperty("customer_number")
    private String customerMobile;
    @JsonProperty("customer_address")
    private String customerAddress;
    @JsonProperty("quantity")
    private int quantity;
    @JsonProperty("detail")
    private String detail;
    @JsonProperty("date")
    private String date;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

//    public int getProductId() {
//        return productId;
//    }
//
//    public void setProductId(int productId) {
//        this.productId = productId;
//    }
//
//    public String getModelNumber() {
//        return modelNumber;
//    }
//
//    public void setModelNumber(String modelNumber) {
//        this.modelNumber = modelNumber;
//    }
//
//    public String getMrp() {
//        return mrp;
//    }
//
//    public void setMrp(String mrp) {
//        this.mrp = mrp;
//    }
//
//    public String getMop() {
//        return mop;
//    }
//
//    public void setMop(String mop) {
//        this.mop = mop;
//    }
//
//    public int getBrandId() {
//        return brandId;
//    }
//
//    public void setBrandId(int brandId) {
//        this.brandId = brandId;
//    }
//
//    public String getBrandName() {
//        return brandName;
//    }
//
//    public void setBrandName(String brandName) {
//        this.brandName = brandName;
//    }
//
//    public int getGroupId() {
//        return groupId;
//    }
//
//    public void setGroupId(int groupId) {
//        this.groupId = groupId;
//    }
//
//    public String getGroupName() {
//        return groupName;
//    }
//
//    public void setGroupName(String groupName) {
//        this.groupName = groupName;
//    }
//
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
