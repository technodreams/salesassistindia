package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.Context;
import android.database.Cursor;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;



/**
 * Created by user on 14/01/2019.
 */

public abstract class BaseModel {
    private SQLiteDbHelper mDbAdapter;
    public BaseModel(Context context) {
        if (null == mDbAdapter) {
            mDbAdapter = SQLiteDbHelper.getDbAdapter(context);
        }
    }

    public abstract Object find(Object object);

    /**
     * @param object
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */
    public abstract long insert(Object object);

    /**
     * @param object
     * @return the number of rows affected
     */
    public abstract int update(Object object);

    /**
     * @param object
     * @return the number of rows affected
     */
    public abstract int delete(Object object);

    public abstract Object parseToObject(Cursor cursor);



}
