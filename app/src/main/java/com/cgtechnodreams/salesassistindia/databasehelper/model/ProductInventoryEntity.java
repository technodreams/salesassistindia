package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class ProductInventoryEntity extends BaseModel{

    private SQLiteDbHelper adapter;
    private Context context;


    private static final String TAG = ProductInventoryEntity.class.getName();

    public static final String TABLE_NAME = "ProductInventory";
    public static final String COL_PRODUCT_ID= "product_id";
    public static final String COL_WAREHOUSE_ID = "warehouse_id";
    public static final String COL_WAREHOUSE_NAME = "warehouse_name";
    public static final String COL_WAREHOUSE_CODE = "warehouse_code";
    public static final String COL_STORAGE_ID = "storage_id";
    public static final String COL_QUANTITY = "quantity";

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_PRODUCT_ID + " INTEGER NOT NULL, " +
            COL_WAREHOUSE_ID + " INTEGER NOT NULL, " +
            COL_WAREHOUSE_NAME + " TEXT, " +
            COL_STORAGE_ID + " INTEGER, " +
            COL_QUANTITY + " INTEGER," +
            COL_WAREHOUSE_CODE + " TEXT " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;
    public ProductInventoryEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);

    }

    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    public Object findByProductId(int productId) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_PRODUCT_ID + "=" + productId;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public  long insert(Object object) {
        if (null == object) {
            return 0;
        }
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONArray list = new JSONArray(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();

            this.adapter.openDatabase();
            this.adapter.openDatabase().beginTransaction();

            for (int i = 0; i < list.length(); i++) {
                final JSONObject model = list.getJSONObject(i);

                Iterator<String> keys = model.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    values.put(key, model.getString(key));
                }
                long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

                values.clear();
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

            }

            this.adapter.openDatabase().setTransactionSuccessful();
            this.adapter.openDatabase().endTransaction();
            LogUtils.setUserActivity(context ,"Product Inventory Insert", "Product Inventory fetched and saved locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        LogUtils.setUserActivity(context ,"Product Inventory Deletion", "Product Inventory Deletion Successful ");
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
