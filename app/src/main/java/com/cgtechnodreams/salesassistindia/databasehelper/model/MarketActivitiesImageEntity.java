package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class MarketActivitiesImageEntity extends BaseModel {
    private SQLiteDbHelper adapter;
    private Context context;
    public static final String TABLE_NAME = "MarketActivitiesImageEntity";
    public static final String COL_PATH = "photo_path";
    public static final String COL_FILE_NAME = "file_name";
    public static final String COL_FILE_URI = "file_uri";
    public static final String COL_MA_ID = "market_activities_id";
    private static final String TAG = MarketActivitiesImageEntity.class.getName();

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_MA_ID + " INTEGER NOT NULL, " +
            COL_PATH + " VARCHAR NOT NULL, " +
            COL_FILE_NAME + " VARCHAR NOT NULL, " +
            COL_FILE_URI + " VARCHAR NOT NULL " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;

    public MarketActivitiesImageEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
    }
    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                //if(!key.equalsIgnoreCase("market_activities_id"))
                    values.put(key, obj.getString(key));
               // el
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            LogUtils.setUserActivity(context ,"Market Activities Image" , "Market Activities Image saved locally");

            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG+ " Error JSONException: ; insert()", e.getMessage());
            this.adapter.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error SQLException:Market Activities Image ; insert()", e.getMessage());
            this.adapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error Exception: Market Activities Image; insert()", e.getMessage());
            this.adapter.close();
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        LogUtils.setUserActivity(context ,TAG, "Market Activities Image deleted locally");
        return 0;
    }
    public int deleteById(int id) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_MA_ID + "=" + id);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }
    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
