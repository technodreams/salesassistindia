package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class DistributorPurchaseEntity extends BaseModel{
    private SQLiteDbHelper adapter;
    private Context context;


    private static final String TAG = ProductClosingEntity.class.getName();

    public static final String TABLE_NAME = "DistributorPurchaseEntity";
    public static final String COL_ID = "id";
    public static final String COL_DISTRIBUTOR_ID = "distributor_id";

    public static final String COL_DISTRIBUTOR_NAME = "distributor_name";
    public static final String COL_PRODUCT_ID = "product_id";
    public static final String COL_PRODUCT_NAME = "product_name";//remove
    public static final String COL_QTY = "quantity";//name,detail,modelnumber same
    public static final String COL_DATE = "date";
    public static final String COL_BILL_NUMBER = "bill_number";
    public static final String COL_DISCOUNT_AMOUNT = "discount_amount";
    public static final String COL_AMOUNT = "amount";
    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_DISTRIBUTOR_ID + " VARCHAR NOT NULL, " +
            COL_PRODUCT_ID + " VARCHAR NOT NULL, " +
            COL_DISTRIBUTOR_NAME + " VARCHAR NOT NULL, " +
            COL_PRODUCT_NAME + " VARCHAR NOT NULL, " +
            COL_QTY + " VARCHAR NOT NULL, " +
            COL_DATE + " VARCHAR, " +
            COL_BILL_NUMBER + " VARCHAR, " +
            COL_DISCOUNT_AMOUNT + " VARCHAR, " +
            COL_AMOUNT + " VARCHAR NOT NULL" +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;
    public DistributorPurchaseEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
    }

    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject jsonObj = new JSONObject(mapper.writeValueAsString(object));

            ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = jsonObj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                if(!key.equals("id"))
                    values.put(key, jsonObj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject jsonObj = new JSONObject(mapper.writeValueAsString(object));


            ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = jsonObj.keys();

            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, jsonObj.getString(key));
            }
            int result = this.adapter.getDatabase().update(TABLE_NAME,
                    values,
                    COL_ID + " = ? ",
                    new String[]{String.valueOf(jsonObj.getInt(COL_ID))});

            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        }
        return 0;
    }


    @Override
    public int delete(Object object) {
        this.adapter.close();
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }

    public int deleteByOutletId(int outletId) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_DISTRIBUTOR_ID + "=" + outletId);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }


    public int deleteById(int id) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_ID + "=" + id );
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }


    public Object findByOutletId(int distributorId) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM "
                    + TABLE_NAME + " WHERE " + COL_DISTRIBUTOR_ID + " = " + distributorId ;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    public Object findByProductAndOutletId(int distributorId, int productId,String date) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM "
                    + TABLE_NAME + " WHERE " + COL_DISTRIBUTOR_ID + " = " + distributorId
                    + " AND " + COL_PRODUCT_ID + " = " + productId
                    + " AND " + COL_DATE + " = " + date;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
}
