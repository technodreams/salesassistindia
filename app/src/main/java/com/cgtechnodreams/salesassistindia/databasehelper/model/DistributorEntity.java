package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class DistributorEntity extends BaseModel{
    private SQLiteDbHelper adapter;
    private Context context;

    public static final String TABLE_NAME = "Distributor";
    public static final String COL_ID = "id";
    public static final String COL_DISTRIBUTOR_ID = "distributor_id";
    public static final String COL_DISTRIBUTOR_NAME = "distributor_name";
    public static final String COL_STOCKIST_ID = "stockist_id";
    public static final String COL_STOCKIST_NAME = "stockist_name";
    public static final String COL_MOBILE_NUMBER = "mobile_number";
    public static final String COL_LANDLINE_NUMBER = "landline_number";
    public static final String COL_EMAIL = "email";
    public static final String COL_ADDRESS = "address";
    public static final String COL_CONTACT_PERSON = "contact_person";
    public static final String COL_PAN_NUMBER = "pan_number";
    public static final String COL_DISTRICT_ID = "district_id";
    public static final String COL_DISTRICT_NAME = "district_name";
    public static final String COL_BUSINESS_UNIT_NAME = "business_unit_name";
    public static final String COL_BUSINESS_UNIT_ID = "business_unit_id";
    public static final String COL_LATITUDE = "latitude";
    public static final String COL_LONGITUDE = "longitude";
    public static final String COL_AREA_ID = "area_id";
    public static final String COL_AREA_NAME = "area_name";

    public static final String COL_CITY = "city";
    public static final String COL_LOCAL_ADMINISTRATIVE = "local_administrative_name";
    public static final String COL_PRICE_GROUP_ID = "price_group_id";
    public static final String COL_PRICE_GROUP_NAME = "price_group_name";
    private static final String TAG = StockistEntity.class.getName();

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_DISTRIBUTOR_ID + " INTEGER NOT NULL, " +
            COL_DISTRIBUTOR_NAME + " TEXT, " +
            COL_MOBILE_NUMBER + " TEXT, " +
            COL_LANDLINE_NUMBER + " TEXT, " +
            COL_EMAIL + " TEXT, " +
            COL_ADDRESS + " TEXT, " +
            COL_CONTACT_PERSON + " TEXT, " +
            COL_PAN_NUMBER + " TEXT, " +
            COL_DISTRICT_ID + " INTEGER, " +
            COL_DISTRICT_NAME + " TEXT, " +
            COL_AREA_ID + " INTEGER, " +
            COL_AREA_NAME + " TEXT, " +
            COL_CITY + " TEXT, " +
            COL_LOCAL_ADMINISTRATIVE + " TEXT, " +
            COL_BUSINESS_UNIT_ID + " INTEGER, " +
            COL_BUSINESS_UNIT_NAME + " TEXT, " +
            COL_PRICE_GROUP_ID + " INTEGER, " +
            COL_PRICE_GROUP_NAME + " TEXT, " +
            COL_STOCKIST_ID + " INTEGER, " +
            COL_STOCKIST_NAME + " TEXT, " +
            COL_LATITUDE + " TEXT, " +
            COL_LONGITUDE + " TEXT " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;

    public DistributorEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
    }

    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }


    @Override
    public  long insert(Object object) {

        if (null == object) {
            return 0;
        }
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONArray list = new JSONArray(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();

            this.adapter.openDatabase();
            this.adapter.openDatabase().beginTransaction();

            for (int i = 0; i < list.length(); i++) {
                final JSONObject model = list.getJSONObject(i);

                Iterator<String> keys = model.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    values.put(key, model.getString(key));
                }
                long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

                values.clear();

                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

            }

            this.adapter.openDatabase().setTransactionSuccessful();
            this.adapter.openDatabase().endTransaction();
            LogUtils.setUserActivity(context ,"Stockist Entry", "Stockist downloaded and saved successfully");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.close();
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
