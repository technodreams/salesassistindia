package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionDetailModel;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class CollectionEntity extends BaseModel {
    private SQLiteDbHelper adapter;
    private Context context;


    private static final String TAG = CollectionEntity.class.getName();
    public static final String TABLE_NAME = "Collection";
    public static final String COL_ID= "id";
    public static final String COL_DISTRIBUTOR_ID = "distributor_id";
    public static final String COL_DISTRIBUTOR_NAME = "distributor_name";
    public static final String COL_BANK_ID = "bank_id";
    public static final String COL_BANK_NAME = "bank_name";
    public static final String COL_COMPANY_ID = "company_id";
    public static final String COL_COMPANY_NAME = "company_name";
    public static final String COL_CHEQUE_NUMBER = "cheque_number";
    public static final String COL_AMOUNT = "collection_amount";
    public static final String COL_LATITUDE= "latitude";
    public static final String COL_LONGITUDE= "longitude";
    public static final String COL_DATE_TIME= "date_time";
    public static final String COL_ACCOUNT_NUMBER= "account_number";
    public static final String COL_REMARKS= "remarks";
    public static final String COL_BUSINESS_UNIT_ID = "business_unit_id";
    public static final String COL_PHOTO_PATH = "photo_path";
    public static final String COL_FILE_NAME = "file_name";
    public static final String COL_FILE_URI = "file_uri";
    public static final String COL_CASH_IN_DATE = "cash_in_date";
    public static final String COL_CHEQUE_STATUS = "cheque_status";
    public static final String COL_PAYMENT_METHOD = "payment_method";

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_DISTRIBUTOR_ID + " INTEGER NOT NULL, " +
            COL_DISTRIBUTOR_NAME + " TEXT NOT NULL, " +
            COL_BANK_ID + " INTEGER NOT NULL, " +
            COL_BANK_NAME + " TEXT NOT NULL, " +
            COL_COMPANY_ID + " INTEGER NOT NULL, " +
            COL_COMPANY_NAME + " TEXT NOT NULL, " +
            COL_AMOUNT + " DOUBLE NOT NULL, " +
            COL_CHEQUE_NUMBER + " VARCHAR NOT NULL, " +
            COL_LATITUDE + " DOUBLE NOT NULL, " +
            COL_LONGITUDE+ " DOUBLE NOT NULL, " +
            COL_REMARKS+ " TEXT, " +
            COL_PAYMENT_METHOD+ " TEXT, " +
            COL_ACCOUNT_NUMBER+ " VARCHAR, " +
            COL_BUSINESS_UNIT_ID+ " INTEGER, " +
            COL_CASH_IN_DATE + " TEXT, "+
            COL_PHOTO_PATH+ " TEXT, " +
            COL_FILE_NAME+ " TEXT, " +
            COL_FILE_URI+ " TEXT, " +
            COL_CHEQUE_STATUS + " TEXT, "+
            COL_DATE_TIME+ " TEXT NOT NULL " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;


    public CollectionEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);

    }
    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }
            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                if(!key.equalsIgnoreCase("id"))
                    values.put(key, obj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            LogUtils.setUserActivity(context ,"Collection Order" , "Collection saved locally");

            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG+ " Error JSONException: ; insert()", e.getMessage());
            this.adapter.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error SQLException: Collection; insert()", e.getMessage());
            this.adapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error Exception: Area; insert()", e.getMessage());
            this.adapter.close();
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, obj.getString(key));
                Log.i(key,obj.getString(key));
            }
            CollectionDetailModel collectionDetailModel = (CollectionDetailModel) object;
            Log.i("CollectionID",collectionDetailModel.getCollectionId() +"");
            long result = this.adapter.getDatabase().update(TABLE_NAME, values, "id="+collectionDetailModel.getCollectionId(),null);
            if (-1 == result) {
                LogUtils.setUserActivity(context ,"Collection Update", "Collection update failes");
                throw new Exception("Unable to update");

            }
            // return 1;
            LogUtils.setUserActivity(context ,"Collection Update", "Collection updated locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }
    public int deleteById(int id) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_ID + "=" + id);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }
    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }

    public Object findById(int distributorId) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE "+ COL_DISTRIBUTOR_ID + " = " + distributorId;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
}
