package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.salesorder.model.FMCGSalesOrder;
import com.cgtechnodreams.salesassistindia.salesorder.model.StockistPendingSyncTotal;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class FMCGSalesOrderEntity extends BaseModel {
    private SQLiteDbHelper adapter;
    private Context context;


    private static final String TAG = FMCGSalesOrderEntity.class.getName();
    public static final String TABLE_NAME = "FMCGSalesOrder";
    public static final String COL_ID= "id";
    public static final String COL_DISTRIBUTOR_ID = "distributor_id";
    public static final String COL_DISTRIBUTOR_NAME = "distributor_name";
    public static final String COL_PRODUCT_ID = "product_id";
    public static final String COL_PRODUCT_NAME = "product_name";
    public static final String COL_DEALER_PRICE = "dealer_price";
    public static final String COL_QUANTITY = "quantity";
    public static final String COL_TOTAL_PRICE= "total_price";
    public static final String COL_LATITUDE= "latitude";
    public static final String COL_LONGITUDE= "longitude";
    public static final String COL_DATE_TIME= "date_time";
    public static final String COL_BUSINESS_UNIT_ID= "business_unit_id";
    public static final String COL_REMARKS= "remarks";

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_DISTRIBUTOR_ID + " INTEGER NOT NULL, " +
            COL_DISTRIBUTOR_NAME + " TEXT NOT NULL, " +
            COL_PRODUCT_ID + " INTEGER NOT NULL, " +
            COL_PRODUCT_NAME + " VARCHAR NOT NULL, " +
            COL_DEALER_PRICE + " REAL NOT NULL, " +
            COL_QUANTITY + " INTEGER NOT NULL, " +
            COL_BUSINESS_UNIT_ID + " INTEGER NOT NULL, " +
            COL_TOTAL_PRICE + " REAL NOT NULL, " +
            COL_LATITUDE + " DOUBLE NOT NULL, " +
            COL_LONGITUDE+ " DOUBLE NOT NULL, " +
            COL_REMARKS+ " VARCHAR, " +
            COL_DATE_TIME+ " TEXT NOT NULL " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;


    public FMCGSalesOrderEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);

    }
    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();

                for (int i = 0; i < colCount; i++) {
                    if(cursor.getColumnName(i).equalsIgnoreCase(COL_TOTAL_PRICE) || cursor.getColumnName(i).equalsIgnoreCase(COL_DEALER_PRICE))
                    {
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getDouble(i));
                    }else{
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                    }

                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    public Object findById(int id, int businessUnitId) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE "+ COL_DISTRIBUTOR_ID + " = " + id + " AND " + COL_BUSINESS_UNIT_ID +  " = " + businessUnitId;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    if(cursor.getColumnName(i).equalsIgnoreCase(COL_TOTAL_PRICE) || cursor.getColumnName(i).equalsIgnoreCase(COL_DEALER_PRICE)){
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getDouble(i));
                    }else{
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                    }

                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    public Object findByOutletID(int id) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE "+ COL_DISTRIBUTOR_ID + " = " + id ;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    if(cursor.getColumnName(i).equalsIgnoreCase(COL_TOTAL_PRICE) || cursor.getColumnName(i).equalsIgnoreCase(COL_DEALER_PRICE)){
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getDouble(i));
                    }else{
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                    }

                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }


    public double getTotalSalesByOutletAndBusinessUnit(int outletId,int businessUnitId) {
        double total = 0;
        try {
            this.adapter.openDatabase();
            Cursor cursor =  this.adapter.getDatabase().rawQuery("SELECT SUM(" + COL_TOTAL_PRICE + ") as Total FROM " + TABLE_NAME  + " WHERE " + COL_DISTRIBUTOR_ID + "=" + outletId +" AND " + COL_BUSINESS_UNIT_ID + "=" + businessUnitId  , null);
            if (cursor.moveToFirst()) {
                total  = cursor.getDouble(cursor.getColumnIndex("Total"));// get final total
            }
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return Utils.round(total,2);
    }

    public double getTotalSalesByOutlet(int outletId) {
        double total = 0;
        try {
            this.adapter.openDatabase();

            Cursor cursor =  this.adapter.getDatabase().rawQuery("SELECT SUM(" + COL_TOTAL_PRICE + ") as Total FROM " + TABLE_NAME  + " WHERE " + COL_DISTRIBUTOR_ID + "=" + outletId  , null);

            if (cursor.moveToFirst()) {

                total  = cursor.getDouble(cursor.getColumnIndex("Total"));// get final total
            }
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return Utils.round(total,2);
    }
    @Override
    public  long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                if(!key.equalsIgnoreCase("id"))
                    values.put(key, obj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            LogUtils.setUserActivity(context ,TAG , "Sales Order saved locally");

            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG+ " Error JSONException: ; insert()", e.getMessage());
            this.adapter.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error SQLException: Area; insert()", e.getMessage());
            this.adapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error Exception: Area; insert()", e.getMessage());
            this.adapter.close();
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, obj.getString(key));
            }
            FMCGSalesOrder salesOrder = (FMCGSalesOrder) object;
            long result = this.adapter.getDatabase().update(TABLE_NAME, values, "id="+salesOrder.getOrderId(),null);
            if (-1 == result) {
                LogUtils.setUserActivity(context ,"Sales Order Update", "Sales Order update failes");
                throw new Exception("Unable to update");

            }
            // return 1;
            LogUtils.setUserActivity(context ,"Sales Order Update", "Sales Order updated locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        }
        return 0;
    }


    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }


    public int deleteById(int id) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_ID + "=" + id);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }


    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }

    public ArrayList<StockistPendingSyncTotal> getOutletPendingSyncTotal() {

            ArrayList<StockistPendingSyncTotal> outletWisePendingSyncTotal = new ArrayList<>();

            ArrayList<String> outletIdList = getOutletIdList();

            for (int i = 0; i< outletIdList.size(); i++){
                StockistPendingSyncTotal stockistPendingSyncTotal = new StockistPendingSyncTotal();
                double total = 0;
                String outletName = "";
                int outletId = Integer.parseInt(outletIdList.get(i));
                stockistPendingSyncTotal.setOutletId(outletId);
                try {
                    this.adapter.openDatabase();
                    Cursor cursor =  this.adapter.getDatabase().rawQuery("SELECT SUM(" + COL_TOTAL_PRICE + ") as Total FROM " + TABLE_NAME  + " WHERE " + COL_DISTRIBUTOR_ID + "=" + outletIdList.get(i)  , null);
                    Cursor cursor2 = this.adapter.getDatabase().rawQuery("SELECT DISTINCT " + COL_DISTRIBUTOR_NAME + " FROM " + TABLE_NAME + " WHERE " + COL_DISTRIBUTOR_ID + "=" + outletIdList.get(i),null );
                    if (cursor.moveToFirst()) {
                        total  = cursor.getDouble(cursor.getColumnIndex("Total"));// get final total
                        stockistPendingSyncTotal.setTotal(total);
                    }
                    if (cursor2.moveToFirst()) {
                        outletName  = cursor2.getString(cursor2.getColumnIndex(COL_DISTRIBUTOR_NAME));
                        stockistPendingSyncTotal.setOutletName(outletName);
                    }
                    outletWisePendingSyncTotal.add(stockistPendingSyncTotal);
                    cursor.close();
                    cursor2.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    Log.d(TAG + "find", e.getMessage());
                } finally {
                    this.adapter.close();
                }

            }
            return outletWisePendingSyncTotal;
        }
    public ArrayList<String> getOutletIdList(){
        ArrayList<String> outletIdList = new ArrayList<>();
        try {
            this.adapter.openDatabase();
            String query = "SELECT DISTINCT " + COL_DISTRIBUTOR_ID +  " FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                outletIdList.add(cursor.getString(cursor.getColumnIndex(COL_DISTRIBUTOR_ID)));
            }
            cursor.close();
        } catch (SQLException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();

        }finally {
            this.adapter.close();
        }
        return outletIdList;

    }


    public Object findWithRemarks(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();

           String query =  "select f.distributor_id,f.distributor_name,f.product_id,f.product_name,f.dealer_price,f.quantity,f.remarks,f.business_unit_id,f.total_price,f.latitude,f.longitude, f.date_time,o.order_remarks,o.current_balance FROM FMCGSalesOrder as f LEFT JOIN OrderRemarksCurrentBalance as o ON f.distributor_id = o.distributor_id AND f.business_unit_id = o.business_unit_id";
//            String query = "SELECT * FROM " + TABLE_NAME +
//                    " LEFT JOIN "+ " OrderRemarksCurrentBalance  "+
//                    "ON (FMCGSalesOrder.distributor_id = OrderRemarksCurrentBalance.distributor_id AND FMCGSalesOrder.business_unit_id = OrderRemarksCurrentBalance.business_unit_id)";
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();

                for (int i = 0; i < colCount; i++) {
                    if(cursor.getColumnName(i).equalsIgnoreCase(COL_TOTAL_PRICE) || cursor.getColumnName(i).equalsIgnoreCase(COL_DEALER_PRICE))
                    {
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getDouble(i));
                    }else{
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                    }

                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

}
