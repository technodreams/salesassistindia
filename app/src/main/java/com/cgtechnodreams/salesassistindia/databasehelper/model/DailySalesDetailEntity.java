package com.cgtechnodreams.salesassistindia.databasehelper.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DailySalesDetailEntity {
    @JsonProperty("sales_id")
    private int salesId;
    @JsonProperty("selling_price")
    private String sellingPrice;
    @JsonProperty("serial_number")
    private String serialNumber;
    @JsonProperty("product_id")
    private int productId;
    @JsonProperty("model_number")
    private String modelNumber;


    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }
}
