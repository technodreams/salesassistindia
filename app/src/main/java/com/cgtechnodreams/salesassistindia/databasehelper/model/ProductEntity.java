package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;


public class ProductEntity extends BaseModel {

    private SQLiteDbHelper adapter;
    private Context context;


    private static final String TAG = ProductEntity.class.getName();

    public static final String TABLE_NAME = "Product";
    public static final String COL_ID= "id";
    public static final String COL_NAME = "name";//remove
    public static final String COL_CODE = "code";//remove
    public static final String COL_DETAIL = "product_description";//name,detail,modelnumber same
    public static final String COL_IMAGE = "image";
    public static final String COL_RETAIL_PRICE = "retail_price";
    public static final String COL_WHOLESALE_PRICE= "wholesale_price";//empty for now might need in future don't remove
    public static final String COL_SPECIAL_PRICE = "special_price";//empty for now might need in future don't remove
    public static final String COL_DEALER_PRICE = "dealer_price";
    public static final String COL_EXCISE_PRICE = "excise_duty";
    public static final String COL_VAT= "vat";
    public static final String COL_BRAND_ID = "brand_id";
    public static final String COL_BRAND_NAME = "brand_name";
    public static final String COL_GROUP_ID= "group_id";
    public static final String COL_GROUP = "group_name";
    public static final String COL_SEGMENT_NAME = "segment_name";
    public static final String COL_SEGMENT_ID = "segment_id";
    public static final String COL_DIVISION_ID = "division_id";
    public static final String COL_DIVISION_NAME = "division_name";
    public static final String COL_BUSINESS_UNIT_ID = "business_unit_id";
    public static final String COL_BUSINESS_UNIT_NAME = "business_unit_name";
    public static final String COL_SUB_GROUP_NAME = "sub_group_name";
    public static final String COL_SUB_GROUP_ID = "sub_group_id";
    public static final String COL_PRICE_GROUP_ID = "price_group_id";
    public static final String COL_PRICE_GROUP_NAME = "price_group_name";
    public static final String COL_GROUP_WISE_PRICE = "price";

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER NOT NULL, " +
            COL_NAME + " TEXT NOT NULL, " +
            COL_CODE + " TEXT NOT NULL, " +
            COL_DETAIL + " TEXT, " +
            COL_IMAGE + " TEXT, " +
            COL_RETAIL_PRICE + " REAL, " +
            COL_WHOLESALE_PRICE + " REAL, " +
            COL_SPECIAL_PRICE + " REAL, " +
            COL_DEALER_PRICE+ " REAL, " +
            COL_EXCISE_PRICE + " REAL, " +
            COL_PRICE_GROUP_ID + " INTEGER, " +
            COL_PRICE_GROUP_NAME + " TEXT, " +
            COL_GROUP_WISE_PRICE + " REAL, " +
            COL_VAT + " REAL, " +
            COL_BRAND_ID + " INTEGER NOT NULL, " +
            COL_BRAND_NAME + " TEXT NOT NULL, " +
            COL_DIVISION_ID + " INTEGER,"+
            COL_DIVISION_NAME + " VARCHAR,"+
            COL_GROUP_ID + " INTEGER, " +
            COL_GROUP + " TEXT, " +
            COL_BUSINESS_UNIT_ID + " INTEGER,"+
            COL_BUSINESS_UNIT_NAME + " TEXT,"+
            COL_SUB_GROUP_ID + " INTEGER,"+
            COL_SUB_GROUP_NAME + " TEXT,"+
            COL_SEGMENT_NAME + " TEXT, " +
            COL_SEGMENT_ID + " INTEGER " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;
    public ProductEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);

    }

    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }


    public Object findBySubGroup(int subGroupId) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_SUB_GROUP_ID +  " =" + subGroupId;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
    public Object findByDivisionId(int divisionId) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_DIVISION_ID +  " =" + divisionId;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
    public Object findByBusinessUnitId(int businessUnit) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_BUSINESS_UNIT_ID +  " =" + businessUnit;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
    public Object findByBrand(int brandId) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_BRAND_ID +  " =" + brandId;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
    public Object findByPriceGroupAndBusinessUnit(int businessUnitId,int priceGroupId) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE "
                    + COL_BUSINESS_UNIT_ID +  " =" + businessUnitId
                    +" AND "
                    + COL_PRICE_GROUP_ID + " = " + priceGroupId;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
    @Override
    public  long insert(Object object) {
        if (null == object) {
            return 0;
        }
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONArray list = new JSONArray(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();

            this.adapter.openDatabase();
            this.adapter.openDatabase().beginTransaction();

            for (int i = 0; i < list.length(); i++) {
                final JSONObject model = list.getJSONObject(i);

                Iterator<String> keys = model.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    values.put(key, model.getString(key));
                }
                long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

                values.clear();
                // setTempSyncId(BaseApplication.getUser().getSpCode() + "-" + (Utils.getTimeInMilliSec() + result));
                // values.put(COL_M_SYNC_ID, getTempSyncId());
                //result = this.adapter.getDatabase().update(TABLE_NAME, values, COL_P_KEY + "=?", new String[]{String.valueOf(result)});
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

            }

            this.adapter.openDatabase().setTransactionSuccessful();
            this.adapter.openDatabase().endTransaction();
            LogUtils.setUserActivity(context ,"Product Insert", "Product fetched and saved locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        LogUtils.setUserActivity(context ,"Product Deletion", "Product Deletion Successful ");
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
