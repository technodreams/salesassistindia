package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Iterator;

public class SurveyAnswerEntity extends  BaseModel implements Serializable {
    private SQLiteDbHelper adapter;
    private Context context;


    private static final String TAG = SurveyAnswerEntity.class.getName();

    public static final String TABLE_NAME = "SurveyAnswer";
  //  public static final String COL_ID= "id";
    public static final String COL_ANSWER = "answer_data";
    public static final String COL_SURVEY_ID = "survey_id";
    public static final String COL_REF_ID_ONE= "ref_id_one";
    public static final String COL_REF_ID_TWO = "ref_id_two";
    public static final String COL_LATITUDE = "latitude";
    public static final String COL_LONGITUDE = "longitude";

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +

            COL_ANSWER + " TEXT NOT NULL, " +
            COL_SURVEY_ID + " TEXT NOT NULL, " +
            COL_REF_ID_ONE + " TEXT, " +
            COL_REF_ID_TWO + " TEXT, " +
            COL_LATITUDE + " TEXT, " +
            COL_LONGITUDE + " TEXT " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;
    public SurveyAnswerEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);

    }
    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    if(cursor.getColumnName(i).equalsIgnoreCase("answer_data")){
                        JSONArray jsonObject = new JSONArray(cursor.getString(i));
                        itemJSONObject.put(cursor.getColumnName(i), jsonObject);
                    }else{
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                    }


                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
    public Object getAnswerList(Object o) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT "+COL_ANSWER + " FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            JSONObject obj = (JSONObject) object;

            ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();

            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, obj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            LogUtils.setUserActivity(context ,"Survey Answer", "Survey Answer saved locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }


    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        LogUtils.setUserActivity(context ,"Survey Answer", "Survey Answer Deleted");
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }


}
