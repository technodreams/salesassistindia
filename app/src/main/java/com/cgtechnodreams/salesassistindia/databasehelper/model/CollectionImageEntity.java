package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionImages;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class CollectionImageEntity extends BaseModel{
    private SQLiteDbHelper adapter;
    private Context context;
    public static final String TABLE_NAME = "CollectionImage";
    public static final String COL_PATH = "photo_path";
    public static final String COL_FILE_NAME = "file_name";
    public static final String COL_FILE_URI = "file_uri";
    public static final String COL_COLLECTION_ID = "collection_id";
    private static final String TAG = CollectionImageEntity.class.getName();

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_COLLECTION_ID + " INTEGER NOT NULL, " +
            COL_PATH + " VARCHAR NOT NULL, " +
            COL_FILE_NAME + " VARCHAR NOT NULL, " +
            COL_FILE_URI + " VARCHAR NOT NULL " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;

    public CollectionImageEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
    }
    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                if(!key.equalsIgnoreCase("id"))
                    values.put(key, obj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            LogUtils.setUserActivity(context ,"Collection Image Order" , "Collection Image saved locally");

            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG+ " Error JSONException: ; insert()", e.getMessage());
            this.adapter.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error SQLException: Collection Image; insert()", e.getMessage());
            this.adapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            LogUtils.setUserActivity(context ,TAG + " Error Exception: Area; insert()", e.getMessage());
            this.adapter.close();
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, obj.getString(key));

            }
            CollectionImages collectionDetailModel = (CollectionImages) object;

            long result = this.adapter.getDatabase().update(TABLE_NAME, values, COL_COLLECTION_ID + " = "+collectionDetailModel.getId(),null);
            if (-1 == result) {
                LogUtils.setUserActivity(context ,"Collection Image Update", "Collection update failes");
                throw new Exception("Unable to update");

            }
            // return 1;
            LogUtils.setUserActivity(context ,"Collection Image Update", "Collection updated locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        LogUtils.setUserActivity(context ,TAG, "Photo Data deleted locally");
        return 0;
    }
    public int deleteById(int id) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_COLLECTION_ID + "=" + id);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }
    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
