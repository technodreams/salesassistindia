package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class StockistRequestEntity extends BaseModel {
    private SQLiteDbHelper adapter;
    private Context context;
    public static final String TABLE_NAME = "StockistRequest";
    public static final String COL_ID = "id";
    public static final String COL_DISTRIBUTOR_NAME = "distributor_name";
    public static final String COL_MOBILE_NUMBER = "mobile_number";
    public static final String COL_LANDLINE_NUMBER = "landline_number";
    public static final String COL_EMAIL = "email";
    public static final String COL_ADDRESS = "address";
    public static final String COL_CONTACT_PERSON = "contact_person";
    public static final String COL_PAN_NUMBER = "pan_number";
    public static final String COL_LATITUDE = "latitude";
    public static final String COL_LONGITUDE = "longitude";
    private static final String TAG = StockistRequestEntity.class.getName();

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER NOT NULL, " +
            COL_DISTRIBUTOR_NAME + " TEXT, " +
            COL_MOBILE_NUMBER + " TEXT, " +
            COL_LANDLINE_NUMBER + " TEXT, " +
            COL_EMAIL + " TEXT, " +
            COL_ADDRESS + " TEXT, " +
            COL_CONTACT_PERSON + " TEXT, " +
            COL_PAN_NUMBER + " TEXT, " +
            COL_LATITUDE + " TEXT, " +
            COL_LONGITUDE + " TEXT " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;

    public StockistRequestEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
    }
    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            JSONObject jsonObj = (JSONObject) object;

            ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = jsonObj.keys();

            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, jsonObj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.close();
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
