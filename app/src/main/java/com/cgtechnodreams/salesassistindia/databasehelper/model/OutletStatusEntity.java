package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class OutletStatusEntity extends BaseModel {
    public static final String TABLE_NAME = "OutletStatus";
    public static final String COL_ID = "id";
    public static final String COL_OUTLET_STATUS = "status";
    public static final String COL_OUTLET_ORDER_STATUS = "outlet_order_status";
    public static final String COL_ORDER_REMARKS = "remarks";
    public static final String COL_LATITUDE= "latitude";
    public static final String COL_LONGITUDE= "longitude";
    public static final String COL_DATE = "date";
    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER NOT NULL, " +
            COL_OUTLET_STATUS + " TEXT, " +
            COL_OUTLET_ORDER_STATUS + " TEXT, " +
            COL_LATITUDE + " TEXT, " +
            COL_LONGITUDE + " TEXT, " +
            COL_ORDER_REMARKS + " TEXT, " +
            COL_DATE + " TEXT " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;
    private static final String TAG = OutletStatusEntity.class.getName();
    private SQLiteDbHelper adapter;
    private Context context;

    public OutletStatusEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
    }

    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    public Object findById(int id) {
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_ID + " = " + id;
        Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);
        JSONObject itemJSONObject = null;
        try {
            if (cursor.moveToFirst()) {
                itemJSONObject = new JSONObject();
                int colCount = cursor.getColumnCount();

                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }
        return itemJSONObject;
    }


    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            JSONObject jsonObj = (JSONObject) object;

            ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = jsonObj.keys();

            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, jsonObj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }


    @Override
    public int delete(Object object) {
        this.adapter.close();
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
