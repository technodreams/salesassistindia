package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class DailySalesDetailModel extends BaseModel {
    private SQLiteDbHelper adapter;
    private Context context;
    UserActivityEntity activityModel;


    private static final String TAG = DailySalesDetailModel.class.getName();

    public static final String TABLE_NAME = "SalesDetail";
    public static final String COL_SALES_DETAIL_ID = "id";
    public static final String COL_SALES_DATE = "date";
    public static final String COL_SALES_ID = "sales_id";//foreing key from salestable
    public static final String COL_SELLING_PRICE = "selling_price";
    public static final String COL_SERIAL_NUMBER = "serial_number";
    public static final String COL_MODEL_NUMBER = "model_number";
    public static final String COL_PRODUCT_ID = "product_id";

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_SALES_DETAIL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_SALES_ID + " TEXT NOT NULL, " +
            COL_SALES_DATE + " TEXT NOT NULL, " +
            COL_SELLING_PRICE + " TEXT NOT NULL, " +
            COL_MODEL_NUMBER + " TEXT NOT NULL, " +
            COL_PRODUCT_ID + " TEXT NOT NULL, " +
            COL_SERIAL_NUMBER + " TEXT NOT NULL " +
            ")";



    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;
    public DailySalesDetailModel(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
        activityModel = new UserActivityEntity(context);

    }


    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public  long insert(Object object) {
        if (null == object) {
            return 0;
        }
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONArray list = new JSONArray(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();

            this.adapter.openDatabase();
            this.adapter.openDatabase().beginTransaction();

            for (int i = 0; i < list.length(); i++) {
                final JSONObject model = list.getJSONObject(i);

                Iterator<String> keys = model.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    values.put(key, model.getString(key));
                }
                long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }
                values.clear();
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

            }

            this.adapter.openDatabase().setTransactionSuccessful();
            this.adapter.openDatabase().endTransaction();
            LogUtils.setUserActivity(context ,"DailySales", "Daily Sales Added Successfully");

            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } finally {
            this.adapter.close();
        }
        return 0;
    }


    @Override
    public int update(Object object) {
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        return 0;
    }

    public long clearDb() {
        try {
            this.adapter.openDatabase();
            long result = this.adapter.getDatabase().delete(TABLE_NAME, null, null);
            if (-1 == result) {
                throw new Exception("Unable to clear db");
            }
            LogUtils.setUserActivity(context ,"Daily Sales", "Deleted Daily Sales Successful ");
            return 1;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    @Override
    public Object parseToObject(Cursor cursor) {
        return null;

    }
    public SQLiteDbHelper getAdapter() {
        this.adapter.openDatabase();
        return this.adapter;
    }


}
