package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class StockistStockEntity extends BaseModel {
    public static final String TABLE_NAME = "StockistStockEntity";
    public static final String COL_DISTRIBUTOR_ID = "distributor_id";
    public static final String COL_PRODUCT_ID = "product_id";
    public static final String COL_PRODUCT_NAME = "product_name";
    public static final String COL_QTY = "qty";
    public static final String COL_LATITUDE = "latitude";
    public static final String COL_LONGITUDE = "longitude";
    public static final String COL_DATE = "date";
    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_DISTRIBUTOR_ID + " INTEGER NOT NULL, " +
            COL_PRODUCT_NAME + " TEXT, " +
            COL_PRODUCT_ID + " INTEGER, " +
            COL_QTY + " INTEGER, " +
            COL_LATITUDE + " DOUBLE, " +
            COL_LONGITUDE + " DOUBLE, " +
            COL_DATE + " TEXT " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;
    private static final String TAG = StockistStockEntity.class.getName();
    private SQLiteDbHelper adapter;
    private Context context;

    public StockistStockEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
    }

    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    public Object findById(int id) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE "+ COL_DISTRIBUTOR_ID + " = " + id;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                        itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));

                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject jsonObj = new JSONObject(mapper.writeValueAsString(object));

            ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = jsonObj.keys();

            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, jsonObj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        }
        return 0;
    }
    @Override
    public int update(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject jsonObj = new JSONObject(mapper.writeValueAsString(object));


            ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = jsonObj.keys();

            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, jsonObj.getString(key));
            }
            int result = this.adapter.getDatabase().update(TABLE_NAME,
                    values,
                    COL_PRODUCT_ID + " = ? AND " + COL_DISTRIBUTOR_ID + " = ?",
                    new String[]{String.valueOf(jsonObj.getString(COL_PRODUCT_ID)), String.valueOf(jsonObj.getString(COL_DISTRIBUTOR_ID))});

            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        }
        return 0;
    }


    @Override
    public int delete(Object object) {
        this.adapter.close();
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }

    public int deleteById(int id) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_DISTRIBUTOR_ID + "=" + id);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }

    public int deleteById(int distributorId, int productId) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME + " WHERE " + COL_DISTRIBUTOR_ID + "=" + distributorId  + " AND " + COL_PRODUCT_ID + "=" + productId  );
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }
    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }

    public long getCount(int productId, int outletId) {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME +  " WHERE " + COL_DISTRIBUTOR_ID + " = " + outletId+" AND " + COL_PRODUCT_ID + " = " + productId;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);
            long i =  cursor.getCount();
            return i;

    }
}