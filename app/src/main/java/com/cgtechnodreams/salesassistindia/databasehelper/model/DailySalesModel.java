package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Iterator;

public class DailySalesModel extends BaseModel implements Serializable {private SQLiteDbHelper adapter;
    private Context context;


    private static final String TAG = DailySalesModel.class.getName();

    public static final String TABLE_NAME = "DailySales";
//    public static final String COL_PRODUCT_ID = "product_id";
//    public static final String COL_MODEL = "model";
//    public static final String COL_MRP = "mrp";
//    public static final String COL_MOP = "mop";
//    public static final String COL_BRAND_ID = "brand_id";
//    public static final String COL_BRAND_NAME = "brand_name";
//    public static final String COL_GROUP_ID = "group_id";
//    public static final String COL_GROUP_NAME = "group_name";
    public static final String COL_NAME= "customer_name";
    public static final String COL_NUMBER= "customer_number";
    public static final String COL_ADDRESS= "customer_address";
    public static final String COL_DATE= "date";
    public static final String COL_QTY= "quantity";
    public static final String COL_ORDER_ID = "id";
    public static final String COL_SALES_DETAIL = "detail";

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ORDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
//            COL_PRODUCT_ID + " TEXT NOT NULL, " +
//            COL_MODEL + " TEXT NOT NULL, " +
//            COL_MRP + " TEXT NOT NULL, " +
//            COL_MOP + " TEXT NOT NULL, " +
//            COL_BRAND_ID + " TEXT NOT NULL, " +
//            COL_BRAND_NAME + " TEXT NOT NULL, " +
//            COL_GROUP_ID + " TEXT NOT NULL, " +
//            COL_GROUP_NAME + " TEXT NOT NULL, " +
            COL_DATE + " TEXT, " +
            COL_NAME + " TEXT, " +
            COL_NUMBER + " TEXT, " +
            COL_ADDRESS + " TEXT, " +
            COL_QTY + " TEXT, " +
            COL_SALES_DETAIL + " TEXT NOT NULL " +
            ")";


    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;
    public DailySalesModel(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);

    }

    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public  long insert(Object object) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, obj.getString(key));
            }

            long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
            if (-1 == result) {
                throw new Exception("Unable to insert");
            }
           // return 1;
            LogUtils.setUserActivity(context ,"Daily Sales", "Daily sales saved locally");
            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        }
        return 0;
    }

    @Override
    public int update(Object object) {

        return 0;
    }
    public int update(Object object,int id) {
        if (null == object) {
            return 0;
        }
        this.adapter.openDatabase();
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONObject obj = new JSONObject(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();
            Iterator<String> userKeysIterator = obj.keys();
            while (userKeysIterator.hasNext()) {
                String key = userKeysIterator.next();
                values.put(key, obj.getString(key));
            }

            long result = this.adapter.getDatabase().update(TABLE_NAME, values, "id="+id,null);
            if (-1 == result) {
                LogUtils.setUserActivity(context ,"Daily Sales Update", "Daily Sales update failes");
                throw new Exception("Unable to update");

            }
            // return 1;
            LogUtils.setUserActivity(context ,"Daily Sales Update", "Daily sales updated locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "update()", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "udpate()", e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        return 0;
    }

    public long clearDb() {
        try {
            this.adapter.openDatabase();
            long result = this.adapter.getDatabase().delete(TABLE_NAME, null, null);
            if (-1 == result) {
                throw new Exception("Unable to clear db");
            }
            LogUtils.setUserActivity(context ,"Upload Daily Sales", "Daily Sales Deleted!!");
            return 1;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    @Override
    public Object parseToObject(Cursor cursor) {
        return null;

    }
    public SQLiteDbHelper getAdapter() {
        this.adapter.openDatabase();
        return this.adapter;
    }

//    public Object getDailySales(Object object) {
//        JSONArray resultJSONArray = null;
//        try {
//            this.adapter.openDatabase();
//            String query = "SELECT product_id, quantity, detail FROM " + TABLE_NAME;
//            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);
//
//            resultJSONArray = new JSONArray();
//            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//                int colCount = cursor.getColumnCount();
//                JSONObject itemJSONObject = new JSONObject();
//                for (int i = 0; i < colCount; i++) {
//                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
//                }
//                resultJSONArray.put(itemJSONObject);
//            }
//            cursor.close();
//        } catch (JSONException e) {
//            e.printStackTrace();
//            Log.d(TAG + "find", e.getMessage());
//        } finally {
//            this.adapter.close();
//        }
//
//        return resultJSONArray;
//    }
    public void setAdapter(SQLiteDbHelper adapter) {
        this.adapter = adapter;
    }
//    public List<DailySales> getDailySales(){
//        List<DailySales> dailySalesList = new ArrayList<DailySales>();
//
//        this.adapter.openDatabase();
//        String query = "SELECT * FROM " + TABLE_NAME;
//        Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);
//        // looping through all rows and adding to list
//        int orderId = 0;
//        if (cursor.moveToFirst()) {
//            do {
//
//                DailySales dailySales = new DailySales();
//                ArrayList<SalesDetail> salesDetail  = new ArrayList<SalesDetail>();
///*
//                orderId = cursor.getInt((cursor.getColumnIndex("id")));
//                dailySales.setProductId(cursor.getInt((cursor.getColumnIndex("product_id"))));
//                dailySales.setQty()));*/
//                if((cursor.getInt(cursor.getColumnIndex("quantity")))>1){
//                    for()
//                }else{
//
//                }
//
//                // adding to todo list
//                dailySalesList.add(dailySales);
//            } while (cursor.moveToNext());
//        }
//
//        return todos;
//        return null;
//    }


}