package com.cgtechnodreams.salesassistindia.databasehelper.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class SyncEntity extends BaseModel {
    private SQLiteDbHelper adapter;
    private Context context;
    public static final String TABLE_NAME = "Sync";
    public static final String COL_ACTION_NAME = "ActionName";
    public static final String COL_ACTION = "ActionDetail";
    public static final String COL_TYPE = "ActionType";
    public static final String COL_IS_SYNC_REQUIRED = "IsSyncRequired";
    public static final String COL_LAST_SYNC_DATA = "LastSync";
    private static final String TAG = SyncEntity.class.getName();

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ACTION_NAME + " TEXT NOT NULL, " +
            COL_ACTION + " TEXT NOT NULL, " +
            COL_TYPE + " TEXT NOT NULL, " +
            COL_IS_SYNC_REQUIRED + " BOOLEAN NOT NULL, " +
            COL_LAST_SYNC_DATA + " TEXT NOT NULL " +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;


    public SyncEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);

    }

    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }
            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }
    public Object getDownloadListData(String type) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_TYPE + "= " + type + " AND " + COL_IS_SYNC_REQUIRED + true;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public  long insert(Object object) {
        if (null == object) {
            return 0;
        }
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONArray list = new JSONArray(mapper.writeValueAsString(object));

            final ContentValues values = new ContentValues();

            this.adapter.openDatabase();
            this.adapter.openDatabase().beginTransaction();

            for (int i = 0; i < list.length(); i++) {
                final JSONObject model = list.getJSONObject(i);

                Iterator<String> keys = model.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                   // @TODO check why there is sync key??
                    if(!key.equalsIgnoreCase("sync"))
                        values.put(key, model.getString(key));
                }
                long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

                values.clear();
                // setTempSyncId(BaseApplication.getUser().getSpCode() + "-" + (Utils.getTimeInMilliSec() + result));
                // values.put(COL_M_SYNC_ID, getTempSyncId());
                //result = this.adapter.getDatabase().update(TABLE_NAME, values, COL_P_KEY + "=?", new String[]{String.valueOf(result)});
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

            }

            this.adapter.openDatabase().setTransactionSuccessful();
            this.adapter.openDatabase().endTransaction();
            LogUtils.setUserActivity(context ,"Brand", "Brand fetched and saved locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } finally {

            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
