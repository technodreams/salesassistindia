package com.cgtechnodreams.salesassistindia.marketactivities.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketActivityCategoryResponse extends BaseResponse {
    public ArrayList<MarketActivityCategory> getActivityCategory() {
        return activityCategory;
    }

    public void setActivityCategory(ArrayList<MarketActivityCategory> activityCategory) {
        this.activityCategory = activityCategory;
    }

    @JsonProperty("data")
    ArrayList<MarketActivityCategory> activityCategory;
}
