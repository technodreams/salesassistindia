package com.cgtechnodreams.salesassistindia.marketactivities.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class MarketActivityCategory implements Serializable {
    @JsonProperty("id")
    int id;
    @JsonProperty("name")
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /**
     * Pay attention here, you have to override the toString method as the
     * ArrayAdapter will reads the toString of the given object for the name
     *
     * @return name
     */
    @Override
    public String toString() {
        return name;
    }
}
