package com.cgtechnodreams.salesassistindia.marketactivities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.cgtechnodreams.salesassistindia.BaseApplication;
import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivitiesImageEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivityCategoryEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivityEntity;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivitiesImage;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivity;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivityCategory;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.CameraUtils;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

import static com.cgtechnodreams.salesassistindia.utils.AppConstant.MEDIA_TYPE_IMAGE;
import static com.cgtechnodreams.salesassistindia.utils.AppConstant.MEDIA_TYPE_IMAGE_MARKET_ACITIVITIES;

public class MarketActivitiesActivity extends AppCompatActivity {
    public static final int MARKET_ACTIVITIES_IMAGE_REQUEST_CODE = 205;
    private static String imageStoragePath = "";
    String imageName = "";
    Uri fileUri;
    PrefManager prefManager;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.activityImage)
    ImageView image;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.category)
    AppCompatSpinner category;
    @BindView(R.id.description)
    EditText description;
    String categoryName;
    ArrayList<MarketActivityCategory> categories = new ArrayList<>();
    int categoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_activities);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        toolbar.setTitle("Market Activities");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        LocationUtils locationUtils = new LocationUtils(MarketActivitiesActivity.this);
        locationUtils.requestLocationUpdates();
        setData();
    }

    private void setData() {
        MarketActivityCategoryEntity entity = new MarketActivityCategoryEntity(MarketActivitiesActivity.this);
        JSONArray array = (JSONArray) entity.find(null);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
            categories = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<MarketActivityCategory>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayAdapter adapter = new ArrayAdapter<>(MarketActivitiesActivity.this, android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adapter);
        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                categoryId = categories.get(i).getId();
                categoryName = categories.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @OnClick({ R.id.btnSave, R.id.takePhoto})
    public void onButtonClicked(View view) {
        switch (view.getId()) {

            case R.id.btnSave:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    if(categoryId == 0 || categoryName.equalsIgnoreCase("")){
                        Toast.makeText(MarketActivitiesActivity.this, "Please select Category", Toast.LENGTH_SHORT).show();
                    }else{
                        saveMarketActivities();
                    }


                }
                break;
            case R.id.takePhoto:
                if (CameraUtils.checkPermissions(getApplicationContext())) {
                    captureImage(MARKET_ACTIVITIES_IMAGE_REQUEST_CODE);
                } else {
                    requestCameraPermission(MEDIA_TYPE_IMAGE, MARKET_ACTIVITIES_IMAGE_REQUEST_CODE);
                }
                break;

        }
    }

    private void saveMarketActivities() {
//        if(!paymentMethod.equalsIgnoreCase("Cheque") && (imageStoragePath.equalsIgnoreCase("") ||imageName.equalsIgnoreCase(""))){
//            Toast.makeText(CollectionActivity.this, "Please take photo of deposit slip", Toast.LENGTH_SHORT).show();
//            return;
//        }
        MarketActivity activity = new MarketActivity();
        MarketActivitiesImage maImage = new MarketActivitiesImage();
        activity.setActivityCategoryId(categoryId);
        activity.setActivityCategory(categoryName);
        activity.setDescription(description.getText().toString());
        activity.setTitle(title.getText().toString());
        activity.setFilename(imageName);
        maImage.setFileName(imageName);
        activity.setFilepath(imageStoragePath);
        activity.setLatitude(Double.parseDouble(prefManager.getLatitude()));
        activity.setLongitude(Double.parseDouble(prefManager.getLongitude()));
        activity.setDateTime(Utils.getCurrentDateTime());
        if(fileUri== null){
            activity.setFileUri("");
            maImage.setFileUri("");
            maImage.setPhotoPath("");
        }
        else{
            activity.setFileUri(fileUri.toString());
            maImage.setFileUri(fileUri.toString());
            maImage.setPhotoPath(imageStoragePath);
        }


        MarketActivityEntity activityEntity = new MarketActivityEntity(MarketActivitiesActivity.this);
        long result = activityEntity.insert(activity);
        if(fileUri != null){
            maImage.setMarketActivitiesId(result);
            MarketActivitiesImageEntity imageEntity = new MarketActivitiesImageEntity(MarketActivitiesActivity.this);
            imageEntity.insert(maImage);
        }

        Toasty.info(MarketActivitiesActivity.this, "Market Activities Saved", Toasty.LENGTH_SHORT).show();
        finish();
    }

    public File getOutputMediaFile(int type) {
        String IMAGE_PATH = Environment.getExternalStorageDirectory()
                + File.separator + BaseApplication.APP_FOLDER_NAME;
        // External sdcard location
        File mediaStorageDir = new File(
                IMAGE_PATH,
                AppConstant.IMAGES_DIRECTORY);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e(AppConstant.IMAGES_DIRECTORY, "Oops! Failed create "
                        + AppConstant.IMAGES_DIRECTORY + " directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE_MARKET_ACITIVITIES) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMGMA_"+categoryId+ "_" + timeStamp + "." + AppConstant.IMAGE_EXTENSION);
        } else {
            return null;
        }

        return mediaFile;
    }
    private void requestCameraPermission(final int type, int resultCode) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage(resultCode);
                            }
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }
    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(MarketActivitiesActivity.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }
    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage(int requestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputMediaFile(MEDIA_TYPE_IMAGE_MARKET_ACITIVITIES);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
            imageName = file.getName();
        }
        fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, requestCode);
    }
    private void previewImage(int resultCode, ImageView imageView) {
        if (resultCode == RESULT_OK) {
            CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
            downSizeImage(imageStoragePath);
            previewCapturedImage(imageView);

        } else if (resultCode == RESULT_CANCELED) {
            fileUri = null;
            imageStoragePath = "";
            imageName = "";
            // user cancelled Image capture
            Toasty.info(getApplicationContext(),
                    "User cancelled image capture", Toast.LENGTH_SHORT, true)
                    .show();
        } else {
            fileUri = null;
            imageStoragePath = "";
            imageName = "";
            // failed to capture image
            Toasty.info(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT, true)
                    .show();
        }
    }
    private void downSizeImage(String imageStoragePath) {
        ByteArrayOutputStream byteArrayOutputStream = Utils.compressImage(imageStoragePath);
        try (OutputStream outputStream = new FileOutputStream(imageStoragePath)) {
            byteArrayOutputStream.writeTo(outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Display image from gallery
     */
    private void previewCapturedImage(ImageView imageView) {


        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            final Bitmap bitmap = BitmapFactory.decodeFile(Uri.parse(imageStoragePath).getPath(),
                    options);
            imageView.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MARKET_ACTIVITIES_IMAGE_REQUEST_CODE) {
            previewImage(resultCode, image);
        }
    }
}
