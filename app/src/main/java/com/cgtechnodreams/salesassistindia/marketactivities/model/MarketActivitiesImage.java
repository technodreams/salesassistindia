package com.cgtechnodreams.salesassistindia.marketactivities.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketActivitiesImage implements Serializable {

    @JsonProperty("photo_path")
    String photoPath;
    @JsonProperty("file_name")
    String fileName;
    @JsonProperty("file_uri")
    String fileUri;
    @JsonProperty("market_activities_id")
    long marketActivitiesId;

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }

    public long getMarketActivitiesId() {
        return marketActivitiesId;
    }

    public void setMarketActivitiesId(long marketActivitiesId) {
        this.marketActivitiesId = marketActivitiesId;
    }
}
