package com.cgtechnodreams.salesassistindia.marketactivities.data;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ApiUploadFile;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivity;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivityCategoryResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarketActivitiesDataService {
    private Context mContext;

    public MarketActivitiesDataService(Context mContext) {
        this.mContext = mContext;
    }

    public void getMarketActivityCategory(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<MarketActivityCategoryResponse> callBack = Api.getService(mContext).getMarketActivityCategory(token);
        callBack.enqueue(new Callback<MarketActivityCategoryResponse>() {
            @Override
            public void onResponse(Call<MarketActivityCategoryResponse> call, Response<MarketActivityCategoryResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }
            @Override
            public void onFailure(Call<MarketActivityCategoryResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void postMarketActivities(String token, ArrayList<MarketActivity> marketActivities, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postMarketActivities(token, marketActivities);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

    public void uploadMarketActivitiesImages(String token, RequestBody description, RequestBody size, List<MultipartBody.Part> files, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ResponseBody> callBack = ApiUploadFile.getService(mContext).uploadMarketActivitiesImages(token,description, size,files);
        callBack.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));


                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

}
