package com.cgtechnodreams.salesassistindia.backgroundlocationupdate;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.model.UserMovement;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.model.UserMovementResponse;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class MovementGPSActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    ProgressDialog dialog;
    PrefManager prefManager = null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    ArrayList<UserMovement> arrayListUserMovement = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_gps);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("User Movement");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    private void getMovementData() {
        if (Utils.isConnectionAvailable(MovementGPSActivity.this)) {

            dialog = new ProgressDialog(this);
            dialog.setMessage("Processing your request...");
            dialog.setCancelable(false);
            dialog.show();

            LocationDataService locationDataService = new LocationDataService(MovementGPSActivity.this);
            locationDataService.getMovementGPS(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    UserMovementResponse movementResponse = (UserMovementResponse) response;
                    if (movementResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(MovementGPSActivity.this, "Movement GPS", movementResponse.getMessage());
                        Toasty.error(MovementGPSActivity.this, movementResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                        if(movementResponse.getStatusCode()== APIConstants.ERROR_UNAUTHORIZED ){
                            Intent intent = new Intent(MovementGPSActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            PrefManager preferenceManager = new PrefManager(MovementGPSActivity.this);
                            preferenceManager.clear();
                            startActivity(intent);
                            finish();
                        }
                    }else{
                        if(movementResponse.getUserMovements().size() !=0){
                            setData(movementResponse.getUserMovements());
                        }

                    }


                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(MovementGPSActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT,true).show();
        }
    }

    private void setData(ArrayList<UserMovement> userMovements) {
        arrayListUserMovement = userMovements;
        LinearLayoutManager layoutManager = new LinearLayoutManager(MovementGPSActivity.this, LinearLayoutManager.VERTICAL, false);
        MovementGPSAdapter adapter = new MovementGPSAdapter(MovementGPSActivity.this, userMovements);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(MovementGPSActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        CameraPosition camera = new CameraPosition.Builder()
                                .target(new LatLng(arrayListUserMovement.get(position).getLatitude(),arrayListUserMovement.get(position).getLongitude()))
                                .zoom(18)  //limite ->21
                                .bearing(120) // 0 - 365
                                .tilt(45) // limite ->90
                                .build();

                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                }));

        CameraPosition camera = new CameraPosition.Builder()
                .target(new LatLng(arrayListUserMovement.get(0).getLatitude(),arrayListUserMovement.get(0).getLongitude()))
                .zoom(12)  //limite ->21
                .bearing(30) // 0 - 365
                .tilt(30) // limite ->90
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));

        for (UserMovement usermovement: arrayListUserMovement) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(usermovement.getLatitude(),
                    usermovement.getLongitude()))
                    .title("Your Location"))
                    .setSnippet(usermovement.getTime());
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                @Override
                // Return null here, so that getInfoContents() is called next.
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    // Inflate the layouts for the info window, title and snippet.
                    View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                            findViewById(R.id.map), false);

                    TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                    title.setText(marker.getTitle());

                    TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                    snippet.setText(marker.getSnippet());

                    return infoWindow;
                }
            });
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        mMap.getUiSettings().setZoomControlsEnabled(true);
        getMovementData();


    }
}
