package com.cgtechnodreams.salesassistindia.backgroundlocationupdate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

/**
 * Created by user on 09/01/2019.
 */

public class GPSStatusBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().matches("android.location.PROVIDERS_CHANGED")){
            if (!((LocationManager) context.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LocationUtils.sendNotification(context, "GPS disabled","Please turn on your GPS");
            }
//            if (!Utils.isTimeNetworkProvided(context)) {
//                LocationUtils.sendNotification(context, "Set Network Provided Time","Updated LocationModel");
//            }
        }

    }
}
