package com.cgtechnodreams.salesassistindia.backgroundlocationupdate;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.model.UserMovementResponse;
import com.cgtechnodreams.salesassistindia.dailysales.model.LocationModel;
import com.cgtechnodreams.salesassistindia.login.model.DeviceModel;
import com.cgtechnodreams.salesassistindia.login.model.LoginResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationDataService {
    private Context mContext;

    public LocationDataService(Context mContext) {
        this.mContext = mContext;
    }

    public void movementGPS(String token, ArrayList<LocationModel> locationModel, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<LocationResponse> callBack = Api.getService(mContext).movementGPS(token , locationModel);
        callBack.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
            }
        });
    }
    public void registerDevice(DeviceModel deviceModel, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<LoginResponse> callBack = Api.getService(mContext).registerDevice(deviceModel);
        callBack.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());

                }
                else{
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
            }
        });
    }
    public void getMovementGPS(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<UserMovementResponse> callBack = Api.getService(mContext).getMovementGPS(token);
        callBack.enqueue(new Callback<UserMovementResponse>() {
            @Override
            public void onResponse(Call<UserMovementResponse> call, Response<UserMovementResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<UserMovementResponse> call, Throwable t) {
                ErrorHandler.httpRequestFailure(mContext,t);
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);

            }
        });
    }
}
