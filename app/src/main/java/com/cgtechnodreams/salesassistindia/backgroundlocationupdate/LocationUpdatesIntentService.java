package com.cgtechnodreams.salesassistindia.backgroundlocationupdate;

/**
 * Created by user on 06/01/2019.
 */

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.google.android.gms.location.LocationResult;

import java.util.List;


/**
 * Handles incoming location updates and displays a notification with the location data.
 *
 * For apps targeting API level 25 ("Nougat") or lower, location updates may be requested
 * using {@link android.app.PendingIntent#getService(Context, int, Intent, int)} or
 * {@link android.app.PendingIntent#getBroadcast(Context, int, Intent, int)}. For apps targeting
 * API level O, only {@code getBroadcast} should be used.
 *
 *  Note: Apps running on "O" devices (regardless of targetSdkVersion) may receive updates
 *  less frequently than the interval specified in the
 *  {@link com.google.android.gms.location.LocationRequest} when the app is no longer in the
 *  foreground.
 */
public class LocationUpdatesIntentService extends IntentService {

  //  private AlertDialog mGpsAlertDialog;
   public static final String ACTION_PROCESS_UPDATES =
            "com.cgtechnodreams.cgfoodsunified.backgroundlocationupdate.action" +
                    ".PROCESS_UPDATES";
    private static final String TAG = LocationUpdatesIntentService.class.getSimpleName();

    public LocationUpdatesIntentService() {
        // Name the worker thread.
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROCESS_UPDATES.equals(action)) {
                LocationResult result = LocationResult.extractResult(intent);
                if (result != null) {
                    List<Location> locations = result.getLocations();
                    LocationUtils.setLocationUpdatesResult(this, locations);
                   // LocationUtils.sendNotification(this, LocationUtils.getLocationResultTitle(this, locations),"Updated LocationModel");
                    Log.i(TAG, LocationUtils.getLocationUpdatesResult(this));
                }
            }
        }
        if (!Utils.isTimeNetworkProvided(this)) {
            LocationUtils.sendNotification(this, "Set Network Provided Time","CG Unified");
        }

        if (!((LocationManager) getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LocationUtils.sendNotification(this, "GPS disabled","Updated LocationModel");
        }

    }


}