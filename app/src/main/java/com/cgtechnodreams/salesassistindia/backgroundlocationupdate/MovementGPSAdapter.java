package com.cgtechnodreams.salesassistindia.backgroundlocationupdate;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.model.UserMovement;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovementGPSAdapter extends RecyclerView.Adapter<MovementGPSAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<UserMovement> userMovements = new ArrayList<>();

    public MovementGPSAdapter(Context context, ArrayList<UserMovement> userMovements) {
        this.mContext = context;
        this.userMovements = userMovements;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public MovementGPSAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_movement, null);

        return new MovementGPSAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovementGPSAdapter.MyViewHolder holder, int position) {
        UserMovement userMovement = userMovements.get(position);
        holder.latlng.setText(userMovement.getLatitude() + ", " + userMovement.getLongitude());
        holder.time.setText(userMovement.getTime());
    }

    @Override
    public int getItemCount() {
        return userMovements.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.latlng)
        TextView latlng;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}