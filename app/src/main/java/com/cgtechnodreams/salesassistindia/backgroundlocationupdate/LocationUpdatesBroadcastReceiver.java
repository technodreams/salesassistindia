package com.cgtechnodreams.salesassistindia.backgroundlocationupdate;

/**
 * Created by user on 06/01/2019.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import com.cgtechnodreams.salesassistindia.databasehelper.model.LocationModelEntity;
import com.cgtechnodreams.salesassistindia.sync.Sync;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.google.android.gms.location.LocationResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Receiver for handling location updates.
 *
 * For apps targeting API level O
 * {@link android.app.PendingIntent#getBroadcast(Context, int, Intent, int)} should be used when
 * requesting location updates. Due to limits on background services,
 * {@link android.app.PendingIntent#getService(Context, int, Intent, int)} should not be used.
 *
 *  Note: Apps running on "O" devices (regardless of targetSdkVersion) may receive updates
 *  less frequently than the interval specified in the
 *  {@link com.google.android.gms.location.LocationRequest} when the app is no longer in the
 *  foreground.
 */
public class LocationUpdatesBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "LUBroadcastReceiver";


    public static final String ACTION_PROCESS_UPDATES =  "com.cgtechnodreams.cgfoodsunified.backgroundlocationupdate.action" +
            ".PROCESS_UPDATES";

    @Override
    public void onReceive(Context context, Intent intent) {
        PrefManager prefManager = new PrefManager(context);
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROCESS_UPDATES.equals(action)) {
                LocationResult result = LocationResult.extractResult(intent);
                if (result != null) {

                    List<Location> locations = result.getLocations();
                    //@TODO verify if we need to save multiple location or last known location only
                    LocationUtils.setLocationUpdatesResult(context, locations);
                    Log.e("BroadCast info",result.getLastLocation().getLatitude() + "," + result.getLastLocation().getLongitude());
                    saveLocationOffline(result.getLastLocation(), context);
                    prefManager.setLatitude(result.getLastLocation().getLatitude() + "");
                    prefManager.setLongitude(result.getLastLocation().getLongitude() + "");
                     LocationUtils.sendNotification(context, LocationUtils.getLocationResultTitle(context, locations),"Update LocationModel");
                    if (Utils.isConnectionAvailable(context) && prefManager.getAuthKey()!=null ) {
                        Sync sync = new Sync(context,prefManager.getRole(),false);
                        sync.uploadLocation();
                    }
                    Log.i(TAG, LocationUtils.getLocationUpdatesResult(context));
                }
            }
        }

    }

    public String getGeoCodeAddress(Location location, Context context) throws IOException {
       // final Location lastKnownLocation = BaseApplication.getLastKnownLocation();
        String geocodeAddress = "";
        if (null != location) {
            final Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            final List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            if (null != addresses && addresses.size() > 0) {
                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();

                geocodeAddress = fetchedAddress.getAddressLine(0);

              //  Bundle gecodeAddressBundle = (null != location.getExtras()) ? location.getExtras() : new Bundle();
               // gecodeAddressBundle.putString(BaseApplication.GEOCODE_ADDRESS, address.length() == 0 ? "" : address);
               // location.setExtras(gecodeAddressBundle);


            }
        }
        return geocodeAddress;
    }

    private void saveLocationOffline(final Location location, Context context) {
        final LocationModelEntity locationModelEntity = new LocationModelEntity(context);

        try {

            final Calendar calendar = Calendar.getInstance();

            final SimpleDateFormat simpleDateFormatCr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            final JSONObject lmLocationTrackData = new JSONObject();

            lmLocationTrackData.put(LocationModelEntity.COL_DATE_TIME, simpleDateFormatCr.format(calendar.getTime()));
            lmLocationTrackData.put(LocationModelEntity.COL_LATITUDE, String.valueOf(location.getLatitude()));
            lmLocationTrackData.put(LocationModelEntity.COL_LONGITUDE, String.valueOf(location.getLongitude()));
            //lmLocationTrackData.put(LocationModelEntity.COL_ADDRESS, address);
            locationModelEntity.insert(lmLocationTrackData);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}