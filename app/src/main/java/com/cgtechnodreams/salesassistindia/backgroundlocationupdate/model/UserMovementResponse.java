package com.cgtechnodreams.salesassistindia.backgroundlocationupdate.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class UserMovementResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<UserMovement> userMovements;

    public ArrayList<UserMovement> getUserMovements() {
        return userMovements;
    }

    public void setUserMovements(ArrayList<UserMovement> userMovements) {
        this.userMovements = userMovements;
    }
}
