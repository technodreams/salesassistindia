package com.cgtechnodreams.salesassistindia.sync;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.RxApi;
import com.cgtechnodreams.salesassistindia.api.RxApiService;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationDataService;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationResponse;
import com.cgtechnodreams.salesassistindia.bank.data.BankDataService;
import com.cgtechnodreams.salesassistindia.bank.model.Bank;
import com.cgtechnodreams.salesassistindia.bank.model.BankResponse;
import com.cgtechnodreams.salesassistindia.dailysales.dataservice.DailySalesDataService;
import com.cgtechnodreams.salesassistindia.dailysales.model.CompetitorSales;
import com.cgtechnodreams.salesassistindia.dailysales.model.CompetitorSalesDetailModel;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySales;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySalesResponse;
import com.cgtechnodreams.salesassistindia.dailysales.model.LocationModel;
import com.cgtechnodreams.salesassistindia.dailysales.model.SalesDetail;
import com.cgtechnodreams.salesassistindia.databasehelper.UserActivity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.AreaEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.BankEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.BrandEnity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CollectionEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CollectionImageEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CompanyEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CompetitorSalesModel;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DailySalesModel;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorDailySalesEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorPurchaseEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorRequestEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DistributorStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.FMCGSalesOrderEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.LocationModelEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivitiesImageEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivityCategoryEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.MarketActivityEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.OrderRemarksCurrentBalanceEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.OutletStatusEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductClosingEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductGroupModel;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductInventoryEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductSubGroupEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistDailySalesEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistDamageStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistRequestEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.StockistStockEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SurveyAnswerEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.SurveyEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.UserActivityEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.UserEntity;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorDailySales;
import com.cgtechnodreams.salesassistindia.dms.data.DistributorDataService;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.data.DistributorPurchaseDataService;
import com.cgtechnodreams.salesassistindia.dms.distributorpurchase.model.DistributorPurchase;
import com.cgtechnodreams.salesassistindia.dms.model.Distributor;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorResponse;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorSalesOrder;
import com.cgtechnodreams.salesassistindia.dms.model.DistributorStockModel;
import com.cgtechnodreams.salesassistindia.dms.model.NewDistributorRequest;
import com.cgtechnodreams.salesassistindia.dms.productclosing.data.ProductClosingDataService;
import com.cgtechnodreams.salesassistindia.dms.productclosing.model.ClosingStock;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.marketactivities.data.MarketActivitiesDataService;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivitiesImage;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivity;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivityCategory;
import com.cgtechnodreams.salesassistindia.marketactivities.model.MarketActivityCategoryResponse;
import com.cgtechnodreams.salesassistindia.outlet.data.StockistDataService;
import com.cgtechnodreams.salesassistindia.outlet.model.NewStockistRequest;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.outlet.model.StockistResponse;
import com.cgtechnodreams.salesassistindia.outlet.model.StockistStatus;
import com.cgtechnodreams.salesassistindia.photo.PhotoUploadDataService;
import com.cgtechnodreams.salesassistindia.photo.model.Photo;
import com.cgtechnodreams.salesassistindia.photo.model.PhotoEntity;
import com.cgtechnodreams.salesassistindia.photo.model.PhotoModel;
import com.cgtechnodreams.salesassistindia.product.dataservice.ProductDataService;
import com.cgtechnodreams.salesassistindia.product.model.Brand;
import com.cgtechnodreams.salesassistindia.product.model.BrandResponse;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.product.model.ProductGroup;
import com.cgtechnodreams.salesassistindia.product.model.ProductGroupResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductInventory;
import com.cgtechnodreams.salesassistindia.product.model.ProductInventoryResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductResponse;
import com.cgtechnodreams.salesassistindia.product.model.ProductSubGroup;
import com.cgtechnodreams.salesassistindia.product.model.ProductSubGroupResponse;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.data.AreaDataService;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.data.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.Area;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.AreaResponse;
import com.cgtechnodreams.salesassistindia.salesorder.dataservice.CompanyDataService;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionDetailModel;
import com.cgtechnodreams.salesassistindia.salesorder.model.CollectionImages;
import com.cgtechnodreams.salesassistindia.salesorder.model.Company;
import com.cgtechnodreams.salesassistindia.salesorder.model.CompanyResponse;
import com.cgtechnodreams.salesassistindia.salesorder.model.FMCGSalesOrder;
import com.cgtechnodreams.salesassistindia.salesorder.model.SalesOrderWithRemarks;
import com.cgtechnodreams.salesassistindia.salesorder.model.StockistDailySales;
import com.cgtechnodreams.salesassistindia.stock.model.StockistStockModel;
import com.cgtechnodreams.salesassistindia.survey.SurveyDataService;
import com.cgtechnodreams.salesassistindia.survey.SurveyResponse;
import com.cgtechnodreams.salesassistindia.survey.model.AnswerModel;
import com.cgtechnodreams.salesassistindia.survey.model.SurveyModel;
import com.cgtechnodreams.salesassistindia.user.UserDataService;
import com.cgtechnodreams.salesassistindia.user.model.User;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class Sync {
    ProgressDialog dialog;
    PrefManager prefManager;
    Context mContext;
    String role;
    boolean isSingleCall = true;
    private ArrayList<String> str = null;

    public Sync(Context mContext, String role, boolean isSingleCall) {
        this.mContext = mContext;
        this.prefManager = new PrefManager(mContext);
        this.role = role;
        this.isSingleCall = isSingleCall;
    }

    public Sync(Context mContext) {
        this.mContext = mContext;
        this.prefManager = new PrefManager(mContext);

    }

    public void downloadAll() {
        RxApiService backendApi = RxApi.getService(mContext);
        Observable<ProductResponse> observable1 = RxApi.getService(mContext).product(prefManager.getAuthKey());
        Observable<ProductGroupResponse> observable2 = RxApi.getService(mContext).productGroup(prefManager.getAuthKey());
        Observable<BrandResponse> observable3 = RxApi.getService(mContext).brand(prefManager.getAuthKey());
        List<Observable<?>> requests = new ArrayList<>();

        requests.add(observable1);
        requests.add(observable2);
        requests.add(observable3);

        // Zip all requests with the Function, which will receive the results.
        Observable.zip(requests, new Function<Object[], Object>() {
            @Override
            public Object apply(Object[] objects) throws Exception {
                Log.i("Class Name", objects.getClass().getSimpleName());
                // Objects[] is an array of combined results of completed requests

                // do something with those results and emit new event
                return new Object();
            }
        })
                // After all requests had been performed the next observer will receive the Object, returned from Function
                .subscribe(
                        // Will be triggered if all requests will end successfully (4xx and 5xx also are successful requests too)
                        new Consumer<Object>() {
                            @Override
                            public void accept(Object o) throws Exception {
                                Log.i("Object", o.getClass().getSimpleName());
                                //Do something on successful completion of all requests
                            }
                        },

                        // Will be triggered if any error during requests will happen
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable e) throws Exception {
                                //Do something on error completion of requests
                            }
                        }
                );
    }


    public void postDistributorPurchase(){
        ArrayList<DistributorPurchase> distributorPurchases= getDistributorPurchase();
        if(distributorPurchases.size()>0){
            uploadDistributorPurchase(distributorPurchases);
        }else{
            Toast.makeText(mContext,"No distributor purchase found to upload", Toast.LENGTH_SHORT).show();
            if(!isSingleCall)
                postClosingStock();
        }
    }
    private ArrayList<DistributorPurchase> getDistributorPurchase() {

        ArrayList<DistributorPurchase> distributorPurchases = new ArrayList<>();
        DistributorPurchaseEntity entity = new DistributorPurchaseEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray purchaseArray = null;
      //  if(selectedOutletId == 0){
            purchaseArray = (JSONArray) entity.find(null);
//        }else{
//            purchaseArray = (JSONArray) entity.findByOutletId(selectedOutletId);
//        }
        try {
            distributorPurchases = objectMapper.readValue(String.valueOf(purchaseArray), new TypeReference<ArrayList<DistributorPurchase>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  distributorPurchases;
    }
    public void uploadDistributorPurchase( ArrayList<DistributorPurchase> distributorPurchases) {

        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Uploading distributor purchase...");
        dialog.setCancelable(false);
        dialog.show();

        DistributorPurchaseDataService service = new DistributorPurchaseDataService(mContext);
        service.uploadDistributorPurchase(distributorPurchases, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {
                    Toasty.error(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                } else {
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    DistributorPurchaseEntity entity = new DistributorPurchaseEntity(mContext);
                    entity.delete(null);

                }
                if (!isSingleCall) {
                    postClosingStock();
                    //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();
                }
            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (!isSingleCall) {
                    postClosingStock();

                }

            }
        });
    }


    private ArrayList<ClosingStock> getClosingList() {

        ArrayList<ClosingStock> openingStocks = new ArrayList<>();
        ProductClosingEntity productClosingEntity = new ProductClosingEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray stockArray = null;
     //   if(selectedOutletId == 0){
            stockArray = (JSONArray) productClosingEntity.find(null);
//        }else{
//            stockArray = (JSONArray) productClosingEntity.findByOutletId(selectedOutletId);
//        }
        try {
            openingStocks = objectMapper.readValue(String.valueOf(stockArray), new TypeReference<ArrayList<ClosingStock>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  openingStocks;
    }

    public void postClosingStock() {
        ArrayList<ClosingStock> closingStocks = getClosingList(); // 0 means find all
        if(closingStocks.size()>0){
            uploadClosingStock(closingStocks);
        }else{
            Toast.makeText(mContext, "No record of closing stock found to upload", Toast.LENGTH_SHORT).show();
            if(!isSingleCall){
                // postClosingStock();
            }

        }
    }
    private void uploadClosingStock(ArrayList<ClosingStock> closingStocks) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Uploading closing stock...");
        dialog.setCancelable(false);
        dialog.show();
        ProductClosingDataService service = new ProductClosingDataService(mContext);
        service.uploadClosingStock(closingStocks, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {
                    Toasty.error(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                } else {
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    ProductClosingEntity productOpeningEntity = new ProductClosingEntity(mContext);
                    productOpeningEntity.delete(null);
                }
            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void getCompany() {
        ProgressDialog dialog;
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Company List...");
            dialog.setCancelable(false);
            dialog.show();

            CompanyDataService companyDataService = new CompanyDataService(mContext);
            companyDataService.getCompanyList(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    CompanyResponse companyResponse = (CompanyResponse) response;
                    if (companyResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Downloading Company List Failed", companyResponse.getMessage());
                        Toasty.error(mContext, companyResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, companyResponse.getStatusCode(), companyResponse.getMessage());
                    } else {
                        Toasty.success(mContext, companyResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveCompanyOffline(companyResponse.getData());
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void saveCompanyOffline(ArrayList<Company> companies) {
        final CompanyEntity companyEntity = new CompanyEntity(mContext);
        companyEntity.delete(null);
        companyEntity.insert(companies);
    }

    public void getBrand() {
        ProgressDialog dialog;
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Brand...");
            dialog.setCancelable(false);
            dialog.show();

            ProductDataService productDataService = new ProductDataService(mContext);
            productDataService.getBrand(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    BrandResponse brandResponse = (BrandResponse) response;
                    if (brandResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Brand Download Failed", brandResponse.getMessage());
                        Toasty.error(mContext, brandResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, brandResponse.getStatusCode(), brandResponse.getMessage());
                    } else {
                        Toasty.success(mContext, brandResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveBrandOffline(brandResponse.getBrands());
                    }
                    if (!isSingleCall) {
                        getBankList();
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void saveBrandOffline(ArrayList<Brand> brands) {
        final BrandEnity brandEnity = new BrandEnity(mContext);
        brandEnity.delete(null);
        brandEnity.insert(brands);
    }

    public void getProduct() {

        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Product...");
            dialog.setCancelable(false);
            dialog.show();

            ProductDataService productDataService = new ProductDataService(mContext);
            productDataService.getProduct(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();

                    ProductResponse productResponse = (ProductResponse) response;

                    if (productResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Product Download Failed", productResponse.getMessage());
                        Toasty.error(mContext, productResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, productResponse.getStatusCode(), productResponse.getMessage());
                    } else {
                        Toasty.success(mContext, productResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveOfflineProduct(productResponse.getProducts());

                    }
                    if (!isSingleCall) {
                        getBrand();
                    }
                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!! Failed to download Product", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void getProductInventory() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Product Group...");
            dialog.setCancelable(false);
            dialog.show();
            ProductDataService productDataService = new ProductDataService(mContext);
            productDataService.getProductInventory(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    ProductInventoryResponse productInventoryResponse = (ProductInventoryResponse) response;
                    if (productInventoryResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Product Inventory Download Failed", productInventoryResponse.getMessage());
                        Toasty.error(mContext, productInventoryResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, productInventoryResponse.getStatusCode(), productInventoryResponse.getMessage());
                    } else {
                        Toasty.success(mContext, productInventoryResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveProductInventoryOffline(productInventoryResponse.getProductInventories());
                    }


                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }

    }

    private void saveProductInventoryOffline(ArrayList<ProductInventory> productInventories) {
        final ProductInventoryEntity inventoryEntity = new ProductInventoryEntity(mContext);
        inventoryEntity.delete(null);
        inventoryEntity.insert(productInventories);
    }
    public void saveOfflineProduct(ArrayList<Product> products) {
        final ProductEntity productEntity = new ProductEntity(mContext);
        productEntity.delete(null);
        productEntity.insert(products);
    }

    public void getCompetitorGroup() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Product Group...");
            dialog.setCancelable(false);
            dialog.show();
            ProductDataService productDataService = new ProductDataService(mContext);
            productDataService.getCompetitorGroup(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    ProductGroupResponse productGroupResponse = (ProductGroupResponse) response;
                    if (productGroupResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Product Group Download Failed", productGroupResponse.getMessage());
                        Toasty.error(mContext, productGroupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, productGroupResponse.getStatusCode(), productGroupResponse.getMessage());
                    } else {
                        Toasty.success(mContext, productGroupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveProductGroupOffline(productGroupResponse.getProductGroups());
                    }

                    if (!isSingleCall) {
                        getSubGroup();
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }

    }

    public void getProductGroup() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Product Group...");
            dialog.setCancelable(false);
            dialog.show();
            ProductDataService productDataService = new ProductDataService(mContext);
            productDataService.getProductGroup(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    ProductGroupResponse productGroupResponse = (ProductGroupResponse) response;
                    if (productGroupResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Product Group Download Failed", productGroupResponse.getMessage());
                        Toasty.error(mContext, productGroupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, productGroupResponse.getStatusCode(), productGroupResponse.getMessage());
                    } else {
                        Toasty.success(mContext, productGroupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveProductGroupOffline(productGroupResponse.getProductGroups());
                    }

                    if (!isSingleCall) {
                        getSubGroup();
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }

    }

    public void getSubGroup() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Product Sub Group...");
            dialog.setCancelable(false);
            dialog.show();
            ProductDataService productDataService = new ProductDataService(mContext);
            productDataService.getSubGroup(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    ProductSubGroupResponse productSubGroupResponse = (ProductSubGroupResponse) response;
                    if (productSubGroupResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Product Sub Group Download Failed", productSubGroupResponse.getMessage());
                        Toasty.error(mContext, productSubGroupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, productSubGroupResponse.getStatusCode(), productSubGroupResponse.getMessage());
                    } else {
                        Toasty.success(mContext, productSubGroupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveSubProductGroupOffline(productSubGroupResponse.getData());
                    }

                    if (!isSingleCall) {
                        getProduct();
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void saveSubProductGroupOffline(ArrayList<ProductSubGroup> subGroups) {
        final ProductSubGroupEntity subGroupEntity = new ProductSubGroupEntity(mContext);
        subGroupEntity.delete(null);
        subGroupEntity.insert(subGroups);
    }

    public void getBankList() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Banks List...");
            dialog.setCancelable(false);
            dialog.show();
            BankDataService bankDataService = new BankDataService(mContext);
            bankDataService.getBank(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    BankResponse bankResponse = (BankResponse) response;
                    if (bankResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Bank Download Failed", bankResponse.getMessage());
                        Toasty.error(mContext, bankResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, bankResponse.getStatusCode(), bankResponse.getMessage());
                    } else {
                        Toasty.success(mContext, bankResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveBankListOffline(bankResponse.getBanks());
                    }
                    if (!isSingleCall) {
                        getMarketActivityCategoryList();
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    if (!isSingleCall) {
                        getMarketActivityCategoryList();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void saveBankListOffline(ArrayList<Bank> banks) {
        final BankEntity bankEntity = new BankEntity(mContext);
        bankEntity.delete(null);
        bankEntity.insert(banks);
    }

    public void getMarketActivityCategoryList() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Market Category List...");
            dialog.setCancelable(false);
            dialog.show();
            MarketActivitiesDataService dataService = new MarketActivitiesDataService(mContext);
            dataService.getMarketActivityCategory(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    MarketActivityCategoryResponse baseResponse = (MarketActivityCategoryResponse) response;
                    if (baseResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Market Activity Category Download Failed", baseResponse.getMessage());
                        Toasty.error(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                    } else {
                        Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveActivityCategory(baseResponse.getActivityCategory());
                    }

                    if (!isSingleCall) {
                        getCompany();
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void saveActivityCategory(ArrayList<MarketActivityCategory> categories) {
        MarketActivityCategoryEntity categoryEntity = new MarketActivityCategoryEntity(mContext);
        categoryEntity.delete(null);
        categoryEntity.insert(categories);
    }

//
//    }


    public void saveProductGroupOffline(ArrayList<ProductGroup> productGroups) {
        final ProductGroupModel productGroup = new ProductGroupModel(mContext);
        productGroup.delete(null);
        productGroup.insert(productGroups);
    }

    public void uploadLocation() {
        if (Utils.isConnectionAvailable(mContext)) {
            LocationModelEntity locationModelEntity = new LocationModelEntity(mContext);
            final JSONArray locationModelArray = (JSONArray) locationModelEntity.find(null);
            ObjectMapper objectMapper = new ObjectMapper();
            TypeReference ref = new TypeReference<List<LocationModel>>() {
            };
            ArrayList<LocationModel> location = new ArrayList<>();

            try {
                location = objectMapper.readValue(locationModelArray.toString(), ref);
                if (location.size() > 0) {
                    postMovementGPS(location);
                } else {
                    Toasty.info(mContext, "All location have been synced!!", Toast.LENGTH_SHORT, true).show();
                    uploadUserActivity();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toasty.info(mContext, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadDailySales() {

        ArrayList<DailySales> salesArrayList = readDailySales();
        if (salesArrayList != null) {
            postDailySales(salesArrayList);
        } else {
            Toasty.info(mContext, "No sales recorded to sync!!", Toast.LENGTH_SHORT, true).show();
            uploadCompetitorSales();
        }
    }

    public void uploadCompetitorSales() {

        ArrayList<CompetitorSales> competitorSalesDetails = readCompetitorSales();
        if (competitorSalesDetails != null) {
            postCompetitorSales(competitorSalesDetails);
        } else {
            Toasty.info(mContext, "No competitor sales to sync!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void uploadCollectionDetail() {
        ArrayList<CollectionDetailModel> collectionDetail = readCollectionDetail();
        if (collectionDetail == null) {
            Toasty.info(mContext, "No collection record found to sync!!", Toast.LENGTH_SHORT, true).show();
            if (!isSingleCall) {
                uploadCollectionImage();

            }
        } else {
            postCollectionDetail(collectionDetail);
        }

    }

    public void uploadSalesOrder() {//SaleMan Order
        ArrayList<SalesOrderWithRemarks> salesOrders = readSalesOrderWithRemarks();
//        ArrayList<FMCGSalesOrder> salesOrders = readSalesOrder();
        if (salesOrders == null) {
            if (!isSingleCall) {
                uploadCollectionDetail();
            }

            Toasty.info(mContext, "No sales order to sync!!", Toast.LENGTH_SHORT, true).show();

        } else {
            postSalesOrder(salesOrders);
        }

    }

    public void uploadDistributorSalesOrder() {
        ArrayList<DistributorSalesOrder> salesOrders = readDistributorSalesOrders();
        if (salesOrders == null) {
            if (!isSingleCall) {
               uploadDistributorStock();
            }

            Toasty.info(mContext, "No sales order to sync!!", Toast.LENGTH_SHORT, true).show();

        } else {
            postDistributorSalesOrder(salesOrders);

        }

    }

    public void uploadStockistDailySales() {
        ArrayList<StockistDailySales> dailySalesArrayList = readStockistDailySales();
        if (dailySalesArrayList != null) {
            postStockistDailySales(dailySalesArrayList);
        } else {
            Toasty.info(mContext, "No distributor sales recorded to sync!!", Toast.LENGTH_SHORT, true).show();
            if (!isSingleCall) {
                uploadStockistRequest();
            }
        }

    }


    public void uploadDistributorDailySales() {
        ArrayList<DistributorDailySales> dailySalesArrayList = readDistributorDailySales();
        if (dailySalesArrayList != null) {
            postDistributorDailySales(dailySalesArrayList);
        } else {
            Toasty.info(mContext, "No distributor sales recorded to sync!!", Toast.LENGTH_SHORT, true).show();
            if (!isSingleCall) {
                uploadDistributorRequest();
            }
        }

    }

    private ArrayList<FMCGSalesOrder> readSalesOrder() {
        FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) salesOrderEntity.find(null);
        ArrayList<FMCGSalesOrder> salesOrders = null;
        if (array.length() > 0) {
            try {
                salesOrders = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<FMCGSalesOrder>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return salesOrders;
    }

    private ArrayList<SalesOrderWithRemarks> readSalesOrderWithRemarks() {
        FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) salesOrderEntity.findWithRemarks(null);
        Log.i("Join Result", array.toString());
        ArrayList<SalesOrderWithRemarks> salesOrders = null;
        if (array.length() > 0) {
            try {
                salesOrders = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<SalesOrderWithRemarks>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return salesOrders;

    }

    private ArrayList<DistributorSalesOrder> readDistributorSalesOrders() {
        DistributorSalesOrderEntity salesOrderEntity = new DistributorSalesOrderEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) salesOrderEntity.find(null);

        ArrayList<DistributorSalesOrder> salesOrders = null;
        if (array.length() > 0) {
            try {
                salesOrders = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<DistributorSalesOrder>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return salesOrders;

    }

    public void uploadStockistStock() {

        ArrayList<StockistStockModel> outletStock = readStockistStock();
        if (outletStock.size() != 0) {
            postStockistStock(outletStock);
        } else {
            Toasty.info(mContext, "Stockist stock hasn't been logged yet!!", Toast.LENGTH_SHORT, true).show();

            if (!isSingleCall) {
                uploadStockistDamageStock();
            }
        }
    }

    private ArrayList<StockistStockModel> readStockistStock() {
        StockistStockEntity stockistStockEntity = new StockistStockEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) stockistStockEntity.find(null);
        ArrayList<StockistStockModel> stockistStockModels = null;

        try {
            stockistStockModels = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<StockistStockModel>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stockistStockModels;
    }

    private void postStockistStock(ArrayList<StockistStockModel> stockistStock) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        // LogUtils.setUserActivity(mContext, "Stockist Damage Stock", "Upload Damage Stock Started");
        StockistDataService stockistDataService = new StockistDataService(mContext);
        stockistDataService.postOutletStock(prefManager.getAuthKey(), stockistStock, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Stockist Damage Stock", "Stockist Damage Stock upload Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    StockistStockEntity outletStock = new StockistStockEntity(mContext);
                    outletStock.delete(null);
                    LogUtils.setUserActivity(mContext, "Stockist Damage Stock", "Stockist Damage Stock Upload Successful ");
                }
                if (!isSingleCall) {
                    uploadStockistDamageStock();
                    //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void uploadStockistDamageStock() {

        ArrayList<StockistStockModel> outletStock = readStockistDamageStock();
        if (outletStock.size() != 0) {
            postStockistDamageStock(outletStock);
        } else {
            Toasty.info(mContext, "Stockist damage stock hasn't been logged yet!!", Toast.LENGTH_SHORT, true).show();

            if (!isSingleCall) {
                uploadPhoto();
            }
            //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();

        }
    }

    private ArrayList<StockistStockModel> readStockistDamageStock() {
        StockistDamageStockEntity distributorStockEntity = new StockistDamageStockEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) distributorStockEntity.find(null);
        ArrayList<StockistStockModel> stockistStockModels = null;

        try {
            stockistStockModels = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<StockistStockModel>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stockistStockModels;
    }

    private void postStockistDamageStock(ArrayList<StockistStockModel> outletStock) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        LogUtils.setUserActivity(mContext, "Stockist Stock", "Upload OutletStock Started");
        StockistDataService stockistDataService = new StockistDataService(mContext);
        stockistDataService.postStockistDamageStock(prefManager.getAuthKey(), outletStock, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Stockist Stock", "Stockist Stock upload Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Log.i("test", baseResponse.getStatusCode() + ": " + baseResponse.getMessage());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case

                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    StockistDamageStockEntity damageStockEntity = new StockistDamageStockEntity(mContext);
                    damageStockEntity.delete(null);
                    LogUtils.setUserActivity(mContext, "Stockist Stock", "Stockist Stock Upload Successful ");
                }
                if (!isSingleCall) {
                    uploadPhoto();
                    //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }
    //FMCGSalesOrder is repalce with SalesOrderWithRemarks
    private void postSalesOrder(ArrayList<SalesOrderWithRemarks> salesOrders) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        LogUtils.setUserActivity(mContext, "Salesman Sales Order", "Upload Salesman SalesOrder Started");
        SalesOrderDataService salesOrderDataService = new SalesOrderDataService(mContext);
        salesOrderDataService.postSalesOrder(prefManager.getAuthKey(), salesOrders, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Sales Order", "Upload Sales Order Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case
                    LogUtils.setUserActivity(mContext, "Sales Order", "Upload Sales Order Successful ");
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(mContext);
                    salesOrderEntity.delete(null);
                    OrderRemarksCurrentBalanceEntity entity = new OrderRemarksCurrentBalanceEntity(mContext);
                    entity.delete(null);
                    if (!isSingleCall) {
                        uploadCollectionDetail();
                        //  Toasty.makeText(mContext, "No sales order to sync!!", Toast.LENGTH_SHORT,true).show();
                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }


    public void uploadDistributorDamageStock() {

        ArrayList<DistributorStockModel> outletStock = readDistributorDamageStock();
        if (outletStock.size() != 0) {
            postDistributorDamageStock(outletStock);
        } else {
            Toasty.info(mContext, "Stockist damage stock hasn't been logged yet!!", Toast.LENGTH_SHORT, true).show();

            if (!isSingleCall) {
                uploadDistributorRequest();

            }
            //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();

        }
    }

    private ArrayList<DistributorStockModel> readDistributorDamageStock() {
        DistributorDamageStockEntity distributorStockEntity = new DistributorDamageStockEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) distributorStockEntity.find(null);
        ArrayList<DistributorStockModel> stockistStockModels = null;

        try {
            stockistStockModels = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<DistributorStockModel>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stockistStockModels;
    }

    private void postDistributorDamageStock(ArrayList<DistributorStockModel> outletStock) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        LogUtils.setUserActivity(mContext, "Distributor Stock", "Upload Distributor Stock Started");
        DistributorDataService distributorDataService = new DistributorDataService(mContext);
        distributorDataService.postDamageDistributorStock(prefManager.getAuthKey(), outletStock, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "DistributorDamage Stock", "Distributor Damage Stock upload Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Log.i("test", baseResponse.getStatusCode() + ": " + baseResponse.getMessage());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case

                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    DistributorDamageStockEntity damageStockEntity = new DistributorDamageStockEntity(mContext);
                    damageStockEntity.delete(null);
                    LogUtils.setUserActivity(mContext, "Distributor Damage Stock", "Distributor Damage Stock Upload Successful ");
                }
                if (!isSingleCall) {
                    uploadDistributorRequest();

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void postDistributorSalesOrder(ArrayList<DistributorSalesOrder> salesOrders) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        LogUtils.setUserActivity(mContext, "Distributor Sales Order", "Upload Distributor SalesOrder Started");
        SalesOrderDataService salesOrderDataService = new SalesOrderDataService(mContext);
        salesOrderDataService.postDistributorSalesOrder(prefManager.getAuthKey(), salesOrders, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Distributor Sales Order", "Upload Distributor Sales Order Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case
                    LogUtils.setUserActivity(mContext, "Distributor Sales Order", "Upload Distributor Sales Order Successful ");
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    DistributorSalesOrderEntity salesOrderEntity = new DistributorSalesOrderEntity(mContext);
                    salesOrderEntity.delete(null);

                    if (!isSingleCall) {
                        uploadDistributorStock();


                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void uploadDistributorStock() {

        ArrayList<DistributorStockModel> outletStock = readDistributorStock();
        if (outletStock.size() != 0) {
            postDistributorStock(outletStock);
        } else {
            Toasty.info(mContext, "Distributor stock hasn't been logged yet!!", Toast.LENGTH_SHORT, true).show();
            if (!isSingleCall) {
                uploadDistributorDamageStock();
            }

        }
    }

    private ArrayList<DistributorStockModel> readDistributorStock() {
        DistributorStockEntity stockistStockEntity = new DistributorStockEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) stockistStockEntity.find(null);
        ArrayList<DistributorStockModel> distributorStockModel = null;

        try {
            distributorStockModel = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<DistributorStockModel>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return distributorStockModel;
    }

    private void postDistributorStock(ArrayList<DistributorStockModel> distributorStock) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        // LogUtils.setUserActivity(mContext, "Stockist Damage Stock", "Upload Damage Stock Started");
        DistributorDataService distributorDataService = new DistributorDataService(mContext);
        distributorDataService.postDistributorStock(prefManager.getAuthKey(), distributorStock, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Distributor Stock", "Stockist Stock upload Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    DistributorStockEntity outletStock = new DistributorStockEntity(mContext);
                    outletStock.delete(null);
                    LogUtils.setUserActivity(mContext, "Distributor Stock", "Distributor Stock Upload Successful ");
                }
                if (!isSingleCall) {
                    uploadDistributorDamageStock();

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }


    private void postCollectionDetail(ArrayList<CollectionDetailModel> collections) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        LogUtils.setUserActivity(mContext, "Salesman Sales Order", "Upload Salesman SalesOrder Started");
        SalesOrderDataService salesOrderDataService = new SalesOrderDataService(mContext);
        salesOrderDataService.postCollection(prefManager.getAuthKey(), collections, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Collection", "Upload Collection Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case
                    LogUtils.setUserActivity(mContext, "Collection", "Upload Collection Successful ");
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    CollectionEntity collectionEntity = new CollectionEntity(mContext);
                    collectionEntity.delete(null);
                    if (!isSingleCall) {
                        uploadCollectionImage();


                        //  Toasty.makeText(mContext, "No sales order to sync!!", Toast.LENGTH_SHORT,true).show();
                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private ArrayList<CollectionDetailModel> readCollectionDetail() {
        CollectionEntity collectionEntity = new CollectionEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) collectionEntity.find(null);
        ArrayList<CollectionDetailModel> collectionDetails = null;
        if (array.length() > 0) {
            try {
                collectionDetails = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<CollectionDetailModel>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return collectionDetails;
    }

    public void downloadStockist() {
        if (Utils.isConnectionAvailable(mContext)) {

            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Stockist...");
            dialog.setCancelable(false);
            dialog.show();

            StockistDataService stockistDataService = new StockistDataService(mContext);
            stockistDataService.getStockist(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    StockistResponse stockistResponse = (StockistResponse) response;
                    if (stockistResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Download Stockist", "Download Stockist Failed. Error " + stockistResponse.getMessage() + " Error Code:" + stockistResponse.getStatusCode());
                        Toasty.error(mContext, stockistResponse.getStatusCode() + ": " + stockistResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, stockistResponse.getStatusCode(), stockistResponse.getMessage());
                    } else {
                        Toasty.success(mContext, stockistResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveStockistOffline(stockistResponse.getStockists());
                    }

                    if (!isSingleCall) {
                        downloadDistributor();

                        //getProductGroup();
                    }


                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void saveUserOffline(ArrayList<User> users) {
        final UserEntity user = new UserEntity(mContext);
        user.delete(null);
        user.insert(users);
    }

    public void uploadSurvey() {
        SurveyAnswerEntity surveyAnswerEntity = new SurveyAnswerEntity(mContext);
        JSONArray surveyAnswerArray = (JSONArray) surveyAnswerEntity.find(null);

        ArrayList<AnswerModel> answerModels = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        TypeReference ref = new TypeReference<List<AnswerModel>>() {
        };

        try {
            answerModels = objectMapper.readValue(surveyAnswerArray.toString(), ref);
            postAnswers(answerModels);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void postAnswers(ArrayList<AnswerModel> answerModels) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Uploading Survey...");
        dialog.setCancelable(false);
        dialog.show();
        SurveyDataService surveyDataService = new SurveyDataService(mContext);
        surveyDataService.postSurvey(prefManager.getAuthKey(), answerModels, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Survey Answer", "Survey Answer Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case
                    SurveyAnswerEntity surveyAnswerEntity = new SurveyAnswerEntity(mContext);
                    surveyAnswerEntity.delete(null);
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void downloadSurvey() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Survey List...");
            dialog.setCancelable(false);
            dialog.show();
            SurveyDataService surveyDataService = new SurveyDataService(mContext);
            surveyDataService.getSurvey(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    SurveyResponse surveyResponse = (SurveyResponse) response;
                    if (surveyResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Download Survey",
                                "Download Survey Failed. Error " + surveyResponse.getMessage() + " Error Code:" + surveyResponse.getStatusCode());
                        Toasty.error(mContext, surveyResponse.getStatusCode() + ": " + surveyResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, surveyResponse.getStatusCode(), surveyResponse.getMessage());
                    } else {
                        Toasty.success(mContext, surveyResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveSurveyOffline(surveyResponse.getData());
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }


    public void uploadUserActivity() {
        UserActivityEntity activityEntity = new UserActivityEntity(mContext);
        JSONArray activitiesArray = (JSONArray) activityEntity.find(null);

        ArrayList<UserActivity> activityModel = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        TypeReference ref = new TypeReference<List<UserActivity>>() {
        };

        try {
            activityModel = objectMapper.readValue(activitiesArray.toString(), ref);
            if (activityModel.size() == 0) {

                // Toasty.info(mContext, "No User Activity to Sync", Toast.LENGTH_SHORT, true).show();
            } else {
                postUserActivity(activityModel);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void uploadPhoto() {
        ProgressDialog dialog;
        if (Utils.isConnectionAvailable(mContext)) {
            PhotoEntity photo = new PhotoEntity(mContext);
            JSONArray jsonArray = (JSONArray) photo.find(null);
            ObjectMapper objectMapper = new ObjectMapper();
            TypeReference ref = new TypeReference<List<PhotoModel>>() {
            };
            ArrayList<PhotoModel> imageList = new ArrayList<>();
            try {
                imageList = objectMapper.readValue(jsonArray.toString(), ref);

            } catch (IOException e) {
                e.printStackTrace();
            }

            RequestBody description = createPartFromString(jsonArray.toString());
            List<MultipartBody.Part> parts = new ArrayList<>();

            if (imageList.size() > 0) {
                for (int i = 0; i < imageList.size(); i++) {
                    parts.add(prepareFilePart("image" + i, Uri.parse(imageList.get(i).getFileUri()), imageList.get(i).getPhotoPath()));
                }
                dialog = new ProgressDialog(mContext);
                dialog.setMessage("Uploading Images...");
                dialog.setCancelable(false);
                dialog.show();
                RequestBody size = createPartFromString("" + parts.size());
                PhotoUploadDataService service = new PhotoUploadDataService(mContext);
                service.uploadImages(prefManager.getAuthKey(), description, size, parts, new OnSuccess() {
                    @Override
                    public void onSuccess(Object response) {
                        if (dialog.isShowing())
                            dialog.dismiss();

                        photo.delete(null);
                        Toasty.success(mContext, "Photo uploaded successfully!!", Toast.LENGTH_SHORT, true).show();
                        LogUtils.setUserActivity(mContext, "Photo", "Photo upload successful");
                        if (!isSingleCall) {
                            uploadDistributorSalesOrder();

                        }

                    }
                }, new OnFailedListener() {
                    @Override
                    public void onFailed(ErrorModel reason) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        LogUtils.setUserActivity(mContext, "Photo", "Photo upload failed" + reason.getErrors());
                    }
                });
            } else {
                Toasty.info(mContext, "No image found to upload. Please take photo and save!", Toast.LENGTH_SHORT, true).show();
                if (!isSingleCall) {
                    uploadDistributorSalesOrder();
                }
            }

        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }

    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create( descriptionString,okhttp3.MultipartBody.FORM);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri, String path) {
        File file = new File(path);
        RequestBody requestFile = RequestBody.create( file,MediaType.parse("image/*"));

        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public void postUserActivity(ArrayList<UserActivity> activityModel) {
        if (!isSingleCall) {
//            ProgressDialog dialog = new ProgressDialog(mContext);
//            dialog.setMessage("Processing your request...");
//            dialog.setCancelable(false);
//            dialog.show();
        }

        UserDataService dataService = new UserDataService(mContext);
        dataService.postUserActivity(prefManager.getAuthKey(), activityModel, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (!isSingleCall) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
                }
                BaseResponse baseResponse = (BaseResponse) response;

                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Upload UserActivity",
                            "Upload UserActivity Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case
                    //  Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    UserActivityEntity userActivityEntity = new UserActivityEntity(mContext);
                    userActivityEntity.clearDb();

                }


            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {


            }
        });
    }

    public void uploadStorePhoto(ArrayList<Photo> photoList) {
    }

    public void postCompetitorSales(ArrayList<CompetitorSales> competitorSalesDetails) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        LogUtils.setUserActivity(mContext, "Competitor Sales", "Upload Competitor Sales Started");
        DailySalesDataService competitorSalesDataService = new DailySalesDataService(mContext);
        competitorSalesDataService.postCompetitorSales(prefManager.getAuthKey(), competitorSalesDetails, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Upload Competitor Sales",
                            "Upload Competitor Sales Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    if (baseResponse.getStatusCode() == APIConstants.ERROR_UNAUTHORIZED) {
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PrefManager preferenceManager = new PrefManager(mContext);
                        preferenceManager.clear();
                        mContext.startActivity(intent);
                        ((Activity) mContext).finish();
                    }
                } else { //success case
                    LogUtils.setUserActivity(mContext, "Competitor Sales", "Upload Competitor Sales Successful ");
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    CompetitorSalesModel competitorSalesModel = new CompetitorSalesModel(mContext);
                    competitorSalesModel.delete(null);

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public ArrayList<CompetitorSales> readCompetitorSales() {
        CompetitorSalesModel competitorSalesModel = new CompetitorSalesModel(mContext);
        JSONArray competitorSalesModelArray = (JSONArray) competitorSalesModel.find(null);
        if (competitorSalesModelArray.length() > 0) {
            ArrayList<CompetitorSales> salesList = new ArrayList<>();

            for (int i = 0; i < competitorSalesModelArray.length(); i++) {
                JSONObject jsonobject = null;
                try {
                    CompetitorSales dailySales = new CompetitorSales();
                    jsonobject = competitorSalesModelArray.getJSONObject(i);
                    dailySales.setBrandId(jsonobject.getInt("brand_id"));
                    dailySales.setBrandName(jsonobject.getString("brand_name"));
                    dailySales.setDate(jsonobject.getString("date"));
                    dailySales.setRemarks(jsonobject.getString("remarks"));
                    dailySales.setTotalSales(jsonobject.getString("total_sales"));

                    String details = jsonobject.getString("sales_detail");
                    JSONArray salesJsonArray = new JSONArray(details);
                    ArrayList<CompetitorSalesDetailModel> salesDetailArrayList = new ArrayList<CompetitorSalesDetailModel>();
                    for (int j = 0; j < salesJsonArray.length(); j++) {
                        JSONObject jsonObject = salesJsonArray.getJSONObject(j);
                        CompetitorSalesDetailModel salesDetail = new CompetitorSalesDetailModel();
                        salesDetail.setId(jsonObject.getInt("id"));
                        salesDetail.setQty(jsonObject.getInt("qty"));
                        salesDetail.setName(jsonObject.getString("name"));
                        salesDetailArrayList.add(salesDetail);

                    }
                    dailySales.setSalesDetail(salesDetailArrayList);
                    salesList.add(dailySales);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return salesList;
        }
        return null;
    }

    public void postMovementGPS(ArrayList<LocationModel> locationList) {
        if (!isSingleCall) {
//            ProgressDialog dialog = new ProgressDialog(mContext);
//            dialog.setMessage("Uploading gps...");
//            dialog.setCancelable(false);
//            dialog.show();
        }

        // Toasty.info(mContext, "Background location update!", Toasty.LENGTH_LONG, true).show();
        LocationDataService locationDataService = new LocationDataService(mContext);
        locationDataService.movementGPS(prefManager.getAuthKey(), locationList, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (!isSingleCall) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
                }

                LocationResponse locationResponse = (LocationResponse) response;
                if (locationResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(mContext, locationResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    //   finish();
                } else { //success case
                    LocationModelEntity locationModelEntity = new LocationModelEntity(mContext);
                    locationModelEntity.clearDb();
                    uploadUserActivity();
                    // Toasty.success(mContext, locationResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (!isSingleCall) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
                }

            }
        });
    }

    public ArrayList<DailySales> readDailySales() {
        DailySalesModel dailySalesModel = new DailySalesModel(mContext);
        JSONArray dailySalesArray = (JSONArray) dailySalesModel.find(null);
        if (dailySalesArray.length() > 0) {
            ArrayList<DailySales> salesList = new ArrayList<>();

            for (int i = 0; i < dailySalesArray.length(); i++) {
                JSONObject jsonobject = null;
                try {
                    DailySales dailySales = new DailySales();
                    jsonobject = dailySalesArray.getJSONObject(i);
                    dailySales.setId(jsonobject.getInt("id"));
                    dailySales.setCustomerAddress(jsonobject.getString("customer_address"));
                    dailySales.setCustomerMobile(jsonobject.getString("customer_number"));
                    dailySales.setCustomerName(jsonobject.getString("customer_name"));
                    dailySales.setQty(jsonobject.getInt("quantity"));
                    dailySales.setDate(jsonobject.getString("date"));
                    String details = jsonobject.getString("detail");
                    JSONArray salesJsonArray = new JSONArray(details);
                    ArrayList<SalesDetail> salesDetailArrayList = new ArrayList<SalesDetail>();
                    for (int j = 0; j < salesJsonArray.length(); j++) {
                        JSONObject jsonObject = salesJsonArray.getJSONObject(j);
                        SalesDetail salesDetail = new SalesDetail();
                        salesDetail.setSellingPrice(jsonObject.getString("selling_price"));
                        salesDetail.setSerialNumber(jsonObject.getString("serial_number"));
                        salesDetail.setProductId(jsonObject.getString("product_id"));
                        salesDetail.setModelNumber(jsonObject.getString("model_number"));
                        salesDetailArrayList.add(salesDetail);

                    }
                    dailySales.setSalesDetail(salesDetailArrayList);
                    salesList.add(dailySales);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return salesList;

        } else {
            return null;
        }

    }

    public ArrayList<StockistDailySales> readStockistDailySales() {
        StockistDailySalesEntity dailySalesEntity = new StockistDailySalesEntity(mContext);
        JSONArray dailySalesArray = (JSONArray) dailySalesEntity.find(null);
        if (dailySalesArray.length() > 0) {
            ArrayList<StockistDailySales> salesList = new ArrayList<>();

            for (int i = 0; i < dailySalesArray.length(); i++) {
                JSONObject jsonobject = null;
                try {
                    StockistDailySales stockistDailySales = new StockistDailySales();
                    jsonobject = dailySalesArray.getJSONObject(i);
                    stockistDailySales.setProductId(jsonobject.getInt("product_id"));
                    stockistDailySales.setDivisionId(jsonobject.getInt("division_id"));
                    stockistDailySales.setDistributorId(jsonobject.getInt("distributor_id"));
                    stockistDailySales.setQuantity(jsonobject.getInt("quantity"));
                    stockistDailySales.setLatitude(jsonobject.getDouble("latitude"));
                    stockistDailySales.setLongitude(jsonobject.getDouble("longitude"));
                    stockistDailySales.setDate(jsonobject.getString("date_time"));
                    stockistDailySales.setMiti(jsonobject.getString("miti"));
                    stockistDailySales.setDistributorName(jsonobject.getString("distributor_name"));
                    stockistDailySales.setProductName(jsonobject.getString("product_name"));
                    stockistDailySales.setDivisionName(jsonobject.getString("division_name"));
                    salesList.add(stockistDailySales);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return salesList;

        } else {
            return null;
        }

    }

    public ArrayList<DistributorDailySales> readDistributorDailySales() {
        DistributorDailySalesEntity dailySalesEntity = new DistributorDailySalesEntity(mContext);
        JSONArray dailySalesArray = (JSONArray) dailySalesEntity.find(null);
        if (dailySalesArray.length() > 0) {
            ArrayList<DistributorDailySales> salesList = new ArrayList<>();

            for (int i = 0; i < dailySalesArray.length(); i++) {
                JSONObject jsonobject = null;
                try {
                    DistributorDailySales stockistDailySales = new DistributorDailySales();
                    jsonobject = dailySalesArray.getJSONObject(i);
                    stockistDailySales.setProductId(jsonobject.getInt("product_id"));
                    stockistDailySales.setDivisionId(jsonobject.getInt("division_id"));
                    stockistDailySales.setDistributorId(jsonobject.getInt("distributor_id"));
                    stockistDailySales.setQuantity(jsonobject.getInt("quantity"));
                    stockistDailySales.setLatitude(jsonobject.getDouble("latitude"));
                    stockistDailySales.setLongitude(jsonobject.getDouble("longitude"));
                    stockistDailySales.setDate(jsonobject.getString("date_time"));
                    stockistDailySales.setMiti(jsonobject.getString("miti"));
                    stockistDailySales.setDistributorName(jsonobject.getString("distributor_name"));
                    stockistDailySales.setProductName(jsonobject.getString("product_name"));
                    stockistDailySales.setDivisionName(jsonobject.getString("division_name"));
                    salesList.add(stockistDailySales);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return salesList;

        } else {
            return null;
        }

    }
    public void postDailySales(ArrayList<DailySales> dailySales) {
        LogUtils.setUserActivity(mContext, "Upload Daily Sales", "Upload Daily Sales Started");
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        DailySalesDataService dailySalesDataService = new DailySalesDataService(mContext);
        dailySalesDataService.postDailySales(prefManager.getAuthKey(), dailySales, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                DailySalesResponse dailySalesResponse = (DailySalesResponse) response;
                if (dailySalesResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Upload Daily Sales",
                            "Upload Daily Sales Failed. Error " + dailySalesResponse.getMessage() + " Error Code:" + dailySalesResponse.getStatusCode());
                    Toasty.error(mContext, dailySalesResponse.getStatusCode() + ": " + dailySalesResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, dailySalesResponse.getStatusCode(), dailySalesResponse.getMessage());
                } else { //success case
                    LogUtils.setUserActivity(mContext, "Upload Daily Sales", "Upload Daily Sales Completed");
                    DailySalesModel dailySalesModel = new DailySalesModel(mContext);
                    dailySalesModel.clearDb();
                    Toasty.success(mContext, dailySalesResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }


            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void postStockistDailySales(ArrayList<StockistDailySales> dailySales) {
        LogUtils.setUserActivity(mContext, "Upload Stockist Daily Sales", "Upload Daily Sales Started");
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        SalesOrderDataService dailySalesDataService = new SalesOrderDataService(mContext);
        dailySalesDataService.postDistributorDailySales(prefManager.getAuthKey(), dailySales, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse dailySalesResponse = (BaseResponse) response;
                if (dailySalesResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Upload Stockist  Sales",
                            "Upload Stockist  Sales Failed. Error " + dailySalesResponse.getMessage() + " Error Code:" + dailySalesResponse.getStatusCode());
                    Toasty.error(mContext, dailySalesResponse.getStatusCode() + ": " + dailySalesResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, dailySalesResponse.getStatusCode(), dailySalesResponse.getMessage());
                } else { //success case
                    LogUtils.setUserActivity(mContext, "Upload Stockist Daily Sales", "Upload Daily Sales Completed");
                    StockistDailySalesEntity dailySalesModel = new StockistDailySalesEntity(mContext);
                    dailySalesModel.delete(null);
                    Toasty.success(mContext, dailySalesResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }
                if (!isSingleCall) {
                    uploadStockistRequest();
                }


            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void postDistributorDailySales(ArrayList<DistributorDailySales> dailySales) {
        LogUtils.setUserActivity(mContext, "Upload Distributor Daily Sales", "Upload Daily Sales Started");
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        DistributorDataService dailySalesDataService = new DistributorDataService(mContext);
        dailySalesDataService.postDistributorDailySales(prefManager.getAuthKey(), dailySales, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse dailySalesResponse = (BaseResponse) response;
                if (dailySalesResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Upload Distributor  Sales",
                            "Upload Distributor  Sales Failed. Error " + dailySalesResponse.getMessage() + " Error Code:" + dailySalesResponse.getStatusCode());
                    Toasty.error(mContext, dailySalesResponse.getStatusCode() + ": " + dailySalesResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, dailySalesResponse.getStatusCode(), dailySalesResponse.getMessage());
                } else { //success case
                    LogUtils.setUserActivity(mContext, "Upload Distributor Daily Sales", "Upload Daily Sales Completed");
                    DistributorDailySalesEntity dailySalesModel = new DistributorDailySalesEntity(mContext);
                    dailySalesModel.delete(null);
                    Toasty.success(mContext, dailySalesResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                }
                if (!isSingleCall) {
                    uploadDistributorRequest();
                }


            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }
    public void saveDistributorOffline(ArrayList<Distributor> distributors) {
        final DistributorEntity entity = new DistributorEntity(mContext);
        entity.delete(null);
        entity.insert(distributors);
    }



    public void saveSurveyOffline(ArrayList<SurveyModel> surveyList) {
        final SurveyEntity surveyEntity = new SurveyEntity(mContext);
        surveyEntity.delete(null);
        surveyEntity.insert(surveyList);
    }

    public void getArea() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Product Group...");
            dialog.setCancelable(false);
            dialog.show();
            AreaDataService productDataService = new AreaDataService(mContext);
            productDataService.getArea(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    AreaResponse areaResponse = (AreaResponse) response;
                    if (areaResponse.getStatusCode() != 200) {
                        Toasty.error(mContext, areaResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        LogUtils.setUserActivity(mContext, "Download Area Failed", areaResponse.getMessage());
                        ErrorHandler.handleSyncError(mContext, areaResponse.getStatusCode(), areaResponse.getMessage());
                    } else {
                        Toasty.success(mContext, areaResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveAreaOffline(areaResponse.getData());
                    }

                    if (!isSingleCall) {
                        // getRoute();
                        downloadStockist();
                    }

                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    private void saveAreaOffline(ArrayList<Area> data) {
        final AreaEntity area = new AreaEntity(mContext);
        area.delete(null);
        area.insert(data
        );
    }

    private void getRoute() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Product Group...");
            dialog.setCancelable(false);
            dialog.show();
            ProductDataService productDataService = new ProductDataService(mContext);
            productDataService.getProductGroup(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    ProductGroupResponse productGroupResponse = (ProductGroupResponse) response;
                    if (productGroupResponse.getStatusCode() != 200) {
                        Toasty.error(mContext, productGroupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        LogUtils.setUserActivity(mContext, "Download Route", "Download Route Failed:" + productGroupResponse.getMessage());
                        ErrorHandler.handleSyncError(mContext, productGroupResponse.getStatusCode(), productGroupResponse.getMessage());
                    } else {
                        Toasty.error(mContext, productGroupResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveProductGroupOffline(productGroupResponse.getProductGroups());
                    }


                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void uploadStockistRequest() {

        StockistRequestEntity stockistRequestEntity = new StockistRequestEntity(mContext);
        final JSONArray outletArray = (JSONArray) stockistRequestEntity.find(null);
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference ref = new TypeReference<List<NewStockistRequest>>() {
        };
        ArrayList<NewStockistRequest> outletRequests = new ArrayList<>();

        try {
            outletRequests = objectMapper.readValue(outletArray.toString(), ref);
            if (outletRequests.size() > 0) {
                postStockistRequest(outletRequests);
            } else {
                Toasty.info(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT, true).show();
                if (!isSingleCall) {
                    uploadStockistStock();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void postDistributorRequest(ArrayList<NewDistributorRequest> outletRequests) {
        LogUtils.setUserActivity(mContext, "Upload Distributor Request", "Upload Create and Update Distributor Request");
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Uploading new Distributor and updates...");
        dialog.setCancelable(false);
        dialog.show();
        DistributorDataService service = new DistributorDataService(mContext);
        service.postDistributorRequest(prefManager.getAuthKey(), outletRequests, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    LogUtils.setUserActivity(mContext, "Upload Distributor Request Failed", "Upload Stockist Request Failed:" + baseResponse.getMessage());
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                    //finish();
                } else { //success case
                    DistributorRequestEntity distributorRequestEntity = new DistributorRequestEntity(mContext);
                    distributorRequestEntity.delete(null);
                    LogUtils.setUserActivity(mContext, "Upload Distributor Request", "Upload Stockist Request Successful");
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    if (!isSingleCall) {
                       // uploadStockistStock();
                        //uploadOutletStatus();
                        //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();
                    }


                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void uploadDistributorRequest() {
        DistributorRequestEntity entity = new DistributorRequestEntity(mContext);
        final JSONArray outletArray = (JSONArray) entity.find(null);
        ObjectMapper objectMapper = new ObjectMapper();
        TypeReference ref = new TypeReference<List<NewDistributorRequest>>() {
        };
        ArrayList<NewDistributorRequest> outletRequests = new ArrayList<>();

        try {
            outletRequests = objectMapper.readValue(outletArray.toString(), ref);
            if (outletRequests.size() > 0) {
                postDistributorRequest(outletRequests);
            } else {
                Toasty.info(mContext, "No Distributor Request to upload!!", Toast.LENGTH_SHORT, true).show();
                if (!isSingleCall) {
                   uploadLocation();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void postStockistRequest(ArrayList<NewStockistRequest> outletRequests) {
        LogUtils.setUserActivity(mContext, "Upload Stockist Request", "Upload Create and Update Stockist Request");
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Uploading new outlets and updates...");
        dialog.setCancelable(false);
        dialog.show();
        StockistDataService service = new StockistDataService(mContext);
        service.postOutletRequest(prefManager.getAuthKey(), outletRequests, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    LogUtils.setUserActivity(mContext, "Upload Stockist Request Failed", "Upload Stockist Request Failed:" + baseResponse.getMessage());
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                    //finish();
                } else { //success case
                    StockistRequestEntity stockistRequestEntity = new StockistRequestEntity(mContext);
                    stockistRequestEntity.delete(null);
                    LogUtils.setUserActivity(mContext, "Upload Stockist Request", "Upload Stockist Request Successful");
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    if (!isSingleCall) {
                        uploadStockistStock();
                        //uploadOutletStatus();
                        //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();
                    }


                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void uploadStockistStatus() {
        OutletStatusEntity statusEntity = new OutletStatusEntity(mContext);
        JSONArray outletRequestArray = (JSONArray) statusEntity.find(null);

        ArrayList<StockistStatus> stockistStatuses = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        TypeReference ref = new TypeReference<List<StockistStatus>>() {
        };

        try {
            stockistStatuses = objectMapper.readValue(outletRequestArray.toString(), ref);
            if (stockistStatuses.size() == 0) {

                Toasty.info(mContext, "No Stockist status logged to Sync", Toast.LENGTH_SHORT, true).show();
                //@TODO check single sync or bulk sync
                if (!isSingleCall) {
                    uploadStockistStock();
                    //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();
                }


            } else {
                postOutletStatus(stockistStatuses);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void postOutletStatus(ArrayList<StockistStatus> stockistStatuses) {
        LogUtils.setUserActivity(mContext, "Upload Stockist Status", "Upload Stockist Visit Status");
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        StockistDataService service = new StockistDataService(mContext);
        service.postStockistStatus(prefManager.getAuthKey(), stockistStatuses, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    LogUtils.setUserActivity(mContext, "Upload Stockist Status", "Upload Stockist Status Failed:" + baseResponse.getMessage());
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());

                } else {
                    OutletStatusEntity statusEntity = new OutletStatusEntity(mContext);
                    statusEntity.delete(null);
                    LogUtils.setUserActivity(mContext, "Upload Stockist Status", "Upload Stockist Status Successful");
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();

                }
                if (!isSingleCall) {
                    uploadStockistStock();
                    //   Toasty.makeText(mContext, "No Stockist Request to upload!!", Toast.LENGTH_SHORT,true).show();
                }


            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void uploadCollectionImage() {
        ProgressDialog dialog;
        if (Utils.isConnectionAvailable(mContext)) {
            CollectionImageEntity photo = new CollectionImageEntity(mContext);
            JSONArray jsonArray = (JSONArray) photo.find(null);
            ObjectMapper objectMapper = new ObjectMapper();
            TypeReference ref = new TypeReference<List<CollectionImages>>() {
            };
            ArrayList<CollectionImages> imageList = new ArrayList<>();
            try {
                imageList = objectMapper.readValue(jsonArray.toString(), ref);

            } catch (IOException e) {
                e.printStackTrace();
            }

            RequestBody description = createPartFromString(jsonArray.toString());
            List<MultipartBody.Part> parts = new ArrayList<>();

            if (imageList.size() > 0) {
                for (int i = 0; i < imageList.size(); i++) {
                    parts.add(prepareFilePart("CollecionImage" + i, Uri.parse(imageList.get(i).getFileUri()), imageList.get(i).getPhotoPath()));
                }
                dialog = new ProgressDialog(mContext);
                dialog.setMessage("Uploading Collection Images...");
                dialog.setCancelable(false);
                dialog.show();
                RequestBody size = createPartFromString("" + parts.size());
                SalesOrderDataService service = new SalesOrderDataService(mContext);
                service.uploadImages(prefManager.getAuthKey(), description, size, parts, new OnSuccess() {
                    @Override
                    public void onSuccess(Object response) {
                        if (dialog.isShowing())
                            dialog.dismiss();

                        photo.delete(null);
                        Toasty.success(mContext, "Collection Image uploaded successfully!!", Toast.LENGTH_SHORT, true).show();
                        LogUtils.setUserActivity(mContext, "Collection Image", "Collection Image upload successful");
                        if (!isSingleCall) {
                            uploadMarketActivities();

                        }
                    }
                }, new OnFailedListener() {
                    @Override
                    public void onFailed(ErrorModel reason) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        if (!isSingleCall) {
                            uploadMarketActivities();

                        }
                        LogUtils.setUserActivity(mContext, "Collection Images", "Photo upload failed" + reason.getErrors());
                    }
                });
            } else {
                Toasty.info(mContext, "No image found to upload. Please take photo and save!", Toast.LENGTH_SHORT, true).show();
                if (!isSingleCall) {
                    uploadMarketActivities();

                }
            }

        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void uploadMarketActivities() {
        ArrayList<MarketActivity> marketActivities = readMarketActivities();
        if (marketActivities == null) {
            Toasty.info(mContext, "No collection record found to sync!!", Toast.LENGTH_SHORT, true).show();
            if (!isSingleCall) {
                uploadMarketActivitiesImages();

            }
        } else {
            postMarketActivity(marketActivities);
        }
    }

    private ArrayList<MarketActivity> readMarketActivities() {
        MarketActivityEntity marketEntity = new MarketActivityEntity(mContext);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) marketEntity.find(null);
        ArrayList<MarketActivity> marketDetails = null;
        if (array.length() > 0) {
            try {
                marketDetails = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<MarketActivity>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return marketDetails;
    }


    private void postMarketActivity(ArrayList<MarketActivity> marketActivities) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        LogUtils.setUserActivity(mContext, "Salesman Sales Order", "Upload Salesman SalesOrder Started");
        MarketActivitiesDataService dataService = new MarketActivitiesDataService(mContext);
        dataService.postMarketActivities(prefManager.getAuthKey(), marketActivities, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                BaseResponse baseResponse = (BaseResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(mContext, "Market Activities", "Upload Market Activities Failed. Error " + baseResponse.getMessage() + " Error Code:" + baseResponse.getStatusCode());
                    Toasty.error(mContext, baseResponse.getStatusCode() + ": " + baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    ErrorHandler.handleSyncError(mContext, baseResponse.getStatusCode(), baseResponse.getMessage());
                } else { //success case
                    LogUtils.setUserActivity(mContext, " Market Activities", "Upload  Market Activities Successful ");
                    Toasty.success(mContext, baseResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                    MarketActivityEntity entity = new MarketActivityEntity(mContext);
                    entity.delete(null);
                    if (!isSingleCall) {
                        uploadMarketActivitiesImages();

                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    public void uploadMarketActivitiesImages() {
        ProgressDialog dialog;
        if (Utils.isConnectionAvailable(mContext)) {
            MarketActivitiesImageEntity photo = new MarketActivitiesImageEntity(mContext);
            JSONArray jsonArray = (JSONArray) photo.find(null);
            ObjectMapper objectMapper = new ObjectMapper();
            TypeReference ref = new TypeReference<List<MarketActivitiesImage>>() {
            };
            ArrayList<MarketActivitiesImage> imageList = new ArrayList<>();
            try {
                imageList = objectMapper.readValue(jsonArray.toString(), ref);

            } catch (IOException e) {
                e.printStackTrace();
            }

            RequestBody description = createPartFromString(jsonArray.toString());
            List<MultipartBody.Part> parts = new ArrayList<>();

            if (imageList.size() > 0) {
                for (int i = 0; i < imageList.size(); i++) {
                    parts.add(prepareFilePart("Image" + i, Uri.parse(imageList.get(i).getFileUri()), imageList.get(i).getPhotoPath()));
                }
                dialog = new ProgressDialog(mContext);
                dialog.setMessage("Uploading Market Activities Images...");
                dialog.setCancelable(false);
                dialog.show();
                RequestBody size = createPartFromString("" + parts.size());
                MarketActivitiesDataService service = new MarketActivitiesDataService(mContext);
                service.uploadMarketActivitiesImages(prefManager.getAuthKey(), description, size, parts, new OnSuccess() {
                    @Override
                    public void onSuccess(Object response) {
                        if (dialog.isShowing())
                            dialog.dismiss();

                        photo.delete(null);
                        Toasty.success(mContext, "Market Activities Image uploaded successfully!!", Toast.LENGTH_SHORT, true).show();
                        LogUtils.setUserActivity(mContext, "Market Activities Image", "Market Activities Image upload successful");
                        if (!isSingleCall) {
                            uploadStockistDailySales();

                        }
                    }
                }, new OnFailedListener() {
                    @Override
                    public void onFailed(ErrorModel reason) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        if (!isSingleCall) {
                            uploadStockistDailySales();
                        }
                        LogUtils.setUserActivity(mContext, " Market Activities Images", " Market Activities Images upload failed" + reason.getErrors());
                    }
                });
            } else {
                Toasty.info(mContext, "No  Market Activities Images found!", Toast.LENGTH_SHORT, true).show();
                if (!isSingleCall) {
                    uploadStockistDailySales();
                }
            }

        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }



    // distributor

    public void downloadDistributor() {
        if (Utils.isConnectionAvailable(mContext)) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Downloading Distributor...");
            dialog.setCancelable(false);
            dialog.show();
            DistributorDataService distributorDataService = new DistributorDataService(mContext);
            distributorDataService.getDistributor(prefManager.getAuthKey(), new OnSuccess() {
                @Override
                public void onSuccess(Object response) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    DistributorResponse distributorResponse = (DistributorResponse) response;
                    if (distributorResponse.getStatusCode() != 200) {//failure case
                        LogUtils.setUserActivity(mContext, "Download Distributor", "Download Distributor Failed. Error " + distributorResponse.getMessage() + " Error Code:" + distributorResponse.getStatusCode());
                        Toasty.error(mContext, distributorResponse.getStatusCode() + ": " + distributorResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        ErrorHandler.handleSyncError(mContext, distributorResponse.getStatusCode(), distributorResponse.getMessage());
                    } else {
                        Toasty.success(mContext, distributorResponse.getMessage(), Toast.LENGTH_SHORT, true).show();
                        saveDistributorOffline(distributorResponse.getDistributors());
                    }

                    if (!isSingleCall) {
                        getProduct();
                        //getProductGroup();
                    }


                }
            }, new OnFailedListener() {
                @Override
                public void onFailed(ErrorModel reason) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Toasty.info(mContext, "No internet Connection Available!!", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void saveStockistOffline(ArrayList<Stockist> stockists) {
        final StockistEntity outlet = new StockistEntity(mContext);
        outlet.delete(null);
        outlet.insert(stockists);
    }


}
