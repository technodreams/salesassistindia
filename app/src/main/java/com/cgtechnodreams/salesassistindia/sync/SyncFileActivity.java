package com.cgtechnodreams.salesassistindia.sync;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.databasehelper.model.LocationModelEntity;
import com.cgtechnodreams.salesassistindia.photo.PhotoUploadDataService;
import com.cgtechnodreams.salesassistindia.photo.model.PhotoEntity;
import com.cgtechnodreams.salesassistindia.photo.model.PhotoModel;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SyncFileActivity extends AppCompatActivity {
    final LocationModelEntity locationModelEntity = new LocationModelEntity(SyncFileActivity.this);
    @BindView(R.id.rlUploadLocation)
    RelativeLayout rlUploadLocation;
    @BindView(R.id.rlUploadDailySale)
    RelativeLayout rlUploadDailySale;
    @BindView(R.id.rlCompetitorSales)
    RelativeLayout rlCompetitorSales;
    @BindView(R.id.rlUploadPhoto)
    RelativeLayout rlUploadPhoto;
    @BindView(R.id.rlUserActivity)
    RelativeLayout rlUSerActivity;
    @BindView(R.id.rlBrand)
    RelativeLayout rlBrand;
    @BindView(R.id.rlProductGroup)
    RelativeLayout rlProductGroup;
    @BindView(R.id.rlProduct)
    RelativeLayout rlProduct;
    @BindView(R.id.rlSurvey)
    RelativeLayout rlSurvey;
    @BindView(R.id.rlUploadSurvey)
    RelativeLayout rlUploadSurvey;
    //SAlesman Specific
    @BindView(R.id.rlUploadSalesOrder)
    RelativeLayout rlUploadSalesOrder;
    @BindView(R.id.rlArea)
    RelativeLayout rlArea;
    @BindView(R.id.rlProductInventory)
    RelativeLayout rlProductInventory;

    @BindView( R.id.rlUploadStockistRequest)
    RelativeLayout rlUploadStockistRequest;
    @BindView(R.id.rlUploadStockistStatus)
    RelativeLayout rlUploadStockistStatus;
    @BindView(R.id.rlUploadStockistStock)
    RelativeLayout rlUploadStockistStock;
    @BindView(R.id.rlProductSubGroup)
    RelativeLayout rlProductSubGroup;
    @BindView(R.id.rlCompetitorGroup)
    RelativeLayout rlCompetitorGroup;
    @BindView(R.id.rlUploadCollectionDetail)
    RelativeLayout rlUploadCollectionDetail;
    @BindView(R.id.rlUploadCollectionImage)
    RelativeLayout rlUploadCollectionImage;
    @BindView(R.id.rlMarketActivities)
    RelativeLayout rlMarketActivities;
    @BindView(R.id.rlMarketActivitiesImages)
    RelativeLayout rlMarketActivitiesImages;
    @BindView(R.id.rlUploadStockistDamageStock)
    RelativeLayout stockistDamageStock;
    @BindView(R.id.rlUploadStockistDailySales)
    RelativeLayout  rlUploadStockistDailySales;

    @BindView(R.id.rlUploadDistributorSalesOrder)
    RelativeLayout rlUploadDistributorSalesOrder;
    @BindView(R.id.rlUploadDistributorDamageStock)
    RelativeLayout rlUploadDistributorDamageStock;
    @BindView(R.id.rlUploadDistributorStock)
    RelativeLayout rlUploadDistributorStock;
    @BindView(R.id.rlUploadDistributorDailySales)
    RelativeLayout rlUploadDistributorDailySales;

    @BindView(R.id.rlbank)
    RelativeLayout rlbank;
    ProgressDialog dialog;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rlCompany)
    RelativeLayout rlCompany;
    @BindView(R.id.rlDonwloadDistributor)
    RelativeLayout rlDownloadDistributor;
    @BindView(R.id.rlUploadDistributorRequest)
    RelativeLayout rlUploadDistributorRequest;
    @BindView(R.id.rlDistributorPurchase)
    RelativeLayout rlDistributorPurchase;
    @BindView(R.id.rlClosingStock)
    RelativeLayout rlClosingStock;

    PrefManager prefManager;
    Sync sync = null;
    private ArrayList<Uri> uriArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_files);
        ButterKnife.bind(this);
        toolbar.setTitle("Sync");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        if (prefManager.getRole().equalsIgnoreCase("ISM")) {
            rlCompetitorSales.setVisibility(View.GONE);
            rlUploadDailySale.setVisibility(View.GONE);
            rlBrand.setVisibility(View.GONE);
            rlProductGroup.setVisibility(View.GONE);
            rlCompany.setVisibility(View.GONE);
            rlProductSubGroup.setVisibility(View.GONE);
            rlProduct.setVisibility(View.GONE);
            rlProductInventory.setVisibility(View.GONE);
            rlUploadSalesOrder.setVisibility(View.GONE);
            rlUploadStockistRequest.setVisibility(View.GONE);
            rlUploadStockistStatus.setVisibility(View.GONE);
            rlUploadStockistStock.setVisibility(View.GONE);
            rlCompetitorGroup.setVisibility(View.GONE);
            rlbank.setVisibility(View.GONE);
        }
        else if (prefManager.getRole().equalsIgnoreCase("Promoter")) {
            rlSurvey.setVisibility(View.GONE);
            rlUploadSurvey.setVisibility(View.GONE);
            rlCompany.setVisibility(View.GONE);
            rlProductSubGroup.setVisibility(View.GONE);
            rlArea.setVisibility(View.GONE);
            rlProductInventory.setVisibility(View.GONE);
            rlUploadSalesOrder.setVisibility(View.GONE);
            rlUploadStockistRequest.setVisibility(View.GONE);
            rlUploadStockistStatus.setVisibility(View.GONE);
            rlUploadStockistStock.setVisibility(View.GONE);
            rlProductGroup.setVisibility(View.GONE);
            rlbank.setVisibility(View.GONE);
        }
        else  {
            rlCompetitorSales.setVisibility(View.GONE);
            rlUploadDailySale.setVisibility(View.GONE);
            rlSurvey.setVisibility(View.GONE);
            rlUploadSurvey.setVisibility(View.GONE);
            rlCompetitorGroup.setVisibility(View.GONE);
            rlProductInventory.setVisibility(View.GONE);
            rlProductGroup.setVisibility(View.GONE);
            rlProductSubGroup.setVisibility(View.GONE);
            rlUploadStockistStatus.setVisibility(View.GONE);

            //@TODO make me visible once the function is required
            rlUploadStockistRequest.setVisibility(View.GONE);
            rlUploadCollectionDetail.setVisibility(View.GONE);
            rlUploadSalesOrder.setVisibility(View.GONE);
            rlUploadDistributorSalesOrder.setVisibility(View.GONE);
            rlUploadCollectionImage.setVisibility(View.GONE);
            rlUploadPhoto.setVisibility(View.GONE);
            rlMarketActivities.setVisibility(View.GONE);
            rlMarketActivitiesImages.setVisibility(View.GONE);
            rlUploadDistributorDailySales.setVisibility(View.GONE);
            rlUploadDistributorDamageStock.setVisibility(View.GONE);
            rlUploadDistributorStock.setVisibility(View.GONE);
            rlUploadStockistStatus.setVisibility(View.GONE);
            rlUploadDailySale.setVisibility(View.GONE);
            rlUSerActivity.setVisibility(View.GONE);
            rlUploadLocation.setVisibility(View.GONE);
            rlUploadStockistRequest.setVisibility(View.GONE);
            rlUploadDistributorRequest.setVisibility(View.GONE);
            rlUploadStockistStock.setVisibility(View.GONE);
            stockistDamageStock.setVisibility(View.GONE);
            rlUploadStockistDailySales.setVisibility(View.GONE);

        }
        prefManager = new PrefManager(this);
        sync = new Sync(this, prefManager.getRole(), true);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @OnClick({R.id.rlUploadLocation,
            R.id.rlBrand,
            R.id.rlProduct,
            R.id.rlProductGroup,
            R.id.rlProductSubGroup,
            R.id.rlUploadDailySale,
            R.id.rlCompetitorSales,
            R.id.rlUploadPhoto,
            R.id.rlUserActivity,
            R.id.rlSurvey,
            R.id.rlUploadSurvey,
            R.id.rlDonwloadStockist,
            R.id.rlDownloadPromoter,
            R.id.rlArea,
            R.id.rlProductInventory,
            R.id.rlUploadSalesOrder,
            R.id.rlUploadStockistRequest,
            R.id.rlUploadStockistStatus,
            R.id.rlUploadStockistStock,
            R.id.rlCompetitorGroup,
            R.id.rlbank,
            R.id.rlUploadCollectionDetail,
            R.id.rlUploadStockistDailySales,
            R.id.rlUploadCollectionImage,
            R.id.rlMarketActivities,
            R.id.rlMarketActivitiesImages,
            R.id.rlUploadStockistDamageStock,
            R.id.rlCompany,
            R.id.rlDonwloadDistributor,
            R.id.rlUploadDistributorStock,
            R.id.rlUploadDistributorDamageStock,
            R.id.rlUploadDistributorSalesOrder,
            R.id.rlUploadDistributorRequest,
            R.id.rlUploadDistributorDailySales,
            R.id.rlClosingStock,
            R.id.rlDistributorPurchase
    })
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.rlDistributorPurchase:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.postDistributorPurchase();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlClosingStock:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.postClosingStock();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadLocation:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadLocation();
                }else{
                  Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlBrand:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getBrand();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlCompany:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getCompany();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlProduct:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getProduct();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlProductGroup:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getProductGroup();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlCompetitorGroup:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getCompetitorGroup();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;

            case R.id.rlCompetitorSales:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadCompetitorSales();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }


                break;
            case R.id.rlUploadDailySale:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadDailySales();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadPhoto:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    uploadPhoto();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadStockistDailySales:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadStockistDailySales();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadDistributorDailySales:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadDistributorDailySales();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUserActivity:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadUserActivity();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlSurvey:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.downloadSurvey();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadSurvey:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadSurvey();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlDonwloadStockist:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.downloadStockist();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlDownloadPromoter:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                  //  sync.downloadPromoter();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadDistributorSalesOrder:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadDistributorSalesOrder();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlUploadSalesOrder:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadSalesOrder();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlUploadCollectionDetail:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadCollectionDetail();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlUploadCollectionImage:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadCollectionImage();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlMarketActivities:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadMarketActivities();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlMarketActivitiesImages:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadMarketActivitiesImages();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlArea:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getArea();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlProductInventory:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getProductInventory();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadStockistRequest:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadStockistRequest();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadDistributorRequest:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadDistributorRequest();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadStockistStatus:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadStockistStatus();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlProductSubGroup:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getSubGroup();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlUploadStockistStock:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadStockistStock();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlUploadDistributorStock:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadDistributorStock();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlUploadStockistDamageStock:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadStockistDamageStock();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlUploadDistributorDamageStock:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.uploadDistributorDamageStock();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }
                break;
            case R.id.rlbank:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.getBankList();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;
            case R.id.rlDonwloadDistributor:
                if(Utils.isConnectionAvailable(SyncFileActivity.this)){
                    sync.downloadDistributor();
                }else{
                    Utils.getConnectionToast(SyncFileActivity.this);
                }

                break;

        }
    }

    private void uploadPhoto() {
        ProgressDialog dialog;
        if (Utils.isConnectionAvailable(SyncFileActivity.this)) {
            PhotoEntity photo = new PhotoEntity(SyncFileActivity.this);
            JSONArray jsonArray = (JSONArray) photo.find(null);
            ObjectMapper objectMapper = new ObjectMapper();
            TypeReference ref = new TypeReference<List<PhotoModel>>() {
            };
            ArrayList<PhotoModel> imageList = new ArrayList<>();
            try {
                imageList = objectMapper.readValue(jsonArray.toString(), ref);

            } catch (IOException e) {
                e.printStackTrace();
            }

            RequestBody description = createPartFromString(jsonArray.toString());
            List<MultipartBody.Part> parts = new ArrayList<>();

            if (imageList.size() > 0) {
                for (int i = 0; i < imageList.size(); i++) {
                    parts.add(prepareFilePart("image" + i, Uri.parse(imageList.get(i).getFileUri()), imageList.get(i).getPhotoPath()));
                }
                dialog = new ProgressDialog(SyncFileActivity.this);
                dialog.setMessage("Uploading Images...");
                dialog.setCancelable(false);
                dialog.show();
                RequestBody size = createPartFromString("" + parts.size());
                PhotoUploadDataService service = new PhotoUploadDataService(SyncFileActivity.this);
                service.uploadImages(prefManager.getAuthKey(), description, size, parts, new OnSuccess() {
                    @Override
                    public void onSuccess(Object response) {
                        if (dialog.isShowing())
                            dialog.dismiss();

                            photo.delete(null);
                            Toasty.success(SyncFileActivity.this, "Photo uploaded successfully!!", Toast.LENGTH_SHORT,true).show();
                            LogUtils.setUserActivity(SyncFileActivity.this, "Photo", "Photo upload successful");
                        //}

                    }
                }, new OnFailedListener() {
                    @Override
                    public void onFailed(ErrorModel reason) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        LogUtils.setUserActivity(SyncFileActivity.this, "Photo", "Photo upload failed" + reason.getErrors());
                    }
                });
            } else {
                Toasty.info(SyncFileActivity.this, "No image found to upload. Please take photo and save!", Toast.LENGTH_SHORT,true).show();
            }

        } else {
            Toasty.info(SyncFileActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT,true).show();
        }

    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri, String path) {
        File file = new File(path);
        RequestBody requestFile = RequestBody.create(MediaType.parse(getContentResolver().getType(fileUri)), file);

        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

//    private void uploadLocation() {
//        final JSONArray locationModelArray = (JSONArray) locationModelEntity.find(null);
//        ObjectMapper objectMapper = new ObjectMapper();
//        TypeReference ref = new TypeReference<List<LocationModel>>() {
//        };
//        ArrayList<LocationModel> location = new ArrayList<>();
//        try {
//            location = objectMapper.readValue(locationModelArray.toString(), ref);
//            Log.i("Latitude", location.get(0).getLatitude());
//            postMovementGPS(location);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    private void uploadDailySales() {
//        ArrayList<DailySales> salesArrayList = readDailySales();
//        if (salesArrayList != null) {
//            postDailySales(salesArrayList);
//        } else {
//            Toasty.makeText(SyncFileActivity.this, "No sales recorded to sync!!", Toast.LENGTH_SHORT,true).show();
//        }
//    }

//    private void uploadCompetitorSales() {
//        ArrayList<CompetitorSales> competitorSalesDetails = readCompetitorSales();
//        if (competitorSalesDetails != null) {
//            postCompetitorSales(competitorSalesDetails);
//        } else {
//            Toasty.makeText(SyncFileActivity.this, "No sales recorded to sync!!", Toast.LENGTH_SHORT,true).show();
//        }
//    }

//    private void downloadOutlet() {
//        if (Utils.isConnectionAvailable(SyncFileActivity.this)) {
//
//            dialog = new ProgressDialog(this);
//            dialog.setMessage("Processing your request...");
//            dialog.setCancelable(false);
//            dialog.show();
//
//            StockistDataService outletDataService = new StockistDataService(SyncFileActivity.this);
//            outletDataService.getOutlet(prefManager.getAuthKey(), new OnSuccess() {
//                @Override
//                public void onSuccess(Object response) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
//                    DistributorResponse outletResponse = (DistributorResponse) response;
//                    saveOutletOffline(outletResponse.getStockists());
//
//                }
//            }, new OnFailedListener() {
//                @Override
//                public void onFailed(ErrorModel reason) {
//                    if (dialog.isShowing()) {
//                        dialog.dismiss();
//                    }
//                }
//            });
//        } else {
//            Toasty.makeText(SyncFileActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT,true).show();
//        }
//    }

//    private void downloadPromoter() {
//        if (Utils.isConnectionAvailable(SyncFileActivity.this)) {
//
//            dialog = new ProgressDialog(this);
//            dialog.setMessage("Processing your request...");
//            dialog.setCancelable(false);
//            dialog.show();
//
//            UserDataService outletDataService = new UserDataService(SyncFileActivity.this);
//            outletDataService.getUser(prefManager.getAuthKey(), new OnSuccess() {
//                @Override
//                public void onSuccess(Object response) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
//                    UserResponse userResponse = (UserResponse) response;
//                    saveUserOffline(userResponse.getUsers());
//
//                }
//            }, new OnFailedListener() {
//                @Override
//                public void onFailed(ErrorModel reason) {
//                    if (dialog.isShowing()) {
//                        dialog.dismiss();
//                    }
//                }
//            });
//        } else {
//            Toasty.makeText(SyncFileActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT,true).show();
//        }
//    }

//    private void saveUserOffline(ArrayList<User> users) {
//        final UserEntity user = new UserEntity(SyncFileActivity.this);
//        user.delete(null);
//        user.insert(users);
//    }
//
//    private void uploadSurvey() {
//        SurveyAnswerEntity surveyAnswerEntity = new SurveyAnswerEntity(SyncFileActivity.this);
//        JSONArray surveyAnswerArray = (JSONArray) surveyAnswerEntity.find(null);
//        ArrayList<AnswerModel> answerModels = new ArrayList<>();
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//        TypeReference ref = new TypeReference<List<AnswerModel>>() {};
//        try {
//            answerModels = objectMapper.readValue(surveyAnswerArray.toString(), ref);
//            postAnswers(answerModels);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    private void postAnswers(ArrayList<AnswerModel> answerModels) {
//        ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Processing your request...");
//        dialog.setCancelable(false);
//        dialog.show();
//        SurveyDataService surveyDataService = new SurveyDataService(SyncFileActivity.this);
//        surveyDataService.postSurvey(prefManager.getAuthKey(), answerModels, new OnSuccess() {
//            @Override
//            public void onSuccess(Object response) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
//                BaseResponse baseResponse = (BaseResponse) response;
//                if (baseResponse.getStatusCode() != 200) {//failure case
//                    Toasty.makeText(SyncFileActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                    finish();
//                } else { //success case
//                    SurveyAnswerEntity surveyAnswerEntity = new SurveyAnswerEntity(SyncFileActivity.this);
//                    surveyAnswerEntity.delete(null);
//                    Toasty.makeText(SyncFileActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                }
//
//            }
//        }, new OnFailedListener() {
//            @Override
//            public void onFailed(ErrorModel reason) {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//
//            }
//        });
//    }
//
//    private void getSurvey() {
//        if (Utils.isConnectionAvailable(SyncFileActivity.this)) {
//            dialog = new ProgressDialog(this);
//            dialog.setMessage("Downloading Survey List...");
//            dialog.setCancelable(false);
//            dialog.show();
//            SurveyDataService surveyDataService = new SurveyDataService(SyncFileActivity.this);
//            surveyDataService.getSurvey(prefManager.getAuthKey(), new OnSuccess() {
//                @Override
//                public void onSuccess(Object response) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
//                    SurveyResponse surveyResponse = (SurveyResponse) response;
//                    saveSurveyOffline(surveyResponse.getData());
//                }
//            }, new OnFailedListener() {
//                @Override
//                public void onFailed(ErrorModel reason) {
//                    if (dialog.isShowing()) {
//                        dialog.dismiss();
//                    }
//                }
//            });
//        } else {
//            Toasty.makeText(SyncFileActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT,true).show();
//        }
//    }
//
//    private void uploadUserActivity() {
//        UserActivityEntity activityEntity = new UserActivityEntity(SyncFileActivity.this);
//        JSONArray activitiesArray = (JSONArray) activityEntity.find(null);
//
//        ArrayList<UserActivity> activityModel = new ArrayList<>();
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//
//        TypeReference ref = new TypeReference<List<UserActivity>>() {
//        };
//
//        try {
//            activityModel = objectMapper.readValue(activitiesArray.toString(), ref);
//            if (activityModel.size() < 1) {
//                Toasty.makeText(SyncFileActivity.this, "No User Activity to Sync", Toast.LENGTH_SHORT,true).show();
//            } else {
//                postUserActivity(activityModel);
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

//    private void postUserActivity(ArrayList<UserActivity> activityModel) {
//        ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Processing your request...");
//        dialog.setCancelable(false);
//        dialog.show();
//        UserDataService dataService = new UserDataService(SyncFileActivity.this);
//        dataService.postUserActivity(prefManager.getAuthKey(), activityModel, new OnSuccess() {
//            @Override
//            public void onSuccess(Object response) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
//                BaseResponse baseResponse = (BaseResponse) response;
//                if (baseResponse.getStatusCode() != 200) {//failure case
//                    Toasty.makeText(SyncFileActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                    finish();
//                } else { //success case
//                    Toasty.makeText(SyncFileActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                }
//
//            }
//        }, new OnFailedListener() {
//            @Override
//            public void onFailed(ErrorModel reason) {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//
//            }
//        });
//    }
//
//    private void uploadStorePhoto(ArrayList<Photo> photoList) {
//    }
//
//    private void postCompetitorSales(ArrayList<CompetitorSales> competitorSalesDetails) {
//        ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Processing your request...");
//        dialog.setCancelable(false);
//        dialog.show();
//        DailySalesDataService competitorSalesDataService = new DailySalesDataService(SyncFileActivity.this);
//        competitorSalesDataService.postCompetitorSales(prefManager.getAuthKey(), competitorSalesDetails, new OnSuccess() {
//            @Override
//            public void onSuccess(Object response) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
//                BaseResponse baseResponse = (BaseResponse) response;
//                if (baseResponse.getStatusCode() != 200) {//failure case
//                    Toasty.makeText(SyncFileActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                    finish();
//                } else { //success case
//                    Toasty.makeText(SyncFileActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                    CompetitorSalesModel competitorSalesModel = new CompetitorSalesModel(SyncFileActivity.this);
//                    competitorSalesModel.delete(null);
//                }
//
//            }
//        }, new OnFailedListener() {
//            @Override
//            public void onFailed(ErrorModel reason) {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//
//            }
//        });
//    }
//
//    private ArrayList<CompetitorSales> readCompetitorSales() {
//        CompetitorSalesModel competitorSalesModel = new CompetitorSalesModel(SyncFileActivity.this);
//        JSONArray competitorSalesModelArray = (JSONArray) competitorSalesModel.find(null);
//        if (competitorSalesModelArray.length() > 0) {
//            ArrayList<CompetitorSales> salesList = new ArrayList<>();
//
//            for (int i = 0; i < competitorSalesModelArray.length(); i++) {
//                JSONObject jsonobject = null;
//                try {
//                    CompetitorSales dailySales = new CompetitorSales();
//                    jsonobject = competitorSalesModelArray.getJSONObject(i);
//                    dailySales.setBrandId(jsonobject.getInt("brand_id"));
//                    dailySales.setBrandName(jsonobject.getString("brand_name"));
//                    dailySales.setDate(jsonobject.getString("date"));
//                    dailySales.setRemarks(jsonobject.getString("remarks"));
//                    dailySales.setTotalSales(jsonobject.getString("total_sales"));
//
//                    String details = jsonobject.getString("sales_detail");
//                    JSONArray salesJsonArray = new JSONArray(details);
//                    ArrayList<CompetitorSalesDetailModel> salesDetailArrayList = new ArrayList<CompetitorSalesDetailModel>();
//                    for (int j = 0; j < salesJsonArray.length(); j++) {
//                        JSONObject jsonObject = salesJsonArray.getJSONObject(j);
//                        CompetitorSalesDetailModel salesDetail = new CompetitorSalesDetailModel();
//                        salesDetail.setId(jsonObject.getInt("id"));
//                        salesDetail.setQty(jsonObject.getInt("qty"));
//                        salesDetail.setName(jsonObject.getString("name"));
//                        salesDetailArrayList.add(salesDetail);
//
//                    }
//                    dailySales.setSalesDetail(salesDetailArrayList);
//                    salesList.add(dailySales);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            return salesList;
//        }
//        return null;
////        CompetitorSalesModel competitorSalesModel = new CompetitorSalesModel(SyncFileActivity.this);
////        final JSONArray competitorSalesArray = (JSONArray) competitorSalesModel.find(null);
////        ObjectMapper objectMapper = new ObjectMapper();
////        TypeReference ref = new TypeReference<List<CompetitorSalesDetail>>() {};
////        ArrayList<CompetitorSalesDetail> competitorSalesDetails = new ArrayList<>();
////        try {
////           return competitorSalesDetails = objectMapper.readValue(competitorSalesArray.toString(), ref);
////
////        } catch (IOException e) {
////            e.printStackTrace();
////            return  null;
////        }
//
//    }
//
//    private void postMovementGPS(ArrayList<LocationModel> locationList) {
//        ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Processing your request...");
//        dialog.setCancelable(false);
//        dialog.show();
//        LocationDataService locationDataService = new LocationDataService(SyncFileActivity.this);
//        locationDataService.movementGPS(prefManager.getAuthKey(), locationList, new OnSuccess() {
//            @Override
//            public void onSuccess(Object response) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
//                LocationResponse locationResponse = (LocationResponse) response;
//                if (locationResponse.getStatusCode() != 200) {//failure case
//                    Toasty.makeText(SyncFileActivity.this, locationResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                    finish();
//                } else { //success case
//                    LocationModelEntity locationModelEntity = new LocationModelEntity(SyncFileActivity.this);
//                    locationModelEntity.clearDb();
//                    Toasty.makeText(SyncFileActivity.this, locationResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                }
//
//            }
//        }, new OnFailedListener() {
//            @Override
//            public void onFailed(ErrorModel reason) {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//
//            }
//        });
//    }
//
//    private ArrayList<DailySales> readDailySales() {
//        DailySalesModel dailySalesModel = new DailySalesModel(SyncFileActivity.this);
//        JSONArray dailySalesArray = (JSONArray) dailySalesModel.find(null);
//        if (dailySalesArray.length() > 0) {
//            ArrayList<DailySales> salesList = new ArrayList<>();
//
//            for (int i = 0; i < dailySalesArray.length(); i++) {
//                JSONObject jsonobject = null;
//                try {
//                    DailySales dailySales = new DailySales();
//                    jsonobject = dailySalesArray.getJSONObject(i);
//                    dailySales.setId(jsonobject.getInt("id"));
//                    dailySales.setCustomerAddress(jsonobject.getString("customer_address"));
//                    dailySales.setCustomerMobile(jsonobject.getString("customer_mobile"));
//                    dailySales.setCustomerName(jsonobject.getString("customer_name"));
//                    dailySales.setQty(jsonobject.getInt("quantity"));
//                    dailySales.setDate(jsonobject.getString("date"));
//                    String details = jsonobject.getString("detail");
//                    JSONArray salesJsonArray = new JSONArray(details);
//                    ArrayList<SalesDetail> salesDetailArrayList = new ArrayList<SalesDetail>();
//                    for (int j = 0; j < salesJsonArray.length(); j++) {
//                        JSONObject jsonObject = salesJsonArray.getJSONObject(j);
//                        SalesDetail salesDetail = new SalesDetail();
//                        salesDetail.setSellingPrice(jsonObject.getString("selling_price"));
//                        salesDetail.setSerialNumber(jsonObject.getString("serial_number"));
//                        salesDetail.setProductId(jsonObject.getString("product_id"));
//                        salesDetail.setModelNumber(jsonObject.getString("model_number"));
//                        salesDetailArrayList.add(salesDetail);
//
//                    }
//                    dailySales.setSalesDetail(salesDetailArrayList);
//                    salesList.add(dailySales);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            return salesList;
//
//        } else {
//            return null;
//        }
//
//    }
//
//    private void postDailySales(ArrayList<DailySales> dailySales) {
//        ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Processing your request...");
//        dialog.setCancelable(false);
//        dialog.show();
//        DailySalesDataService dailySalesDataService = new DailySalesDataService(SyncFileActivity.this);
//        dailySalesDataService.postDailySales(prefManager.getAuthKey(), dailySales, new OnSuccess() {
//            @Override
//            public void onSuccess(Object response) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
//                DailySalesResponse dailySalesResponse = (DailySalesResponse) response;
//                if (dailySalesResponse.getStatusCode() != 200) {//failure case
//                    Toasty.makeText(SyncFileActivity.this, dailySalesResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                    finish();
//                } else { //success case
//                    DailySalesModel dailySalesModel = new DailySalesModel(SyncFileActivity.this);
//                    dailySalesModel.clearDb();
//                    Toasty.makeText(SyncFileActivity.this, dailySalesResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                }
//
//            }
//        }, new OnFailedListener() {
//            @Override
//            public void onFailed(ErrorModel reason) {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//
//            }
//        });
//    }
//
//    private void getProduct() {
//        if (Utils.isConnectionAvailable(SyncFileActivity.this)) {
//            dialog = new ProgressDialog(this);
//            dialog.setMessage("Processing your request...");
//            dialog.setCancelable(false);
//            dialog.show();
//
//            ProductDataService productDataService = new ProductDataService(SyncFileActivity.this);
//            productDataService.getProduct(prefManager.getAuthKey(), new OnSuccess() {
//                @Override
//                public void onSuccess(Object response) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
//                    ProductResponse productResponse = (ProductResponse) response;
//                    saveOfflineProduct(productResponse.getProducts());
//
//                }
//            }, new OnFailedListener() {
//                @Override
//                public void onFailed(ErrorModel reason) {
//                    if (dialog.isShowing()) {
//                        dialog.dismiss();
//                    }
//                    Toasty.makeText(SyncFileActivity.this,"Error:"+ reason.getErrors() + " Message:" + reason.getMessage(),Toast.LENGTH_SHORT,true).show();
//                }
//            });
//        } else {
//            Toasty.makeText(SyncFileActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT,true).show();
//        }
//    }

//    private void saveOfflineProduct(ArrayList<Product> products) {
//        final ProductEntity productEntity = new ProductEntity(SyncFileActivity.this);
//        productEntity.delete(null);
//        productEntity.insert(products);
//    }
//
//    private void getProductGroup() {
//        if (Utils.isConnectionAvailable(SyncFileActivity.this)) {
//
//            dialog = new ProgressDialog(this);
//            dialog.setMessage("Processing your request...");
//            dialog.setCancelable(false);
//            dialog.show();
//
//            ProductDataService productDataService = new ProductDataService(SyncFileActivity.this);
//            productDataService.getProductGroup(prefManager.getAuthKey(), new OnSuccess() {
//                @Override
//                public void onSuccess(Object response) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
//                    ProductGroupResponse productGroupResponse = (ProductGroupResponse) response;
//                    saveProductGroupOffline(productGroupResponse.getProductGroups());
//
//                }
//            }, new OnFailedListener() {
//                @Override
//                public void onFailed(ErrorModel reason) {
//                    if (dialog.isShowing()) {
//                        dialog.dismiss();
//                    }
//                }
//            });
//        } else {
//            Toasty.makeText(SyncFileActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT,true).show();
//        }
//
//    }
//
//    private void saveOutletOffline(ArrayList<Stockist> outlets) {
//        final StockistEntity outlet = new StockistEntity(SyncFileActivity.this);
//        outlet.delete(null);
//        outlet.insert(outlets);
//    }

//    private void saveProductGroupOffline(ArrayList<ProductGroup> productGroups) {
//        final ProductGroupModel productGroup = new ProductGroupModel(SyncFileActivity.this);
//        productGroup.delete(null);
//        productGroup.insert(productGroups);
//    }
//
//    private void getBrand() {
//        if (Utils.isConnectionAvailable(SyncFileActivity.this)) {
//            dialog = new ProgressDialog(this);
//            dialog.setMessage("Processing your request...");
//            dialog.setCancelable(false);
//            dialog.show();
//
//            ProductDataService productDataService = new ProductDataService(SyncFileActivity.this);
//            productDataService.getBrand(prefManager.getAuthKey(), new OnSuccess() {
//                @Override
//                public void onSuccess(Object response) {
//                    if (dialog.isShowing())
//                        dialog.dismiss();
//                    BrandResponse brandResponse = (BrandResponse) response;
//                    saveBrandOffline(brandResponse.getBrands());
//
//
//                }
//            }, new OnFailedListener() {
//                @Override
//                public void onFailed(ErrorModel reason) {
//                    if (dialog.isShowing()) {
//                        dialog.dismiss();
//                    }
//                }
//            });
//        } else {
//            Toasty.makeText(SyncFileActivity.this, "No internet Connection Available!!", Toast.LENGTH_SHORT,true).show();
//        }
//    }

//    private void saveBrandOffline(ArrayList<Brand> brands) {
//        final BrandEnity brandModel = new BrandEnity(SyncFileActivity.this);
//
//        brandModel.delete(null);
//        brandModel.insert(brands);
//    }
//
//    private void saveSurveyOffline(ArrayList<SurveyModel> surveyList) {
//        final SurveyEntity surveyEntity = new SurveyEntity(SyncFileActivity.this);
//        surveyEntity.delete(null);
//        surveyEntity.insert(surveyList);
//    }
}
