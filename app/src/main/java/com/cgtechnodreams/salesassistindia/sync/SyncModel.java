package com.cgtechnodreams.salesassistindia.sync;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SyncModel implements Serializable {
    @JsonProperty("ActionName")
    public  String actionName;
    @JsonProperty("ActionDetail")
    public  String action;
    @JsonProperty("ActionType")
    public String type;
    @JsonProperty("IsSyncRequired")
    public boolean isSync;
    @JsonProperty("LastSync")
    public String dateTime;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
