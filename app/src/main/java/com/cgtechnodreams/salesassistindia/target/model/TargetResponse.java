package com.cgtechnodreams.salesassistindia.target.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
@JsonIgnoreProperties(ignoreUnknown = true)
public class TargetResponse extends BaseResponse {
    public ArrayList<Target> getTargetList() {
        return targetList;
    }

    public void setTargetList(ArrayList<Target> targetList) {
        this.targetList = targetList;
    }

    @JsonProperty("data")
    ArrayList<Target> targetList;
}
