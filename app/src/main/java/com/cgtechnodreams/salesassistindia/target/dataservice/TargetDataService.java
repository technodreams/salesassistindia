package com.cgtechnodreams.salesassistindia.target.dataservice;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesManTargetResponse;
import com.cgtechnodreams.salesassistindia.target.model.TargetResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TargetDataService {
    private Context mContext;

    public TargetDataService(Context mContext) {
        this.mContext = mContext;
    }

    public void getTarget(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<TargetResponse> callBack = Api.getService(mContext).getTarget(token);
        callBack.enqueue(new Callback<TargetResponse>() {
            @Override
            public void onResponse(Call<TargetResponse> call, Response<TargetResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                }
            }

            @Override
            public void onFailure(Call<TargetResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
    public void getSalesManTarget(String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<SalesManTargetResponse> callBack = Api.getService(mContext).getSalesManTarget(token);
        callBack.enqueue(new Callback<SalesManTargetResponse>() {
            @Override
            public void onResponse(Call<SalesManTargetResponse> call, Response<SalesManTargetResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<SalesManTargetResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }
}
