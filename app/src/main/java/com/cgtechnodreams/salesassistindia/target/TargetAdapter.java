package com.cgtechnodreams.salesassistindia.target;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.target.model.Target;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TargetAdapter extends RecyclerView.Adapter<TargetAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<Target> targetList = new ArrayList<>();

    public TargetAdapter(Context context, ArrayList<Target> targetList) {
        this.mContext = context;
        this.targetList = targetList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public TargetAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_target, null);
        return new TargetAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TargetAdapter.MyViewHolder holder, int position) {
        Target target = targetList.get(position);
        holder.month.setText("Month: " + target.getMonth());
        holder.year.setText("Year: " + target.getYear());
        holder.quantity.setText("Quantity: " + target.getQuantity());
        holder.amount.setText("Amount: " + target.getAmount());
        holder.pse.setText("PSE Index: " + target.getPse());

    }

    @Override
    public int getItemCount() {
        return targetList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.amount)
        TextView amount;
        @BindView(R.id.year)
        TextView year;
        @BindView(R.id.month)
        TextView month;
        @BindView(R.id.pseIndex)
        TextView pse;
        @BindView(R.id.quantity)
        TextView quantity;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}