package com.cgtechnodreams.salesassistindia.target;

import android.app.ProgressDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.target.dataservice.TargetDataService;
import com.cgtechnodreams.salesassistindia.target.model.Target;
import com.cgtechnodreams.salesassistindia.target.model.TargetResponse;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class TargetActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    TargetAdapter adapter;
    ProgressBar dialog;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target);
        ButterKnife.bind(this);
        toolbar.setTitle("Target");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);

        getTarget();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getTarget() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        TargetDataService dataService = new TargetDataService(this);
        dataService.getTarget(prefManager.getAuthKey(), new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                TargetResponse baseResponse = (TargetResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(TargetActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    //handleSyncError(mContext,brandResponse.getStatusCode());
                    finish();
                } else { //success case
                    Toasty.success(TargetActivity.this, baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    setData(baseResponse.getTargetList());
             }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<Target> targetList) {
        LinearLayoutManager favouriteListManager = new LinearLayoutManager(TargetActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new TargetAdapter(TargetActivity.this, targetList);
        recyclerView.setLayoutManager(favouriteListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
}
