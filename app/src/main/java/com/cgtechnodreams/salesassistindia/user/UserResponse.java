package com.cgtechnodreams.salesassistindia.user;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.user.model.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponse extends BaseResponse {
    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    @JsonProperty("data")
    private ArrayList<User> users;
}
