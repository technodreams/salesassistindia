package com.cgtechnodreams.salesassistindia.fieldwork;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.fieldwork.model.DispatchedOrder;

import java.util.List;

public class PartialDispatchedAdapter extends RecyclerView.Adapter<PartialDispatchedAdapter.ViewHolder> {

    private final List<DispatchedOrder> mValues;
    private final Context mContext;

    public PartialDispatchedAdapter(List<DispatchedOrder> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public PartialDispatchedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_partial_dispatched, parent, false);
        return new PartialDispatchedAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PartialDispatchedAdapter.ViewHolder holder, int position) {
        holder.dispatchQuantity.setText(mValues.get(position).getDispatchQuantity() + "");
        holder.warehouse.setText("Warehouse: " + mValues.get(position).getWarehouseName());
        holder.productName.setText(mValues.get(position).getProductName());
        holder.date.setText(mValues.get(position).getMiti());
        holder.dispatchedValue.setText(mValues.get(position).getDispatchedValue());
        holder.orderQty.setText(mValues.get(position).getOrderQuantity() + "");
        holder.orderValue.setText(mValues.get(position).getOrderValue() + "");
        holder.pendingQuantity.setText(mValues.get(position).getPendingQuantity() + "");
        holder.pendingValue.setText(mValues.get(position).getPendingValue() + "");

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView orderValue;
        public final TextView dispatchedValue;
        public final TextView pendingValue;
        public final TextView orderQty;
        public final TextView dispatchQuantity;
        public final TextView pendingQuantity;
        public final TextView productName;
        public final TextView warehouse;
        public final TextView date;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            orderValue = (TextView) view.findViewById(R.id.orderValue);
            dispatchedValue = (TextView) view.findViewById(R.id.dispatchValue);
            pendingValue = (TextView) view.findViewById(R.id.pendingValue);
            orderQty = (TextView) view.findViewById(R.id.orderQty);
            dispatchQuantity = (TextView) view.findViewById(R.id.dispatchQty);
            pendingQuantity = (TextView) view.findViewById(R.id.pendingQty);
            productName = (TextView)view.findViewById(R.id.productName);
            date = (TextView) view.findViewById(R.id.orderDate);
            warehouse = (TextView) view.findViewById(R.id.warehouse);
        }

    }
}
