package com.cgtechnodreams.salesassistindia.fieldwork;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.OutletStatusEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.outlet.model.StockistStatus;
import com.cgtechnodreams.salesassistindia.photo.PhotoActivity;
import com.cgtechnodreams.salesassistindia.salesorder.CollectionActivity;
import com.cgtechnodreams.salesassistindia.salesorder.StockistDailySalesActivity;
import com.cgtechnodreams.salesassistindia.salesorder.StockistSalesOrderActivity;
import com.cgtechnodreams.salesassistindia.stock.StockDialog;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class StockistVisitFragment extends Fragment {

    boolean isOpen = false;
    boolean hasOrder =false;
    Stockist mStockist = null;

//    @BindView(R.id.radioStatus)
//    RadioGroup radioOutletStatus;
//    @BindView(R.id.radioOrderStatus)
//    RadioGroup radioOrderStatus;
//    @BindView(R.id.containerOrderStatus)
//    LinearLayout containerOrderStatus;
//    @BindView(R.id.btnPlaceOrder)
//    Button btnPlaceOrder;
    @BindView(R.id.cardAddOrder)
    CardView cardOrder;
//    @NotEmpty(messageId = R.string.field_required)
//    @BindView(R.id.etReason)
//    EditText etReason;
//    @BindView(R.id.btnConfirmOutletStatus)
//    Button btnConfirmOutletStatus;
//    @BindView(R.id.containerRemark)
//    LinearLayout containerRemark;
//    @BindView(R.id.btnSaveReason)
//    Button btnSaveReason;
//    @BindView(R.id.radioHasOrder)
//    RadioButton radioHasOrder;
//    @BindView(R.id.radioNoOrder)
//    RadioButton radioNoOrder;
//    @BindView(R.id.radioOpen)
//    RadioButton radioOpen;
//    @BindView(R.id.radioClosed)
//    RadioButton radioClosed;

    PrefManager prefManager = null;



    public StockistVisitFragment() {
        // Required empty public constructor
    }
    public static StockistVisitFragment newInstance(Stockist stockist) {
        StockistVisitFragment fragment = new StockistVisitFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.STOCKIST_DETAIL, stockist);
        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_outlet_visit, container, false);
        mStockist = (Stockist) getArguments().getSerializable(AppConstant.STOCKIST_DETAIL);
        getActivity().setTitle(mStockist.getName());
        prefManager = new PrefManager(getActivity());
        ButterKnife.bind(this, rootView);


//        radioOutletStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                switch (checkedId) {
//                    case R.id.radioOpen:
//                        containerOrderStatus.setVisibility(View.VISIBLE);
//                        btnConfirmOutletStatus.setVisibility(View.GONE);
//                        isOpen = true;
//                        break;
//                    case R.id.radioClosed:
//                        isOpen = false;
//                        containerOrderStatus.setVisibility(View.GONE);
//                        btnConfirmOutletStatus.setVisibility(View.VISIBLE);
//                        radioHasOrder.setChecked(false);
//                        radioNoOrder.setChecked(false);
//                        btnPlaceOrder.setVisibility(View.GONE);
//                        btnSaveReason.setVisibility(View.GONE);
//                        containerRemark.setVisibility(View.GONE);
//                        break;
//                }
//            }
//        });
//        radioOrderStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                switch (checkedId) {
//                    case R.id.radioHasOrder:
//                        btnPlaceOrder.setVisibility(View.VISIBLE);
//                        containerRemark.setVisibility(View.GONE);
//                        btnSaveReason.setVisibility(View.GONE);
//                        hasOrder = true;
//                        break;
//                    case R.id.radioNoOrder:
//                        hasOrder = false;
//                        containerRemark.setVisibility(View.VISIBLE);
//                        btnSaveReason.setVisibility(View.VISIBLE);
//                        btnPlaceOrder.setVisibility(View.GONE);
//                        break;
//                }
//            }
//        });
        return rootView;
    }

    @OnClick({R.id.cardAddOrder, R.id.cardOutletStock, R.id.cardPhoto,R.id.cardCollection,R.id.cardDailySales})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
//            case R.id.btnConfirmOutletStatus:
//                setOutletStatus("CLOSED");
//                radioOpen.setEnabled(false);
//                radioOpen.setClickable(false);
//                radioClosed.setEnabled(false);
//                radioClosed.setClickable(false);
//                btnConfirmOutletStatus.setEnabled(false);
//                btnConfirmOutletStatus.setClickable(false);
//                break;
//            case R.id.btnSaveReason:
//
//                if (!etReason.getText().toString().equalsIgnoreCase("")) {
//                    setOutletStatus("OPEN");
//                    radioOpen.setEnabled(false);
//                    radioClosed.setEnabled(false);
//                    containerOrderStatus.setVisibility(View.GONE);
//                }else{
//                    Toasty.info(getActivity(),"Please enter the reason",Toast.LENGTH_SHORT,true).show();
//                }
//                break;
            case R.id.cardAddOrder:

                Intent placeOrder = new Intent(getActivity(), StockistSalesOrderActivity.class);
                placeOrder.putExtra(AppConstant.STOCKIST_DETAIL, mStockist);
                startActivity(placeOrder);
                break;
            case R.id.cardDailySales:

                Intent dailySales = new Intent(getActivity(), StockistDailySalesActivity.class);
                dailySales.putExtra(AppConstant.STOCKIST_DETAIL, mStockist);
                startActivity(dailySales);
                break;
            case R.id.cardOutletStock:
                FragmentManager fm =(getActivity()).getSupportFragmentManager();
                StockDialog dialogFragment = new StockDialog().newInstance(mStockist);
                dialogFragment.show(fm, "dialog");
                break;
            case R.id.cardPhoto:
                Intent photo = new Intent(getActivity(), PhotoActivity.class);
                photo.putExtra(AppConstant.STOCKIST_DETAIL, mStockist);
                startActivity(photo);
                break;
            case R.id.cardCollection:
                Intent collection = new Intent(getActivity(), CollectionActivity.class);
                collection.putExtra(AppConstant.STOCKIST_DETAIL, mStockist);
                startActivity(collection);

        }

    }

//    private void setOutletStatus(String status) {
//        DistributorStatus outletStatus = new DistributorStatus();
//            outletStatus.setStatus(status);
//            if(status.equalsIgnoreCase("CLOSED")){
//                outletStatus.setOrderRemarks("Stockist Closed");
//                outletStatus.setOutletOrderStatus("No Order");
//            }else{
//                if(hasOrder){
//                    outletStatus.setOrderRemarks("Has Order");
//                    outletStatus.setOutletOrderStatus("Has Order");
//                }else{
//                    outletStatus.setOrderRemarks(etReason.getText().toString());
//                    outletStatus.setOutletOrderStatus("No Order");
//                }
//            }
//        outletStatus.setDistributorId(mStockist.getDistributorId());
//        outletStatus.setDate(Utils.getCurrentDate());
//        outletStatus.setLatitude(prefManager.getLatitude());
//        outletStatus.setLongitude(prefManager.getLongitude());
//
//        ObjectMapper Obj = new ObjectMapper();
//
//        JSONObject jsonObject = null;
//        try {
//
//            String jsonStr = Obj.writeValueAsString(outletStatus);
//            jsonObject= new JSONObject(jsonStr);
//        }
//
//        catch (IOException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        OutletStatusEntity outletStatusEntity = new OutletStatusEntity(getActivity());
//        long i = outletStatusEntity.insert(jsonObject);
//
//
//    }

    private void showOrderDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_status);
        dialog.show();

        Button btnDismiss = (Button) dialog.findViewById(R.id.btnDismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        // if decline button is clicked, close the custom dialog
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                dialog.dismiss();
            }
        });
    }

    private void showOutletStatusDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_status);
        dialog.show();
        RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radioStatus);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioOpen:
                        isOpen = true;
                        break;
                    case R.id.radioClosed:
                        isOpen = false;
                        break;
                }
            }
        });


        Button declineButton = (Button) dialog.findViewById(R.id.btnDismiss);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StockistStatus stockistStatus = new StockistStatus();
                if(isOpen)
                    stockistStatus.setStatus("OPEN");
                else
                    stockistStatus.setStatus("CLOSE");
                stockistStatus.setOutletId(mStockist.getDistributorId());
                stockistStatus.setDate(Utils.getCurrentDate());
                OutletStatusEntity outletStatusEntity = new OutletStatusEntity(getActivity());
                long i = outletStatusEntity.insert(stockistStatus);
                if(i>0){
                  //  @TODO set user activity and do needful function
                    //set user activity;
                }

                dialog.dismiss();
            }
        });
    }
}
