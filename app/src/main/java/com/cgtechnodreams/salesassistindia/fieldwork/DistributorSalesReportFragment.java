package com.cgtechnodreams.salesassistindia.fieldwork;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.SalesmanSalesDetailAdapter;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.data.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesReport;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.model.SalesReportResponse;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class DistributorSalesReportFragment extends Fragment {
    SalesmanSalesDetailAdapter  adapter= null;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.grandTotal)
    TextView grandTotal;
    @BindView(R.id.reportDate)
    TextView reportDate;
    PrefManager prefManager;
    Stockist mStockist = null;
    public DistributorSalesReportFragment() {
        // Required empty public constructor
    }
    public static DistributorSalesReportFragment newInstance(Stockist stockist) {
        DistributorSalesReportFragment fragment = new DistributorSalesReportFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("OutletDetail", stockist);
        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_outlet_sales_report, container, false);
        ButterKnife.bind(this,rootView);
        mStockist = (Stockist) getArguments().getSerializable("OutletDetail");
        prefManager = new PrefManager(getActivity());
        if(Utils.isConnectionAvailable(getActivity())){
            getSalesReport(mStockist.getDistributorId());
        }else{
            Toasty.info(getActivity(),"No internet connection to fetch Sales Report!!",Toast.LENGTH_SHORT,true).show();
        }
        return rootView;
    }

    private void getSalesReport(int outletId) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        SalesOrderDataService dataService = new SalesOrderDataService(getActivity());
        dataService.getOutletSalesOrderReport(prefManager.getAuthKey(),outletId, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                SalesReportResponse salesReport = (SalesReportResponse) response;
                if (salesReport.getStatusCode() != 200) {//failure case
                    Toasty.error(getActivity(), salesReport.getMessage(), Toast.LENGTH_SHORT,true).show();

                } else { //success case
                    if(salesReport.getSalesReportData().size()>0){
                        Toasty.success(getActivity(),  salesReport.getMessage(), Toast.LENGTH_SHORT,true).show();
                        setData(salesReport.getSalesReportData().get(0));
                    }else{
                        Toasty.info(getActivity(), "No Data Found!!", Toast.LENGTH_SHORT,true).show();
                    }
                    
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(SalesReport salesReportData) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapter = new SalesmanSalesDetailAdapter(getActivity(), salesReportData.getSalesDetail());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        grandTotal.setText("Grand Total " + salesReportData.getGrandTotal());
    }

}
