package com.cgtechnodreams.salesassistindia.fieldwork;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesorder.model.DOReceived;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReceivedOrderDialogFragment extends DialogFragment {
    @BindView(R.id.productName)
    TextView productName;
    @BindView(R.id.btnUpdate)
    public Button btnUpdate;
    @BindView(R.id.dispatchQuantity)
    TextView dispatchQuantity;
    @BindView(R.id.receivedQuantity)
    AppCompatEditText receivedQuantity;
    private DOReceived doReceived;
    public ReceivedOrderDialogFragment newInstance(DOReceived doReceived) {
        Bundle args = new Bundle();
        args.putSerializable("DOReceived", doReceived);
        this.setArguments(args);
        return this;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_received_order, container, false);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        if (getArguments() != null) {
            doReceived = (DOReceived) getArguments().getSerializable("DOReceived");
            dispatchQuantity.setText(doReceived.getDispatchQuantity());
            productName.setText(doReceived.getProductName());
            receivedQuantity.setText(doReceived.getDispatchQuantity());
        }

    }

    @OnClick(R.id.btnUpdate)
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                if (receivedQuantity.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please enter received quantity", Toast.LENGTH_SHORT).show();
                } else{
                    updateOrder(receivedQuantity.getText().toString());

//                    salesOrder.setQuantity(Integer.parseInt(productQuantity.getText().toString()));
//                    salesOrder.setTotalPrice(Double.parseDouble(productQuantity.getText().toString())*salesOrder.getUnitPrice());
//                    updateOrder(salesOrder);
                }
        }
    }

    private void updateOrder(String receivedQuantity) {
//        FMCGSalesOrderEntity salesOrderEntity = new FMCGSalesOrderEntity(getActivity());
//        salesOrderEntity.update(salesOrder);
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }
}
