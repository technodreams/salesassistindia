package com.cgtechnodreams.salesassistindia.fieldwork.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActualDispatchedResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<ActualDispatched> data;

    public ArrayList<ActualDispatched> getData() {
        return data;
    }

    public void setData(ArrayList<ActualDispatched> data) {
        this.data = data;
    }
}
