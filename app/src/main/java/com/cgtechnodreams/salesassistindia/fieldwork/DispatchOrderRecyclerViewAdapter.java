package com.cgtechnodreams.salesassistindia.fieldwork;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.fieldwork.model.ActualDispatched;

import java.util.List;

public class DispatchOrderRecyclerViewAdapter extends RecyclerView.Adapter<DispatchOrderRecyclerViewAdapter.ViewHolder> {

    private final List<ActualDispatched> mValues;
    private final Context mContext;

    public DispatchOrderRecyclerViewAdapter(List<ActualDispatched> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_dispatchorder, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.dispatchQuantity.setText(mValues.get(position).getQuantity());
        holder.warehouse.setText("Warehouse: " + mValues.get(position).getWarehouseName());
        holder.productName.setText(mValues.get(position).getProductName());
        holder.date.setText(mValues.get(position).getMiti());
        holder.unitPrice.setText(mValues.get(position).getProductPrice());
        holder.total.setText(mValues.get(position).getTotal());


//        holder.mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                    FragmentManager fm =((AppCompatActivity) mContext).getSupportFragmentManager();
//                    ReceivedOrderDialogFragment dialogFragment = new ReceivedOrderDialogFragment().newInstance(mValues.get(position));
//                    dialogFragment.show(fm, "dialog");
//
//                }
//           // }
//        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView total;
        public final TextView unitPrice;
        public final TextView dispatchQuantity;
        public final TextView productName;
        public final TextView warehouse;
        public final TextView date;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            total = (TextView) view.findViewById(R.id.total);
            dispatchQuantity = (TextView) view.findViewById(R.id.dispatchQty);
            unitPrice = (TextView) view.findViewById(R.id.unitPrice);
            productName = (TextView)view.findViewById(R.id.productName);
            date = (TextView) view.findViewById(R.id.orderDate);
            warehouse = (TextView) view.findViewById(R.id.warehouse);
        }

    }
}
