package com.cgtechnodreams.salesassistindia.fieldwork;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.fieldwork.model.ActualDispatched;
import com.cgtechnodreams.salesassistindia.fieldwork.model.ActualDispatchedResponse;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.data.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.salesorder.model.DOReceived;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class DispatchReceivedFragment extends Fragment {
    DispatchOrderRecyclerViewAdapter adapter;
    // TODO: Customize parameters
    private int mColumnCount = 1;
    Stockist mStockist;
    private OnListFragmentInteractionListener mListener;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.fromDate)
    EditText fromDate;
    @BindView(R.id.toDate)
    EditText toDate;
    PrefManager prefManager = null;
    Calendar from = Calendar.getInstance();
    int fromYear, fromMonth, fromDay;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public static DispatchReceivedFragment newInstance(Stockist stockist) {
        DispatchReceivedFragment fragment = new DispatchReceivedFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("OutletDetail", stockist);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @OnClick({R.id.fromDate, R.id.toDate,R.id.btnGenerateDispatchReceived})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.toDate:
                if(fromDate.getText().toString().equalsIgnoreCase("")){
                    Toasty.info(getActivity(),"Please select from date first", Toast.LENGTH_SHORT,true).show();
                }else{

                    Calendar now = Calendar.getInstance();
                    DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                            month = month +1;
                            String tempDay = day+"";
                            String tempMonth = month+"";
                            if(day<10){
                                tempDay = "0"+day;
                            }
                            if(month< 10){
                                tempMonth = "0"+month;
                            }
                            String date = year + "-" + (tempMonth ) + "-" + tempDay;
                            toDate.setText(date);

                        }
                    });
                    dpd.setVersion(DatePickerDialog.Version.VERSION_2);
                        dpd.setMaxDate(now);
                    //   dpd.setMinDate(dateConverter.get(fromYear,fromMonth,fromDay));
                    //   dateConverter.get
                    //@TODO Minimum date handle left
                    dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                    dpd.show(getFragmentManager(), "DatePicker");

                }

                // dpd.setMaxDate(dateConverter.getTodayNepaliDate());
                break;
            case R.id.fromDate:

                from = Calendar.getInstance();
                DatePickerDialog fromDPD = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {

                        month = month +1;
                        String tempDay = day+"";
                        String tempMonth = month+"";
                        if(day<10){
                            tempDay = "0"+day;
                        }
                        if(month< 10){
                            tempMonth = "0"+month;
                        }
                        String date = year + "-" + (tempMonth ) + "-" + tempDay;
                        fromDate.setText(date);
                    }
                });
                fromDPD.setMaxDate(from);
                fromDPD.setVersion(DatePickerDialog.Version.VERSION_2);
                fromDPD.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                fromDPD.show(getFragmentManager(), "DatePicker");
                break;
            case R.id.btnGenerateDispatchReceived:
                if(Utils.isConnectionAvailable(getActivity())){
                    if(fromDate.getText().toString().equalsIgnoreCase("") || toDate.getText().toString().equalsIgnoreCase("")){
                        Toasty.info(getActivity(), "Date missing",Toast.LENGTH_SHORT,true).show();
                    }else{
                        getDispatchedOrder(mStockist.getDistributorId(),fromDate.getText().toString(),toDate.getText().toString());
                    }

                }else{
                    Toasty.info(getActivity(),"No internet connection to fetch Sales Report!!", Toast.LENGTH_SHORT,true).show();
                }

        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dispatchorder_list, container, false);
        mStockist = (Stockist) getArguments().getSerializable("OutletDetail");
        prefManager = new PrefManager(getActivity());
        ButterKnife.bind(this,view);
        return view;
    }

    private void getDispatchedOrder(int outletId,String fromDate, String toDate) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        SalesOrderDataService dataService = new SalesOrderDataService(getActivity());
        dataService.getActualDispatched(prefManager.getAuthKey(),outletId,fromDate,toDate, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                ActualDispatchedResponse baseResponse = (ActualDispatchedResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(getActivity(), baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();

                } else { //success case
                    if(baseResponse.getData().size()>0){
                        Toasty.success(getActivity(),  baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                        setData(baseResponse.getData());
                    }else{
                        Toasty.info(getActivity(), "No Data Found!!", Toast.LENGTH_SHORT,true).show();
                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<ActualDispatched> data) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapter = new DispatchOrderRecyclerViewAdapter( data,getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DOReceived item);
    }
}
