package com.cgtechnodreams.salesassistindia.fieldwork;


import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.outlet.NewStockistActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;

/**
 * A simple {@link Fragment} subclass.
 */
public class StockistInfoFragment extends Fragment  implements OnMapReadyCallback {
    private static final String TAG = StockistInfoFragment.class.getSimpleName();
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;
    private LatLng mDefaultLocation = null;
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.tvStockistName)
    TextView tvStockistName;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.tvContactPerson)
    TextView tvContactPerson;
    @BindView(R.id.tvLandLineNumber)
    TextView tvLandLineNumber;
    @BindView(R.id.tvMobileNumber)
    TextView tvMobileNumber;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.tvPanNumber)
    TextView tvPanNumber;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.btnEdit)
    Button btnEdit;



    Stockist mStockist = null;
    public StockistInfoFragment() {
        // Required empty public constructor
    }
    public static StockistInfoFragment newInstance(Stockist stockist) {
        StockistInfoFragment fragment = new StockistInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.STOCKIST_DETAIL, stockist);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_outlet_info, container, false);
        ButterKnife.bind(this,rootView);
        mStockist = (Stockist) getArguments().getSerializable(AppConstant.STOCKIST_DETAIL);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setDetail(mStockist);
        return rootView;
    }

    private void setDetail(Stockist mStockist) {
        if(mStockist.getAddress()!= null)
            tvAddress.setText(mStockist.getAddress());
        else
            tvAddress.setText("");
        tvStockistName.setText(mStockist.getName());
        tvContactPerson.setText(mStockist.getContactPerson());
        tvMobileNumber.setText(mStockist.getMobileNumber());
        if(mStockist.getEmail()!= null)
            tvEmail.setText(mStockist.getEmail());
        else
            tvEmail.setText("");
        if(mStockist.getLandLine()!= null)
            tvLandLineNumber.setText(mStockist.getLandLine());
        else
            tvLandLineNumber.setText("");
        if(mStockist.getPanNumber()!= null)
            tvPanNumber.setText(mStockist.getPanNumber());
        else
            tvPanNumber.setText("");



    }

    @OnClick({R.id.btnEdit})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btnEdit:
                Intent intent = new Intent(getActivity(), NewStockistActivity.class);
                intent.putExtra(AppConstant.STOCKIST_DETAIL, mStockist);
                startActivity(intent);
                break;
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();

        CameraPosition camera = new CameraPosition.Builder()
                .target(new LatLng(Double.parseDouble(mStockist.getLatitude()),Double.parseDouble(mStockist.getLongitude())))
                .zoom(14)  //limite ->21
                .bearing(120) // 0 - 365
                .tilt(45) // limite ->90
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));

        mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(mStockist.getLatitude()),
                Double.parseDouble(mStockist.getLongitude())))
                .title("Stockist Location"));
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                        (FrameLayout) getActivity().findViewById(R.id.map), false);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());

                return infoWindow;
            }
        });


    }
}
