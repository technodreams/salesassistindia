package com.cgtechnodreams.salesassistindia.fieldwork.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DispatchedOrderResponse extends BaseResponse {
    @JsonProperty("data")
    private ArrayList<DispatchedOrder> data;

    public ArrayList<DispatchedOrder> getData() {
        return data;
    }

    public void setData(ArrayList<DispatchedOrder> data) {
        this.data = data;
    }
}
