package com.cgtechnodreams.salesassistindia.fieldwork;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.fieldwork.model.DispatchedOrder;
import com.cgtechnodreams.salesassistindia.fieldwork.model.DispatchedOrderResponse;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.salesmansalesorder.data.SalesOrderDataService;
import com.cgtechnodreams.salesassistindia.salesorder.model.DOReceived;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class PartialDispatchedFragment extends Fragment {
    PartialDispatchedAdapter adapter;
    // TODO: Customize parameters
    private int mColumnCount = 1;
    Stockist mStockist;
    private PartialDispatchedFragment.OnListFragmentInteractionListener mListener;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    PrefManager prefManager = null;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public static PartialDispatchedFragment newInstance(Stockist stockist) {
        PartialDispatchedFragment fragment = new PartialDispatchedFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("OutletDetail", stockist);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partial_dispatched, container, false);
        mStockist = (Stockist) getArguments().getSerializable("OutletDetail");
        prefManager = new PrefManager(getActivity());
        ButterKnife.bind(this,view);
        if(Utils.isConnectionAvailable(getActivity())){
            getDispatchedOrder(mStockist.getDistributorId());
        }else{
            Toasty.info(getActivity(),"No internet connection to fetch Sales Report!!", Toast.LENGTH_SHORT,true).show();
        }
        return view;
    }

    private void getDispatchedOrder(int outletId) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        SalesOrderDataService dataService = new SalesOrderDataService(getActivity());
        dataService.getPartialDispatchedOrder(prefManager.getAuthKey(),outletId, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                DispatchedOrderResponse baseResponse = (DispatchedOrderResponse) response;
                if (baseResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(getActivity(), baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();

                } else { //success case
                    if(baseResponse.getData().size()>0){
                        Toasty.success(getActivity(),  baseResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                        setData(baseResponse.getData());
                    }else{
                        Toasty.info(getActivity(), "No Data Found!!", Toast.LENGTH_SHORT,true).show();
                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<DispatchedOrder> data) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapter = new PartialDispatchedAdapter(data,getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DOReceived item);
    }
}
