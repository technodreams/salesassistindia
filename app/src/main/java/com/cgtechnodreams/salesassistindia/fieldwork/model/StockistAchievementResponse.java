package com.cgtechnodreams.salesassistindia.fieldwork.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StockistAchievementResponse extends BaseResponse {
    @JsonProperty("data")
    ArrayList<StockistAchievement> outletAcheivements;

    public ArrayList<StockistAchievement> getOutletAcheivements() {
        return outletAcheivements;
    }

    public void setOutletAcheivements(ArrayList<StockistAchievement> outletAcheivements) {
        this.outletAcheivements = outletAcheivements;
    }
}
