package com.cgtechnodreams.salesassistindia.fieldwork.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DispatchedOrder implements Serializable {
    @JsonProperty("id")
    private int id;
    @JsonProperty("code")
    private String code;
    @JsonProperty("date_np")
    private String miti;
    @JsonProperty("date")
    private String date;
    @JsonProperty("distributor_name")
    private String distributorName;
    @JsonProperty("distributor_id")
    private int distributorId;
    @JsonProperty("division_name")
    private String divisionName;
    @JsonProperty("division_id")
    private int divisonId;
    @JsonProperty("product_name")
    private String productName;
    @JsonProperty("product_id")
    private int productId;
    @JsonProperty("warehouse_id")
    private int warehouseId;
    @JsonProperty("warehouse_name")
    private String warehouseName;
    @JsonProperty("order_quantity")
    private int orderQuantity;
    @JsonProperty("order_value")
    private String orderValue;
    @JsonProperty("dispatch_quantity")
    private int dispatchQuantity;
    @JsonProperty("dispatch_value")
    private String dispatchedValue;
    @JsonProperty("pending_quantity")
    private int pendingQuantity;
    @JsonProperty("pending_value")
    private String pendingValue;
    @JsonProperty("reason_of_rejection")
    private String reasonOfRejection;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMiti() {
        return miti;
    }

    public void setMiti(String miti) {
        this.miti = miti;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public int getDivisonId() {
        return divisonId;
    }

    public void setDivisonId(int divisonId) {
        this.divisonId = divisonId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(String orderValue) {
        this.orderValue = orderValue;
    }

    public int getDispatchQuantity() {
        return dispatchQuantity;
    }

    public void setDispatchQuantity(int dispatchQuantity) {
        this.dispatchQuantity = dispatchQuantity;
    }

    public String getDispatchedValue() {
        return dispatchedValue;
    }

    public void setDispatchedValue(String dispatchedValue) {
        this.dispatchedValue = dispatchedValue;
    }

    public int getPendingQuantity() {
        return pendingQuantity;
    }

    public void setPendingQuantity(int pendingQuantity) {
        this.pendingQuantity = pendingQuantity;
    }

    public String getPendingValue() {
        return pendingValue;
    }

    public void setPendingValue(String pendingValue) {
        this.pendingValue = pendingValue;
    }

    public String getReasonOfRejection() {
        return reasonOfRejection;
    }

    public void setReasonOfRejection(String reasonOfRejection) {
        this.reasonOfRejection = reasonOfRejection;
    }
}
