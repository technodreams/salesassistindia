package com.cgtechnodreams.salesassistindia.fieldwork;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductEntity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.product.adapter.ProductAvailabilityAdapter;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAvailabilityActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    ProductAvailabilityAdapter adapter;
    ArrayList<Product> productList = null;
    Stockist mStockist = null;
    boolean isDamageStock = false;
    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_avaibility);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Product Stock");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mStockist = (Stockist)getIntent().getSerializableExtra("OutletDetail");
        int brandId = getIntent().getIntExtra("BrandId", 0);
        isDamageStock = getIntent().getBooleanExtra("IsDamage",false);
        date = getIntent().getStringExtra("Date");

        if(isDamageStock){
            getSupportActionBar().setTitle("Add Damage Stock");
        }
        ProductEntity productEntity = new ProductEntity(ProductAvailabilityActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) productEntity.findByBrand(brandId);


        try {
            productList = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Product>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(ProductAvailabilityActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new ProductAvailabilityAdapter(ProductAvailabilityActivity.this, productList, mStockist, isDamageStock,date);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }
}