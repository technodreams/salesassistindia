package com.cgtechnodreams.salesassistindia.fieldwork;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.fieldwork.model.StockistAchievement;
import com.cgtechnodreams.salesassistindia.fieldwork.model.StockistAchievementResponse;
import com.cgtechnodreams.salesassistindia.outlet.data.StockistDataService;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class DistributorTargetFragment extends Fragment {


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    PrefManager prefManager;
    Stockist mStockist = null;
    StockistTargetAdapter adapter;

    public DistributorTargetFragment() {
        // Required empty public constructor
    }
    public static DistributorTargetFragment newInstance(Stockist stockist) {
        DistributorTargetFragment fragment = new DistributorTargetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("OutletDetail", stockist);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_outlet_target, container, false);
        ButterKnife.bind(this,rootView);
        mStockist = (Stockist) getArguments().getSerializable("OutletDetail");

        prefManager = new PrefManager(getActivity());
        if(Utils.isConnectionAvailable(getActivity())){
            getTarget(mStockist.getDistributorId());
        }else{
            Toasty.info(getActivity(),"No internet connection!!",Toast.LENGTH_SHORT,true).show();
        }
        return rootView;
    }

    private void getTarget(int outletId) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        StockistDataService dataService = new StockistDataService(getActivity());
        dataService.getStockistAchievement(prefManager.getAuthKey(),outletId, new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                StockistAchievementResponse achievementResponse = (StockistAchievementResponse) response;
                if (achievementResponse.getStatusCode() != 200) {//failure case
                    Toasty.error(getActivity(), "Error: Error Code" + achievementResponse.getStatusCode(), Toast.LENGTH_SHORT,true).show();

                } else { //success case
                    if(achievementResponse.getOutletAcheivements().size()>0){
                        setData(achievementResponse.getOutletAcheivements());
                        Toasty.success(getActivity(), achievementResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    }else{
                        Toasty.info(getActivity(), "No Data Found!!", Toast.LENGTH_SHORT,true).show();
                    }

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

    private void setData(ArrayList<StockistAchievement> stockistAchievements) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapter = new StockistTargetAdapter(getActivity(), stockistAchievements);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

}
