package com.cgtechnodreams.salesassistindia.fieldwork;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.fieldwork.model.StockistAchievement;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StockistTargetAdapter extends RecyclerView.Adapter<StockistTargetAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<StockistAchievement> mData = new ArrayList<>();

    public StockistTargetAdapter(Context context, ArrayList<StockistAchievement> data) {
        this.mContext = context;
        this.mData = data;
    }

    @Override
    public StockistTargetAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_outlet_achievement, null);
        return new StockistTargetAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StockistTargetAdapter.MyViewHolder holder, int position) {
        holder.targetAmount.setText("Target Amt: " + mData.get(position).getAmount());
        holder.targetQty.setText("Target Quantity: " + mData.get(position).getQuantity());
        holder.year.setText("Year: " + mData.get(position).getYear() );
        holder.month.setText("Month: " + mData.get(position).getMonth() );
        holder.achievedQuantity.setText("Achieved Qty : " + mData.get(position).getAchieveQuantity() );
        holder.achievedAmount.setText("Achieved Amount : " + mData.get(position).getAchieveAmount() );
        holder.achievedAmountPercentage.setText("Achieved Amt % : " + mData.get(position).getAchieveAmountPercentage());
        holder.achievedQtyPercentage.setText("Achieved Qty % : " + mData.get(position).getGetAchieveQuantityPercentage());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.month)
        TextView month;
        @BindView(R.id.year)
        TextView year;

        @BindView(R.id.targetAmount)
        TextView targetAmount;
        @BindView(R.id.targetQty)
        TextView targetQty;

        @BindView(R.id.achievedAmount)
        TextView achievedAmount;
        @BindView(R.id.achievedQuantity)
        TextView achievedQuantity;
        @BindView(R.id.achievedAmountPercentage)
        TextView achievedAmountPercentage;
        @BindView(R.id.achievedQtyPercentage)
        TextView achievedQtyPercentage;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);



        }
    }
}


