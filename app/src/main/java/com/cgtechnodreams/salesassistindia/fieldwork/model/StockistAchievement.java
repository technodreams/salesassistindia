package com.cgtechnodreams.salesassistindia.fieldwork.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StockistAchievement implements Serializable {
    @JsonProperty("quantity")
    private int quantity;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("month")
    private String month;
    @JsonProperty("year")
    private int year;
    @JsonProperty("distributorId")
    private int distributorId;
    @JsonProperty("achieve_quantity")
    private String achieveQuantity;
    @JsonProperty("achieve_amount")
    private String achieveAmount;
    @JsonProperty("achieve_quantity_percentage")
    private String getAchieveQuantityPercentage;
    @JsonProperty("achieve_amount_percentage")
    private String achieveAmountPercentage;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getOutletId() {
        return distributorId;
    }

    public void setOutletId(int outletId) {
        this.distributorId = outletId;
    }

    public String getAchieveQuantity() {
        return achieveQuantity;
    }

    public void setAchieveQuantity(String achieveQuantity) {
        this.achieveQuantity = achieveQuantity;
    }

    public String getAchieveAmount() {
        return achieveAmount;
    }

    public void setAchieveAmount(String achieveAmount) {
        this.achieveAmount = achieveAmount;
    }

    public String getGetAchieveQuantityPercentage() {
        return getAchieveQuantityPercentage;
    }

    public void setGetAchieveQuantityPercentage(String getAchieveQuantityPercentage) {
        this.getAchieveQuantityPercentage = getAchieveQuantityPercentage;
    }

    public String getAchieveAmountPercentage() {
        return achieveAmountPercentage;
    }

    public void setAchieveAmountPercentage(String achieveAmountPercentage) {
        this.achieveAmountPercentage = achieveAmountPercentage;
    }
}
