package com.cgtechnodreams.salesassistindia.attendance.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AttendenceResponse extends BaseResponse {
    private AttendenceResponseData data;
    public AttendenceResponseData getData() {
        return data;
    }
    public void setData(AttendenceResponseData data) {
        this.data = data;
    }
}
