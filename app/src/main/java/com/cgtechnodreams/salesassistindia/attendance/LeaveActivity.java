package com.cgtechnodreams.salesassistindia.attendance;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.attendance.model.LeaveModel;
import com.cgtechnodreams.salesassistindia.attendance.model.PostLeaveResponse;
import com.cgtechnodreams.salesassistindia.home.MainActivity;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class LeaveActivity extends AppCompatActivity {
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.dateFrom)
    EditText etDateFrom;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.dateTo)
    EditText etDateTo;
    @BindView(R.id.leaveRemarks)
    EditText leaveRemarks;
    PrefManager prefManager;
    ProgressDialog dialog;
    @BindView(R.id.leaveName)
    Spinner leaveName;
    @BindView(R.id.leaveType)
    Spinner leaveType;
    String strLeaveType;
    String strLeaveName;
    Calendar cal;
    int day, month, year;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);

        ButterKnife.bind(this);
        prefManager = new PrefManager(this);
        toolbar.setTitle("Leave Request");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayAdapter aa = new ArrayAdapter(LeaveActivity.this, android.R.layout.simple_spinner_item, getResources().getTextArray(R.array.leave_name));
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        leaveName.setAdapter(aa);

        ArrayAdapter leaveTypeAdapter = new ArrayAdapter(LeaveActivity.this, android.R.layout.simple_spinner_item, getResources().getTextArray(R.array.leave_type));
        leaveTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        leaveType.setAdapter(leaveTypeAdapter);

        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        leaveType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strLeaveType = (String) parent.getItemAtPosition(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toasty.info(LeaveActivity.this, "Please select leave type", Toast.LENGTH_SHORT,true).show();
            }
        });
        leaveName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strLeaveName = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toasty.info(LeaveActivity.this, "Please select leave name", Toast.LENGTH_SHORT,true).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    @OnClick({R.id.dateFrom, R.id.dateTo, R.id.btnLeaveRequest})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.dateTo:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                            String date = year + "-" + (month + 1) + "-" + day;
                            etDateTo.setText(date);
                        }
                    });
                dpd.setVersion(DatePickerDialog.Version.VERSION_2);
                dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                dpd.show(getSupportFragmentManager(), "DatePicker");
            dpd.setMaxDate(cal);
                break;
            case R.id.dateFrom:
                Calendar from = Calendar.getInstance();
                DatePickerDialog fromDPD = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        String date = year + "-" + (month + 1) + "-" + day;
                        etDateFrom.setText(date);
                    }
                });
                fromDPD.setVersion(DatePickerDialog.Version.VERSION_2);
                fromDPD.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                fromDPD.show(getSupportFragmentManager(), "DatePicker");
                break;

            case R.id.btnLeaveRequest:
                if ((strLeaveType.equalsIgnoreCase("First Half") ||
                        strLeaveType.equalsIgnoreCase("Second Half")) &&
                        (!etDateFrom.getText().toString().equalsIgnoreCase(etDateTo.getText().toString()))) {
                    Toasty.info(LeaveActivity.this, "Date cannot be different for leave type half", Toasty.LENGTH_LONG,true).show();
                    return;
                } else if (strLeaveType.trim().contains("Choose") || strLeaveName.trim().contains("Choose")) {
                    Toasty.info(LeaveActivity.this, "Please select mandatory fields", Toasty.LENGTH_LONG,true).show();
                } else if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    if(Utils.isConnectionAvailable(LeaveActivity.this)){
                        postLeaveRequest();
                    }else{
                        Utils.getConnectionToast(LeaveActivity.this);
                    }

                }
                break;

        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void postLeaveRequest() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        final LeaveModel request = new LeaveModel();
        request.setLeaveStartDate(etDateFrom.getText().toString());
        request.setLeaveEndDate(etDateTo.getText().toString());
        request.setRemarks(leaveRemarks.getText().toString());
        request.setLeaveName(strLeaveName);
        request.setLeaveType(strLeaveType);
        AttendenceDataService leaveDataService = new AttendenceDataService(LeaveActivity.this);
        leaveDataService.postLeaveRequest(request, prefManager.getAuthKey(), new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                PostLeaveResponse leaveResponse = (PostLeaveResponse) response;
                if (leaveResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(LeaveActivity.this, "Leave", leaveResponse.getMessage());
                        Toasty.error(LeaveActivity.this, leaveResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                        if(leaveResponse.getStatusCode()== APIConstants.ERROR_UNAUTHORIZED ){
                            Intent intent = new Intent(LeaveActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            PrefManager preferenceManager = new PrefManager(LeaveActivity.this);
                            preferenceManager.clear();
                            startActivity(intent);
                            finish();
                    }
                } else { //success case
                    LogUtils.setUserActivity(LeaveActivity.this, "Leave", "Leave Request Successfull");
                    Toasty.success(LeaveActivity.this,leaveResponse.getMessage(),Toast.LENGTH_SHORT,true).show();
                    Intent intent = new Intent(LeaveActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }
}
