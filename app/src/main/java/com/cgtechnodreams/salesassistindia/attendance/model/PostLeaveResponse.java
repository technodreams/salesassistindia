package com.cgtechnodreams.salesassistindia.attendance.model;

import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PostLeaveResponse extends BaseResponse {

}
