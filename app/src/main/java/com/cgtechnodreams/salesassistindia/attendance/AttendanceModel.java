package com.cgtechnodreams.salesassistindia.attendance;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by user on 06/01/2019.
 */

public class AttendanceModel implements Serializable{
    @JsonProperty("type")
    String direction;
    @JsonProperty("latitude")
    String latitude;
    @JsonProperty("longitude")
    String longitude;
    @JsonProperty("image")
    String photo;
    @JsonProperty("remarks")
    String remarks;

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
