package com.cgtechnodreams.salesassistindia.attendance;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.attendance.model.AttendenceResponse;
import com.cgtechnodreams.salesassistindia.attendance.model.LeaveModel;
import com.cgtechnodreams.salesassistindia.attendance.model.PostLeaveResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendenceDataService {

        private Context mContext;

        public AttendenceDataService(Context mContext) {
            this.mContext = mContext;
        }

        public void userAttendence(AttendanceModel model, String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
            Call<AttendenceResponse> callBack = Api.getService(mContext).userAttendence(model, token);
            callBack.enqueue(new Callback<AttendenceResponse>() {
                @Override
                public void onResponse(Call<AttendenceResponse> call, Response<AttendenceResponse> response) {
                    if (response.isSuccessful()) {
                        onSuccessListener.onSuccess(response.body());
                    }
                    else{
                        onErrorListener.onFailed(ErrorUtils.parseError(response));
                        ErrorHandler.handleSyncError(mContext,response.code(),response.message());

                    }
                }

                @Override
                public void onFailure(Call<AttendenceResponse> call, Throwable t) {
                    t.printStackTrace();
                    ErrorHandler.httpRequestFailure(mContext,t);
                    ErrorModel errorModel = new ErrorModel();
                    errorModel.setErrors(t.toString());
                    errorModel.setMessage("Error");
                    onErrorListener.onFailed(errorModel);

                }
            });
        }
        public void postLeaveRequest(LeaveModel model, String token, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<PostLeaveResponse> callBack = Api.getService(mContext).postLeave(model, token);
        callBack.enqueue(new Callback<PostLeaveResponse>() {
            @Override
            public void onResponse(Call<PostLeaveResponse> call, Response<PostLeaveResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    onErrorListener.onFailed(ErrorUtils.parseError(response));

                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());

                }
            }

            @Override
            public void onFailure(Call<PostLeaveResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorHandler.httpRequestFailure(mContext,t);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);

            }
        });
    }
}