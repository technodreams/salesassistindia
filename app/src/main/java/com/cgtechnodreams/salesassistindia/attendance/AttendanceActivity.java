package com.cgtechnodreams.salesassistindia.attendance;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.api.APIConstants;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.attendance.model.AttendenceResponse;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.login.LoginActivity;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.CameraUtils;
import com.cgtechnodreams.salesassistindia.utils.ImageUtils;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;;

import static com.cgtechnodreams.salesassistindia.utils.AppConstant.MEDIA_TYPE_IMAGE;

public class AttendanceActivity extends AppCompatActivity {
    @BindView(R.id.imgSelfie)
    ImageView selfieImage;
    @BindView(R.id.checkIn)
    RadioButton checkIn;
    @BindView(R.id.checkOut)
    RadioButton checkOut;
    String picture = null;
    String directionType = "";
    boolean isDirectionChecked = false;
    @BindView(R.id.attendanceRemarks)
    EditText attendenceRemarks;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    // Bitmap sampling size
    public static final int BITMAP_SAMPLE_SIZE = 8;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 101;
    private static String imageStoragePath;
    PrefManager prefManager;
    LocationUtils locationUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        ButterKnife.bind(this);
        toolbar.setTitle("Daily Attendence");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prefManager = new PrefManager(this);
        locationUtils = new LocationUtils(AttendanceActivity.this);

        locationUtils.requestLocationUpdates();

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        isDirectionChecked = checked;

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.checkIn:
                if (checked)
                    directionType = "check_in";
                   // Toasty.makeText(AttendanceActivity.this, "Check In", Toast.LENGTH_SHORT,true).show();
                break;
            case R.id.checkOut:
                if (checked)
                    directionType = "check_out";
                  //  Toasty.makeText(AttendanceActivity.this, "Check Out", Toast.LENGTH_SHORT,true).show();
                break;
        }
    }

    @OnClick({R.id.cameraImage, R.id.btnAttendance})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.cameraImage:
                if (CameraUtils.checkPermissions(getApplicationContext())) {
                    captureImage();
                } else {
                    requestCameraPermission(MEDIA_TYPE_IMAGE);
                }


                break;
            case R.id.btnAttendance:
               // if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    if(!isDirectionChecked){
                        Toasty.info(AttendanceActivity.this,"Please select the direction!!", Toast.LENGTH_SHORT,true ).show();
                       // Toasty.makeText(AttendanceActivity.this,"Please select the direction!!",Toast.LENGTH_SHORT,true).show();
                        return;
                    }
                    if (picture != null) {


                        if (!((LocationManager) this.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                            Toasty.info(AttendanceActivity.this, "Please turn on your gps.", Toast.LENGTH_SHORT,true).show();
                            LocationUtils locationUtils = new LocationUtils(AttendanceActivity.this);
                            locationUtils.requestLocationUpdates(null);
                        }else{
                            if(Utils.isConnectionAvailable(AttendanceActivity.this)){
                                updateAttendance(directionType, picture,attendenceRemarks.getText().toString());
                            }else{
                                Utils.getConnectionToast(AttendanceActivity.this);
                            }

                        }

                    } else {
                        Toasty.info(getApplicationContext(), "Please take your selfie photo", Toast.LENGTH_SHORT,true).show();
                    }

               // }
                break;
        }

    }
    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == AppConstant.MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage();
                            }
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }
    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = CameraUtils.getOutputMediaFile(AppConstant.MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
    /**
     * Activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Refreshing the gallery
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);

                // successfully captured the image
                // display it in image view
                downSizeImage(imageStoragePath);
                previewCapturedImage();

            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toasty.info(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT, true)
                        .show();
            } else {
                // failed to capture image
                Toasty.error(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT, true)
                        .show();
            }
        }
    }

    private void downSizeImage(String imageStoragePath) {
        ByteArrayOutputStream byteArrayOutputStream = Utils.compressImage(imageStoragePath);
        try(OutputStream outputStream = new FileOutputStream(imageStoragePath)) {
            byteArrayOutputStream.writeTo(outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Display image from gallery
     */
    private void previewCapturedImage() { // bimatp factory
        try{
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 1;
            // fileUri = Uri.parse(mCurrentPhotoPath);
            //    File file = new File(fileUri.getPath());
            final Bitmap bitmap = BitmapFactory.decodeFile(Uri.parse(imageStoragePath).getPath(),
                    options);

            selfieImage.setImageBitmap(bitmap);
            picture = ImageUtils.getBase64Image(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(AttendanceActivity.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    private void updateAttendance(String directionType, String picture, String remarks) {


        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Processing your request...");
        dialog.setCancelable(false);
        dialog.show();
        LocationUtils locationUtils = new LocationUtils(AttendanceActivity.this);
        locationUtils.requestLocationUpdates(null);
        final AttendanceModel model = new AttendanceModel();
        model.setDirection(directionType);
        model.setPhoto(picture);
        model.setRemarks(remarks);
        model.setLatitude(prefManager.getLatitude());
        model.setLongitude(prefManager.getLongitude());
        AttendenceDataService attendenceService = new AttendenceDataService(AttendanceActivity.this);
        attendenceService.userAttendence(model,prefManager.getAuthKey(), new OnSuccess() {
            @Override
            public void onSuccess(Object response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                AttendenceResponse attendenceResponse = (AttendenceResponse) response;
                if (attendenceResponse.getStatusCode() != 200) {//failure case
                    LogUtils.setUserActivity(AttendanceActivity.this, "Attendance", attendenceResponse.getMessage());
                    Toasty.error(AttendanceActivity.this, attendenceResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    if(attendenceResponse.getStatusCode()== APIConstants.ERROR_UNAUTHORIZED ){
                        Intent intent = new Intent(AttendanceActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PrefManager preferenceManager = new PrefManager(AttendanceActivity.this);
                        preferenceManager.clear();
                        startActivity(intent);
                        finish();
                    }

                } else { //success case
                    Toasty.success(AttendanceActivity.this, attendenceResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
                    LogUtils.setUserActivity(AttendanceActivity.this ,"Attendance", "Attendance " + model.getDirection());
                    finish();

                }

            }
        }, new OnFailedListener() {
            @Override
            public void onFailed(ErrorModel reason) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toasty.error(AttendanceActivity.this,reason.getErrors() + " " + reason.getMessage(),Toast.LENGTH_SHORT,true).show();

            }
        });

    }
}
