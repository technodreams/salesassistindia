package com.cgtechnodreams.salesassistindia.dailysales;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySales;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DailySalesListAdapter extends RecyclerView.Adapter<DailySalesListAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<DailySales> dailySales = new ArrayList<>();

    public DailySalesListAdapter(Context context, ArrayList<DailySales> dailySales) {
        this.mContext = context;
        this.dailySales = dailySales;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public DailySalesListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_daily_sales_list, null);
        return new DailySalesListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        DailySales eachSales = dailySales.get(position);
        holder.name.setText("Customer Name: " + eachSales.getCustomerName());
        holder.mobileNumber.setText("Mobile Number: " + eachSales.getCustomerMobile());
        holder.quantity.setText("Date: " + eachSales.getDate());
        holder.address.setText("Address: " + eachSales.getCustomerAddress());
    }



    @Override
    public int getItemCount() {
        return dailySales.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.number)
        TextView mobileNumber;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}