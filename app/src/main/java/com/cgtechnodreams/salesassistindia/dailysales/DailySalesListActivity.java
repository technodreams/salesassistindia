package com.cgtechnodreams.salesassistindia.dailysales;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySales;
import com.cgtechnodreams.salesassistindia.sync.Sync;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class DailySalesListActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    DailySalesListAdapter adapter;
    ProgressBar dialog;
    PrefManager prefManager;
    ArrayList<DailySales> dailySales = new ArrayList<DailySales>();

    private static final int REQUEST_CODE_ORDER_UPDATE = 103;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_sales_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        prefManager = new PrefManager(this);
        if (!((LocationManager) this.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            Toasty.info(DailySalesListActivity.this, "Please turn on your gps.", Toast.LENGTH_SHORT,true).show();
            LocationUtils locationUtils = new LocationUtils(DailySalesListActivity.this);
            locationUtils.requestLocationUpdates(null);
        }

        getOrderList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrderList();

    }

    private void getOrderList() {
        Sync sync = new Sync(DailySalesListActivity.this);

        dailySales =  sync.readDailySales();

        if(dailySales !=null){
            LinearLayoutManager favouriteListManager = new LinearLayoutManager(DailySalesListActivity.this, LinearLayoutManager.VERTICAL, false);
            adapter = new DailySalesListAdapter(DailySalesListActivity.this, dailySales);
            recyclerView.setLayoutManager(favouriteListManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);

            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(DailySalesListActivity.this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Intent intent = new Intent(DailySalesListActivity.this, DailySalesActivity.class);
                            intent.putExtra("EachSales",dailySales.get(position));

                            startActivity(intent);
                            //finish();
                        }
                        @Override
                        public void onLongItemClick(View view, int position) {
                            // do whatever
                        }
                    }));
        }else{

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
