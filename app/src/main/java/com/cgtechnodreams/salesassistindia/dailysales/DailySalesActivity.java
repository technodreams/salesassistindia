package com.cgtechnodreams.salesassistindia.dailysales;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySales;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DailySalesEntity;
import com.cgtechnodreams.salesassistindia.databasehelper.model.DailySalesModel;
import com.cgtechnodreams.salesassistindia.product.SearchProductActivity;
import com.cgtechnodreams.salesassistindia.product.model.Product;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

;

public class DailySalesActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_SEARCH_PRODUCT = 100;
    private static final int REQUEST_CODE_SEARCH_GROUP = 101;
//    @BindView(R.id.product)
//    EditText etProduct;
    //    @BindView(R.id.productGroup)
//    EditText etProductGroup;
//    @BindView(R.id.mrp)
//    EditText mrp;
//    @BindView(R.id.mop)
//    EditText mop;
Menu menu;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.qty)
    EditText quantity;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.customerAddress)
    EditText etAddress;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.customerName)
    EditText etName;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.customerNumber)
    EditText etMobileNumber;
    @BindView(R.id.dynamicContainer)
    LinearLayout dynamicContainer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.date)
    EditText date;
    Product selectedProduct = null;
    PrefManager prefManager;
    ArrayList<DailySales> dailySalesList = new ArrayList<>();
    private List<EditText> dynamicSerailEditText;
    private List<EditText> dynamicSellingPriceEditText = null;
    private List<EditText> dynamicProduct;
//    private List<Integer> dynamicProductId;
    private ArrayList<Product> productArrayList;
    private EditText tempEditText = null;
    private EditText tempEditProduct = null;
    Calendar cal;
    int year,month,day;
    DailySales eachSales = null;
    LocationUtils locationUtils;
    boolean isUpdate = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_sales);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setTitle("Daily Sales");
        prefManager = new PrefManager(this);
        eachSales = (DailySales) getIntent().getSerializableExtra("EachSales");
        locationUtils = new LocationUtils(DailySalesActivity.this);
        locationUtils.requestLocationUpdates();

        if(eachSales!=null){
            isUpdate = true;
            setData(eachSales);

        }

        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
       // FormValidator.startLiveValidation(this, findViewById(R.id.container), new SimpleErrorPopupCallback(this, false));
    }

    private void setData(DailySales eachSales) {
        etName.setText(eachSales.getCustomerName());
        etAddress.setText(eachSales.getCustomerAddress());
        etMobileNumber.setText(eachSales.getCustomerMobile());
        date.setText(eachSales.getDate());
        quantity.setText(eachSales.getQty() + "");
        setProductDetail(eachSales);
    }

    private void setProductDetail(DailySales eachSales) {
        dynamicSerailEditText = new ArrayList<EditText>();
        dynamicSellingPriceEditText = new ArrayList<EditText>();
        dynamicProduct = new ArrayList<EditText>();
        //   dynamicProductId = new ArrayList<Integer>();
        productArrayList = new ArrayList<Product>();

        if (dynamicContainer.getChildCount() > 0) ;
        dynamicContainer.removeAllViews();
        for (int i = 0; i < eachSales.getSalesDetail().size(); i++) {
            if(isUpdate){
                Product product = new Product();
                product.setProductId(Integer.parseInt(eachSales.getSalesDetail().get(i).getProductId()));
                product.setProductName(eachSales.getSalesDetail().get(i).getModelNumber());
                productArrayList.add(i,product);
            }
            LinearLayout.LayoutParams productParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            EditText etProduct = new EditText(this);
            etProduct.setHint("Select Product");
            etProduct.setText(eachSales.getSalesDetail().get(i).getModelNumber());
            int padding = (int) Utils.convertDpToPixel(DailySalesActivity.this,8);
            etProduct.setPadding(padding, padding,padding,padding);
//            etProduct.setInputType(InputType.TYPE_CLASS_NUMBER);
            etProduct.setTag(i);
            etProduct.setBackground(getResources().getDrawable(R.drawable.bg_editext_border));
            etProduct.setFocusable(false);
            etProduct.setLayoutParams(productParam);
            dynamicProduct.add(etProduct);

            etProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tempEditProduct = dynamicProduct.get(Integer.parseInt(view.getTag().toString()));

                    Intent productIntent = new Intent(DailySalesActivity.this, SearchProductActivity.class);
                    productIntent.putExtra("Index",Integer.parseInt(view.getTag().toString()));
                    startActivityForResult(productIntent, REQUEST_CODE_SEARCH_PRODUCT);
                }
            });
            dynamicContainer.addView(etProduct);
            LinearLayout linearLayout = new LinearLayout(this);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            linearLayout.setWeightSum(1);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);

            LinearLayout.LayoutParams paramEditText = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
            EditText editText = new EditText(this);
            editText.setHint("Serial Number");
            editText.setPadding(padding,padding,padding,padding);
            editText.setTag("et" + i);
            editText.setText(eachSales.getSalesDetail().get(i).getSerialNumber());
            editText.setLayoutParams(paramEditText);
            linearLayout.addView(editText);
            dynamicSerailEditText.add(editText);

            LinearLayout.LayoutParams paramBtn = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
            Button btn = new Button(this);
            btn.setTag(i);
            btn.setText("Scan");
            btn.setLayoutParams(paramBtn);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tempEditText = dynamicSerailEditText.get(Integer.parseInt(view.getTag().toString()));
                    IntentIntegrator integrator = new IntentIntegrator(DailySalesActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                    integrator.setPrompt("Scan a barcode");
                    integrator.setCameraId(0);  // Use a specific camera of the device
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(true);
                    integrator.initiateScan();
                    Toasty.info(DailySalesActivity.this, editText.getText().toString(), Toast.LENGTH_SHORT,true).show();
                }

            });
            linearLayout.addView(btn);
            dynamicContainer.addView(linearLayout);
            LinearLayout.LayoutParams sellinngPrice = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            EditText etSellingPrice = new EditText(this);
            etSellingPrice.setHint("Selling Price");
            etSellingPrice.setInputType(InputType.TYPE_CLASS_NUMBER);
            etSellingPrice.setTag("sp" + i);
            etSellingPrice.setPadding(padding,padding,padding,padding);
            etSellingPrice.setText(eachSales.getSalesDetail().get(i).getSellingPrice());
            etSellingPrice.setLayoutParams(sellinngPrice);
            dynamicSellingPriceEditText.add(etSellingPrice);
            dynamicContainer.addView(etSellingPrice);
        }

    }
    @OnClick({R.id.btnSerialNumber,R.id.date})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSerialNumber:
                if (quantity.getText().toString().equals("")) {
                    Toasty.info(DailySalesActivity.this, "You need to add quantity first!!", Toast.LENGTH_SHORT,true).show();
                } else {
                    int qty = Integer.parseInt(quantity.getText().toString());
                    createSerialNumberView(qty);
                }
                break;

            case R.id.date:

              DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        String selectedDate = year + "-" + (month + 1) + "-" + day;
                        date.setText(selectedDate);
                    }
                });
                dpd.setVersion(DatePickerDialog.Version.VERSION_2);
                dpd.setMaxDate(cal);
                dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
                dpd.show(getSupportFragmentManager(), "DatePicker");
                break;
        }

    }

    private void saveDailySales() {
        DailySalesEntity dailySalesEntity = new DailySalesEntity();
        dailySalesEntity.setDate(date.getText().toString());
        dailySalesEntity.setCustomerAddress(etAddress.getText().toString());
        dailySalesEntity.setCustomerMobile(etMobileNumber.getText().toString());
        dailySalesEntity.setCustomerName(etName.getText().toString());
        int qty = Integer.parseInt(quantity.getText().toString());
        dailySalesEntity.setQuantity(qty);
        DailySalesModel dailySalesModel = new DailySalesModel(DailySalesActivity.this);
        //ArrayList<DailySalesDetailEntity> salesDetailList = new ArrayList<>();
        JSONArray array = new JSONArray();
        JSONObject obj = null;
        for (int i = 0; i < qty; i++) {
            obj = new JSONObject();
            try {
                if(dynamicProduct.size()!=qty){
                    Toasty.info(DailySalesActivity.this,
                            "Quantity and Product detail mismatch! Please press add product detail button",
                            Toast.LENGTH_SHORT,true).show();
                    return;
                }
                else if(dynamicProduct == null || dynamicSerailEditText == null || dynamicSellingPriceEditText == null){
                    Toasty.info(DailySalesActivity.this, "Please press add form detail button",Toast.LENGTH_SHORT,true).show();
                    return;
                }
                else if( TextUtils.isEmpty(dynamicProduct.get(i).getText())){
                    dynamicProduct.get(i).setError( "First name is required!" );
                    return;
                }else if( TextUtils.isEmpty(dynamicSellingPriceEditText.get(i).getText())){
                    dynamicSellingPriceEditText.get(i).setError( "First name is required!" );
                    return;
                }else if( TextUtils.isEmpty(dynamicSerailEditText.get(i).getText())){
                    dynamicSerailEditText.get(i).setError( "First name is required!" );
                    return;
                }else{
                    obj.put("model_number",dynamicProduct.get(i).getText());
                    obj.put("product_id",productArrayList.get(i).getProductId());
                    obj.put("selling_price", (dynamicSellingPriceEditText.get(i).getText().toString()));
                    obj.put("serial_number", dynamicSerailEditText.get(i).getText().toString());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            array.put(obj);
        }

        dailySalesEntity.setDetail(array.toString());
        if(isUpdate){
            dailySalesModel.update(dailySalesEntity,eachSales.getId());
            finish();
        }else{
            int salesId = (int) dailySalesModel.insert(dailySalesEntity);
            if (salesId > 0) {
                dynamicContainer.removeAllViews();
                quantity.setText("");
                Toasty.info(DailySalesActivity.this,"Sales record inserted! your sales id is " + salesId, Toast.LENGTH_SHORT,true).show();
                finish();
            }
        }

        //DailySalesDetailModel dailySalesDetailModel = new DailySalesDetailModel(DailySalesActivity.this);
       // dailySalesDetailModel.insert(salesDetailList);
       // Toasty.makeText(DailySalesActivity.this,"Daily Sales Saved Sucessfully!", Toast.LENGTH_SHORT,true).show();
        LogUtils.setUserActivity(DailySalesActivity.this, "Daily Sales", "Daily Sales Saved");


    }
//    private void postDailySales(ArrayList<DailySales> dailySales) {
//
//        ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Processing your request...");
//        dialog.setCancelable(false);
//        dialog.show();
////        LocationUtils locationUtils = new LocationUtils(DailySalesActivity.this);
////        locationUtils.requestLocationUpdates(null);
//        DailySalesDataService dailySalesDataService = new DailySalesDataService(DailySalesActivity.this);
//        dailySalesDataService.postDailySales(prefManager.getAuthKey(), dailySales, new OnSuccess() {
//            @Override
//            public void onSuccess(Object response) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
//                DailySalesResponse dailySalesResponse = (DailySalesResponse) response;
//                if (dailySalesResponse.getStatusCode() != 200) {//failure case
//                    Toasty.makeText(DailySalesActivity.this, dailySalesResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                    finish();
//                } else { //success case
//                    Toasty.makeText(DailySalesActivity.this, dailySalesResponse.getMessage(), Toast.LENGTH_SHORT,true).show();
//                }
//
//            }
//        }, new OnFailedListener() {
//            @Override
//            public void onFailed(ErrorModel reason) {
//                if (dialog.isShowing()) {
//                    dialog.dismiss();
//                }
//
//            }
//        });
//    }
    private void createSerialNumberView(int qty) {
        dynamicSerailEditText = new ArrayList<EditText>();
        dynamicSellingPriceEditText = new ArrayList<EditText>();
        dynamicProduct = new ArrayList<EditText>();
     //   dynamicProductId = new ArrayList<Integer>();
        productArrayList = new ArrayList<Product>();
        if (dynamicContainer.getChildCount() > 0) ;
        dynamicContainer.removeAllViews();
        for (int i = 0; i < qty; i++) {
            LinearLayout.LayoutParams productParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            EditText etProduct = new EditText(this);
            etProduct.setHint("Select Product");
//            etProduct.setInputType(InputType.TYPE_CLASS_NUMBER);
            etProduct.setTag(i);
            etProduct.setBackground(getResources().getDrawable(R.drawable.bg_editext_border));
            etProduct.setFocusable(false);
            etProduct.setLayoutParams(productParam);
            dynamicProduct.add(etProduct);
            etProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tempEditProduct = dynamicProduct.get(Integer.parseInt(view.getTag().toString()));

                    Intent productIntent = new Intent(DailySalesActivity.this, SearchProductActivity.class);
                    productIntent.putExtra("Index",Integer.parseInt(view.getTag().toString()));
                    startActivityForResult(productIntent, REQUEST_CODE_SEARCH_PRODUCT);
                }
            });
            dynamicContainer.addView(etProduct);
            LinearLayout linearLayout = new LinearLayout(this);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            linearLayout.setWeightSum(1);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);

            LinearLayout.LayoutParams paramEditText = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
            EditText editText = new EditText(this);
            editText.setHint("Serial Number");
            editText.setTag("et" + i);
            editText.setLayoutParams(paramEditText);
            linearLayout.addView(editText);
            dynamicSerailEditText.add(editText);

            LinearLayout.LayoutParams paramBtn = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
            Button btn = new Button(this);
            btn.setTag(i);
            btn.setText("Scan");
            btn.setLayoutParams(paramBtn);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tempEditText = dynamicSerailEditText.get(Integer.parseInt(view.getTag().toString()));
                    IntentIntegrator integrator = new IntentIntegrator(DailySalesActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                    integrator.setPrompt("Scan a barcode");
                    integrator.setCameraId(0);  // Use a specific camera of the device
                    integrator.setBeepEnabled(false);
                    integrator.setBarcodeImageEnabled(true);
                    integrator.initiateScan();
                    Toasty.info(DailySalesActivity.this, editText.getText().toString(), Toast.LENGTH_SHORT,true).show();
                }

            });
            linearLayout.addView(btn);
            dynamicContainer.addView(linearLayout);
            LinearLayout.LayoutParams sellinngPrice = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            EditText etSellingPrice = new EditText(this);
            etSellingPrice.setHint("Selling Price");
            etSellingPrice.setInputType(InputType.TYPE_CLASS_NUMBER);
            etSellingPrice.setTag("sp" + i);
            etSellingPrice.setLayoutParams(sellinngPrice);
            dynamicSellingPriceEditText.add(etSellingPrice);
            dynamicContainer.addView(etSellingPrice);
        }

    }
    // Get the results:
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

       // this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_daily_sales, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
         /*   case R.id.action_edit:
                editSalesDetail();
                break;*/
            case R.id.action_save:
                if (FormValidator.validate(this, new SimpleErrorPopupCallback(this, true))) {
                    if(dynamicProduct != null)
                        saveDailySales();
                    else
                        Toasty.info(DailySalesActivity.this, "Please Enter Product Detail!!",Toast.LENGTH_SHORT,true).show();

                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    private void editSalesDetail() {
        Intent i = new Intent(DailySalesActivity.this,DailySalesListActivity.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toasty.info(this, "Cancelled", Toasty.LENGTH_LONG).show();
            } else {
                tempEditText.setText(result.getContents());
                Toasty.info(this, "Scanned: " + result.getContents(), Toasty.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
//        if (requestCode == REQUEST_CODE_SEARCH_GROUP) {
//            if(resultCode == Activity.RESULT_OK){
//                ProductGroup group = (ProductGroup) data.getSerializableExtra("Result");
//                etProductGroup.setText(group.getName());
//            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//                //Write your code if there's no result
//            }
//        }else
        if (requestCode == REQUEST_CODE_SEARCH_PRODUCT) {
            if (resultCode == Activity.RESULT_OK) {
                selectedProduct = (Product) data.getSerializableExtra("Result");

                int index = data.getExtras().getInt("Index");
                productArrayList.add(index,selectedProduct);
                tempEditProduct.setText(selectedProduct.getProductName());
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }


}
