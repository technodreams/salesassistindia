package com.cgtechnodreams.salesassistindia.dailysales;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dailysales.adapter.CompetitorSalesAdapter;
import com.cgtechnodreams.salesassistindia.databasehelper.model.BrandEnity;
import com.cgtechnodreams.salesassistindia.product.model.Brand;
import com.cgtechnodreams.salesassistindia.utils.RecyclerItemClickListener;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompetitorSalesActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    CompetitorSalesAdapter adapter;
    ArrayList<Brand> brandArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competitor_sales);
        ButterKnife.bind(this);
        toolbar.setTitle("Competitor Brand");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        BrandEnity brandEnity = new BrandEnity(CompetitorSalesActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) brandEnity.getBrandByCompetitor(1);


        try {
            brandArrayList = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<Brand>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager favouriteListManager = new LinearLayoutManager(CompetitorSalesActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new CompetitorSalesAdapter(CompetitorSalesActivity.this, brandArrayList);
        recyclerView.setLayoutManager(favouriteListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(CompetitorSalesActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(CompetitorSalesActivity.this,CompetitorSalesGroupActivity.class);
                        intent.putExtra( "Id",brandArrayList.get(position).getId());
                        intent.putExtra("BrandName",brandArrayList.get(position).getName());
                        startActivity(intent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                }));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
