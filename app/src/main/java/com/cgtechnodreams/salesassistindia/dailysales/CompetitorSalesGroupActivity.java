package com.cgtechnodreams.salesassistindia.dailysales;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dailysales.adapter.CompetitorSalesDetailAdapter;
import com.cgtechnodreams.salesassistindia.dailysales.model.CompetitorSalesProductGroupModel;
import com.cgtechnodreams.salesassistindia.product.model.ProductGroup;
import com.cgtechnodreams.salesassistindia.dailysales.model.CompetitorSalesDetail;
import com.cgtechnodreams.salesassistindia.databasehelper.model.CompetitorSalesModel;
import com.cgtechnodreams.salesassistindia.databasehelper.model.ProductGroupModel;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;

public class CompetitorSalesGroupActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.remarks)
    EditText remarks;
    @NotEmpty(messageId = R.string.field_required)
    @BindView(R.id.totalSalesAmount)
    EditText salesAmount;
    @BindView(R.id.btnSave)
    Button btnSave;
    private int brandId = 0;
    private String brandName = "";

    CompetitorSalesDetailAdapter adapter;
    ArrayList<ProductGroup> productGroupArrayList = new ArrayList<>();
    ArrayList<CompetitorSalesProductGroupModel> competitorSalesProductGroup = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competitor_sales_group);
        ButterKnife.bind(this);
        toolbar.setTitle("Competitor Group");
       brandName = getIntent().getStringExtra("BrandName");
        brandId = getIntent().getIntExtra("Id",0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ProductGroupModel productGroupModel = new ProductGroupModel(CompetitorSalesGroupActivity.this);
        ObjectMapper objectMapper = new ObjectMapper();
        JSONArray array = (JSONArray) productGroupModel.find(null);
        try {
            competitorSalesProductGroup = objectMapper.readValue(String.valueOf(array), new TypeReference<ArrayList<CompetitorSalesProductGroupModel>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinearLayoutManager favouriteListManager = new LinearLayoutManager(CompetitorSalesGroupActivity.this, LinearLayoutManager.VERTICAL, false);
        adapter = new CompetitorSalesDetailAdapter(CompetitorSalesGroupActivity.this, competitorSalesProductGroup);
        recyclerView.setLayoutManager(favouriteListManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setItemViewCacheSize(productGroupArrayList.size());

//        JSONArray jsonArray = new JSONArray();
//        for (int i = 0; i < productGroupArrayList.size(); i++) {
//
//            View view = recyclerView.getChildAt(i);
//            EditText nameEditText = (EditText) view.findViewById(R.id.qty);
//            int qty = Integer.parseInt(nameEditText.getText().toString());
//            int groupId = productGroupArrayList.get(i).getId();
//            JSONObject obj = new JSONObject();
//            try {
//                obj.put("group_id",groupId);
//                obj.put("group_name", productGroupArrayList.get(i).getName());
//                obj.put("qty",qty);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            jsonArray.put(obj);
//        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strRemarks = remarks.getText().toString();;
                String strSalesAmount = salesAmount.getText().toString();
                if (FormValidator.validate(CompetitorSalesGroupActivity.this, new SimpleErrorPopupCallback(CompetitorSalesGroupActivity.this, true))) {
                    List<CompetitorSalesProductGroupModel> competitorSalesDetail = adapter.retrieveData();
                    if (competitorSalesDetail.size() > 0) {
                        final ObjectMapper mapper = new ObjectMapper();
                        try {
//                            System.out.println(mapper.writeValueAsString(competitorSalesDetail));

                            saveSalesDetail(mapper.writeValueAsString(competitorSalesDetail));
                            LogUtils.setUserActivity(CompetitorSalesGroupActivity.this, "Competitor Sales", "Competitor Sales Saved");
                            Toasty.info(CompetitorSalesGroupActivity.this, "Competitor Sales Group Saved Sucessfully", Toast.LENGTH_SHORT,true).show();
                            finish();
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toasty.info(CompetitorSalesGroupActivity.this, "No sales has been recorded for brand ", Toast.LENGTH_SHORT,true).show();
                    }
                }

//
            }
        });

    }

    private void saveSalesDetail(String strCompetitorSalesDetail) {
        CompetitorSalesDetail competitorSalesDetail = new CompetitorSalesDetail();
        competitorSalesDetail.setBrandId(brandId);
       competitorSalesDetail.setBrandName(brandName);
        competitorSalesDetail.setRemarks(remarks.getText().toString());
        competitorSalesDetail.setTotalSales(salesAmount.getText().toString());
        competitorSalesDetail.setSalesDetail(strCompetitorSalesDetail);
        competitorSalesDetail.setDate(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date()));
        CompetitorSalesModel competitorSalesModel = new CompetitorSalesModel(CompetitorSalesGroupActivity.this);
        competitorSalesModel.insert(competitorSalesDetail);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
