package com.cgtechnodreams.salesassistindia.dailysales.dataservice;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.Api;
import com.cgtechnodreams.salesassistindia.api.BaseResponse;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.dailysales.model.CompetitorSales;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySales;
import com.cgtechnodreams.salesassistindia.dailysales.model.DailySalesResponse;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailySalesDataService {
    private Context mContext;

    public DailySalesDataService(Context mContext) {
        this.mContext = mContext;
    }

    public void postDailySales(String token, ArrayList<DailySales> dailySales, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<DailySalesResponse> callBack = Api.getService(mContext).postDailysales(token,dailySales);
        callBack.enqueue(new Callback<DailySalesResponse>() {
            @Override
            public void onResponse(Call<DailySalesResponse> call, Response<DailySalesResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<DailySalesResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);

            }
        });
    }
    public void postCompetitorSales(String token, ArrayList<CompetitorSales> competitorSalesDetails, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<BaseResponse> callBack = Api.getService(mContext).postCompetitorSalesDetail(token,competitorSalesDetails);
        callBack.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));
                }


            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

}