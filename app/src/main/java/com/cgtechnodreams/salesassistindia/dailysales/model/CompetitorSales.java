package com.cgtechnodreams.salesassistindia.dailysales.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

public class CompetitorSales implements Serializable {
    @JsonProperty("brand_id")
    private int brandId;
    @JsonProperty("brand_name")
    private String brandName;
    @JsonProperty("date")
    private String date;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("total_sales")
    private String totalSales;

    public ArrayList<CompetitorSalesDetailModel> getSalesDetail() {
        return salesDetail;
    }

    public void setSalesDetail(ArrayList<CompetitorSalesDetailModel> salesDetail) {
        this.salesDetail = salesDetail;
    }

    @JsonProperty("sales_detail")
    private ArrayList<CompetitorSalesDetailModel> salesDetail;

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }


}
