package com.cgtechnodreams.salesassistindia.dailysales.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailySales implements Serializable {
    @JsonProperty("id")
    int id;
    @JsonProperty("customer_name")
   String customerName;
    @JsonProperty("customer_address")
    String customerAddress;
    @JsonProperty("customer_number")
    String customerMobile;
    @JsonProperty("date")
    String date;
    @JsonProperty("quantity")
    int qty;
    @JsonProperty("detail")
    ArrayList<SalesDetail> salesDetail;


    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<SalesDetail> getSalesDetail() {
        return salesDetail;
    }

    public void setSalesDetail(ArrayList<SalesDetail> salesDetail) {
        this.salesDetail = salesDetail;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
