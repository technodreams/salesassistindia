package com.cgtechnodreams.salesassistindia.dailysales.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.dailysales.model.CompetitorSalesProductGroupModel;
import com.cgtechnodreams.salesassistindia.product.model.ProductGroup;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompetitorSalesDetailAdapter extends RecyclerView.Adapter<CompetitorSalesDetailAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<CompetitorSalesProductGroupModel> productGroupList = new ArrayList<>();

    public CompetitorSalesDetailAdapter(Context context, ArrayList<CompetitorSalesProductGroupModel> productGroupList) {
        this.mContext = context;
        this.productGroupList = productGroupList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public CompetitorSalesDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_competitor_sales, null);

        return new CompetitorSalesDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompetitorSalesDetailAdapter.MyViewHolder holder, int position) {
        ProductGroup productGroup = productGroupList.get(position);
        holder.group.setText(productGroup.getName());

    }

    @Override
    public int getItemCount() {
        return productGroupList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.productGroup)
        TextView group;
        @BindView(R.id.qty)
        EditText etQty;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            etQty.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {}

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(!(etQty.getText().toString().equalsIgnoreCase(""))){
                        productGroupList.get(getAdapterPosition()).setQuantity(Integer.parseInt(etQty.getText().toString()));
                    }

                }
            });
        }
    }
    public List<CompetitorSalesProductGroupModel> retrieveData()
    {
        return productGroupList;
    }
}