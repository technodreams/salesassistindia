package com.cgtechnodreams.salesassistindia.dailysales.model;

import com.cgtechnodreams.salesassistindia.product.model.ProductGroup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompetitorSalesProductGroupModel extends ProductGroup {
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("qty")
    int quantity;
}
