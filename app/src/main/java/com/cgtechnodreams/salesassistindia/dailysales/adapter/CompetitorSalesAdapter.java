package com.cgtechnodreams.salesassistindia.dailysales.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.product.model.Brand;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompetitorSalesAdapter extends RecyclerView.Adapter<CompetitorSalesAdapter.MyViewHolder> {

    Context mContext;
    PrefManager prefManager;
    ArrayList<Brand> brandList = new ArrayList<>();

    public CompetitorSalesAdapter(Context context, ArrayList<Brand> brandList) {
        this.mContext = context;
        this.brandList = brandList;
        prefManager = new PrefManager(mContext);
    }

    @Override
    public CompetitorSalesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_brand, null);

        return new CompetitorSalesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompetitorSalesAdapter.MyViewHolder holder, int position) {
        Brand brand = brandList.get(position);
        holder.brand.setText(brand.getName());
    }

    @Override
    public int getItemCount() {
        return brandList.size();
    }
//    public void setFilter(ArrayList<Product> newList) {
//        brandList = new ArrayList<>();
//        productList.addAll(newList);
//        notifyDataSetChanged();
//    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView brand;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}