package com.cgtechnodreams.salesassistindia;

/**
 * Created by user on 08/01/2019.
 */

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.StrictMode;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;

import com.cgtechnodreams.salesassistindia.utils.ConsoleColorConstants;

//import net.sqlcipher.database.SQLiteDatabase;


/**
 * Created by user on 08/01/2019.
 */

public class BaseApplication extends Application {
    public static final String APP_FOLDER_NAME = "SalesAssist_India";
    public static final String LOCATION_PREF = "LOCATION_PREF";
    public static final String LAST_FIXED_TIME = "LAST_FIXED_TIME";
    public static final String LAST_LATITUDE = "LAST_LATITUDE";
    public static final String LAST_LONGITUDE = "LAST_LONGITUDE";
    private static final String TAG = "==>: " + "BaseApplication";
    public static String GEOCODE_ADDRESS = "GEOCODE_ADDRESS";

    private static Location lastKnownLocation;
    private static SharedPreferences sLocationPreferences;
    //public  static UserActivityEntity activityModel = new UserActivityEntity(this);
    //  public static Context sApplicationContext;

    @Nullable
    public static Location getLastKnownLocation() {
        if (lastKnownLocation == null) {
            if (!sLocationPreferences.getString(LAST_LATITUDE, "").equals("")) {
                long locationThreshold = 2 * 60 * 60 * 1000;//location must not be older than 2 hrs
                long currentTime = System.currentTimeMillis();
                if (currentTime - sLocationPreferences.getLong(LAST_FIXED_TIME, 0) < locationThreshold) {
                    lastKnownLocation = new Location("CG-ERP");
                    lastKnownLocation.setLatitude(Double.parseDouble(sLocationPreferences.getString(LAST_LATITUDE, "0")));
                    lastKnownLocation.setLongitude(Double.parseDouble(sLocationPreferences.getString(LAST_LONGITUDE, "0")));
                    return lastKnownLocation;
                }
            }
        }
        //todo workaround for LocationModel not determined
        if (lastKnownLocation == null) {
            lastKnownLocation = new Location("CG-ERP");
            lastKnownLocation.setLatitude(Double.parseDouble("0"));
            lastKnownLocation.setLongitude(Double.parseDouble("0"));
        }
        return lastKnownLocation;
    }

    public static void setLastKnownLocation(Location lastKnownLocation) {
        try {
            BaseApplication.lastKnownLocation = lastKnownLocation;
            SharedPreferences.Editor editor = BaseApplication.sLocationPreferences.edit();
            editor.putLong(LAST_FIXED_TIME, lastKnownLocation.getTime());
            editor.putString(LAST_LATITUDE, lastKnownLocation.getLatitude() + "");
            editor.putString(LAST_LONGITUDE, lastKnownLocation.getLongitude() + "");
            editor.apply();
        } catch (Exception e) {
            //    assertShouldNotReachHere(android.util.Log.ERROR, e.getMessage());
            Log.e(TAG, "setLastKnownLocation: --> " + e.getMessage());
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
       // SQLiteDatabase.loadLibs(getApplicationContext());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Log.d(TAG, "\n" +
                ConsoleColorConstants.TOP_BORDER + "\n" +
                ConsoleColorConstants.HORIZONTAL_LINE + "\t \t \t " + BaseApplication.class.getSimpleName() + "CHECK VARIANT DIFFERENCES ...................\n" +
                ConsoleColorConstants.MIDDLE_BORDER + "\n" +
                ConsoleColorConstants.HORIZONTAL_LINE + "\n" +
                ConsoleColorConstants.HORIZONTAL_LINE + "\t \t " +
                "DashboardActivity" + ConsoleColorConstants.HORIZONTAL_LINE + " LoginActivity" + "\n"+
                ConsoleColorConstants.HORIZONTAL_LINE + "\n" +
                ConsoleColorConstants.HORIZONTAL_LINE + "\n" +
                ConsoleColorConstants.BOTTOM_BORDER);
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
