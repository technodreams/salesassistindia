package com.cgtechnodreams.salesassistindia.photo;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.backgroundlocationupdate.LocationUtils;
import com.cgtechnodreams.salesassistindia.outlet.StockistActivity;
import com.cgtechnodreams.salesassistindia.outlet.model.Stockist;
import com.cgtechnodreams.salesassistindia.photo.model.PhotoEntity;
import com.cgtechnodreams.salesassistindia.photo.model.PhotoModel;
import com.cgtechnodreams.salesassistindia.utils.AppConstant;
import com.cgtechnodreams.salesassistindia.utils.CameraUtils;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;
import com.cgtechnodreams.salesassistindia.utils.Utils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static com.cgtechnodreams.salesassistindia.utils.AppConstant.MEDIA_TYPE_IMAGE;

public class PhotoActivity extends AppCompatActivity {


    // Bitmap sampling size
    public static final int BITMAP_SAMPLE_SIZE = 8;

//    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
//    public static final int CAMERA_FRONT_SHOWROOM_REQUEST_CODE = 101;
//    public static final int CAMERA_BACK_SHOWROOM_REQUEST_CODE = 102;
//    public static final int CAMERA_MAINrequestCameraPermission_SHOWROOM_REQUEST_CODE = 103;
    public static final int CAMERA_LEFT_SHOWROOM_REQUEST_CODE = 104;
//    public static final int CAMERA_RIGHT_SHOWROOM_REQUEST_CODE = 105;

    Stockist stockistObj = null;
    private static String imageStoragePath;
//    @BindView(R.id.containerAddPhotoBack)
//    LinearLayout containerAddPhotoBack;
//    @BindView(R.id.containerAddPhotoFront)
//    LinearLayout containerAddPhotoFront;
//    @BindView(R.id.containerAddPhotoLeft)
//    LinearLayout containerAddPhotoLeft;
//    @BindView(R.id.containerAddPhotoRight)
//    LinearLayout containerAddPhotoRight;
//    @BindView(R.id.containerAddPhotoMain)
//    LinearLayout containerAddPhotoMain;
    @BindView(R.id.leftImage)
    ImageView leftImage;
//    @BindView(R.id.rightImage)
//    ImageView rightImage;
//    @BindView(R.id.frontImage)
//    ImageView frontImage;
//    @BindView(R.id.mainImage)
//    ImageView mainImage;
//    @BindView(R.id.remarks)
//    AppCompatActivity remarks;
//    @BindView(R.id.backImage)
//    ImageView backImage;
    ArrayList<String> photoPathList = new ArrayList<String>();
    ArrayList<PhotoModel> photoModelsArrayList = new ArrayList<PhotoModel>();
    HashMap<Integer,PhotoModel> photoModels=new HashMap<Integer,PhotoModel>();
    String imageName;
    PrefManager prefManager;
    @BindView(R.id.btnSave)
    Button btnSave;
    Uri fileUri;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.stockist)
    EditText stockist;
    @BindView(R.id.remarks)
    AppCompatEditText remarks;
    LocationUtils locationUtils = null;

    public static final int REQUEST_CODE_OUTLETS = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ButterKnife.bind(this);
        toolbar.setTitle("Stockist Photo");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        stockistObj =(Stockist) getIntent().getSerializableExtra(AppConstant.STOCKIST_DETAIL);
        if(stockistObj !=null){
            stockist.setText(stockistObj.getName());
            stockist.setEnabled(false);

        }
        prefManager = new PrefManager(this);
        locationUtils = new LocationUtils(PhotoActivity.this);
        locationUtils.requestLocationUpdates();
        if (!CameraUtils.isDeviceSupportCamera(getApplicationContext())) {
            Toasty.error(getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toasty.LENGTH_LONG).show();
            // will close the app if the device doesn't have camera
            finish();
        }
    }

    @OnClick({ R.id.containerLeft,R.id.btnSave,R.id.stockist})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.containerLeft:
                if(stockistObj !=null) {
                if (CameraUtils.checkPermissions(getApplicationContext())) {
                    captureImage(CAMERA_LEFT_SHOWROOM_REQUEST_CODE);
                } else {
                    requestCameraPermission(MEDIA_TYPE_IMAGE,CAMERA_LEFT_SHOWROOM_REQUEST_CODE);
                }
                }else{
                    Toasty.info(PhotoActivity.this, "Please select distributor first!!", Toast.LENGTH_SHORT,true).show();
                }
                break;
            case R.id.stockist:
                Intent intent = new Intent(PhotoActivity.this, StockistActivity.class);
                startActivityForResult(intent,REQUEST_CODE_OUTLETS);
                break;
            case R.id.btnSave:

            if(photoModels.size()<1){

                Toasty.info(PhotoActivity.this,"Please take photo",Toast.LENGTH_SHORT,true).show();
            }else{
                int count = 0;
                for (Map.Entry m:photoModels.entrySet()) {
                    photoModelsArrayList.add((PhotoModel) m.getValue());
                    photoModelsArrayList.get(count).setDescription(remarks.getText().toString());
                    count++;
                }

                PhotoEntity photoEntity = new PhotoEntity(PhotoActivity.this);
                photoEntity.insert(photoModelsArrayList);
                finish();
                break;
            }
        }
    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type, int resultCode) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage(resultCode);
                            }
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }


    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage(int requestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);

        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
            imageName = file.getName();
        }
        fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, requestCode);
    }

    /**
     * Activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_OUTLETS:
                    stockistObj = (Stockist) data.getSerializableExtra("result");
                    stockist.setText(stockistObj.getName());
                    break;
                case CAMERA_LEFT_SHOWROOM_REQUEST_CODE:
                    previewImage(resultCode, leftImage);
                    saveTempArray();
                    break;
            }
        }

    }

    private void saveTempArray() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String currentDateTime = sdf.format(new Date());
        PhotoModel eachPhotoDetail = new PhotoModel();
        eachPhotoDetail.setDateSaved(currentDateTime);
        eachPhotoDetail.setDescription(remarks.getText().toString());
        eachPhotoDetail.setFileName(imageName);
        eachPhotoDetail.setLatitude(Double.parseDouble(prefManager.getLatitude()));
        eachPhotoDetail.setLogitude(Double.parseDouble(prefManager.getLongitude()));
        eachPhotoDetail.setDistributorId(stockistObj.getDistributorId());
        eachPhotoDetail.setPhotoPath(imageStoragePath);
        eachPhotoDetail.setFileUri(fileUri.toString());
        photoModels.put(0,eachPhotoDetail);
    }

    private void previewImage(int resultCode, ImageView imageView) {
        if (resultCode == RESULT_OK) {
            CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);

            downSizeImage(imageStoragePath);
            previewCapturedImage(imageView);

        } else if (resultCode == RESULT_CANCELED) {
            Toasty.info(getApplicationContext(),
                    "User cancelled image capture", Toast.LENGTH_SHORT,true)
                    .show();
        } else {
            Toasty.info(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT,true)
                    .show();
        }
    }

    private void downSizeImage(String imageStoragePath) {
        ByteArrayOutputStream byteArrayOutputStream = Utils.compressImage(imageStoragePath);
        try(OutputStream outputStream = new FileOutputStream(imageStoragePath)) {
            byteArrayOutputStream.writeTo(outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Display image from gallery
     */



    private void previewCapturedImage(ImageView imageView) {
        try{
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            final Bitmap bitmap = BitmapFactory.decodeFile(Uri.parse(imageStoragePath).getPath(),
                    options);
            imageView.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(PhotoActivity.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
