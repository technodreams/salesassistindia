package com.cgtechnodreams.salesassistindia.photo.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.widget.Toast;

import com.cgtechnodreams.salesassistindia.databasehelper.SQLiteDbHelper;
import com.cgtechnodreams.salesassistindia.databasehelper.model.BaseModel;
import com.cgtechnodreams.salesassistindia.utils.Log;
import com.cgtechnodreams.salesassistindia.utils.LogUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import es.dmoral.toasty.Toasty;

public class PhotoEntity extends BaseModel {
    private SQLiteDbHelper adapter;
    private Context context;
    public static final String TABLE_NAME = "Images";
    public static final String COL_ID = "id";
    public static final String COL_PATH = "photo_path";
    public static final String COL_FILE_NAME = "file_name";
    public static final String COL_DISTRIBUTOR_ID = "distributor_id";
    public static final String COL_DESCRIPTION= "description";
    public static final String COL_LATITUDE = "latitude";
    public static final String COL_LONGITUDE = "longitude";
    public static final String COL_DATE_SAVED = "date_saved";
    public static final String COL_FILE_URI = "file_uri";
    private static final String TAG = PhotoEntity.class.getName();

    public static final String TABLE_SCRIPT = "CREATE TABLE " + TABLE_NAME + " (" +
            COL_ID + " INTEGER NOT NULL, " +
            COL_PATH + " VARCHAR NOT NULL, " +
            COL_FILE_NAME + " VARCHAR NOT NULL, " +
            COL_FILE_URI + " VARCHAR NOT NULL, " +
            COL_DISTRIBUTOR_ID + " INTEGER NOT NULL, " +
            COL_DESCRIPTION + " VARCHAR NOT NULL, " +
            COL_LATITUDE + " DOUBLE NOT NULL, " +
            COL_LONGITUDE + " DOUBLE NOT NULL, " +
            COL_DATE_SAVED + " VARCHAR NOT NULL" +
            ")";
    public static final String DROP_TABLE_SCRIPT = "DELETE FROM " + TABLE_NAME;

    public PhotoEntity(Context context) {
        super(context);
        this.context = context;
        this.adapter = SQLiteDbHelper.getDbAdapter(context);
    }
    @Override
    public Object find(Object object) {
        JSONArray resultJSONArray = null;
        try {
            this.adapter.openDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = this.adapter.getDatabase().rawQuery(query, null);

            resultJSONArray = new JSONArray();
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int colCount = cursor.getColumnCount();
                JSONObject itemJSONObject = new JSONObject();
                for (int i = 0; i < colCount; i++) {
                    itemJSONObject.put(cursor.getColumnName(i), cursor.getString(i));
                }
                resultJSONArray.put(itemJSONObject);
            }

            cursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "find", e.getMessage());
        } finally {
            this.adapter.close();
        }

        return resultJSONArray;
    }

    @Override
    public long insert(Object object) {
        if (null == object) {
            return 0;
        }
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JSONArray list = new JSONArray(mapper.writeValueAsString(object));
            final ContentValues values = new ContentValues();

            this.adapter.openDatabase();
            this.adapter.openDatabase().beginTransaction();

            for (int i = 0; i < list.length(); i++) {
                final JSONObject model = list.getJSONObject(i);

                Iterator<String> keys = model.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    values.put(key, model.getString(key));
                }
                long result = this.adapter.getDatabase().insertOrThrow(TABLE_NAME, null, values);
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

                values.clear();
                // setTempSyncId(BaseApplication.getUser().getSpCode() + "-" + (Utils.getTimeInMilliSec() + result));
                // values.put(COL_M_SYNC_ID, getTempSyncId());
                //result = this.adapter.getDatabase().update(TABLE_NAME, values, COL_P_KEY + "=?", new String[]{String.valueOf(result)});
                if (-1 == result) {
                    throw new Exception("Unable to insert");
                }

            }

            this.adapter.openDatabase().setTransactionSuccessful();
            this.adapter.openDatabase().endTransaction();
            Toasty.info(context,"Photo saved",Toast.LENGTH_SHORT,true).show();
            LogUtils.setUserActivity(context ,"Photo", "Photo Data saved locally");
            return 1;

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG + "insert()", e.getMessage());
            this.adapter.close();
        } finally {
            this.adapter.close();
        }
        return 0;
    }

    @Override
    public int update(Object object) {
        return 0;
    }

    @Override
    public int delete(Object object) {
        this.adapter.openDatabase();
        this.adapter.openDatabase().beginTransaction();
        this.adapter.openDatabase().execSQL("delete from " + TABLE_NAME);
        this.adapter.openDatabase().setTransactionSuccessful();
        this.adapter.openDatabase().endTransaction();
        this.adapter.getDatabase().close();
        LogUtils.setUserActivity(context ,"Photo", "Photo Data deleted locally");
        return 0;
    }

    @Override
    public Object parseToObject(Cursor cursor) {
        return null;
    }
}
