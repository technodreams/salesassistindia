package com.cgtechnodreams.salesassistindia.photo;

import android.content.Context;

import com.cgtechnodreams.salesassistindia.api.ApiUploadFile;
import com.cgtechnodreams.salesassistindia.api.ErrorModel;
import com.cgtechnodreams.salesassistindia.api.ErrorUtils;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnFailedListener;
import com.cgtechnodreams.salesassistindia.api.interfaces.OnSuccess;
import com.cgtechnodreams.salesassistindia.utils.ErrorHandler;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoUploadDataService {
    private Context mContext;

    public PhotoUploadDataService(Context mContext) {
        this.mContext = mContext;
    }
    public void uploadImages(String token, RequestBody description, RequestBody size, List<MultipartBody.Part> files, final OnSuccess onSuccessListener, final OnFailedListener onErrorListener) {
        Call<ResponseBody> callBack = ApiUploadFile.getService(mContext).uploadMultipleFilesDynamic(token,description, size,files);
        callBack.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    onSuccessListener.onSuccess(response.body());
                }
                else{
                    ErrorHandler.handleSyncError(mContext,response.code(),response.message());
                    onErrorListener.onFailed(ErrorUtils.parseError(response));


                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                ErrorModel errorModel = new ErrorModel();
                errorModel.setErrors(t.toString());
                errorModel.setMessage("Error");
                onErrorListener.onFailed(errorModel);
                ErrorHandler.httpRequestFailure(mContext,t);
            }
        });
    }

}
