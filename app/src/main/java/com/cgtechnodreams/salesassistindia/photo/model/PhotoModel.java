package com.cgtechnodreams.salesassistindia.photo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PhotoModel implements Serializable {
    @JsonProperty("id")
    private int id;

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }

    @JsonProperty("file_uri")
    private String fileUri;
    @JsonProperty("photo_path")
    private String photoPath;
    @JsonProperty("file_name")
    private String fileName;
    @JsonProperty("distributor_id")
    private int distributorId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("latitude")
    private double latitude;
    @JsonProperty("longitude")
    private double logitude;
    @JsonProperty("date_saved")
    private String  dateSaved;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(int distributorId) {
        this.distributorId = distributorId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLogitude() {
        return logitude;
    }

    public void setLogitude(double logitude) {
        this.logitude = logitude;
    }

    public String getDateSaved() {
        return dateSaved;
    }

    public void setDateSaved(String dateSaved) {
        this.dateSaved = dateSaved;
    }


}
