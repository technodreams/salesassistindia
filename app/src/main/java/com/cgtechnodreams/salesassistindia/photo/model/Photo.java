package com.cgtechnodreams.salesassistindia.photo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Photo implements Serializable {
    public ArrayList<String> getPhotoString() {
        return photoString;
    }

    public void setPhotoString(ArrayList<String> photoString) {
        this.photoString = photoString;
    }

    private ArrayList<String> photoString;
}
