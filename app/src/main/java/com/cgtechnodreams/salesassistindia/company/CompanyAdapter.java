package com.cgtechnodreams.salesassistindia.company;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cgtechnodreams.salesassistindia.R;
import com.cgtechnodreams.salesassistindia.salesorder.CollectionActivity;
import com.cgtechnodreams.salesassistindia.salesorder.model.Company;
import com.cgtechnodreams.salesassistindia.utils.PrefManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.MyViewHolder>{
    Context mContext;
    PrefManager prefManager;
    ArrayList<Company> companies = new ArrayList<>();

    public CompanyAdapter(Context context, ArrayList<Company> companies) {
        this.mContext = context;
        this.companies = companies;
        prefManager = new PrefManager(mContext);
    }
    @NonNull
    @Override
    public CompanyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_bank, null);
        return new CompanyAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyAdapter.MyViewHolder holder, int position) {
        Company company = companies.get(position);
        holder.name.setText(company.getName());
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    public void setFilter(ArrayList<Company> newList) {
        companies = new ArrayList<>();
        companies.addAll(newList);
        notifyDataSetChanged();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, CollectionActivity.class);
                    intent.putExtra("CompanyDetail",companies.get(getAdapterPosition()));
                    ((Activity) mContext).setResult(Activity.RESULT_OK,intent);
                    ((Activity) mContext).finish();
                }
            });

        }

    }
}

